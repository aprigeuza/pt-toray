
ALTER PROCEDURE  [sp_SpinningProcessConsMat]
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;
    --  -- DECLARE
    -- --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    -- --
    -- -- SET @comp_id=1
    -- -- SET @f_year=2020
    -- -- SET @f_month=7
    -- -- SET @user_id='SYS'
    -- -- SET @client_ip='192.168.1.1'

    DECLARE @dept10 INT
    DECLARE @dept20 INT
    DECLARE @mc_loc VARCHAR(MAX)
    DECLARE @tr_code VARCHAR(MAX)
    DECLARE @proc_code VARCHAR(MAX)
    DECLARE @proc_time DATETIME
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_no INT
    DECLARE @item_code VARCHAR(MAX)
    DECLARE @proc_type VARCHAR(MAX)
    DECLARE @mat_qty_unit VARCHAR(MAX)
    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT

    SET @dept10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id) AND (dept_seq=10))
    SET @dept20=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id) AND (dept_seq=20))

    SET @proc_no=1
    SET @proc_time=GETDATE()
    SET @rec_sts='A'
    -- DECLARE @c INTEGER
    -- SET @c = (SELECT COUNT(*) FROM istem_costing.dbo.tr_prod A WHERE (A.comp_id=@comp_id) AND (A.f_year=@f_year) AND (A.f_month=@f_month) AND (A.dept=@dept20))
    -- IF @c >= 1
    -- BEGIN
    --     SET @proc_no=(SELECT MAX(proc_no) + 1 FROM istem_costing.dbo.tr_prod A WHERE (A.comp_id=@comp_id) AND (A.f_year=@f_year) AND (A.f_month=@f_month) AND (A.dept=@dept20))
    --     SET @rec_sts='T'
    -- END
    --
    -- DECLARE @id INT
    -- DECLARE @c_id INT
    -- SET @id = 0
    --
    -- DECLARE @_TEMP TABLE (
    -- 	[comp_id] [numeric](1, 0) NOT NULL,
    -- 	[f_year] [numeric](4, 0) NOT NULL,
    -- 	[f_month] [numeric](2, 0) NOT NULL,
    -- 	[dept] [numeric](3, 0) NOT NULL,
    -- 	[cost_sheet_id] [varchar](4) NOT NULL,
    -- 	[item_code] [varchar](45) NOT NULL,
    -- 	[prod_qty] [numeric](12, 2) NULL,
    -- 	[material_cost] [numeric](12, 2) NULL,
    -- 	[wv_leno] [numeric](12, 2) NULL,
    -- 	[proc_time] [datetime] NULL,
    -- 	[user_id] [varchar](6) NULL,
    -- 	[client_ip] [varchar](15) NULL,
    -- 	[rec_sts] [char](1) NULL,
    -- 	[proc_no] [tinyint] NULL
    -- )
    --
    -- INSERT INTO @_TEMP
    --     (
    --         comp_id
    --        ,f_year
    --        ,f_month
    --        ,dept
    --        ,cost_sheet_id
    --        ,item_code
    --        ,prod_qty
    --        ,material_cost
    --        ,proc_time
    --        ,user_id
    --        ,client_ip
    --        ,rec_sts
    --        ,proc_no
    --     )
    --     SELECT
    --         A.comp_id AS comp_id
    --        ,A.f_year AS f_year
    --        ,A.f_month AS f_month
    --        ,A.dept AS dept
    --        ,A.cost_sheet_id AS cost_sheet_id
    --        ,A.mat_code AS item_code
    --        ,A.in_qty AS prod_qty
    --        ,B.sum_of_cons_mat_amount AS material_cost
    --        ,@proc_time
    --        ,@user_id
    --        ,@client_ip
    --        ,@rec_sts
    --        ,@proc_no
    --     FROM istem_costing.dbo.tr_inv_in AS A
    --     LEFT JOIN (
    --             SELECT
    --                 A.item_code
    --                 ,SUM(A.cons_mat_amount) AS sum_of_cons_mat_amount
    --             FROM istem_costing.dbo.tr_wip AS A
    --             WHERE
    --                     (A.comp_id=@comp_id)
    --                 AND (A.f_year=@f_year)
    --                 AND (A.f_month=@f_month)
    --                 AND (A.dept=@dept20)
    --             GROUP BY A.item_code
    --         ) AS B ON (
    --             B.item_code=A.mat_code
    --         )
    --     WHERE
    --             (A.comp_id=@comp_id)
    --         AND (A.f_year=@f_year)
    --         AND (A.f_month=@f_month)
    --         AND (A.cost_sheet_id='A20')
    --         AND (A.dept=@dept10)
    --
    -- DECLARE @cur_item_code VARCHAR(MAX)
    -- DECLARE @cur_cost_sheet_id VARCHAR(MAX)
    -- DECLARE @cur_material_cost DECIMAL(12,2)
    -- DECLARE @cur_prod_qty DECIMAL(12,2)
    --
    -- DECLARE temp_cursor CURSOR FOR
    --     SELECT item_code, cost_sheet_id, material_cost, prod_qty FROM @_TEMP;
    --
    -- OPEN temp_cursor;
    -- FETCH NEXT FROM temp_cursor INTO @cur_item_code, @cur_cost_sheet_id, @cur_material_cost, @cur_prod_qty;
    --
    -- WHILE @@FETCH_STATUS = 0
    -- BEGIN
    --     SELECT
    --         @c = COUNT(@c)
    --         FROM istem_costing.dbo.tr_prod AS A
    --         WHERE
    --                 (A.comp_id=@comp_id)
    --             AND (A.f_year=@f_year)
    --             AND (A.f_month=@f_month)
    --             AND (A.dept=@dept20)
    --         AND (A.item_code=@cur_item_code)
    --             AND (A.cost_sheet_id=@cur_cost_sheet_id)
    --     IF @c >= 1
    --     BEGIN
    --         SET @rec_sts='T'
    --         UPDATE istem_costing.dbo.tr_prod
    --             SET
    --                  material_cost = @cur_material_cost
    --                 ,proc_time = @proc_time
    --                 ,user_id = @user_id
    --                 ,client_ip = @client_ip
    --                 ,rec_sts = @rec_sts
    --                 ,proc_no = proc_no+1
    --             WHERE
    --                     (comp_id=@comp_id)
    --                 AND (f_year=@f_year)
    --                 AND (f_month=@f_month)
    --                 AND (dept=@dept20)
    --                 AND (item_code=@cur_item_code)
    --                 AND (cost_sheet_id=@cur_cost_sheet_id)
    --     END
    --     ELSE
    --     BEGIN
    --         INSERT INTO istem_costing.dbo.tr_prod
    --             (
    --                 comp_id
    --                ,f_year
    --                ,f_month
    --                ,dept
    --                ,cost_sheet_id
    --                ,item_code
    --                ,prod_qty
    --                ,material_cost
    --                ,proc_time
    --                ,user_id
    --                ,client_ip
    --                ,rec_sts
    --                ,proc_no
    --             )
    --             VALUES
    --             (
    --                 @comp_id
    --                ,@f_year
    --                ,@f_month
    --                ,@dept20
    --                ,@cur_cost_sheet_id
    --                ,@cur_item_code
    --                ,@cur_prod_qty
    --                ,@cur_material_cost
    --                ,@proc_time
    --                ,@user_id
    --                ,@client_ip
    --                ,@rec_sts
    --                ,@proc_no
    --             )
    --     END
    --     SET @rec_sts='A'
    --     SET @proc_no=1
    --     -- SELECT          @comp_id AS comp_id
    --     --                ,@f_year AS f_year
    --     --                ,@f_month AS f_month
    --     --                ,@dept20 AS dept20
    --     --                ,@cur_cost_sheet_id AS cur_cost_sheet_id
    --     --                ,@cur_item_code AS cur_item_code
    --     --                ,@cur_prod_qty AS cur_prod_qty
    --     --                ,@cur_material_cost AS cur_material_cost
    --     --                ,@proc_time AS proc_time
    --     --                ,@user_id AS user_id
    --     --                ,@client_ip AS client_ip
    --     --                ,@rec_sts AS rec_sts
    --     --                ,@proc_no AS proc_no
    --     FETCH NEXT FROM temp_cursor INTO @cur_item_code, @cur_cost_sheet_id, @cur_material_cost, @cur_prod_qty;
    -- END;
    --
    -- CLOSE temp_cursor;
    -- DEALLOCATE temp_cursor
    --
    -- SELECT COUNT(*) AS c FROM @_TEMP

    DECLARE @_TEMP_TR_PROD TABLE (
        comp_id NUMERIC(1,0) NOT NULL,
        f_year NUMERIC(4,0) NOT NULL,
        f_month NUMERIC(2,0) NOT NULL,
        dept NUMERIC(3,0) NOT NULL,
        cost_sheet_id VARCHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
        item_code VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
        prod_qty NUMERIC(12,2) DEFAULT 0 NULL,
        material_cost NUMERIC(12,2) DEFAULT 0 NULL,
        wv_leno NUMERIC(12,2) DEFAULT 0 NULL,
        proc_time datetime NULL,
        user_id VARCHAR(6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
        client_ip VARCHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
        rec_sts CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
        proc_no TINYINT NULL
    )

    SELECT
        @proc_no=ISNULL(MAX(proc_no), 0) + 1
        FROM istem_costing.dbo.tr_prod
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept20);
    SET @rec_sts = 'A'
    IF @proc_no >= 2
    BEGIN
        SET @rec_sts = 'T'
    END

    INSERT INTO @_TEMP_TR_PROD
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,item_code
            ,prod_qty
            ,material_cost
            ,wv_leno
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
            @comp_id
            ,@f_year
            ,@f_month
            ,B.v_dept AS dept
            ,A.cost_sheet_id AS cost_sheet_id
            ,A.mat_code AS item_code
            ,A.v_tot_prod_qty AS prod_qty
            ,NULL AS material_cost
            ,NULL AS wv_leno
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
        FROM (
            SELECT
                cost_sheet_id
                ,mat_code
                ,SUM(in_qty) AS v_tot_prod_qty
            FROM istem_costing.dbo.tr_inv_in
            WHERE
                comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept=@dept10
                AND cost_sheet_id IN ('A20')
            GROUP BY
                cost_sheet_id
                ,mat_code
        ) AS A
        INNER JOIN (
            SELECT
                cost_sheet_id,
                dept AS v_dept
            FROM istem_costing.dbo.ms_cost_group_detail
            WHERE comp_id=@comp_id
        ) AS B ON (B.cost_sheet_id=A.cost_sheet_id)

    UPDATE T SET
        T.material_cost=S.material_cost
    FROM @_TEMP_TR_PROD AS T
    LEFT JOIN (
        SELECT
             A.item_code
            ,SUM(A.cons_mat_amount) AS material_cost
        FROM istem_costing.dbo.tr_wip AS A
        WHERE
                (A.comp_id=@comp_id)
            AND (A.f_year=@f_year)
            AND (A.f_month=@f_month)
            AND (A.dept=@dept20)
        GROUP BY A.item_code
    ) AS S ON (S.item_code=T.item_code)

    -- Delete
    DELETE FROM istem_costing.dbo.tr_prod
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (SELECT dept FROM @_TEMP_TR_PROD GROUP BY dept)
    -- Insert
    INSERT INTO istem_costing.dbo.tr_prod
            (
                comp_id
                ,f_year
                ,f_month
                ,dept
                ,cost_sheet_id
                ,item_code
                ,prod_qty
                ,material_cost
                ,wv_leno
                ,proc_time
                ,user_id
                ,client_ip
                ,rec_sts
                ,proc_no
            )
            SELECT
                comp_id
                ,f_year
                ,f_month
                ,dept
                ,cost_sheet_id
                ,item_code
                ,prod_qty
                ,material_cost
                ,wv_leno
                ,proc_time
                ,user_id
                ,client_ip
                ,rec_sts
                ,proc_no
            FROM @_TEMP_TR_PROD;

END
