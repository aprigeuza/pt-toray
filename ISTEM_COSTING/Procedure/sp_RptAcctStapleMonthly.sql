
-- =============================================
-- Author:		Apri
-- Create date: 2020-11-24
-- Description:
--				Staple Monthly
--				Report Untuk Accounting
-- =============================================
ALTER PROCEDURE  [sp_RptAcctStapleMonthly]
	@COMP_ID INT, @F_YEAR INT, @F_MONTH INT
AS
BEGIN
	SET NOCOUNT ON;
	
	-- DECLARE
	--     @comp_id INT, @f_year INT, @f_month INT
	--
	-- SET @comp_id=1
	-- SET @f_year=2020
	-- SET @f_month=7


	DECLARE @lp_f_year INT, @lp_f_month INT

	IF @f_month = 1
	BEGIN
		SET @lp_f_month = 12
		SET @lp_f_year = @f_year - 1
	END
	ELSE
	BEGIN
		SET @lp_f_month = @f_month - 1
		SET @lp_f_year = @f_year
	END

	DECLARE @dept INT

	SET @dept = (SELECT TOP 1 dept FROM istem_costing.dbo.ms_dept WHERE comp_id=@COMP_ID AND dept_seq=10)

	SELECT
		 @comp_id AS comp_id
		,@f_year AS f_year
		,@f_month AS f_month
		,A.cost_sheet_id AS cost_sheet_id
		,A.mat_code AS mat_code
		,A.mat_name AS mat_name
		,NULLIF( A.bf_qty , 0) AS bf_qty
		,NULLIF( (NULLIF(A.bf_amount, 0) / NULLIF(A.bf_qty, 0)) , 0) AS bf_price
		,NULLIF( A.bf_amount , 0) AS bf_amount
		,NULLIF( A.rc_qty , 0) AS rc_qty
		,NULLIF( (NULLIF(A.rc_amount, 0) / NULLIF(A.rc_qty, 0)) , 0) AS rc_price
		,NULLIF( A.rc_amount , 0) AS rc_amount
		,NULLIF( A.co_sp_qty , 0) AS co_sp_qty
		,NULLIF( NULLIF(ISNULL(A.bf_amount, 0)+ISNULL(A.rc_amount, 0), 0)/NULLIF(ISNULL(A.bf_qty, 0)+ISNULL(A.rc_qty, 0), 0) , 0) AS co_sp_price
		,NULLIF( A.co_sp_qty * NULLIF(ISNULL(A.bf_amount, 0)+ISNULL(A.rc_amount, 0), 0)/NULLIF(ISNULL(A.bf_qty, 0)+ISNULL(A.rc_qty, 0), 0) , 0) AS co_sp_amount
		,NULLIF( A.co_ot_qty , 0) AS co_ot_qty
		,NULLIF( NULLIF(ISNULL(A.bf_amount, 0)+ISNULL(A.rc_amount, 0), 0)/NULLIF(ISNULL(A.bf_qty, 0)+ISNULL(A.rc_qty, 0), 0) , 0) AS co_ot_price
		,NULLIF( A.co_ot_amount , 0) AS co_ot_amount
		,NULLIF( (ISNULL(A.bf_qty, 0)+ISNULL(A.rc_qty, 0)-ISNULL(A.co_sp_qty, 0)-ISNULL(A.co_ot_qty, 0)) , 0) AS cf_qty
		,NULLIF( NULLIF(ISNULL(A.bf_amount, 0)+ISNULL(A.rc_amount, 0), 0)/NULLIF(ISNULL(A.bf_qty, 0)+ISNULL(A.rc_qty, 0), 0) , 0) AS cf_price
		,NULLIF( NULLIF( (ISNULL(A.bf_qty, 0)+ISNULL(A.rc_qty, 0)-ISNULL(A.co_sp_qty, 0)-ISNULL(A.co_ot_qty, 0)) , 0) * NULLIF(ISNULL(A.bf_amount, 0)+ISNULL(A.rc_amount, 0), 0)/NULLIF(ISNULL(A.bf_qty, 0)+ISNULL(A.rc_qty, 0), 0) , 0) AS cf_amount
	FROM (
		SELECT
			MT.cost_sheet_id
			,MT.mat_code  AS mat_code
			,ISNULL(M.FrgnName, '') AS mat_name
			,BF.bf_qty
			,BF.bf_amount
			,RC.rc_qty
			,RC.rc_amount
			,CO_SP.co_sp_qty
			,CO_SP.co_sp_amount
			,CO_OT.co_ot_qty
			,CO_OT.co_ot_amount
		FROM
		(
			SELECT * FROM
			(
				SELECT cost_sheet_id, mat_code FROM istem_costing.dbo.tr_bal_mat WHERE (comp_id = @comp_id) AND (f_year = @lp_f_year) AND (f_month = @lp_f_month) AND (cost_sheet_id IN ('A10', 'A11', 'A13')) AND (dept=@dept) GROUP BY cost_sheet_id, mat_code
				UNION ALL
				SELECT cost_sheet_id, mat_code FROM istem_costing.dbo.tr_inv_in WHERE (comp_id = @comp_id) AND (f_year = @f_year) AND (f_month = @f_month) AND (cost_sheet_id IN ('A10', 'A11', 'A13')) AND (dept=@dept) GROUP BY cost_sheet_id, mat_code
				UNION ALL
				SELECT cost_sheet_id, mat_code FROM istem_costing.dbo.tr_inv_out_detail WHERE (comp_id = @comp_id) AND (f_year = @f_year) AND (f_month = @f_month) AND (cost_sheet_id IN ('A10', 'A11', 'A13')) AND (dept=@dept) GROUP BY cost_sheet_id, mat_code
			) AS A GROUP BY cost_sheet_id, mat_code
		) AS MT
		INNER JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM AS M ON (M.ItemCode COLLATE SQL_Latin1_General_CP1_CI_AS = MT.mat_code COLLATE SQL_Latin1_General_CP1_CI_AS)
		-- BF
		LEFT JOIN
		(
    		SELECT
    			mat_code
				,SUM(ISNULL(cf_qty, 0)) AS bf_qty --
    			,SUM(ISNULL(cf_amount, 0)) AS bf_amount --

    		FROM istem_costing.dbo.tr_bal_mat
    		WHERE
    			(comp_id = @comp_id)
    			AND
    			(f_year = @lp_f_year)
    			AND
    			(f_month = @lp_f_month)
    			AND
    			(dept=@dept)
    		GROUP BY mat_code
		) AS BF ON
		(BF.mat_code= MT.mat_code)

		-- Receive
		LEFT JOIN
		(
    		SELECT
    			mat_code
    			,SUM(ISNULL(in_qty, 0)) AS rc_qty -- E
    			,SUM(ISNULL(in_amount, 0)) AS rc_amount -- F
    		FROM istem_costing.dbo.tr_inv_in
    		WHERE
    			(comp_id = @comp_id)
    			AND
    			(f_year = @f_year)
    			AND
    			(f_month = @f_month)
    			AND
    			(dept=@dept)
    		GROUP BY mat_code
		) AS RC ON
		(RC.mat_code=MT.mat_code)

		-- Consume Spinning
		LEFT JOIN
		(
		  SELECT
    		mat_code
			,SUM(ISNULL(out_qty, 0)) AS co_sp_qty
    		,SUM(ISNULL(out_amount, 0)) AS co_sp_amount
		  FROM istem_costing.dbo.tr_inv_out_detail
		  WHERE
    		(comp_id = @comp_id)
    		AND
    		(f_year = @f_year)
    		AND
    		(f_month = @f_month)
    		AND
    		(dept=@dept)
    		AND
    		(out_dest=700)
		  GROUP BY mat_code
		) AS CO_SP ON
		(CO_SP.mat_code=MT.mat_code)


		-- Consume Other
		LEFT JOIN
		(
		  SELECT
    		mat_code
			,SUM(ISNULL(out_qty, 0)) AS co_ot_qty
    		,SUM(ISNULL(out_amount, 0)) AS co_ot_amount
		  FROM istem_costing.dbo.tr_inv_out_detail
		  WHERE
    		(comp_id = @comp_id)
    		AND
    		(f_year = @f_year)
    		AND
    		(f_month = @f_month)
    		AND
    		(dept=@dept)
    		AND
    		(out_dest<>700)
		  GROUP BY mat_code
		) AS CO_OT ON
		(CO_OT.mat_code = MT.mat_code)
	) AS A
	ORDER BY
		A.cost_sheet_id,
		A.mat_code

END
