ALTER PROCEDURE  sp_AcctTotalCostDyeing
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Acct - Total Cost Dyeing

    -- DECLARE
    -- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=7
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept_seq10 INT
    DECLARE @dept10 INT
    DECLARE @dept11 INT
    DECLARE @dept_seq40 INT
    DECLARE @dept40 INT
    DECLARE @tr_code VARCHAR(MAX)
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_time AS DATETIME
    DECLARE @proc_no INT
    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT

    SET @dept_seq10 = 10
    SET @dept10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=@dept_seq10))
    SET @dept11=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=11))
    SET @dept_seq40 = 40
    SET @dept40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=@dept_seq40))
    SET @tr_code=''
    SET @proc_time = GETDATE()
    SET @rec_sts='A'
    SET @proc_no=1

    IF @f_month = 1
    BEGIN
        SET @lp_f_month = 12
        SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
        SET @lp_f_month = @f_month - 1
        SET @lp_f_year = @f_year
    END

    DECLARE @_TEMP TABLE(
         item_code          VARCHAR(MAX)
        ,dy_proc_type       VARCHAR(MAX)
        ,dyes_type_code     VARCHAR(MAX)
        ,color_shade_code   VARCHAR(MAX)
        ,item_desc          VARCHAR(MAX)
        ,product_qty        NUMERIC(16,4)
        ,eq_electric        NUMERIC(16,4)
        ,eq_water           NUMERIC(16,4)
        ,eq_steam           NUMERIC(16,4)
        ,eq_lng             NUMERIC(16,4)
        ,eq_production      NUMERIC(16,4)
        ,eq_diestuff        NUMERIC(16,4)
        ,eq_chemical        NUMERIC(16,4)
        ,eq_resin           NUMERIC(16,4)
        ,eq_labor           NUMERIC(16,4)
        ,eq_repair          NUMERIC(16,4)
        ,cd_electric        NUMERIC(16,4)
        ,cd_water           NUMERIC(16,4)
        ,cd_steam           NUMERIC(16,4)
        ,cd_lng             NUMERIC(16,4)
        ,cd_production      NUMERIC(16,4)
        ,cd_diestuff        NUMERIC(16,4)
        ,cd_chemical        NUMERIC(16,4)
        ,cd_resin           NUMERIC(16,4)
        ,grey_fabric        NUMERIC(16,4)
        ,material_cost      NUMERIC(16,4)
        ,pc_fixed           NUMERIC(16,4)
        ,pc_labour          NUMERIC(16,4)
        ,pc_repair          NUMERIC(16,4)
        ,total_cost         NUMERIC(16,4)
        ,unit_cost          NUMERIC(16,4)
        ,pack_cost          NUMERIC(16,4)
    )

    DECLARE @tot_product_qty   NUMERIC(16,4)
    DECLARE @tot_eq_electric   NUMERIC(16,4)
    DECLARE @tot_eq_water      NUMERIC(16,4)
    DECLARE @tot_eq_steam      NUMERIC(16,4)
    DECLARE @tot_eq_lng        NUMERIC(16,4)
    DECLARE @tot_eq_production NUMERIC(16,4)
    DECLARE @tot_eq_diestuff   NUMERIC(16,4)
    DECLARE @tot_eq_chemical   NUMERIC(16,4)
    DECLARE @tot_eq_resin      NUMERIC(16,4)
    DECLARE @tot_eq_labor      NUMERIC(16,4)
    DECLARE @tot_eq_repair     NUMERIC(16,4)
    DECLARE @tot_cd_electric   NUMERIC(16,4)
    DECLARE @tot_cd_water      NUMERIC(16,4)
    DECLARE @tot_cd_steam      NUMERIC(16,4)
    DECLARE @tot_cd_lng        NUMERIC(16,4)
    DECLARE @tot_cd_production NUMERIC(16,4)
    DECLARE @tot_cd_diestuff   NUMERIC(16,4)
    DECLARE @tot_cd_chemical   NUMERIC(16,4)
    DECLARE @tot_cd_resin      NUMERIC(16,4)
    DECLARE @tot_grey_fabric   NUMERIC(16,4)
    DECLARE @tot_material_cost NUMERIC(16,4)
    DECLARE @tot_pc_fixed      NUMERIC(16,4)
    DECLARE @tot_pc_labour     NUMERIC(16,4)
    DECLARE @tot_pc_repair     NUMERIC(16,4)
    DECLARE @tot_total_cost    NUMERIC(16,4)
    DECLARE @tot_unit_cost     NUMERIC(16,4)
    DECLARE @tot_pack_cost     NUMERIC(16,4)


    -- Insert Item
    PRINT 'Proc : 1';
    INSERT INTO @_TEMP
        (
             dy_proc_type
            ,dyes_type_code
            ,color_shade_code
            ,item_code
            ,item_desc
            ,product_qty
        )
        SELECT
             A.dy_proc_type
            ,A.dyes_type_code
            ,A.color_shade_code
            ,A.grey_no
            ,A.grey_no
            ,SUM(A.prod_qty)
        FROM istem_costing.dbo.tr_prod_fg AS A
        WHERE
            A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept=@dept40
            AND A.wh_class NOT IN ('7')
        GROUP BY
            A.grey_no
            ,A.dy_proc_type
            ,A.dyes_type_code
            ,A.color_shade_code


    PRINT 'Proc : 2';
    DECLARE @_TR_WIP_EQV_DETAIL TABLE (
        item_code VARCHAR(45),
        dy_proc_type VARCHAR(12),
        dyes_type_code VARCHAR(12),
        color_shade_code VARCHAR(12),
        eq_electric NUMERIC(15, 4),
        eq_water NUMERIC(15, 4),
        eq_steam NUMERIC(15, 4),
        eq_lng NUMERIC(15, 4),
        eq_production NUMERIC(15, 4),
        eq_labor NUMERIC(15, 4),
        eq_repair NUMERIC(15, 4),
        eq_diestuff NUMERIC(15, 4),
        eq_chemical NUMERIC(15, 4),
        eq_resin NUMERIC(15, 4)
    )
    INSERT INTO @_TR_WIP_EQV_DETAIL
        (
            item_code
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
            ,eq_electric
            ,eq_water
            ,eq_steam
            ,eq_lng
            ,eq_production
            ,eq_labor
            ,eq_repair
            ,eq_diestuff
            ,eq_chemical
            ,eq_resin
        )
        SELECT
            item_code
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
            ,R01 -- eq_electric
            ,R02 -- eq_water
            ,R03 -- eq_steam
            ,R05 -- eq_lng
            ,R07 -- eq_production
            ,R10 -- eq_labor
            ,R11 -- eq_repair
            ,R13 -- eq_diestuff
            ,R15 -- eq_chemical
            ,R17 -- eq_resin
        FROM (
            SELECT
                item_code
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
                ,convert_type_code
                ,prod_equiv_qty
            FROM istem_costing.dbo.tr_prod_detail2
            WHERE
                comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept=@dept40
        ) AS t
        PIVOT (
            SUM(prod_equiv_qty)
            FOR convert_type_code IN (
                 [R01] -- eq_electric
                ,[R02] -- eq_water
                ,[R03] -- eq_steam
                ,[R05] -- eq_lng
                ,[R07] -- eq_production
                ,[R10] -- eq_labor
                ,[R11] -- eq_repair
                ,[R13] -- eq_diestuff
                ,[R15] -- eq_chemical
                ,[R17] -- eq_resin
            )
        ) AS pivot_table

    PRINT 'Proc : 3';
    UPDATE T SET
            T.eq_electric       = S.eq_electric * (NULLIF(T.product_qty, 0)/NULLIF(A.sum_product_qty, 0))
            ,T.eq_water         = S.eq_water * (NULLIF(T.product_qty, 0)/NULLIF(A.sum_product_qty, 0))
            ,T.eq_steam         = S.eq_steam * (NULLIF(T.product_qty, 0)/NULLIF(A.sum_product_qty, 0))
            ,T.eq_production    = S.eq_production * (NULLIF(T.product_qty, 0)/NULLIF(A.sum_product_qty, 0))
            ,T.eq_lng           = S.eq_lng * (NULLIF(T.product_qty, 0)/NULLIF(A.sum_product_qty, 0))
            ,T.eq_diestuff      = S.eq_diestuff * (NULLIF(T.product_qty, 0)/NULLIF(A.sum_product_qty, 0))
            ,T.eq_chemical      = S.eq_chemical * (NULLIF(T.product_qty, 0)/NULLIF(A.sum_product_qty, 0))
            ,T.eq_resin         = S.eq_resin * (NULLIF(T.product_qty, 0)/NULLIF(A.sum_product_qty, 0))
            ,T.eq_labor         = S.eq_labor * (NULLIF(T.product_qty, 0)/NULLIF(A.sum_product_qty, 0))
            ,T.eq_repair        = S.eq_repair * (NULLIF(T.product_qty, 0)/NULLIF(A.sum_product_qty, 0))
        FROM @_TEMP AS T
        LEFT JOIN @_TR_WIP_EQV_DETAIL AS S ON (S.item_code=T.item_code AND S.dy_proc_type=T.dy_proc_type AND S.dyes_type_code=T.dyes_type_code AND S.color_shade_code=T.color_shade_code)
        LEFT JOIN (
            SELECT
                item_code
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
                ,SUM(product_qty) AS sum_product_qty
            FROM @_TEMP
            GROUP BY
                item_code
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
        ) AS A ON (A.item_code=T.item_code AND A.dy_proc_type=T.dy_proc_type AND A.dyes_type_code=T.dyes_type_code AND A.color_shade_code=T.color_shade_code)

    PRINT 'Proc : 4';
    SELECT
        @tot_product_qty = ISNULL(SUM(ISNULL(product_qty, 0)), 0)
        ,@tot_eq_electric = ISNULL(SUM(ISNULL(eq_electric, 0)), 0)
        ,@tot_eq_water = ISNULL(SUM(ISNULL(eq_water, 0)), 0)
        ,@tot_eq_production = ISNULL(SUM(ISNULL(eq_production, 0)), 0)
        ,@tot_eq_steam = ISNULL(SUM(ISNULL(eq_steam, 0)), 0)
        ,@tot_eq_lng = ISNULL(SUM(ISNULL(eq_lng, 0)), 0)
        ,@tot_eq_diestuff = ISNULL(SUM(ISNULL(eq_diestuff, 0)), 0)
        ,@tot_eq_chemical = ISNULL(SUM(ISNULL(eq_chemical, 0)), 0)
        ,@tot_eq_resin = ISNULL(SUM(ISNULL(eq_resin, 0)), 0)
        ,@tot_eq_labor = ISNULL(SUM(ISNULL(eq_labor, 0)), 0)
        ,@tot_eq_repair = ISNULL(SUM(ISNULL(eq_repair, 0)), 0)
    FROM @_TEMP


    PRINT 'Proc : 5';
    SELECT @tot_cd_electric = ISNULL(SUM(cons_wip_cost_amount), 0) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40 AND cost_sheet_id='C02'
    SELECT @tot_cd_water = ISNULL(SUM(cons_wip_cost_amount), 0) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40 AND cost_sheet_id='C03'
    SELECT @tot_cd_production = ISNULL(SUM(cons_wip_cost_amount), 0) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40 AND cost_sheet_id='N10'
    SELECT @tot_cd_steam = ISNULL(SUM(cons_wip_cost_amount), 0) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40 AND cost_sheet_id='C04'
    SELECT @tot_cd_lng = ISNULL(SUM(cons_wip_cost_amount), 0) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40 AND cost_sheet_id='C01'
    SELECT @tot_cd_diestuff = ISNULL(SUM(cons_wip_cost_amount), 0) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40 AND cost_sheet_id='B30'
    SELECT @tot_cd_chemical = ISNULL(SUM(cons_wip_cost_amount), 0) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40 AND cost_sheet_id='B31'
    SELECT @tot_cd_resin = ISNULL(SUM(cons_wip_cost_amount), 0) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40 AND cost_sheet_id='B32'

    PRINT 'Proc : 6';
    UPDATE T SET
        T.cd_electric = NULLIF(@tot_cd_electric, 0) / NULLIF(@tot_eq_electric, 0) * NULLIF(T.eq_electric, 0)
        ,T.cd_water = NULLIF(@tot_cd_water, 0) / NULLIF(@tot_eq_water, 0) * NULLIF(T.eq_water, 0)
        ,T.cd_production = NULLIF(@tot_cd_steam, 0) / NULLIF(@tot_eq_steam, 0) * NULLIF(T.eq_production, 0)
        ,T.cd_steam = NULLIF(@tot_cd_steam, 0) / NULLIF(@tot_eq_steam, 0) * NULLIF(T.eq_steam, 0)
        ,T.cd_lng = NULLIF(@tot_cd_lng, 0) / NULLIF(@tot_eq_lng, 0) * NULLIF(T.eq_lng, 0)
        ,T.cd_diestuff = NULLIF(@tot_cd_diestuff, 0) / NULLIF(@tot_eq_diestuff, 0) * NULLIF(T.eq_diestuff, 0)
        ,T.cd_chemical = NULLIF(@tot_cd_chemical, 0) / NULLIF(@tot_eq_chemical, 0) * NULLIF(T.eq_chemical, 0)
        ,T.cd_resin = NULLIF(@tot_cd_resin, 0) / NULLIF(@tot_eq_resin, 0) * NULLIF(T.eq_resin, 0)
    FROM @_TEMP AS T

    PRINT 'Proc : 7';
    UPDATE T SET
        T.material_cost = S1.material_cost
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no AS item_code,
            material_cost
        FROM istem_costing.dbo.tr_prod_fg
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept40
            AND wh_class NOT IN ('7')
    ) AS S1 ON (S1.item_code=T.item_code)

    SELECT @tot_material_cost=ISNULL(SUM(material_cost), 0) FROM @_TEMP


    PRINT 'Proc : 8';
    SELECT @tot_grey_fabric=ISNULL(SUM(A.cons_mat_amount), 0) FROM (
        SELECT ISNULL(A.cons_mat_amount, 0) AS cons_mat_amount FROM istem_costing.dbo.tr_wip AS A WHERE A.comp_id=@comp_id AND A.f_year=@f_year AND A.f_month=@f_month AND A.dept=@dept40
    ) AS A
    UPDATE T SET
            T.grey_fabric = NULLIF(@tot_grey_fabric, 0) * (NULLIF(T.product_qty, 0)/NULLIF(A.sum_product_qty, 0))
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT
                item_code
                ,SUM(product_qty) AS sum_product_qty
            FROM @_TEMP
            GROUP BY
                item_code
        ) AS A ON (A.item_code=T.item_code)

    -- Total Fixed Cost per Dept
    PRINT 'Proc : 9';
    SELECT
            @tot_pc_labour=SUM(A.manex_amount)
        FROM istem_costing.dbo.tr_manex AS A
        LEFT JOIN istem_costing.dbo.ms_cost_group AS B ON (B.comp_id=A.comp_id AND B.cost_sheet_id=A.cost_sheet_id)
        WHERE
            A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept=@dept40
            AND A.tr_code='FCD'
            AND B.fix_cost_group='R10'

    -- Total Fixed Cost per Dept
    PRINT 'Proc : 10';
    SELECT
            @tot_pc_repair=SUM(A.manex_amount)
        FROM istem_costing.dbo.tr_manex AS A
        LEFT JOIN istem_costing.dbo.ms_cost_group AS B ON (B.comp_id=A.comp_id AND B.cost_sheet_id=A.cost_sheet_id)
        WHERE
            A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept=@dept40
            AND A.tr_code IN ('FCD', 'AUFC', 'ACGA', 'ACEN')
            AND B.fix_cost_group='R11'

    -- Total tot_pc_labour
    PRINT 'Proc : 11';
    SELECT
            @tot_pc_labour=SUM(A.manex_amount)
        FROM istem_costing.dbo.tr_manex AS A
        LEFT JOIN istem_costing.dbo.ms_cost_group AS B ON (B.comp_id=A.comp_id AND B.cost_sheet_id=A.cost_sheet_id)
        WHERE
            A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept=@dept40
            AND A.tr_code IN ('FCD', 'AUFC', 'ACGA', 'ACEN')
            AND B.fix_cost_group='R10'

    -- Total Fixed Cost per Dept
    PRINT 'Proc : 12';
    SELECT
            @tot_pc_fixed=ISNULL(SUM(A.manex_amount) , 0) - (ISNULL(@tot_pc_repair, 0) - ISNULL(@tot_pc_labour, 0))
        FROM istem_costing.dbo.tr_manex AS A
        WHERE
            A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept=@dept40
            AND A.tr_code IN ('FCD', 'AUFC', 'ACGA', 'ACEN')

    PRINT 'Proc : 13';
    UPDATE T SET
        T.pc_fixed      = NULLIF(@tot_pc_fixed, 0)/NULLIF(@tot_eq_production, 0) * T.eq_production
        ,T.pc_labour    = NULLIF(@tot_pc_labour, 0)/NULLIF(@tot_eq_labor, 0) * T.eq_labor
        ,T.pc_repair    = NULLIF(@tot_pc_repair, 0)/NULLIF(@tot_eq_repair, 0) * T.eq_repair
    FROM @_TEMP AS T

    PRINT 'Proc : 14';
    UPDATE T SET
        T.total_cost = NULLIF(
                ISNULL(T.cd_electric, 0) +
                ISNULL(T.cd_water, 0) +
                ISNULL(T.cd_steam, 0) +
                ISNULL(T.cd_lng, 0) +
                ISNULL(T.cd_chemical, 0) +
                ISNULL(T.cd_diestuff, 0) +
                ISNULL(T.cd_resin, 0) +
                ISNULL(T.grey_fabric, 0) +
                ISNULL(T.material_cost, 0) +
                ISNULL(T.pc_fixed, 0) +
                ISNULL(T.pc_labour, 0) +
                ISNULL(T.pc_repair, 0)
            ,0)
    FROM @_TEMP AS T

    PRINT 'Proc : 15';
    UPDATE T SET
        T.unit_cost = NULLIF(NULLIF(T.total_cost, 0)/NULLIF(T.product_qty, 0), 0)
    FROM @_TEMP AS T


    -- 520.100.130.147	 Packing Materials Domestic
    -- 520.100.130.148	 Packing Materials Export

    -- Domestic
    DECLARE @_pack_mat_dom DECIMAL(16,4)
    DECLARE @_pack_mat_price_dom_pass DECIMAL(16,4)
    DECLARE @_pack_mat_price_dom_nonpass DECIMAL(16,4)

    -- Export
    DECLARE @_pack_mat_exp DECIMAL(16,4)
    DECLARE @_pack_mat_price_exp_pass DECIMAL(16,4)
    DECLARE @_pack_mat_price_exp_nonpass DECIMAL(16,4)

    PRINT 'Proc : 16';
    -- Get Packing Materials Amount
    SELECT @_pack_mat_dom = ISNULL(expense_amount, 0) FROM istem_costing.dbo.tr_sap_sme WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40 AND acct_code='520.100.130.147';
    SELECT @_pack_mat_exp = ISNULL(expense_amount, 0) FROM istem_costing.dbo.tr_sap_sme WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40 AND acct_code='520.100.130.148';

    PRINT 'Proc : 17';
    -- Domestic
    -- Get Set Packing Materials Price
    SELECT @_pack_mat_price_dom_pass = NULLIF(@_pack_mat_dom, 0)/NULLIF(SUM(prod_qty), 0) FROM istem_costing.dbo.tr_prod_fg WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40 AND wh_class NOT IN ('7') AND dom_exp='D' AND undelivery=0; -- PASS
    SELECT @_pack_mat_price_dom_nonpass = NULLIF(@_pack_mat_dom, 0)/NULLIF(SUM(prod_qty), 0) FROM istem_costing.dbo.tr_prod_fg WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40 AND wh_class NOT IN ('7') AND dom_exp='D' AND undelivery=1; -- NON PASS

    PRINT 'Proc : 18';
    -- Export
    -- Get Set Packing Materials Price
    SELECT @_pack_mat_price_exp_pass = NULLIF(@_pack_mat_exp, 0)/NULLIF(SUM(prod_qty), 0) FROM istem_costing.dbo.tr_prod_fg WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40 AND wh_class NOT IN ('7') AND dom_exp='E' AND undelivery=0; -- PASS
    SELECT @_pack_mat_price_exp_nonpass = NULLIF(@_pack_mat_exp, 0)/NULLIF(SUM(prod_qty), 0) FROM istem_costing.dbo.tr_prod_fg WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40 AND wh_class NOT IN ('7') AND dom_exp='E' AND undelivery=1; -- NON PASS

    DECLARE @_TEMP_PROD TABLE (
        item_code VARCHAR(45)
        ,prod_qty NUMERIC(16,4)
        ,alloc_dom_pass_qty NUMERIC(16,4)
        ,alloc_dom_pass_price NUMERIC(16,4)
        ,alloc_dom_pass_amount NUMERIC(16,4)
        ,alloc_dom_non_pass_qty NUMERIC(16,4)
        ,alloc_dom_non_pass_price NUMERIC(16,4)
        ,alloc_dom_non_pass_amount NUMERIC(16,4)
        ,alloc_dom_total_amount NUMERIC(16,4)
        ,alloc_exp_pass_qty NUMERIC(16,4)
        ,alloc_exp_pass_price NUMERIC(16,4)
        ,alloc_exp_pass_amount NUMERIC(16,4)
        ,alloc_exp_non_pass_qty NUMERIC(16,4)
        ,alloc_exp_non_pass_price NUMERIC(16,4)
        ,alloc_exp_non_pass_amount NUMERIC(16,4)
        ,alloc_exp_total_amount NUMERIC(16,4)
        ,total_packing_all NUMERIC(16,4)
    )

    PRINT 'Proc : 19';
    -- Insert All Item
    INSERT INTO @_TEMP_PROD
        (
            item_code
            ,prod_qty
        )
        SELECT
            grey_no
            ,SUM(prod_qty) AS prod_qty
        FROM istem_costing.dbo.tr_prod_fg
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept40
            AND wh_class NOT IN ('7')
        GROUP BY grey_no

    PRINT 'Proc : 20';
    -- Update Domestic
    UPDATE T SET
        T.alloc_dom_pass_qty=P.prod_qty
        ,T.alloc_dom_pass_price=@_pack_mat_price_dom_pass
        ,T.alloc_dom_pass_amount=P.prod_qty*@_pack_mat_price_dom_pass
        ,T.alloc_dom_non_pass_qty=NP.prod_qty
        ,T.alloc_dom_non_pass_price=@_pack_mat_price_dom_nonpass
        ,T.alloc_dom_non_pass_amount=NP.prod_qty*@_pack_mat_price_dom_nonpass
    FROM @_TEMP_PROD AS T
    LEFT JOIN (
        SELECT
            grey_no AS item_code
            ,SUM(prod_qty) AS prod_qty
        FROM istem_costing.dbo.tr_prod_fg
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept40
            AND wh_class NOT IN ('7')
            AND dom_exp='D'
            AND undelivery=0
        GROUP BY grey_no
    ) AS P ON (P.item_code=T.item_code)
    LEFT JOIN (
        SELECT
            grey_no AS item_code
            ,SUM(prod_qty) AS prod_qty
        FROM istem_costing.dbo.tr_prod_fg
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept40
            AND wh_class NOT IN ('7')
            AND dom_exp='D'
            AND undelivery=1
        GROUP BY grey_no
    ) AS NP ON (NP.item_code=T.item_code)
    -- Update Domestic - Total Amount
    UPDATE T SET
        T.alloc_dom_total_amount=(ISNULL(T.alloc_dom_pass_amount, 0)+ISNULL(T.alloc_dom_non_pass_amount, 0))
    FROM @_TEMP_PROD AS T

    PRINT 'Proc : 21';
    -- Update Export
    UPDATE T SET
        T.alloc_exp_pass_qty=P.prod_qty
        ,T.alloc_exp_pass_price=@_pack_mat_price_exp_pass
        ,T.alloc_exp_pass_amount=P.prod_qty*@_pack_mat_price_exp_pass
        ,T.alloc_exp_non_pass_qty=NP.prod_qty
        ,T.alloc_exp_non_pass_price=@_pack_mat_price_exp_nonpass
        ,T.alloc_exp_non_pass_amount=NP.prod_qty*@_pack_mat_price_exp_nonpass
    FROM @_TEMP_PROD AS T
    LEFT JOIN (
        SELECT
            grey_no AS item_code
            ,SUM(prod_qty) AS prod_qty
        FROM istem_costing.dbo.tr_prod_fg
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept40
            AND wh_class NOT IN ('7')
            AND dom_exp='D'
            AND undelivery=0
        GROUP BY grey_no
    ) AS P ON (P.item_code=T.item_code)
    LEFT JOIN (
        SELECT
            grey_no AS item_code
            ,SUM(prod_qty) AS prod_qty
        FROM istem_costing.dbo.tr_prod_fg
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept40
            AND wh_class NOT IN ('7')
            AND dom_exp='D'
            AND undelivery=1
        GROUP BY grey_no
    ) AS NP ON (NP.item_code=T.item_code)
    -- Update Export - Total Amount
    UPDATE T SET
        T.alloc_exp_total_amount=(ISNULL(T.alloc_exp_pass_amount, 0)+ISNULL(T.alloc_exp_non_pass_amount, 0))
    FROM @_TEMP_PROD AS T

    -- Update All Total Amount
    UPDATE T SET
        T.total_packing_all=(ISNULL(T.alloc_dom_total_amount, 0)+ISNULL(T.alloc_exp_total_amount, 0))
    FROM @_TEMP_PROD AS T

    PRINT 'Proc : 22';
    UPDATE A SET
        A.pack_cost = NULLIF(B.sum_total_packing_all, 0) * (NULLIF(C.sum_prod_qty, 0)/NULLIF(A.product_qty, 0))
    FROM @_TEMP AS A
    LEFT JOIN (
        SELECT
            item_code
            ,SUM(total_packing_all) AS sum_total_packing_all
        FROM @_TEMP_PROD
        GROUP BY
            item_code
    ) AS B ON (B.item_code = A.item_code)
    LEFT JOIN (
        SELECT
            grey_no AS item_code,
            SUM(prod_qty) AS sum_prod_qty
        FROM istem_costing.dbo.tr_prod_fg
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept40
            AND wh_class NOT IN ('7')
        GROUP BY grey_no
    ) AS C ON (C.item_code=A.item_code)

    -- Hasil Query Di Atas Kemudian
    -- Nanti Simpan / Update kembali ke tr_prod_fg ke kolom [process_cost], [packing_cost]

    -- Update ke tr_prod_fg
    SELECT @proc_no=ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_prod_fg WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40;
    INSERT INTO istem_costing.dbo.tr_prod_fg_hist SELECT * FROM istem_costing.dbo.tr_prod_fg WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40;
    UPDATE T SET
        T.process_cost=ISNULL(S.pc_fixed, 0) + ISNULL(S.pc_labour, 0) + ISNULL(S.pc_repair, 0)
        ,T.material_cost=S.material_cost
        ,T.pack_mat_cost=S.pack_cost
        ,T.rec_sts='T'
        ,T.proc_no=@proc_no
        ,T.proc_time=@proc_time
        ,T.user_id=@user_id
        ,T.client_ip=@client_ip
    FROM istem_costing.dbo.tr_prod_fg AS T
    LEFT JOIN @_TEMP AS S ON (S.item_code=T.grey_no AND S.dy_proc_type=T.dy_proc_type AND S.dyes_type_code=T.dyes_type_code AND S.color_shade_code=T.color_shade_code)
    WHERE
        T.comp_id=@comp_id
        AND T.f_year=@f_year
        AND T.f_month=@f_month
        AND T.dept=@dept40
        AND T.wh_class NOT IN ('7')

    -- Update ke tr_prod_detail2
    SELECT @proc_no=ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_prod_detail2 WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40;
    INSERT INTO istem_costing.dbo.tr_prod_detail2_hist SELECT * FROM istem_costing.dbo.tr_prod_detail2 WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40;
    UPDATE T SET
        T.prod_cost_distr = S.prod_cost_distr
        ,T.rec_sts='T'
        ,T.proc_no=@proc_no
        ,T.proc_time=@proc_time
        ,T.user_id=@user_id
        ,T.client_ip=@client_ip
    FROM istem_costing.dbo.tr_prod_detail2 AS T
    LEFT JOIN (
        SELECT * FROM (
            SELECT
                item_code
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
                , 'R01' AS convert_type_code
                , eq_electric AS prod_cost_distr
            FROM @_TEMP
            UNION ALL
            SELECT
                item_code
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
                , 'R02' AS convert_type_code
                , eq_water AS prod_cost_distr
            FROM @_TEMP
            UNION ALL
            SELECT
                item_code
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
                , 'R03' AS convert_type_code
                , eq_steam AS prod_cost_distr
            FROM @_TEMP
            UNION ALL
            SELECT
                item_code
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
                , 'R07' AS convert_type_code
                , eq_production AS prod_cost_distr
            FROM @_TEMP
            UNION ALL
            SELECT
                item_code
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
                , 'R05' AS convert_type_code
                , eq_lng AS prod_cost_distr
            FROM @_TEMP
            UNION ALL
            SELECT
                item_code
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
                , 'R09' AS convert_type_code
                , eq_chemical AS prod_cost_distr
            FROM @_TEMP
            UNION ALL
            SELECT
                item_code
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
                , 'R10' AS convert_type_code
                , eq_labor AS prod_cost_distr
            FROM @_TEMP
            UNION ALL
            SELECT
                item_code
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
                , 'R11' AS convert_type_code
                , eq_repair AS prod_cost_distr
            FROM @_TEMP
            UNION ALL
            SELECT
                item_code
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
                , 'R17' AS convert_type_code
                , eq_resin AS prod_cost_distr
            FROM @_TEMP
            UNION ALL
            SELECT
                item_code
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
                , 'R13' AS convert_type_code
                , eq_diestuff AS prod_cost_distr
            FROM @_TEMP
            UNION ALL
            SELECT
                item_code
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
                , 'R15' AS convert_type_code
                , eq_chemical AS prod_cost_distr
            FROM @_TEMP
        ) AS A
        WHERE A.prod_cost_distr IS NOT NULL
    )
    AS S ON (S.item_code=T.item_code AND S.dy_proc_type=T.dy_proc_type AND S.dyes_type_code=T.dyes_type_code AND S.color_shade_code=T.color_shade_code)
    WHERE
        T.comp_id=@comp_id
        AND T.f_year=@f_year
        AND T.f_month=@f_month
        AND T.dept=@dept40

    -- Update ke tr_prod_detail1
    SELECT @proc_no=ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_prod_detail1 WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40;
    INSERT INTO istem_costing.dbo.tr_prod_detail1_hist SELECT * FROM istem_costing.dbo.tr_prod_detail1 WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40;
    UPDATE T SET
        T.tot_prod_cost_distr = S.sum_of_prod_cost_distr
        ,T.rec_sts='T'
        ,T.proc_no=@proc_no
        ,T.proc_time=@proc_time
        ,T.user_id=@user_id
        ,T.client_ip=@client_ip
    FROM istem_costing.dbo.tr_prod_detail1 AS T
    LEFT JOIN (
            SELECT
                A.cost_sheet_id,
                SUM(A.prod_cost_distr) AS sum_of_prod_cost_distr
            FROM istem_costing.dbo.tr_prod_detail2 AS A
            WHERE
                A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
                AND A.dept=@dept40
            GROUP BY A.cost_sheet_id
    ) AS S ON (S.cost_sheet_id=T.cost_sheet_id)
    WHERE
        T.comp_id=@comp_id
        AND T.f_year=@f_year
        AND T.f_month=@f_month
        AND T.dept=@dept40

   PRINT 'Proc : 18';
   SELECT @proc_no=ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_prod_detail3 WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40;
   SET @rec_sts='A'
   IF @proc_no > 1
   BEGIN
    SET @rec_sts='T'
   END
   DELETE FROM istem_costing.dbo.tr_prod_detail3 WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40;
   INSERT INTO istem_costing.dbo.tr_prod_detail3
       (
           comp_id
           ,f_year
           ,f_month
           ,dept
           ,cost_sheet_id
           ,item_code
           ,fix_cost_group
           ,fix_cost_amount
           ,proc_time
           ,user_id
           ,client_ip
           ,rec_sts
           ,proc_no
       )
       SELECT *
       FROM (
           SELECT
                @comp_id AS comp_id
               ,@f_year AS f_year
               ,@f_month AS f_month
               ,@dept40 AS dept
               ,A.cost_sheet_id AS cost_sheet_id
               ,A.item_code AS item_code
               ,'R10' AS fix_cost_group
               ,B.pc_labour AS fix_cost_amount
               ,@proc_time AS proc_time
               ,@user_id AS user_id
               ,@client_ip AS client_ip
               ,@rec_sts AS rec_sts
               ,@proc_no AS proc_no
           FROM istem_costing.dbo.tr_prod AS A
           INNER JOIN @_TEMP AS B ON (B.item_code=A.item_code)
           WHERE
               A.comp_id=@comp_id
               AND A.f_year=@f_year
               AND A.f_month=@f_month
               AND A.dept=@dept40
           UNION ALL
           SELECT
                @comp_id AS comp_id
               ,@f_year AS f_year
               ,@f_month AS f_month
               ,@dept40 AS dept
               ,A.cost_sheet_id AS cost_sheet_id
               ,A.item_code AS item_code
               ,'R11' AS fix_cost_group
               ,B.pc_repair AS fix_cost_amount
               ,@proc_time AS proc_time
               ,@user_id AS user_id
               ,@client_ip AS client_ip
               ,@rec_sts AS rec_sts
               ,@proc_no AS proc_no
           FROM istem_costing.dbo.tr_prod AS A
           INNER JOIN @_TEMP AS B ON (B.item_code=A.item_code)
           WHERE
               A.comp_id=@comp_id
               AND A.f_year=@f_year
               AND A.f_month=@f_month
               AND A.dept=@dept40
           UNION ALL
           SELECT
                @comp_id AS comp_id
               ,@f_year AS f_year
               ,@f_month AS f_month
               ,@dept40 AS dept
               ,A.cost_sheet_id AS cost_sheet_id
               ,A.item_code AS item_code
               ,'R07' AS fix_cost_group
               ,B.pc_fixed AS fix_cost_amount
               ,@proc_time AS proc_time
               ,@user_id AS user_id
               ,@client_ip AS client_ip
               ,@rec_sts AS rec_sts
               ,@proc_no AS proc_no
           FROM istem_costing.dbo.tr_prod AS A
           INNER JOIN @_TEMP AS B ON (B.item_code=A.item_code)
           WHERE
               A.comp_id=@comp_id
               AND A.f_year=@f_year
               AND A.f_month=@f_month
               AND A.dept=@dept40
       ) AS A
       WHERE NULLIF(A.fix_cost_amount, 0) IS NOT NULL

   PRINT 'Proc : 19';
   SELECT @proc_no=ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_prod WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40;
   INSERT INTO istem_costing.dbo.tr_prod_hist SELECT * FROM istem_costing.dbo.tr_prod WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40;
   UPDATE T
       SET
       T.process_cost = ISNULL(S.pc_fixed, 0) + ISNULL(S.pc_labour, 0) + ISNULL(S.pc_repair, 0)
       ,T.rec_sts='T'
       ,T.proc_no=@proc_no
       ,T.proc_time=@proc_time
       ,T.user_id=@user_id
       ,T.client_ip=@client_ip
   FROM istem_costing.dbo.tr_prod AS T
   LEFT JOIN @_TEMP AS S ON (S.item_code=T.item_code)
   WHERE
       T.comp_id=@comp_id
       AND T.f_year=@f_year
       AND T.f_month=@f_month
       AND T.dept=@dept40
       AND ISNULL(S.pc_fixed, 0) <> 0
       AND ISNULL(S.pc_labour, 0) <> 0
       AND ISNULL(S.pc_repair, 0) <> 0
END
