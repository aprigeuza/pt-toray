
ALTER PROCEDURE  [sp_SpinningProcessWIPCALC]
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;
    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=7
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'


    DECLARE @dept_10 INT
    DECLARE @dept_20 INT
    DECLARE @mc_loc VARCHAR(MAX)
    DECLARE @tr_code VARCHAR(MAX)
    DECLARE @proc_code VARCHAR(MAX)
    DECLARE @proc_time AS DATETIME
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_no INT
    DECLARE @item_code VARCHAR(MAX)
    DECLARE @proc_type VARCHAR(MAX)
    DECLARE @mat_qty_unit VARCHAR(MAX)
    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT

    SET @dept_10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id) AND (dept_seq=10))
    SET @dept_20=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id) AND (dept_seq=20))

    IF @f_month = 1
    BEGIN
        SET @lp_f_month = 12
        SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
        SET @lp_f_month = @f_month - 1
        SET @lp_f_year = @f_year
    END

    DECLARE @_TEMP TABLE(
         res VARCHAR(MAX)
        ,mat_code VARCHAR(MAX)
        ,mat_name VARCHAR(MAX)
        ,product_item VARCHAR(MAX)
        ,product_item2 VARCHAR(MAX)
        ,cost_sheet_id VARCHAR(MAX)
        ,bf_qty NUMERIC(14,5) DEFAULT 0
        ,bf_price NUMERIC(14,5) DEFAULT 0
        ,bf_amount NUMERIC(14,5) DEFAULT 0
        ,rc_qty NUMERIC(14,5) DEFAULT 0
        ,rc_price NUMERIC(14,5) DEFAULT 0
        ,rc_amount NUMERIC(14,5) DEFAULT 0
        ,co_qty NUMERIC(14,5) DEFAULT 0
        ,co_price NUMERIC(14,5) DEFAULT 0
        ,co_amount NUMERIC(14,5) DEFAULT 0
        ,cf_qty NUMERIC(14,5) DEFAULT 0
        ,cf_price NUMERIC(14,5) DEFAULT 0
        ,cf_amount NUMERIC(14,5) DEFAULT 0
        ,wh_conv_qty NUMERIC(14,5) DEFAULT 0
        ,so VARCHAR(MAX) DEFAULT NULL
    )

    -- Insert Header
    INSERT INTO @_TEMP
            (res,mat_code,mat_name, cost_sheet_id, so)
        SELECT
            'H',MT.mat_code, M.FrgnName, M.U_Cost_ID, 'A'
        FROM (
            SELECT mat_code
            FROM istem_costing.dbo.tr_bal_wip AS A
            WHERE
                    (A.comp_id=@comp_id)
                AND (A.f_year=@lp_f_year)
                AND (A.f_month=@lp_f_month)
                AND (A.dept=@dept_20)
            GROUP BY mat_code
            UNION ALL
            SELECT mat_code
            FROM istem_costing.dbo.tr_inv_out_detail AS A
            WHERE
                    (A.comp_id=@comp_id)
                AND (A.f_year=@f_year)
                AND (A.f_month=@f_month)
                AND (A.dept=@dept_10)
                AND (A.out_dest=@dept_20)
            GROUP BY mat_code
        ) MT
        LEFT JOIN istem_costing.dbo.v_SAPItem AS M ON (M.ItemCode COLLATE SQL_Latin1_General_CP1_CI_AS = MT.mat_code COLLATE SQL_Latin1_General_CP1_CI_AS)
        GROUP BY MT.mat_code, M.FrgnName, M.U_Cost_ID

    -- Insert Detail 1
    INSERT INTO @_TEMP
        (res,mat_code,mat_name,product_item, cost_sheet_id, so)
        SELECT
            'D1' AS res
            ,H.mat_code
            ,H.mat_name
            ,D1.item_category AS product_item
            ,H.cost_sheet_id
            ,'B1' AS so
        FROM @_TEMP AS H
        INNER JOIN (
            SELECT mat_code, item_category FROM istem_costing.dbo.tr_bal_wip
                WHERE (comp_id=@comp_id) AND (f_year=@lp_f_year) AND (f_month=@lp_f_month) AND (dept=@dept_20)
            UNION ALL
            SELECT mat_code, item_code FROM istem_costing.dbo.tr_dept_calc
                WHERE (comp_id=@comp_id) AND (f_year=@f_year) AND (f_month=@f_month) AND (dept=@dept_20) AND (tr_code LIKE 'WIP%')
         -- UNION ALL
         --    SELECT mat_code, item_category FROM istem_costing.dbo.tr_wip
         --        WHERE (comp_id=@comp_id) AND (f_year=@f_year) AND (f_month=@f_month) AND (dept=@dept_20)
        ) AS D1 ON (D1.mat_code=H.mat_code)
        WHERE H.res = 'H'
        GROUP BY H.mat_code,H.mat_name,D1.item_category,H.cost_sheet_id

    -- Insert Detail 2
    INSERT INTO @_TEMP
        (res,mat_code,mat_name,product_item,product_item2, cost_sheet_id, so)
        SELECT
            'D2' AS res
            ,D1.mat_code
            ,D1.mat_name
            ,D1.product_item AS product_item
            ,D2.yarn_code AS product_item2
            ,D1.cost_sheet_id
            ,'B2' AS so
        FROM @_TEMP AS D1
        INNER JOIN (
            SELECT
             (yarn_type+yarn_mat_type+yarn_sf_length+yarn_version) AS yarn_category
            ,yarn_code
            FROM  istem_sms.dbo.sp_ms_yarn
        ) AS D2 ON (D2.yarn_category=D1.product_item)
        WHERE D1.res = 'D1'
        GROUP BY D1.mat_code,D1.cost_sheet_id,D1.mat_name,D1.product_item,D2.yarn_code

    -- SELECT * FROM @_TEMP

    -- Update Detail 2 - WH Conv Qty
    UPDATE T
        SET
            T.wh_conv_qty=ISNULL(S.wh_conv_qty, 0)
        FROM @_TEMP AS T
        INNER JOIN (
            SELECT
                 A.res AS res
                ,ISNULL(A.mat_code, '') AS mat_code
                ,ISNULL(A.product_item, '') AS product_item
                ,ISNULL(A.product_item2, '') AS product_item2
                ,B.wh_conv_qty AS wh_conv_qty
            FROM @_TEMP AS A
            LEFT JOIN (
                SELECT
                    (A.yarn_type+A.yarn_mat_type+A.yarn_sf_length+A.yarn_version) AS product_item
                    ,B.item_code AS product_item2
                    ,B.mat_code
                    ,ISNULL(SUM(ISNULL(B.wh_conv_qty, 0)),0) AS wh_conv_qty
                FROM istem_sms.dbo.sp_ms_yarn AS A
                LEFT JOIN (
                    SELECT
                        B.item_code
                        ,B.mat_code
                        ,ISNULL(SUM(ISNULL(B.mat_qty, 0)),0) AS wh_conv_qty
                    FROM istem_costing.dbo.tr_dept_calc AS B
                    WHERE
                        B.comp_id=@comp_id
                        AND B.f_year=@f_year
                        AND B.f_month=@f_month
                        AND B.dept=@dept_20
                        AND B.tr_code='WH'
                    GROUP BY B.item_code,B.mat_code,B.item_code
                ) AS B ON (B.item_code=A.yarn_code)
                GROUP BY (A.yarn_type+A.yarn_mat_type+A.yarn_sf_length+A.yarn_version),B.mat_code,B.item_code
            ) AS B ON (B.mat_code=A.mat_code AND B.product_item=A.product_item AND B.product_item2=A.product_item2)
            WHERE A.res='D2'
        ) AS S ON (S.res=T.res AND S.product_item=T.product_item AND S.mat_code=T.mat_code AND S.product_item2=T.product_item2)
        WHERE T.res='D2'

    -- update 3 des 2020
    DELETE FROM @_TEMP WHERE res='D2' AND ISNULL(wh_conv_qty, 0)=0

    -- Update Detail 1 - WH Conv Qty
    UPDATE T
        SET
            T.wh_conv_qty=S.wh_conv_qty
        FROM @_TEMP AS T
        INNER JOIN (
            SELECT
                (A.yarn_type+A.yarn_mat_type+A.yarn_sf_length+A.yarn_version) AS product_item
                ,B.mat_code
                ,SUM(B.wh_conv_qty) AS wh_conv_qty
            FROM istem_sms.dbo.sp_ms_yarn AS A
            LEFT JOIN (
                SELECT
                    B.item_code
                    ,B.mat_code
                    ,SUM(ISNULL(B.mat_qty, 0)) AS wh_conv_qty
                FROM istem_costing.dbo.tr_dept_calc AS B
                WHERE
                    B.comp_id=@comp_id
                    AND B.f_year=@f_year
                    AND B.f_month=@f_month
                    AND B.dept=@dept_20
                    AND B.tr_code='WH'
                GROUP BY B.item_code,B.mat_code
            ) AS B ON (B.item_code=A.yarn_code)
            GROUP BY (A.yarn_type+A.yarn_mat_type+A.yarn_sf_length+A.yarn_version),B.mat_code
        ) AS S ON (S.mat_code = T.mat_code AND S.product_item=T.product_item)
        WHERE T.res='D1'

    -- Update Header - WH Conv Qty
    UPDATE T
        SET
            T.wh_conv_qty=S.wh_conv_qty
        FROM @_TEMP AS T
        INNER JOIN (
            SELECT
                 ISNULL(A.mat_code, '') AS mat_code
                 ,SUM(ISNULL(A.wh_conv_qty, 0)) AS wh_conv_qty
            FROM @_TEMP AS A
            WHERE A.res='D1'
            GROUP BY A.mat_code
        ) AS S ON (S.mat_code=T.mat_code)
        WHERE T.res='H'

    -- Update Detail 1 - bf_qty, bf_price, bf_amount
    UPDATE T
        SET
            T.bf_qty=S.bf_qty
            ,T.bf_price=ISNULL(NULLIF(S.bf_amount, 0)/NULLIF(S.bf_qty, 0), 0)
            ,T.bf_amount=S.bf_amount
        FROM @_TEMP AS T
        INNER JOIN (
            SELECT
                 A.res AS res
                ,ISNULL(A.mat_code, '') AS mat_code
                ,ISNULL(A.product_item, '') AS product_item
                ,ISNULL(BF.qty, 0) AS bf_qty
                ,ISNULL(BF.amount, 0) AS bf_amount
            FROM @_TEMP AS A
            -- BF
            LEFT JOIN (
                SELECT
                    mat_code
                    ,item_code AS product_item
                    ,SUM(ISNULL(A.cf_wip_qty, 0)) AS qty
                    ,SUM(ISNULL(A.cf_wip_amount, 0)) AS amount
                    ,0 AS price
                FROM istem_costing.dbo.tr_bal_wip AS A
                WHERE
                        (A.comp_id=@comp_id)
                    AND (A.f_year=@lp_f_year)
                    AND (A.f_month=@lp_f_month)
                    AND (A.dept=@dept_20)
                    AND (ISNULL(A.item_code, '') = '')
                GROUP BY mat_code,item_code
            ) AS BF ON (BF.mat_code=A.mat_code AND BF.product_item=A.product_item)
            WHERE A.res='D1'
        ) AS S ON (S.res=T.res AND S.mat_code=T.mat_code AND S.product_item=T.product_item)
        WHERE T.res='D1'
    -- Update Detail 1 - cf_qty
    UPDATE T
        SET
            T.cf_qty=S.cf_qty
        FROM @_TEMP AS T
        INNER JOIN (
            SELECT
                 A.res AS res
                ,ISNULL(A.mat_code, '') AS mat_code
                ,ISNULL(A.product_item, '') AS product_item
                ,ISNULL(CF.qty, 0) AS cf_qty
                ,ISNULL(CF.amount, 0) AS cf_amount
            FROM @_TEMP AS A
            LEFT JOIN (
                SELECT
                    mat_code
                    ,item_code AS product_item
                    ,SUM(ISNULL(A.mat_qty, 0)) AS qty
                    ,SUM(ISNULL(A.mat_amount, 0)) AS amount
                    ,0 AS price
                FROM istem_costing.dbo.tr_dept_calc AS A
                WHERE
                        (A.comp_id=@comp_id)
                    AND (A.f_year=@f_year)
                    AND (A.f_month=@f_month)
                    AND (A.dept=@dept_20)
                    AND (A.tr_code='WIP')
                GROUP BY mat_code,item_code
            ) AS CF ON (CF.mat_code=A.mat_code AND CF.product_item=A.product_item)
            WHERE A.res='D1'
        ) AS S ON (S.res=T.res AND S.mat_code=T.mat_code AND S.product_item=T.product_item)
        WHERE T.res='D1'

    -- Update Header - bf_qty, bf_price, bf_amount
    UPDATE T
        SET
            T.bf_qty=S.bf_qty -- A
            ,T.bf_price=S.bf_price -- B
            ,T.bf_amount=S.bf_amount -- C
        FROM @_TEMP AS T
        INNER JOIN (
            SELECT
                 A.res AS res
                ,A.mat_code AS mat_code
                ,ISNULL(BF.qty, 0) AS bf_qty
                ,ISNULL(BF.price, 0) AS bf_price
                ,ISNULL(BF.amount, 0) AS bf_amount
            FROM @_TEMP AS A
            -- BF
            LEFT JOIN (
                SELECT
                    mat_code
                    ,SUM(ISNULL(A.cf_wip_qty, 0)) AS qty
                    ,SUM(ISNULL(A.cf_wip_amount, 0)) AS amount
                    ,ISNULL(NULLIF(SUM(ISNULL(A.cf_wip_amount, 0)), 0)/NULLIF(SUM(ISNULL(A.cf_wip_qty, 0)), 0), 0) AS price
                FROM istem_costing.dbo.tr_bal_wip AS A
                WHERE
                        (A.comp_id=@comp_id)
                    AND (A.f_year=@lp_f_year)
                    AND (A.f_month=@lp_f_month)
                    AND (A.dept=@dept_20)
                    AND (ISNULL(A.item_code, '') = '')
                GROUP BY mat_code
            ) AS BF ON (BF.mat_code=A.mat_code)
            WHERE A.res='H'
        ) AS S ON (S.res=T.res AND S.mat_code=T.mat_code)
        WHERE T.res='H'
    -- Update Header - rc_qty, rc_price, rc_amount
    UPDATE T
        SET
             T.rc_qty=S.rc_qty -- D
            ,T.rc_price=S.rc_price -- E
            ,T.rc_amount=S.rc_amount -- F
        FROM @_TEMP AS T
        INNER JOIN (
            SELECT
                 A.res AS res
                ,A.mat_code AS mat_code
                ,ISNULL(RC.qty, 0) AS rc_qty
                ,ISNULL(RC.price, 0) AS rc_price
                ,ISNULL(RC.amount, 0) AS rc_amount
            FROM @_TEMP AS A
            -- Receive
            LEFT JOIN (
                --SELECT
                --    mat_code
                --    ,SUM(ISNULL(A.out_qty, 0)) AS qty
                --    ,SUM(ISNULL(A.out_amount, 0)) AS amount
                --    ,ISNULL(NULLIF(SUM(ISNULL(A.out_amount, 0)), 0)/NULLIF(SUM(ISNULL(A.out_qty, 0)), 0), 0) AS price
                --FROM istem_costing.dbo.tr_inv_out_detail AS A
                --WHERE
                --        (A.comp_id=@comp_id)
                --    AND (A.f_year=@f_year)
                --    AND (A.f_month=@f_month)
                --    AND (A.dept=@dept_10)
                --    AND (A.cost_sheet_id IN ('A10', 'A11', 'A12', 'A13'))
                --GROUP BY mat_code

                -- update 1 des 2020, link ke tr_dept_calc untuk dapatkan mat_amount
    			SELECT
                    A.mat_code
                    ,SUM(ISNULL(B.mat_qty, 0)) AS qty
                    ,SUM(ISNULL(B.mat_amount, 0)) AS amount
                    ,ISNULL(NULLIF(SUM(ISNULL(B.mat_amount, 0)), 0)/NULLIF(SUM(ISNULL(B.mat_qty, 0)), 0), 0) AS price
                FROM istem_costing.dbo.tr_inv_out_detail AS A
                LEFT JOIN istem_costing.dbo.tr_dept_calc AS B
    				ON (
    					B.comp_id=A.comp_id
    					AND B.f_year=A.f_year
    					AND B.f_month=A.f_month
    					AND B.mat_code=A.mat_code
    				)
                WHERE
                        (A.comp_id=@comp_id)
                    AND (A.f_year=@f_year)
                    AND (A.f_month=@f_month)
                    AND (A.dept=@dept_10)
                    AND (A.cost_sheet_id IN ('A10', 'A11', 'A12', 'A13'))
                    AND (B.tr_code='RCVRM')
                GROUP BY A.mat_code

            ) AS RC ON (RC.mat_code=A.mat_code)
            WHERE A.res='H'
        ) AS S ON (S.res=T.res AND S.mat_code=T.mat_code)
        WHERE T.res='H'

    -- Update Header - cf_qty
    UPDATE T
        SET
            T.cf_qty=S.cf_qty
        FROM @_TEMP AS T
        INNER JOIN (
            SELECT
                 ISNULL(A.mat_code, '') AS mat_code
                ,SUM(ISNULL(A.cf_qty, 0)) AS cf_qty
            FROM @_TEMP AS A
            WHERE A.res='D1'
            GROUP BY A.mat_code
        ) AS S ON (S.mat_code=T.mat_code)
        WHERE T.res='H'

    -- Update Header - co_qty, co_price, co_amount
    UPDATE T
        SET
            T.co_qty=S.co_qty
            ,T.co_price=ISNULL(NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0), 0)
            ,T.co_amount=S.co_qty*ISNULL(NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0), 0)
        FROM @_TEMP AS T
        INNER JOIN (
            SELECT
                 A.res AS res
                ,ISNULL(A.mat_code, '') AS mat_code
                ,(ISNULL(A.bf_qty, 0)+ISNULL(A.rc_qty, 0))-ISNULL(A.cf_qty, 0) AS co_qty -- G
            FROM @_TEMP AS A
            WHERE A.res='H'
        ) AS S ON (S.res=T.res AND S.mat_code=T.mat_code)
        WHERE T.res='H'

    -- Update Header - cf_price, cf_amount
    UPDATE T
        SET
            T.cf_price=S.co_price -- Consume Price = CF Price
            ,T.cf_amount=ISNULL(NULLIF(T.cf_qty, 0)*NULLIF(S.co_price, 0), 0) -- Consume Price = CF Price
        FROM @_TEMP AS T
        INNER JOIN (
            SELECT
                 A.res AS res
                ,ISNULL(A.mat_code, '') AS mat_code
                ,(ISNULL(A.bf_qty, 0)+ISNULL(A.rc_qty, 0))-ISNULL(A.cf_qty, 0) AS co_qty -- G
                ,ISNULL(NULLIF((ISNULL(A.bf_amount, 0)+ISNULL(A.rc_amount, 0)), 0)/NULLIF((ISNULL(A.bf_qty, 0)+ISNULL(A.rc_qty, 0)), 0), 0) AS co_price -- H
            FROM @_TEMP AS A
            WHERE A.res='H'
        ) AS S ON (S.res=T.res AND S.mat_code=T.mat_code)
        WHERE T.res='H'

    -- Update Detail 1 - rc_price, cf_price, cf_amount
    UPDATE T
        SET
             T.rc_price=H.rc_price
            ,T.co_price=H.co_price
            ,T.cf_price=H.cf_price
            ,T.cf_amount=ISNULL(NULLIF(T.cf_qty, 0)*NULLIF(H.cf_price, 0), 0)
        FROM @_TEMP AS T
        INNER JOIN @_TEMP AS H ON (H.res='H' AND H.mat_code=T.mat_code)
        WHERE T.res='D1'

    -- Update Detail 1 - rc_qty, rc_amount, co_qty, co_amount
    UPDATE T
        SET
            T.rc_qty=ISNULL((NULLIF(T.wh_conv_qty, 0)/NULLIF(H.wh_conv_qty, 0)) * NULLIF(H.rc_qty, 0), 0)
            ,T.rc_amount=ISNULL(((NULLIF(T.wh_conv_qty, 0)/NULLIF(H.wh_conv_qty, 0)) * NULLIF(H.rc_qty, 0)) * NULLIF(H.rc_price, 0), 0)
            ,T.co_qty=ISNULL((NULLIF(T.wh_conv_qty, 0)/NULLIF(H.wh_conv_qty, 0)) * NULLIF(H.co_qty, 0), 0)
            ,T.co_amount=ISNULL(((NULLIF(T.wh_conv_qty, 0)/NULLIF(H.wh_conv_qty, 0)) * NULLIF(H.co_qty, 0)) * NULLIF(H.co_price, 0), 0)
        FROM @_TEMP AS T
        INNER JOIN @_TEMP AS H ON (H.res='H' AND H.mat_code=T.mat_code)
        WHERE
            T.res='D1'
            AND NULLIF(H.wh_conv_qty, 0) IS NOT NULL

        -- Update Detail 1 - rc_qty, rc_amount, co_qty, co_amount
    UPDATE T
        SET
            T.rc_qty=ISNULL((NULLIF(T.cf_qty, 0)/NULLIF(H.cf_qty, 0)) * NULLIF(H.rc_qty, 0), 0)
            ,T.rc_amount=ISNULL(((NULLIF(T.cf_qty, 0)/NULLIF(H.cf_qty, 0)) * NULLIF(H.rc_qty, 0)) * NULLIF(H.rc_price, 0), 0)
            ,T.co_qty=ISNULL((NULLIF(T.cf_qty, 0)/NULLIF(H.cf_qty, 0)) * NULLIF(H.co_qty, 0), 0)
            ,T.co_amount=ISNULL(((NULLIF(T.cf_qty, 0)/NULLIF(H.cf_qty, 0)) * NULLIF(H.co_qty, 0)) * NULLIF(H.co_price, 0), 0)
        FROM @_TEMP AS T
        INNER JOIN @_TEMP AS H ON (H.res='H' AND H.mat_code=T.mat_code)
        WHERE
            T.res='D1'
            AND NULLIF(H.wh_conv_qty, 0) IS NULL
            AND NULLIF(H.cf_qty, 0) IS NOT NULL

    -- Update Detail 2 - co_amount
    UPDATE T
        SET
            T.bf_qty=ISNULL(((NULLIF(T.wh_conv_qty, 0)/NULLIF(D1.wh_conv_qty, 0)) * NULLIF(D1.bf_qty, 0)), 0)
            ,T.bf_price=D1.bf_price
            ,T.bf_amount=ISNULL(((NULLIF(T.wh_conv_qty, 0)/NULLIF(D1.wh_conv_qty, 0)) * NULLIF(D1.bf_amount, 0)), 0)
            ,T.rc_qty=ISNULL(((NULLIF(T.wh_conv_qty, 0)/NULLIF(D1.wh_conv_qty, 0)) * NULLIF(D1.rc_qty, 0)), 0)
            ,T.rc_price=D1.rc_price
            ,T.rc_amount=ISNULL(((NULLIF(T.wh_conv_qty, 0)/NULLIF(D1.wh_conv_qty, 0)) * NULLIF(D1.rc_amount, 0)), 0)
            ,T.co_qty=ISNULL(((NULLIF(T.wh_conv_qty, 0)/NULLIF(D1.wh_conv_qty, 0)) * NULLIF(D1.co_qty, 0)), 0)
            ,T.co_price=D1.co_price
            ,T.co_amount=ISNULL(((NULLIF(T.wh_conv_qty, 0)/NULLIF(D1.wh_conv_qty, 0)) * NULLIF(D1.co_amount, 0)), 0)
            ,T.cf_qty=ISNULL(((NULLIF(T.wh_conv_qty, 0)/NULLIF(D1.wh_conv_qty, 0)) * NULLIF(D1.cf_qty, 0)), 0)
            ,T.cf_price=D1.cf_price
            ,T.cf_amount=ISNULL(((NULLIF(T.wh_conv_qty, 0)/NULLIF(D1.wh_conv_qty, 0)) * NULLIF(D1.cf_amount, 0)), 0)
        FROM @_TEMP AS T
        INNER JOIN @_TEMP AS D1 ON (D1.res='D1' AND D1.mat_code=T.mat_code AND D1.product_item=T.product_item)
        WHERE T.res='D2'

    -- SELECT * FROM @_TEMP ORDER BY mat_code, ISNULL(product_item, ''), ISNULL(product_item2, ''), so;

    -- Cek tr_wip
    -- Find Data
    SET @proc_time=GETDATE()
    SET @proc_no=1
    SET @rec_sts='A'

    DECLARE @c INT
    SET @c = (SELECT COUNT(*) FROM istem_costing.dbo.tr_wip WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept_20))

    IF @c >= 1
    BEGIN

        -- Save History
        INSERT INTO istem_costing.dbo.tr_wip_hist
                (comp_id
                ,f_year
                ,f_month
                ,dept
                ,cost_sheet_id
                ,mat_code
                ,item_category
                ,item_code
                ,qty_unit
                ,rcv_mat_qty
                ,rcv_mat_amount
                ,cons_mat_qty
                ,cons_mat_amount
                ,cf_wip_qty
                ,cf_wip_amount
                ,adj_wip_qty
                ,adj_wip_amount
                ,proc_time
                ,user_id
                ,client_ip
                ,rec_sts
                ,proc_no)
                SELECT
                     comp_id
                    ,f_year
                    ,f_month
                    ,dept
                    ,cost_sheet_id
                    ,mat_code
                    ,item_category
                    ,item_code
                    ,qty_unit
                    ,rcv_mat_qty
                    ,rcv_mat_amount
                    ,cons_mat_qty
                    ,cons_mat_amount
                    ,cf_wip_qty
                    ,cf_wip_amount
                    ,adj_wip_qty
                    ,adj_wip_amount
                    ,proc_time
                    ,user_id
                    ,client_ip
                    ,rec_sts
                    ,proc_no
                FROM istem_costing.dbo.tr_wip
                WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept_20)

        -- Jika sudah ada
        -- Jadikan rec_sts = T
        SET @rec_sts = 'T'

        SET @proc_no = (SELECT MAX(ISNULL(proc_no, 0)) + 1 FROM istem_costing.dbo.tr_wip WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept_20))
    END

    DELETE
        FROM istem_costing.dbo.tr_wip
        WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept_20)

    INSERT INTO istem_costing.dbo.tr_wip
            (
                comp_id
               ,f_year
               ,f_month
               ,dept
               ,cost_sheet_id
               ,mat_code
               ,item_category
               ,item_code
               ,qty_unit
               ,rcv_mat_qty
               ,rcv_mat_amount
               ,cons_mat_qty
               ,cons_mat_amount
               ,cf_wip_qty
               ,cf_wip_amount
               -- ,adj_wip_qty
               -- ,adj_wip_amount
               ,proc_time
               ,user_id
               ,client_ip
               ,rec_sts
               ,proc_no
            )
    		SELECT
                 @comp_id AS comp_id
                ,@f_year AS f_year
                ,@f_month AS f_month
                ,@dept_20 AS dept
                ,ISNULL(A.cost_sheet_id, '') AS cost_sheet_id
                ,ISNULL(A.mat_code, '') AS mat_code
                ,ISNULL(A.product_item, '') AS item_category
                ,ISNULL(A.product_item2, '') AS item_code
                ,B.mat_qty_unit AS qty_unit
                ,A.rc_qty AS rcv_mat_qty
                ,A.rc_amount AS rcv_mat_amount
                ,A.co_qty AS cons_mat_qty
                ,A.co_amount AS cons_mat_amount
                ,A.cf_qty AS cf_wip_qty
                ,A.cf_amount AS cf_wip_amount
                -- ,NULL AS adj_wip_qty
                -- ,NULL AS adj_wip_amount
                ,@proc_time AS proc_time
                ,@user_id AS user_id
                ,@client_ip AS client_ip
                ,@rec_sts AS rec_sts
                ,@proc_no AS proc_no
    		FROM @_TEMP AS A
            INNER JOIN
            (
                SELECT mat_code, item_code, mat_qty_unit FROM istem_costing.dbo.tr_dept_calc AS A
                WHERE
                        A.comp_id=@comp_id
                    AND A.f_year=@f_year
                    AND A.f_month=@f_month
                    AND A.dept=@dept_20
                GROUP BY mat_code, item_code, mat_qty_unit
            )
             AS B ON (B.mat_code=A.mat_code AND B.item_code=A.product_item)
            WHERE A.res = 'D1' OR A.res='D2'
    		-- GROUP BY A.mat_code, A.product_item,A.product_item2,A.cost_sheet_id,B.mat_qty_unit
            ORDER BY A.mat_code, ISNULL(A.product_item, ''), ISNULL(A.product_item2, ''), so

    -- Adjustment????!
    INSERT INTO istem_costing.dbo.tr_wip
            (
                comp_id
               ,f_year
               ,f_month
               ,dept
               ,cost_sheet_id
               ,mat_code
               ,item_category
               ,item_code
               ,qty_unit
               ,adj_wip_qty
               ,adj_wip_amount
               ,proc_time
               ,user_id
               ,client_ip
               ,rec_sts
               ,proc_no
            )
            SELECT
                 @comp_id AS comp_id
                ,@f_year AS f_year
                ,@f_month AS f_month
                ,@dept_20 AS dept
                ,ISNULL(A1.cost_sheet_id, '') AS cost_sheet_id
                ,ISNULL(A1.mat_code, '') AS mat_code
                ,ISNULL(A1.item_category, '') AS item_category
                ,ISNULL(A1.item_code, '') AS item_code
                ,A1.qty_unit AS qty_unit
                ,A1.cf_wip_qty AS adj_wip_qty
                ,A1.cf_wip_amount AS adj_wip_amount
                ,@proc_time AS proc_time
                ,@user_id AS user_id
                ,@client_ip AS client_ip
                ,@rec_sts AS rec_sts
                ,@proc_no AS proc_no
            FROM @_TEMP AS A
            INNER JOIN (
                SELECT * FROM istem_costing.dbo.tr_bal_wip AS A
                WHERE
                        A.comp_id=@comp_id
                    AND A.f_year=@lp_f_year
                    AND A.f_month=@lp_f_month
                    AND A.dept=@dept_20
                    AND (ISNULL(A.item_code, '') = '')
            ) AS A1 ON (A1.mat_code=A.mat_code AND A1.item_category=A.product_item)
            LEFT JOIN (
                SELECT mat_code, item_code, COUNT(*) AS c FROM istem_costing.dbo.tr_dept_calc AS A
                WHERE
                        A.comp_id=@comp_id
                    AND A.f_year=@f_year
                    AND A.f_month=@f_month
                    AND A.dept=@dept_20
                GROUP BY mat_code, item_code
            ) AS A3 ON (A3.mat_code=A.mat_code AND A3.item_code=A.product_item)
            WHERE
                NULLIF(A.res, '') = 'D1'
                AND ISNULL(A3.c, 0) = 0

    DELETE FROM [istem_costing].[dbo].[acct_wip_report] WHERE comp_id=@comp_id AND dept=@dept_20 AND f_year=@f_year AND f_month=@f_month
    INSERT INTO [istem_costing].[dbo].[acct_wip_report]
               ([comp_id]
               ,[dept]
               ,[f_year]
               ,[f_month]
               ,[mat_code]
               ,[mat_name]
               ,[product_item]
               ,[product_item2]
               ,[bf_qty]
               ,[bf_price]
               ,[bf_amount]
               ,[rc_qty]
               ,[rc_price]
               ,[rc_amount]
               ,[co_qty]
               ,[co_price]
               ,[co_amount]
               ,[cf_qty]
               ,[cf_price]
               ,[cf_amount]
               ,[wh_conv_qty]
               ,[var_num1]
               ,[var_num2]
               ,[var_num3]
               ,[var_num4]
               ,[var_num5]
               ,[remark1]
               ,[remark2]
               ,[remark3]
               ,[remark4]
               ,[remark5]
               ,[generate_date])
         SELECT
                @comp_id AS comp_id
               ,@dept_20 AS dept
               ,@f_year AS f_year
               ,@f_month AS f_month
               ,A.mat_code AS mat_code
               ,A.mat_name AS mat_name
               ,A.product_item AS product_item
               ,A.product_item2 AS product_item2
               ,NULLIF(A.bf_qty, 0) AS bf_qty
               ,NULLIF(A.bf_price, 0) AS bf_price
               ,NULLIF(A.bf_amount, 0) AS bf_amount
               ,NULLIF(A.rc_qty, 0) AS rc_qty
               ,NULLIF(A.rc_price, 0) AS rc_price
               ,NULLIF(A.rc_amount, 0) AS rc_amount
               ,NULLIF(A.co_qty, 0) AS co_qty
               ,NULLIF(A.co_price, 0) AS co_price
               ,NULLIF(A.co_amount, 0) AS co_amount
               ,NULLIF(A.cf_qty, 0) AS cf_qty
               ,NULLIF(A.cf_price, 0) AS cf_price
               ,NULLIF(A.cf_amount, 0) AS cf_amount
               ,NULLIF(A.wh_conv_qty, 0) AS wh_conv_qty
               ,NULL AS var_num1
               ,NULL AS var_num2
               ,NULL AS var_num3
               ,NULL AS var_num4
               ,NULL AS var_num5
               ,A.res AS remark1
               ,NULL AS remark2
               ,NULL AS remark3
               ,NULL AS remark4
               ,NULL AS remark5
               ,@proc_time
           FROM @_TEMP AS A ORDER BY mat_code, ISNULL(product_item, ''), ISNULL(product_item2, ''), so



    -- Insert tr_slip
    --  Di pending sementara 19 Januari 2021
    -- SET @proc_time=GETDATE()
    -- SET @proc_no=1
    -- SET @rec_sts='A'
    --
    -- SET @proc_no = (SELECT ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_slip
    --                WHERE (f_year = @f_year)
    --                    AND (f_month = @f_month)
    --                    AND (dept = @dept_20)
    --                    AND (jnl_code = 'JV08')
    --                    AND (jnl_detail_no = 10)
    --                    AND (jnl_description = 'SP WIP Adjustment'))
    -- IF @proc_no >= 2
    -- BEGIN
    --    -- Jika sudah ada
    --    -- Jadikan rec_sts = T
    --    SET @rec_sts = 'T'
    -- END
    -- SELECT
    --     @comp_id AS comp_id
    --    ,@f_year AS f_year
    --    ,@f_month AS f_month
    --    ,@dept_20 AS dept
    --    ,'JV08' AS jnl_code
    --    ,10 AS jnl_detail_no
    --    ,'SP WIP Adjustment' AS jnl_description
    --    ,SUM(adj_qty) AS jnl_amount
    --    ,@proc_time AS proc_time
    --    ,@user_id AS user_id
    --    ,@client_ip AS client_ip
    --    ,@rec_sts AS rec_sts
    --    ,@proc_no AS proc_no
    -- FROM @_TEMP AS A
    -- WHERE A.res = 'H'



    SELECT COUNT(*) AS C FROM @_TEMP


    -- SELECT * FROM istem_costing.dbo.tr_wip
    --     WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept_20)
END
