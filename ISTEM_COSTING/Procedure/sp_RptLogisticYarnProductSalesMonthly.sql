
-- =============================================
-- Author:		Apri
-- Create date: 2021-02-10
-- Description:
--				Yarn Product Sales Monthly
--				Report Untuk Logistic
-- =============================================
ALTER PROCEDURE  [sp_RptLogisticYarnProductSalesMonthly]
	@COMP_ID INT, @F_YEAR INT, @F_MONTH INT
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @LP_f_year INT, @LP_f_month INT

    IF @F_Month = 1
    BEGIN
    	SET @LP_f_month = 12
    	SET @LP_f_year = @F_Year - 1
    END
    ELSE
    BEGIN
    	SET @LP_f_month = @F_Month - 1
    	SET @LP_f_year = @F_Year
    END

    DECLARE @dept INT

    SET @dept = (SELECT TOP 1 dept FROM ms_dept WHERE comp_id=@COMP_ID AND dept_seq=10)

    SELECT
    	MT.mat_code  AS MATERIAL_CODE
    	,ISNULL(M.FrgnName, '') AS MATERIAL_NAME

    	,NULLIF(BF.BF, 0) AS BF -- A
    	,NULLIF(RC.RC, 0) AS RC -- D

    	,NULLIF(CWV.CWV, 0) AS CWV -- G
    	,NULLIF(CSALES.CSALES, 0) AS CSALES -- I
    	,NULLIF(CDYING.CDYING, 0) AS CDYING -- I
    	,NULLIF(COTHER.COTHER, 0) AS COTHER -- J
    	,NULLIF((ISNULL(CWV.CWV, 0)+ISNULL(CSALES.CSALES, 0)+ISNULL(CDYING.CDYING, 0)+ISNULL(COTHER.COTHER, 0)), 0) AS C_TOTAL

    	-- (A+D) - (G+J)
    	,NULLIF((ISNULL(BF.BF, 0) + ISNULL(RC.RC, 0)) - (ISNULL(CWV.CWV, 0)+ISNULL(CSALES.CSALES, 0)+ISNULL(CDYING.CDYING, 0)+ISNULL(COTHER.COTHER, 0)), 0) AS CF
    FROM
    (
    	SELECT * FROM (
    		SELECT mat_code FROM tr_bal_mat WHERE (comp_id = @COMP_ID) AND (f_year = @LP_f_year) AND (f_month = @LP_f_month) AND (dept = @dept) AND (cost_sheet_id IN ('A25')) GROUP BY mat_code, mat_usage
    		UNION ALL
    		SELECT mat_code FROM tr_inv_in WHERE (comp_id = @COMP_ID) AND (f_year = @F_YEAR) AND (f_month = @F_MONTH) AND (dept = @dept) AND (cost_sheet_id IN ('A25')) GROUP BY mat_code, mat_usage
    		UNION ALL
    		SELECT mat_code FROM tr_inv_out_detail WHERE (comp_id = @COMP_ID) AND (f_year = @F_YEAR) AND (f_month = @F_MONTH) AND (dept = @dept) AND (cost_sheet_id IN ('A25')) GROUP BY mat_code, mat_usage
    	) AS A GROUP BY mat_code
    ) AS MT
    INNER JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM AS M ON (M.ItemCode COLLATE SQL_Latin1_General_CP1_CI_AS = MT.mat_code COLLATE SQL_Latin1_General_CP1_CI_AS)

    -- BF
    LEFT JOIN (
    	SELECT mat_code, SUM(ISNULL(cf_qty, 0)) AS BF FROM tr_bal_mat WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@LP_f_year)AND(f_month=@LP_f_month)AND(cost_sheet_id IN ('A25')) GROUP BY mat_code
    ) AS BF ON (BF.mat_code=MT.mat_code)


    LEFT JOIN (
    	SELECT mat_code, SUM(ISNULL(in_qty, 0)) AS RC FROM tr_inv_in WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@F_YEAR)AND(f_month=@F_MONTH)AND(cost_sheet_id IN ('A25')) GROUP BY mat_code
    ) AS RC ON (RC.mat_code=MT.mat_code)

    -- Consume
    LEFT JOIN (
    	SELECT mat_code, SUM(ISNULL(out_qty, 0)) AS CWV FROM tr_inv_out_detail WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@F_YEAR)AND(f_month=@F_MONTH)AND(cost_sheet_id IN ('A25'))AND(out_dest=800) GROUP BY mat_code
    ) AS CWV ON (CWV.mat_code=MT.mat_code)
    LEFT JOIN (
    	SELECT mat_code, SUM(ISNULL(out_qty, 0)) AS CSALES FROM tr_inv_out_detail WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@F_YEAR)AND(f_month=@F_MONTH)AND(cost_sheet_id IN ('A25'))AND(out_dest=300) GROUP BY mat_code
    ) AS CSALES ON (CSALES.mat_code=MT.mat_code)
    LEFT JOIN (
    	SELECT mat_code, SUM(ISNULL(out_qty, 0)) AS CDYING FROM tr_inv_out_detail WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@F_YEAR)AND(f_month=@F_MONTH)AND(cost_sheet_id IN ('A25'))AND(out_dest=900) GROUP BY mat_code
    ) AS CDYING ON (CDYING.mat_code=MT.mat_code)
    LEFT JOIN (
    	SELECT mat_code, SUM(ISNULL(out_qty, 0)) AS COTHER FROM tr_inv_out_detail WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@F_YEAR)AND(f_month=@F_MONTH)AND(cost_sheet_id IN ('A25'))AND(out_dest NOT IN (800, 300, 900)) GROUP BY mat_code
    ) AS COTHER ON (COTHER.mat_code=MT.mat_code)
    ORDER BY M.ItmsGrpCod, MT.mat_code

END
