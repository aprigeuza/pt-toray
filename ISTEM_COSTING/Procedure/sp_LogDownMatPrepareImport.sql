
-- =============================================
-- Prepare Import
-- Prepare Import Untuk Logistic Download Material
-- =============================================
ALTER PROCEDURE  [sp_LogDownMatPrepareImport]
    @CompID INT, @F_YEAR INT, @F_MONTH INT
AS
BEGIN
SET NOCOUNT ON;

    DECLARE @dept AS INT, @except_mat_usage CHAR(1)
    SELECT @dept = dept FROM ms_dept WHERE (comp_id = @CompID) AND (dept_seq = 10)
    SET @except_mat_usage = 'W'
    DECLARE @tr_sap_purchase_proc_no INT, @tr_inv_in_proc_no INT, @tr_inv_out_header_proc_no INT

    IF @CompID=1
    BEGIN

        INSERT INTO [tr_sap_purchase_hist]
        --([comp_id], [f_year], [f_month], [cost_sheet_id], [doc_type], [doc_date], [gr_rtv_no], [pi_gr_no], [supplier_name], [item_code], [item_name], [item_grp_cod], [frgn_name], [unit], [curr], [qty_rec], [price], [trans_amount], [landed_cost], [bale_qty], [nett_qty], [proc_time], [user_id], [client_ip], [rec_sts], [proc_no])
        SELECT * FROM [tr_sap_purchase] WHERE (comp_id = @CompID) AND (f_year=@F_YEAR) AND (f_month=@F_MONTH) AND (cost_sheet_id IN ('A10','A11','A12','A13','A21','A31'))

        INSERT INTO  [tr_inv_in_hist]
        --(comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, mat_usage, in_bale_qty, in_qty, in_amount, in_invoice_qty, qty_unit, proc_time, user_id, rec_sts, proc_no, client_ip)
        SELECT * FROM  [tr_inv_in] WHERE (comp_id = @CompID) AND (f_year = @F_YEAR) AND (f_month = @F_MONTH) AND (dept=@dept) AND (mat_usage <> @except_mat_usage)  AND (cost_sheet_id IN ('A10','A11','A12','A13','A21','A31', 'A20'))

        INSERT INTO [tr_inv_out_header_hist]
        --([comp_id], [f_year], [f_month], [dept], [cost_sheet_id], [mat_code], [proc_time], [user_id], [client_ip], [rec_sts], [proc_no])
        SELECT * FROM [tr_inv_out_header] WHERE (comp_id = @CompID) AND (f_year=@F_YEAR) AND (f_month=@F_MONTH) AND (dept=@dept)  AND (cost_sheet_id IN ('A10','A11','A12','A13','A20','A21','A31'))

        INSERT INTO [tr_inv_out_detail_hist]
        --([comp_id], [f_year], [f_month], [dept], [cost_sheet_id], [mat_code], [mat_usage], [detail_no], [out_dest], [out_bale_qty], [out_qty], [out_invoice_qty], [out_amount], [qty_unit], [rec_sts], [proc_no])
        SELECT * FROM [tr_inv_out_detail] WHERE (comp_id = @CompID) AND (f_year=@F_YEAR) AND (f_month=@F_MONTH) AND (dept=@dept) AND (mat_usage <> @except_mat_usage) AND (cost_sheet_id IN ('A10','A11','A12','A13','A20','A21','A31'))

        SELECT @tr_sap_purchase_proc_no = MAX(proc_no) FROM [tr_sap_purchase] WHERE (f_year = @F_YEAR) AND (f_month = @F_MONTH) AND (comp_id = @CompID) AND (cost_sheet_id IN ('A10','A11','A12','A13','A21','A31'))
        SELECT @tr_inv_in_proc_no = MAX(proc_no) FROM [tr_inv_in] WHERE (f_year = @F_YEAR) AND (f_month = @F_MONTH) AND (comp_id = @CompID) AND (dept=@dept) AND (mat_usage <> @except_mat_usage) AND (cost_sheet_id IN ('A10','A11','A12','A13','A21','A31', 'A20'))
        SELECT @tr_inv_out_header_proc_no = MAX(proc_no) FROM [tr_inv_out_header] WHERE (f_year = @F_YEAR) AND (f_month = @F_MONTH) AND (comp_id = @CompID) AND (dept=@dept) AND (cost_sheet_id IN ('A10','A11','A12','A13','A20','A21','A31'))

        DELETE FROM [tr_sap_purchase] WHERE (comp_id = @CompID) AND (f_year=@F_YEAR) AND (f_month=@F_MONTH) AND (cost_sheet_id IN ('A10','A11','A12','A13','A21','A31'))
        DELETE FROM [tr_inv_in] WHERE (comp_id = @CompID) AND (f_year=@F_YEAR) AND (f_month=@F_MONTH) AND (dept=@dept) AND (mat_usage <> @except_mat_usage) AND (cost_sheet_id IN ('A10','A11','A12','A13','A21','A31', 'A20'))
        DELETE FROM [tr_inv_out_header] WHERE (comp_id = @CompID) AND (f_year=@F_YEAR) AND (f_month=@F_MONTH) AND (dept=@dept) AND (cost_sheet_id IN ('A10','A11','A12','A13','A20','A21','A31'))
        DELETE FROM [tr_inv_out_detail] WHERE (comp_id = @CompID) AND (f_year=@F_YEAR) AND (f_month=@F_MONTH) AND (dept=@dept) AND (mat_usage <> @except_mat_usage) AND (cost_sheet_id IN ('A10','A11','A12','A13','A20','A21','A31'))

    END

    IF @CompID=2
    BEGIN
        INSERT INTO [tr_sap_purchase_hist]
        --([comp_id], [f_year], [f_month], [cost_sheet_id], [doc_type], [doc_date], [gr_rtv_no], [pi_gr_no], [supplier_name], [item_code], [item_name], [item_grp_cod], [frgn_name], [unit], [curr], [qty_rec], [price], [trans_amount], [landed_cost], [bale_qty], [nett_qty], [proc_time], [user_id], [client_ip], [rec_sts], [proc_no])
        SELECT * FROM [tr_sap_purchase] WHERE (comp_id = @CompID) AND (f_year=@F_YEAR) AND (f_month=@F_MONTH) AND (cost_sheet_id IN ('A10','A11','A12','A13','A21','A31'))
        SELECT @tr_sap_purchase_proc_no = MAX(proc_no) FROM [tr_sap_purchase] WHERE (f_year = @F_YEAR) AND (f_month = @F_MONTH) AND (comp_id = @CompID) AND (cost_sheet_id IN ('A10','A11','A12','A13','A21','A31'))
        DELETE FROM [tr_sap_purchase] WHERE (comp_id = @CompID) AND (f_year=@F_YEAR) AND (f_month=@F_MONTH) AND (cost_sheet_id IN ('A10','A11','A12','A13','A21','A31'))
    END

    SELECT
        ISNULL(@tr_sap_purchase_proc_no, 0) AS tr_sap_purchase_proc_no,
        ISNULL(@tr_inv_in_proc_no, 0) AS tr_inv_in_proc_no,
        ISNULL(@tr_inv_out_header_proc_no, 0) AS tr_inv_out_header_proc_no

END
