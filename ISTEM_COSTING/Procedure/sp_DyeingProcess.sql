ALTER PROCEDURE  sp_DyeingProcess
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;
    PRINT 'EXEC dbo.sp_DyeingProcessPrepare';
    EXEC dbo.sp_DyeingProcessPrepare @comp_id, @f_year, @f_month, @user_id, @client_ip;

    PRINT 'EXEC dbo.sp_DyeingProcessGreyDelivery';
    EXEC dbo.sp_DyeingProcessGreyDelivery @comp_id, @f_year, @f_month, @user_id, @client_ip;

    PRINT 'EXEC dbo.sp_DyeingProcessGreyCostLogtGreyPurc';
    EXEC dbo.sp_DyeingProcessGreyCostLogtGreyPurc @comp_id, @f_year, @f_month, @user_id, @client_ip;

    PRINT 'EXEC dbo.sp_DyeingProcessGreyCostLogtWeavingGreyBal';
    EXEC dbo.sp_DyeingProcessGreyCostLogtWeavingGreyBal @comp_id, @f_year, @f_month, @user_id, @client_ip;

    PRINT 'EXEC dbo.sp_DyeingProcessWIPDownload';
    EXEC dbo.sp_DyeingProcessWIPDownload @comp_id, @f_year, @f_month, @user_id, @client_ip;

    PRINT 'EXEC dbo.sp_DyeingProcessWIPCalc';
    EXEC dbo.sp_DyeingProcessWIPCalc @comp_id, @f_year, @f_month, @user_id, @client_ip;

    PRINT 'EXEC dbo.sp_DyeingProcessDownloadFGWH';
    EXEC dbo.sp_DyeingProcessDownloadFGWH @comp_id, @f_year, @f_month, @user_id, @client_ip;

    PRINT 'EXEC dbo.sp_DyeingProcessDownloadFGDelivery';
    EXEC dbo.sp_DyeingProcessDownloadFGDelivery @comp_id, @f_year, @f_month, @user_id, @client_ip;

    PRINT 'EXEC dbo.sp_DyeingProdWIPEquivalent';
    EXEC dbo.sp_DyeingProdWIPEquivalent @comp_id, @f_year, @f_month, @user_id, @client_ip;

    PRINT 'EXEC dbo.sp_DyeingGenerateBalWIP';
    EXEC dbo.sp_DyeingGenerateBalWIP @comp_id, @f_year, @f_month, @user_id, @client_ip;
END
