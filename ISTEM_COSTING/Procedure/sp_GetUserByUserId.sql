

-- =============================================
-- Get User
-- Get User By User ID
-- =============================================
ALTER PROCEDURE  [sp_GetUserByUserId]
	@user_id varchar(6)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	DECLARE @_TEMP TABLE(
		[comp_id] [numeric](1, 0) NOT NULL,
		[dept] [numeric](3, 0) NOT NULL,
		[user_id] [varchar](6) NOT NULL,
		[user_name] [varchar](25) NOT NULL,
		[password] [varchar](8) NOT NULL,
		[proc_time] [datetime] NULL,
		[entry_by] [varchar](6) NULL,
		[rec_sts] [char](1) NULL,
		[proc_no] [tinyint] NULL,
		[act_rec] [char](1) NULL,
		[client_ip] [varchar](15) NULL)

	INSERT INTO @_TEMP
	SELECT * FROM ms_user WHERE (user_id=@user_id)

	SELECT * FROM @_TEMP
END



