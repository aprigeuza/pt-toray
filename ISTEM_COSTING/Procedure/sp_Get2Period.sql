ALTER PROCEDURE  sp_Get2Period
	@comp_id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE @CurrentPeriod Varchar(30), @LastPeriod Varchar(30)

	DECLARE @Current_F_Year INT, @Current_F_Month INT
	DECLARE @Last_F_Year INT, @Last_F_Month INT

	SELECT
		@Current_F_Year = f_year
		,@Current_F_Month = f_month
		,@CurrentPeriod = txt_period
	FROM istem_costing.dbo.v_SAPPeriod
	WHERE
		comp_id=@comp_id
		AND period_status = 'N'
	ORDER BY f_year, f_month DESC

	IF @Current_F_Month = 1
	BEGIN
		SET @Last_F_Year = @Current_F_Year - 1
		SET @Last_F_Month = 12
	END
	ELSE
	BEGIN
		SET @Last_F_Year = @Current_F_Year
		SET @Last_F_Month = @Current_F_Month - 1
	END


	SET @CurrentPeriod = CAST(@Current_F_Year AS Varchar) + '-' + RIGHT('0' + CAST(@Current_F_Month AS VARCHAR(2)), 2)
	SET @LastPeriod = CAST(@Last_F_Year AS Varchar) + '-' + RIGHT('0' + CAST(@Last_F_Month AS VARCHAR(2)), 2)

	PRINT @CurrentPeriod
	PRINT @LastPeriod


	SELECT *
	FROM istem_costing.dbo.v_SAPPeriod
	WHERE
	comp_id=@comp_id
	AND ((f_year = @Current_F_Year AND f_month = @Current_F_Month) OR (f_year = @Last_F_Year AND f_month = @Last_F_Month))
END
