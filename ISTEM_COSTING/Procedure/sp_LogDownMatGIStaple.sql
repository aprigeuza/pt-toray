
-- =============================================
-- SUB 2
-- Delivery Staple to Spinning/other Dept
-- =============================================
ALTER PROCEDURE  [sp_LogDownMatGIStaple]
	@CompID INT, @F_YEAR INT, @F_MONTH INT, @LoginID VARCHAR(20), @ClientIP VARCHAR(24), @ProcNo INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @FromDate DATE, @ToDate DATE, @RecSts CHAR(1), @dept AS INT

	SELECT TOP 1 @FromDate = F_RefDate, @ToDate = T_RefDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)

	SELECT @dept = dept FROM ms_dept WHERE (comp_id = @CompID) AND (dept_seq = 10)

	IF @ProcNo >=  1
	BEGIN
		SET @ProcNo = @ProcNo + 1
		SET @RecSts = 'T'
	END
	ELSE
	BEGIN
		SET @ProcNo = 1
		SET @RecSts = 'A'
	END

	DECLARE @_TEMP TABLE (
		[comp_id] [numeric](1, 0) NOT NULL,
		[f_year] [numeric](4, 0) NOT NULL,
		[f_month] [numeric](2, 0) NOT NULL,
		[dept] [numeric](3, 0) NOT NULL,
		[cost_sheet_id] [varchar](4) NOT NULL,
		[mat_code] [varchar](45) NOT NULL,
		[mat_usage] [char](1) NOT NULL,
		[out_dest] [numeric](3, 0) NOT NULL,
		[out_bale_qty] [numeric](5, 0) NULL,
		[out_qty] [numeric](14, 2) NULL,
		[out_invoice_qty] [numeric](14, 2) NULL,
		[out_amount] [numeric](14, 2) NULL,
		[qty_unit] [varchar](5) NULL,
		[rec_sts] [char](1) NULL,
		[proc_no] [tinyint] NULL,
	    [out_qty_pcs] numeric(9,2) NULL)
	INSERT INTO @_TEMP
	SELECT
		  @CompID AS comp_id
		  ,@F_YEAR AS f_year
		  ,@F_MONTH AS f_month
		  ,@dept AS dept
		  ,A.Cost_ID AS cost_sheet_id
		  ,A.ItemCode AS mat_code
		  ,ISNULL(A.U_Yarn_Usage, '') AS mat_usage
		  ,A.out_dest AS out_dest
		  ,SUM(A.out_bale_Qty) AS out_bale_qty
		  ,CASE WHEN A.Cost_ID = 'A11' THEN SUM(A.out_nett_Qty) ELSE SUM(A.out_qty) END AS out_qty
		  ,SUM(A.out_qty) AS out_invoice_qty
		  ,0 AS out_amount
		  ,'LBS' AS qty_unit
		  ,@RecSts AS rec_sts
		  ,@ProcNo AS proc_no
		  ,CASE WHEN A.Cost_ID = 'A31' THEN SUM(A.out_bale_Qty) ELSE NULL END AS out_qty_pcs
		FROM (
		  SELECT
			T0.docdate,
			T1.ItemCode,
			T2.itmsgrpcod,
			T1.Dscription,
			T2.FrgnName,
			T1.Quantity AS out_qty,
			T1.OcrCode AS out_dest,
			ISNULL(T1.U_Bale_Qty,0) AS out_bale_Qty,
			ISNULL(T1.U_Nett_Qty,0) AS out_nett_Qty,
			T3.U_Cost_ID as Cost_ID,
			T2.BuyUnitMsr AS Unit,
			T1.U_Yarn_Usage
		  FROM SAP_ISM.SBO_ISM_LIVE.DBO.OIGE T0
		  LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.IGE1 T1 on T0.docentry=T1.DocEntry
		  LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.oitm T2 on T1.itemcode=T2.itemcode
		  LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.oitb T3 on T2.ItmsGrpCod=T3.ItmsGrpCod
		  WHERE
			(T2.ItmsGrpCod BETWEEN 106 AND 108)
			AND
			(ISNULL(T0.U_GI_Rea, '') <> 'ADJ')

		  UNION ALL

		  SELECT
			T0.docdate,
			T1.ItemCode,
			T2.itmsgrpcod,
			T1.Dscription,
			T2.FrgnName,
			T1.Quantity*-1 AS out_qty,
			T1.OcrCode AS out_dest,
			ISNULL(T1.U_Bale_Qty,0)*-1 AS out_bale_Qty,
			ISNULL(T1.U_Nett_Qty,0)*-1 AS out_nett_Qty,
			T3.U_Cost_ID AS Cost_ID,
			T2.BuyUnitMsr AS Unit,
			T1.U_Yarn_Usage
		  FROM SAP_ISM.SBO_ISM_LIVE.DBO.OIGN T0
		  LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.IGN1 T1 on T0.docentry=T1.DocEntry
		  LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.oitm T2 on T1.itemcode=T2.itemcode
		  LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.oitb T3 on T2.ItmsGrpCod=T3.ItmsGrpCod
		  WHERE
			(T2.ItmsGrpCod BETWEEN 106 AND 108)
			AND (T1.OcrCode<>700)
			AND (ISNULL(T0.U_GR_Rea, '')='ADJ')
		) A
		WHERE
		  (A.DocDate>= @FromDate AND A.DocDate<= @ToDate)
		GROUP BY
		  A.Cost_ID
		  ,A.ItemCode
		  ,A.U_Yarn_Usage
		  ,A.out_dest
		  ,A.Unit
		ORDER BY
		  A.Cost_ID
		  ,A.ItemCode
		  ,A.U_Yarn_Usage
		  ,A.out_dest
		  ,A.Unit

    -- Update tanggal 2 oktober 2020
    -- karena ada masalah di tr_inv_in yang juga di pakai di weaving wip process
    UPDATE @_TEMP SET mat_usage='S' WHERE cost_sheet_id='A31'

	-- INSERT Detail
	 INSERT INTO [istem_costing].[dbo].[tr_inv_out_detail]
				(comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, mat_usage, out_dest, out_bale_qty, out_qty, out_invoice_qty, out_amount, qty_unit, rec_sts, proc_no, out_qty_pcs)
				SELECT comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, mat_usage, out_dest, out_bale_qty, out_qty, out_invoice_qty, out_amount, qty_unit, rec_sts, proc_no, out_qty_pcs FROM @_TEMP

	-- Insert ke header
	INSERT INTO [tr_inv_out_header]
		  (comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, proc_time, user_id, client_ip, rec_sts, proc_no)
		  SELECT @CompID, @F_YEAR, @F_MONTH, @dept, cost_sheet_id, mat_code, GETDATE() AS proc_time, @LoginID, @ClientIP, @RecSts, @ProcNo FROM @_TEMP GROUP BY comp_id, f_year, f_month, cost_sheet_id, mat_code

	-- SELECT COUNT(*) AS NumRows FROM @_TEMP

	DECLARE @res INT
	SELECT @res = COUNT(*) FROM @_TEMP;
	RETURN @res;
END
