
-- =============================================
-- SUB 5
-- Data Consume/Delivery
-- =============================================
ALTER PROCEDURE  [sp_LogDownMatConsumeDelivery]
	@CompID INT, @F_YEAR INT, @F_MONTH INT, @LoginID VARCHAR(20), @ClientIP VARCHAR(24), @ProcNo INT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @FromDate DATE, @ToDate DATE, @RecSts CHAR(1), @dept AS INT

	SELECT TOP 1 @FromDate = F_RefDate, @ToDate = T_RefDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)
	SELECT @dept = dept FROM ms_dept WHERE (comp_id = @CompID) AND (dept_seq = 10)
	IF @ProcNo >=  1
		BEGIN
			SET @ProcNo = @ProcNo + 1
			SET @RecSts = 'T'
		END
		ELSE
		BEGIN
			SET @ProcNo = 1
			SET @RecSts = 'A'
		END


	DECLARE @_TEMP TABLE(
		[comp_id] [numeric](1, 0) NOT NULL,
		[f_year] [numeric](4, 0) NOT NULL,
		[f_month] [numeric](2, 0) NOT NULL,
		[dept] [numeric](3, 0) NOT NULL,
		[cost_sheet_id] [varchar](4) NOT NULL,
		[mat_code] [varchar](45) NOT NULL,
		[mat_usage] [char](1) NOT NULL,
		[out_dest] [numeric](3, 0) NOT NULL,
		[out_bale_qty] [numeric](5, 0) NULL,
		[out_qty] [numeric](14, 2) NULL,
		[out_invoice_qty] [numeric](14, 2) NULL,
		[out_amount] [numeric](14, 2) NULL,
		[qty_unit] [varchar](5) NULL,
		[rec_sts] [char](1) NULL,
		[proc_no] [tinyint] NULL)

	INSERT INTO @_TEMP
		SELECT
			@CompID AS comp_id
			,@F_YEAR AS f_year
			,@F_MONTH AS f_month
			,@dept AS dept
			,'A31' AS cost_sheet_id
			,A.ItemCode AS mat_code
			,ISNULL(A.mat_usage, '') AS mat_usage
			,CASE WHEN U_ypur_wvname <> '' THEN A.out_dest ELSE 999 END AS out_dest
			,0 AS out_bale_qty
			,SUM(A.out_qty) AS out_qty
			,SUM(A.out_qty) AS out_invoice_qty
			,0 AS out_amount
			,'MTR' AS qty_unit
			,@RecSts AS rec_sts
			,@ProcNo AS proc_no
		FROM(
			SELECT
				 T0.docdate
				,T1.ItemCode
				,T2.itmsgrpcod
				,T1.Dscription
				,T2.FrgnName
				,T1.Quantity AS out_qty
				,T1.OcrCode AS out_dest
				,ISNULL(T1.U_Bale_Qty,0) AS out_bale_Qty
				,ISNULL(T1.U_Nett_Qty,0) AS out_nett_Qty
				,T1.u_yarn_usage AS mat_usage
				,ISNULL(T2.U_ypur_wvname, '') AS U_ypur_wvname
			FROM SAP_ISM.SBO_ISM_LIVE.DBO.OIGE T0
			LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.IGE1 T1 on T0.docentry=T1.DocEntry
			LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM T2 on T1.itemcode=T2.itemcode
			WHERE
			(T2.ItmsGrpCod=111 AND ISNULL(U_GR_Rea,'') <> 'ADJ' )


			UNION ALL

			SELECT
				 T0.docdate
				,T1.ItemCode
				,T2.itmsgrpcod
				,T1.Dscription
				,T2.FrgnName
				,T1.Quantity*-1 AS out_qty,T1.OcrCode AS out_dest
				,ISNULL(T1.U_Bale_Qty,0)*-1 AS out_bale_Qty
				,ISNULL(T1.U_Nett_Qty,0)*-1 AS out_nett_Qty
				,T1.u_yarn_usage as mat_usage
				,ISNULL(T2.U_ypur_wvname, '') AS U_ypur_wvname
			FROM SAP_ISM.SBO_ISM_LIVE.DBO.OIGN T0
			LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.IGN1 T1 on T0.docentry=T1.DocEntry
			LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM T2 on T1.itemcode=T2.itemcode
			WHERE
			  (T2.ItmsGrpCod =111)
			  AND
			  (T1.OcrCode=800 AND ISNULL(U_GR_Rea,'') = 'ADJ')
		) A
		WHERE
		(A.DocDate>=@FromDate AND A.DocDate<=@ToDate)
		AND
		(A.ItmsGrpCod = 111)
		GROUP BY
		A.ItemCode,
		A.mat_usage,
		A.U_ypur_wvname,
		A.out_dest
		ORDER BY
		A.ItemCode,
		A.mat_usage,
		A.U_ypur_wvname,
		A.out_dest

    -- Update tanggal 2 oktober 2020
    -- karena ada masalah di tr_inv_in yang juga di pakai di weaving wip process
    UPDATE @_TEMP SET mat_usage='S' WHERE cost_sheet_id='A31'

	-- INSERT Detail
	 INSERT INTO [istem_costing].[dbo].[tr_inv_out_detail]
				(comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, mat_usage, out_dest, out_bale_qty, out_qty, out_invoice_qty, out_amount, qty_unit, rec_sts, proc_no)
				SELECT comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, mat_usage, out_dest, out_bale_qty, out_qty, out_invoice_qty, out_amount, qty_unit, rec_sts, proc_no FROM @_TEMP

	-- Insert ke header
	INSERT INTO [tr_inv_out_header]
		  (comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, proc_time, user_id, client_ip, rec_sts, proc_no)
		  SELECT @CompID, @F_YEAR, @F_MONTH, @dept, cost_sheet_id, mat_code, GETDATE() AS proc_time, @LoginID, @ClientIP, @RecSts, @ProcNo FROM @_TEMP GROUP BY comp_id, f_year, f_month, cost_sheet_id, mat_code

	-- SELECT COUNT(*) AS NumRows FROM @_TEMP
	
	
	DECLARE @res INT
	SELECT @res = COUNT(*) FROM @_TEMP;
	RETURN @res;
	
END


