ALTER PROCEDURE  [sp_AcctCalcPropCostDept]
    @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    PRINT 'SUB2 : sp_AcctCalcPropCostDept'

    -- SUB2	Proportional Cost of each Dept
    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=7
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept_10 INT
    DECLARE @dept_20 INT
    DECLARE @dept_30 INT
    DECLARE @dept_40 INT
    DECLARE @dept_61 INT
    DECLARE @proc_time AS DATETIME
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_no INT
    DECLARE @tr_code VARCHAR(MAX)

    SET @dept_10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=10))
    SET @dept_20=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=20))
    SET @dept_30=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=30))
    SET @dept_40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40))
    SET @dept_61=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=61))
    SET @proc_time = GETDATE()
    SET @rec_sts='A'
    SET @proc_no=1
    SET @tr_code='PCD'

    -- PERIOD
    DECLARE @from_date DATE, @to_date DATE
    SELECT TOP 1 @from_date = F_RefDate, @to_date = T_RefDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)

    DECLARE @_TR_MANEX TABLE (
    	[comp_id] [numeric](1, 0) NOT NULL,
    	[f_year] [numeric](4, 0) NOT NULL,
    	[f_month] [numeric](2, 0) NOT NULL,
    	[tr_code] [varchar](5) NOT NULL,
    	[dept] [numeric](3, 0) NOT NULL,
    	[ori_dept] [numeric](3, 0) NOT NULL,
    	[proc_code] [varchar](7) NOT NULL,
    	[cost_sheet_id] [varchar](4) NOT NULL,
    	[manex_amount] [numeric](14, 2) NULL,
    	[proc_time] [datetime] NULL,
    	[user_id] [varchar](6) NULL,
    	[client_ip] [varchar](15) NULL,
    	[rec_sts] [char](1) NULL,
    	[proc_no] [tinyint] NULL
    )

    DECLARE @sum_of_manex_amount_dept_505 DECIMAL(16,5)
    SET @sum_of_manex_amount_dept_505 = (SELECT
        SUM(manex_amount) AS manex_amount
    FROM istem_costing.dbo.tr_manex
    WHERE
        comp_id=@comp_id
        AND f_year=@f_year
        AND f_month=@f_month
        AND dept IN (505)
        AND tr_code='AUPC'
    GROUP BY dept)


    INSERT INTO @_TR_MANEX
    (
        comp_id
        ,f_year
        ,f_month
        ,tr_code
        ,dept
        ,ori_dept
        ,proc_code
        ,cost_sheet_id
        ,manex_amount
    )
    SELECT
         @comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,A.tr_code AS tr_code
        ,A.dept AS dept
        ,A.v_ori_dept AS ori_dept
        ,B.proc_code AS proc_code
        ,A.cost_sheet_id AS cost_sheet_id
        ,A.v_prop_amt AS manex_amount
    FROM (
        SELECT
            'C05' AS cost_sheet_id
            ,SUM(A.expense_amount) + @sum_of_manex_amount_dept_505 AS v_prop_amt
            ,900 AS dept
            ,505 AS v_ori_dept
            ,@tr_code AS tr_code
        FROM istem_costing.dbo.tr_sap_sme AS A
        WHERE
            A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (505)
            AND A.acct_code LIKE '520.1%'
        GROUP BY A.dept

        UNION ALL

        SELECT
             A.cost_sheet_id AS cost_sheet_id
            ,SUM(A.expense_amount) AS v_prop_amt
            ,800 AS dept
            ,A.dept AS v_ori_dept
            ,@tr_code AS tr_code
        FROM istem_costing.dbo.tr_sap_sme AS A
        WHERE
            A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (800)
            AND A.acct_code LIKE '520.1%'
        GROUP BY A.cost_sheet_id, A.dept

        UNION ALL

        SELECT
             A.cost_sheet_id AS cost_sheet_id
            ,SUM(A.expense_amount) AS v_prop_amt
            ,900 AS dept
            ,A.dept AS v_ori_dept
            ,@tr_code AS tr_code
        FROM istem_costing.dbo.tr_sap_sme AS A
        WHERE
            A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (900)
            AND A.acct_code LIKE '520.1%'
        GROUP BY A.cost_sheet_id, A.dept
    ) AS A
    INNER JOIN (
        SELECT * FROM istem_costing.dbo.ms_process WHERE comp_id=@comp_id
    ) AS B ON (B.tr_code=A.tr_code)

    DECLARE @c INT
    SET @c = (SELECT COUNT(*) FROM istem_costing.dbo.tr_manex_hist AS A WHERE A.comp_id=@comp_id AND A.f_year=@f_year AND A.f_month=@f_month)
    IF @c >= 1
    BEGIN
        SET @proc_no=(SELECT ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_manex_hist WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month);
        SET @rec_sts='T';
    END

    INSERT INTO istem_costing.dbo.tr_manex (comp_id, f_year, f_month, tr_code, dept, ori_dept, proc_code, cost_sheet_id, manex_amount, proc_time, user_id, client_ip, rec_sts, proc_no)
        SELECT comp_id, f_year, f_month, tr_code, dept, ori_dept, proc_code, cost_sheet_id, manex_amount, @proc_time, @user_id, @client_ip, @rec_sts, @proc_no FROM @_TR_MANEX;

END
