

-- =============================================
-- Get Departement
-- Get Departement By CompId & Dept_Seq = 1
-- =============================================
ALTER PROCEDURE  [sp_GetDept]
(
	@comp_id numeric(1, 0)
)
AS
	SET NOCOUNT ON;
SELECT dept FROM ms_dept
WHERE
comp_id = @comp_id
AND
dept_seq = 1


