

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE  [sp_rpt_monthlyReportStapleBasedOnInvoice]
AS
BEGIN
	SET NOCOUNT ON;

	SELECT
		T1.item_code AS MATERIAL_CODE
		,T1.item_name AS MATERIAL_NAME
		,T1.comp_id
		,T1.f_year
		,T1.f_month

		--  BF
		,SUM(T2.cf_invoice_qty) AS BF_QTY
		,SUM(T2.cf_amount) AS BF_PRICE
		,(SUM(T2.cf_amount)/SUM(T2.cf_invoice_qty)) AS BF_AMOUNT

		-- Receive
		,SUM(T3.in_invoice_qty) AS RC_QTY
		,SUM(T3.in_amount) AS RC_PRICE
		,(SUM(T3.in_amount)/SUM(T3.in_invoice_qty)) AS RC_AMOUNT

		-- Consume
		,SUM(T4.out_invoice_qty) AS CO_QTY
		,((SUM(T2.cf_amount)/SUM(T2.cf_invoice_qty)) + SUM(T3.in_amount)/SUM(T3.in_invoice_qty)) / (SUM(T2.cf_invoice_qty) + SUM(T3.in_invoice_qty)) AS CO_PRICE
		,SUM(T4.out_invoice_qty) * ((SUM(T2.cf_amount)/SUM(T2.cf_invoice_qty)) + SUM(T3.in_amount)/SUM(T3.in_invoice_qty)) / (SUM(T2.cf_invoice_qty) + SUM(T3.in_invoice_qty)) AS CO_AMOUNT

		-- Other
		,SUM(T5.out_invoice_qty) AS CO_OT_QTY
		,((SUM(T2.cf_amount)/SUM(T2.cf_invoice_qty)) + SUM(T3.in_amount)/SUM(T3.in_invoice_qty)) / (SUM(T2.cf_invoice_qty) + SUM(T3.in_invoice_qty)) AS CO_OT_PRICE
		,(SUM(T5.out_invoice_qty) * ((SUM(T2.cf_amount)/SUM(T2.cf_invoice_qty)) + SUM(T3.in_amount)/SUM(T3.in_invoice_qty)) / (SUM(T2.cf_invoice_qty) + SUM(T3.in_invoice_qty)) ) AS CO_OT_AMOUNT

		-- CF
		,((SUM(T2.cf_invoice_qty) + SUM(T3.in_invoice_qty)) - (SUM(T4.out_invoice_qty) + SUM(T5.out_invoice_qty))) AS CF_QTY
		,((SUM(T2.cf_amount)/SUM(T2.cf_invoice_qty)) + (SUM(T3.in_amount)/SUM(T3.in_invoice_qty)) / (SUM(T2.cf_invoice_qty) + SUM(T3.in_invoice_qty))) AS CF_PRICE
		,((SUM(T2.cf_invoice_qty) + SUM(T3.in_invoice_qty)) - (SUM(T4.out_invoice_qty) + SUM(T5.out_invoice_qty))) * ((SUM(T2.cf_amount)/SUM(T2.cf_invoice_qty)) + (SUM(T3.in_amount)/SUM(T3.in_invoice_qty)) / (SUM(T2.cf_invoice_qty) + SUM(T3.in_invoice_qty))) CF_AMOUNT
	FROM dbo.tr_sap_purchase AS T1
	LEFT JOIN dbo.tr_bal_mat T2 ON
	(T2.mat_code = T1.item_code AND T2.dept = 400 AND T2.cost_sheet_id IN ('A10', 'A11', 'A12', 'A13'))

	LEFT JOIN dbo.tr_inv_in T3 ON
	(T3.mat_code = T1.item_code AND T2.dept = 400 AND T3.cost_sheet_id IN ('A01', 'A02', 'A03', 'A04'))

	LEFT JOIN dbo.tr_inv_out_detail T4 ON
	(T4.mat_code = T1.item_code AND T4.out_dest=700 AND T4.cost_sheet_id IN ('A01', 'A02', 'A03', 'A04'))

	LEFT JOIN dbo.tr_inv_out_detail T5 ON
	(T5.mat_code = T1.item_code AND T4.out_dest != 700 AND T5.cost_sheet_id IN ('A01', 'A02', 'A03', 'A04'))

	GROUP BY T1.comp_id, T1.f_year, T1.f_month, T1.item_code, T1.item_name

END



