ALTER PROCEDURE  sp_WeavingGenerateBalWIP
    @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT
    DECLARE @rec_sts VARCHAR(1)
    DECLARE @proc_no INT
    DECLARE @dept INT

    SET @dept = (SELECT dept FROM istem_costing.dbo.ms_dept WHERE comp_id=@comp_id AND dept_seq=30)

    IF @f_month = 1
    BEGIN
        SET @lp_f_month = 12
        SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
        SET @lp_f_month = @f_month - 1
        SET @lp_f_year = @f_year
    END


    SET @rec_sts='A'
    SET @proc_no=(SELECT ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_bal_wip WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month)

    IF @proc_no >= 1
    BEGIN
        SET @rec_sts='T'

        INSERT INTO istem_costing.dbo.tr_bal_wip_hist (comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, item_category, item_code, qty_unit, cf_wip_qty, cf_wip_amount, proc_time, user_id, client_ip, rec_sts, proc_no)
            SELECT comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, item_category, item_code, qty_unit, cf_wip_qty, cf_wip_amount, proc_time, user_id, client_ip, rec_sts, proc_no FROM istem_costing.dbo.tr_bal_wip WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept;

        DELETE FROM istem_costing.dbo.tr_bal_wip WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept;
    END

    INSERT INTO istem_costing.dbo.tr_bal_wip
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,mat_code
            ,item_category
            ,item_code
            ,qty_unit
            ,cf_wip_qty
            ,cf_wip_amount
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
             @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,A.dept AS dept
            ,A.cost_sheet_id AS cost_sheet_id
            ,A.mat_code AS mat_code
            ,A.item_category AS item_category
            ,A.item_code AS item_code
            ,A.qty_unit AS qty_unit
            ,A.cf_wip_qty AS cf_wip_qty
            ,A.cf_wip_amount AS cf_wip_amount
            ,GETDATE() AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
        FROM istem_costing.dbo.tr_wip AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept=@dept
            AND (
                ISNULL(A.cf_wip_qty, 0) <> 0
            )
END;
