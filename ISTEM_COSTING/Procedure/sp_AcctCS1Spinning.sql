ALTER PROCEDURE  sp_AcctCS1Spinning
    @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    PRINT 'SUB1 : sp_AcctCS1Spinning'
    --
    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=7
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'


    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT
    IF @f_month = 1
    BEGIN
        SET @lp_f_month = 12
        SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
        SET @lp_f_month = @f_month - 1
        SET @lp_f_year = @f_year
    END

    DECLARE @dept INT
    DECLARE @prod_dept INT
    DECLARE @proc_time DATETIME
    DECLARE @rec_sts VARCHAR
    DECLARE @proc_no INT

    SET @dept = (SELECT dept FROM istem_costing.dbo.ms_dept WHERE comp_id=@comp_id AND dept_seq=20)
    SET @prod_dept = 1
    SET @proc_time=GETDATE()
    SET @rec_sts='A'
    SET @proc_no=(
        SELECT ISNULL(MAX(proc_no), 0) + 1
        FROM istem_costing.dbo.tr_cost_sheet_hist
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
    )

    IF @proc_no >= 1
    BEGIN
        SET @rec_sts='T'
    END

    DECLARE @_TEMP_TR_COST_SHEET TABLE (
        row_key varchar(5) NOT NULL,
    	comp_id numeric(1,0) NOT NULL,
    	f_year numeric(4,0) NOT NULL,
    	f_month numeric(2,0) NOT NULL,
    	dept numeric(3,0) NOT NULL,
    	cost_sheet_id varchar(4) NOT NULL,
    	item_code varchar(45) NOT NULL,
    	cs_qty numeric(19,2) DEFAULT 0 NULL,
    	cs_amount numeric(19,2) DEFAULT 0 NULL,
    	cs_unit_qty numeric(12,4) DEFAULT 0 NULL,
    	cs_unit_price numeric(12,4) DEFAULT 0 NULL,
    	cs_unit_cost numeric(12,4) DEFAULT 0 NULL,
    	cs_exch_qty numeric(19,2) DEFAULT 0 NULL,
    	proc_time datetime NULL,
    	user_id varchar(6) NULL,
    	client_ip varchar(15) NULL,
    	rec_sts char(1) NULL,
    	proc_no tinyint NULL,
    	process_cost numeric(18,2) NULL
    )


    -- 1	Dept --> ms_dept WHERE prod_dept = 1 & dept_seq=20
    DECLARE @main_item_code VARCHAR(45)
    SET @main_item_code = (SELECT main_item_code FROM istem_costing.dbo.ms_dept WHERE comp_id=@comp_id AND dept=@dept AND prod_dept=@prod_dept)

    -- 2	Product Exchange & Working Product
    -- 2.a	Product Exchange (cs_qty)
    DECLARE @2a_cs_qty DECIMAL(16, 4)
    DECLARE @2a_cs_exch_qty DECIMAL(16, 4)

    SELECT @2a_cs_qty = SUM(prod_qty)
    FROM istem_costing.dbo.tr_prod
    WHERE
        comp_id=@comp_id
        AND f_year=@f_year
        AND f_month=@f_month
        AND dept=@dept
    GROUP BY dept

    SELECT @2a_cs_exch_qty = SUM(tot_prod_equiv_qty)
    FROM istem_costing.dbo.tr_prod_detail1
    WHERE
        comp_id=@comp_id
        AND f_year=@f_year
        AND f_month=@f_month
        AND dept=@dept
        AND cost_sheet_id='N10'

    INSERT INTO @_TEMP_TR_COST_SHEET
    VALUES ('2.A', @comp_id, @f_year, @f_month, @dept, 'N10', @main_item_code, @2a_cs_qty, 0, 0, 0, 0, @2a_cs_exch_qty, @proc_time, @user_id, @client_ip, @rec_sts, @proc_no, 0)

    -- 2.b.	Working Product
    DECLARE @2b_cs_qty DECIMAL(16, 4)
    SELECT @2b_cs_qty=rcv_wip_cost_qty
    FROM istem_costing.dbo.tr_wip_cost
    WHERE
        comp_id=@comp_id
        AND f_year=@f_year
        AND f_month=@f_month
        AND dept=@dept
        AND cost_sheet_id='H99'

    INSERT INTO @_TEMP_TR_COST_SHEET
    VALUES ('2.B', @comp_id, @f_year, @f_month, @dept, 'P10', @main_item_code, @2b_cs_qty, 0, 0, 0, 0, 0, @proc_time, @user_id, @client_ip, @rec_sts, @proc_no, 0)

    -- 2.c	Hitung Working Product per Cost Element --> create pada tr_cost_sheet sebagai Q% (Working Product …..)
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key ,comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
          '2.C' AS row_key
        , @comp_id AS comp_id
        , @f_year AS f_year
        , @f_month AS f_month
        , @dept AS dept
        , CASE
            WHEN A.cost_sheet_id = 'C02' THEN 'Q60'
            WHEN A.cost_sheet_id = 'C03' THEN 'Q70'
            WHEN A.cost_sheet_id = 'C04' THEN 'Q80'
            ELSE ''
        END AS cost_sheet_id
        , @main_item_code AS item_code
        , A.utility_cons_qty AS cs_qty
        , A.manex_amount AS cs_amount
        , NULLIF(A.utility_cons_qty, 0)/NULLIF(A.tot_prod_equiv_qty, 0) AS cs_unit_qty
        , 0 AS cs_unit_price
        , 0 AS cs_unit_cost
        , A.tot_prod_equiv_qty AS cs_exch_qty
        , @proc_time AS proc_time
        , @user_id AS user_id
        , @client_ip AS client_ip
        , @rec_sts AS rec_sts
        , @proc_no AS proc_no
        , 0 AS process_cost
    FROM (
        SELECT
              A.comp_id
            , A.f_year
            , A.f_month
            , A.dept
            , A.convert_type_code
            , D.cost_sheet_id
            , A.utility_cons_qty
            , B.tot_prod_equiv_qty
            , D.manex_amount
        FROM istem_costing.dbo.tr_utility_cons A
        LEFT JOIN tr_prod_detail1 B ON
          A.comp_id= B.comp_id AND
          A.f_year = B.f_year AND
          A.f_month = B.f_month AND
          A.dept = B.dept AND
          A.convert_type_code = B.convert_type_code
        LEFT JOIN istem_costing.dbo.ms_convert_type C ON
          A.comp_id = c.comp_id AND
          A.convert_type_code = c.convert_type_code
        LEFT JOIN istem_costing.dbo.tr_manex D on
          A.comp_id= D.comp_id AND
          A.f_year = D.f_year AND
          A.f_month= D.f_month AND
          A.dept = D.dept AND
          c.cost_sheet_id = D.cost_sheet_id
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept=@dept
            AND C.cost_sheet_id BETWEEN 'C02' AND 'C04'
    ) AS A

    -- 3	Raw Material
    -- 3.a	(A) Dari ms_dept. main_item_code link ke v_sp_ms_yarn_detail untuk ambil komposisi material apa saja yang dipakai
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '3.A' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,B.cost_sheet_id AS cost_sheet_id
        ,@main_item_code AS item_code
        ,B.sum_cons_mat_qty AS cs_qty
        ,0 AS cs_amount
        ,NULLIF(B.sum_cons_mat_qty, 0)/NULLIF(C.sum_prod_qty, 0) AS cs_unit_qty
        ,NULLIF(D.sum_out_amount, 0)/NULLIF(D.sum_out_qty, 0) AS cs_unit_price
        ,NULLIF(B.sum_cons_mat_qty, 0)/NULLIF(C.sum_prod_qty, 0) * NULLIF(D.sum_out_amount, 0)/NULLIF(D.sum_out_qty, 0) AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM (
        SELECT
            A.comp_id
            , A.dept
            , A.main_item_code
            , B.mat_code
        FROM istem_costing.dbo.ms_dept A
        LEFT JOIN istem_costing.dbo.v_sp_ms_yarn_detail B ON A.main_item_code= B.yarn_code
        WHERE A.comp_id=@comp_id AND A.dept_seq=20
    ) AS A
    LEFT JOIN (
        SELECT
            mat_code
            ,cost_sheet_id
            ,SUM(cons_mat_qty) AS sum_cons_mat_qty
        FROM istem_costing.dbo.tr_wip
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND item_code=''
        GROUP BY mat_code, cost_sheet_id
    ) AS B ON (B.mat_code=A.mat_code)
    LEFT JOIN (
        SELECT
            A.yarn_code
            ,B.sum_prod_qty
        FROM istem_costing.dbo.v_sp_ms_yarn AS A
        LEFT JOIN (
            SELECT
                B.yarn_type
                ,B.yarn_mat_type
                ,SUM(A.prod_qty) as sum_prod_qty
            FROM istem_costing.dbo.tr_prod AS A
            INNER JOIN istem_costing.dbo.v_sp_ms_yarn AS B ON (A.item_code=B.yarn_code)
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
                AND A.dept=@dept
            GROUP BY B.yarn_type, B.yarn_mat_type
        ) AS B ON (B.yarn_type=A.yarn_type AND B.yarn_mat_type=A.yarn_mat_type)
        AND A.yarn_code=@main_item_code
    ) AS C ON (C.yarn_code=A.main_item_code)
    LEFT JOIN (
        SELECT
            mat_code
            ,cost_sheet_id
            ,SUM(out_qty) AS sum_out_qty
            ,SUM(out_amount) AS sum_out_amount
        FROM  istem_costing.dbo.tr_inv_out_detail
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND out_dest=@dept
        GROUP BY  mat_code, cost_sheet_id
    ) AS D ON (D.mat_code=A.mat_code AND D.cost_sheet_id=B.cost_sheet_id)

    -- 3.b	SubTotal Raw Material
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key ,comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
         '3.B' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'A99' AS cost_sheet_id
        ,@main_item_code AS item_code
        ,SUM(cs_qty) AS cs_qty
        ,SUM(cs_amount) AS cs_amount
        ,SUM(cs_unit_qty) AS cs_unit_qty
        ,0 AS cs_unit_price
        ,0 AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM @_TEMP_TR_COST_SHEET
    WHERE row_key = '3.A'

    -- 4	Utility Prop Cost
    -- 	Dari hasil Query no. 2.c
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key ,comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
         '4.A' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,A.cost_sheet_id AS cost_sheet_id
        ,@main_item_code AS item_code
        ,A.utility_cons_qty AS cs_qty
        ,A.manex_amount AS cs_amount
        ,NULLIF(A.utility_cons_qty, 0)/NULLIF(A.tot_prod_equiv_qty, 0) AS cs_unit_qty
        ,0 AS cs_unit_price
        ,0 AS cs_unit_cost
        ,A.tot_prod_equiv_qty AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM (
        SELECT
              A.comp_id
            , A.f_year
            , A.f_month
            , A.dept
            , A.convert_type_code
            , D.cost_sheet_id
            , A.utility_cons_qty
            , B.tot_prod_equiv_qty
            , D.manex_amount
        FROM istem_costing.dbo.tr_utility_cons A
        LEFT JOIN tr_prod_detail1 B ON
          A.comp_id= B.comp_id AND
          A.f_year = B.f_year AND
          A.f_month = B.f_month AND
          A.dept = B.dept AND
          A.convert_type_code = B.convert_type_code
        LEFT JOIN istem_costing.dbo.ms_convert_type C ON
          A.comp_id = c.comp_id AND
          A.convert_type_code = c.convert_type_code
        LEFT JOIN istem_costing.dbo.tr_manex D on
          A.comp_id= D.comp_id AND
          A.f_year = D.f_year AND
          A.f_month= D.f_month AND
          A.dept = D.dept AND
          c.cost_sheet_id = D.cost_sheet_id
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept=@dept
            AND C.cost_sheet_id BETWEEN 'C02' AND 'C04'
    ) AS A


    -- 5	Proportional Cost dan Fixed Cost

    -- 5.b	SubTotal Proportional Cost
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '5.B' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'E99' AS cost_sheet_id
        ,@main_item_code AS item_code
        ,0 AS cs_qty
        ,SUM(cs_amount) AS cs_amount
        ,NULLIF(( SELECT SUM(cs_qty) FROM @_TEMP_TR_COST_SHEET WHERE row_key='3.B'), 0)/NULLIF(@2a_cs_exch_qty, 0) AS cs_unit_qty
        ,0 AS cs_unit_price
        ,SUM(cs_unit_cost) AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM @_TEMP_TR_COST_SHEET
    WHERE
        (
            cost_sheet_id LIKE 'C%'
            OR cost_sheet_id LIKE 'D%'
            OR cost_sheet_id LIKE 'A%'
        )
        AND row_key != '3.B'

    -- 5.a	tr_manex
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '5.A' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,A.cost_sheet_id AS cost_sheet_id
        ,@main_item_code AS item_code
        ,0 AS cs_qty
        ,A.sum_manex_amount AS cs_amount
        ,0 AS cs_unit_qty
        ,0 AS cs_unit_price
        ,NULLIF(A.sum_manex_amount, 0)/NULLIF(@2a_cs_exch_qty, 0) AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM (
        SELECT
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,sum(manex_amount) AS sum_manex_amount
        FROM istem_costing.dbo.tr_manex
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND (cost_sheet_id ='C01' or cost_sheet_id >= 'D01')
        GROUP by comp_id, f_year,f_month,dept,cost_sheet_id

    ) AS A


    -- 5.c	SubTotal Fixed Cost
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '5.C' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'T40' AS cost_sheet_id
        ,@main_item_code AS item_code
        ,0 AS cs_qty
        ,SUM(cs_amount) AS cs_amount
        ,0 AS cs_unit_qty
        ,0 AS cs_unit_price
        ,NULLIF(SUM(cs_amount), 0)/NULLIF(@2a_cs_exch_qty, 0)  AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM @_TEMP_TR_COST_SHEET
    WHERE row_key='5.A'


    -- 6	Warehousing Cost
    --      (A) Total Material Cost dan Process Cost dan total production Qty per Dept
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '6' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'M10' AS cost_sheet_id
        ,@main_item_code AS item_code
        ,0 AS cs_qty
        ,NULLIF(( ISNULL(mat_proc_cost, 0)+ISNULL(cost_dist,0) ), 0) AS cs_amount
        ,0 AS cs_unit_qty
        ,0 AS cs_unit_price
        ,NULLIF(( ISNULL(mat_proc_cost, 0)+ISNULL(cost_dist,0) ), 0)/NULLIF(@2a_cs_qty, 0)  AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM (
        SELECT
            @main_item_code AS item_code
            ,SUM(prod_qty) AS sum_prod_qty
            ,SUM(material_cost)+SUM(process_cost) AS mat_proc_cost
        FROM istem_costing.dbo.tr_prod
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
    ) AS A
    LEFT JOIN (
        SELECT
            @main_item_code AS item_code
            ,SUM(tot_prod_cost_distr) AS cost_dist
        FROM istem_costing.dbo.tr_prod_detail1
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
    ) AS B ON (B.item_code=A.item_code)



    -- 7	Manufacturing Cost
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '7' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'J10' AS cost_sheet_id
        ,@main_item_code AS item_code
        ,0 AS cs_qty
        ,SUM(cs_amount) AS cs_amount
        ,0 AS cs_unit_qty
        ,0 AS cs_unit_price
        ,SUM(cs_unit_cost)  AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM @_TEMP_TR_COST_SHEET
    WHERE row_key IN ('5.B', '5.C')


    -- 8	Variance of WIP
    DECLARE @8d DECIMAL(16, 4)
    SET @8d = ( SELECT sum(cf_wip_cost_amount) FROM istem_costing.dbo.tr_bal_wip_cost WHERE comp_id=@comp_id AND f_year=@lp_f_year AND f_month=@lp_f_month AND dept=@dept) - (SELECT sum(cf_wip_cost_amount) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept)

    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '8' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'K10' AS cost_sheet_id
        ,@main_item_code AS item_code
        ,0 AS cs_qty
        ,@8d AS cs_amount
        ,0 AS cs_unit_qty
        ,0 AS cs_unit_price
        ,(SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '6')-(SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '7') AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost;

    -- 11	Total Manufacturing Cost
    INSERT INTO @_TEMP_TR_COST_SHEET
        (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
        SELECT
            CASE
                WHEN cost_sheet_id='A99' THEN '11.A'
                WHEN cost_sheet_id='C01' OR cost_sheet_id LIKE 'D%' OR cost_sheet_id LIKE 'E%' THEN '11.B'
                WHEN cost_sheet_id='C99' THEN '11.C'
                WHEN cost_sheet_id='H99' THEN '11.D'
                ELSE ''
            END AS row_key
            ,@comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept AS dept
            ,CASE
                WHEN cost_sheet_id='A99' THEN 'S10'
                WHEN cost_sheet_id='C01' OR cost_sheet_id LIKE 'D%' OR cost_sheet_id LIKE 'E%' THEN 'S20'
                WHEN cost_sheet_id='C99' THEN 'S30'
                WHEN cost_sheet_id='H99' THEN 'S40'
                ELSE ''
            END AS cost_sheet_id
            ,@main_item_code AS item_code
            ,0 AS cs_qty
            ,CASE
                WHEN cost_sheet_id='A99' THEN cons_wip_cost_amount
                WHEN cost_sheet_id='C01' OR cost_sheet_id LIKE 'D%' OR cost_sheet_id LIKE 'E%' THEN cons_wip_cost_amount
                WHEN cost_sheet_id='C99' THEN cons_wip_cost_amount
                WHEN cost_sheet_id='H99' THEN cons_wip_cost_amount
                ELSE 0
            END AS cs_amount
            ,0 AS cs_unit_qty
            ,0 AS cs_unit_price
            ,0 AS cs_unit_cost
            ,0 AS cs_exch_qty
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,0 AS process_cost
        FROM (
            SELECT * FROM istem_costing.dbo.tr_wip_cost
            WHERE
                    comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept=@dept
        ) AS A

    INSERT INTO @_TEMP_TR_COST_SHEET
        SELECT
            '11.E' AS row_key
            ,@comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept AS dept
            ,'S50' AS cost_sheet_id
            ,@main_item_code AS item_code
            ,0 AS cs_qty
            ,SUM(cs_amount) AS cs_amount
            ,0 AS cs_unit_qty
            ,0 AS cs_unit_price
            ,0 AS cs_unit_cost
            ,0 AS cs_exch_qty
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,0 AS process_cost
        FROM @_TEMP_TR_COST_SHEET
        WHERE row_key LIKE '11.%'

    -- 12	Remarks
    INSERT INTO @_TEMP_TR_COST_SHEET
        SELECT
            '12.A' AS row_key
            ,@comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept AS dept
            ,'T10' AS cost_sheet_id
            ,@main_item_code AS item_code
            ,0 AS cs_qty
            ,0 AS cs_amount
            ,0 AS cs_unit_qty
            ,0 AS cs_unit_price
            ,sum(cs_unit_cost) / sum(cs_unit_qty) AS cs_unit_cost
            ,0 AS cs_exch_qty
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,0 AS process_cost
        FROM @_TEMP_TR_COST_SHEET
        WHERE row_key = '3.A'

    INSERT INTO @_TEMP_TR_COST_SHEET
        SELECT
            '12.B' AS row_key
            ,@comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept AS dept
            ,'T20' AS cost_sheet_id
            ,@main_item_code AS item_code
            ,0 AS cs_qty
            ,0 AS cs_amount
            ,0 AS cs_unit_qty
            ,0 AS cs_unit_price
            ,sum(cs_unit_cost) / (SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '12.A') AS cs_unit_cost
            ,0 AS cs_exch_qty
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,0 AS process_cost
        FROM @_TEMP_TR_COST_SHEET
        WHERE row_key = '3.A'

    INSERT INTO @_TEMP_TR_COST_SHEET
        SELECT
            '12.C' AS row_key
            ,@comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept AS dept
            ,'T30' AS cost_sheet_id
            ,@main_item_code AS item_code
            ,0 AS cs_qty
            ,0 AS cs_amount
            ,0 AS cs_unit_qty
            ,0 AS cs_unit_price
            ,(SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '5.B')-
             (SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '12.A')-
             (SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '12.B') AS cs_unit_cost
            ,0 AS cs_exch_qty
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,0 AS process_cost

    INSERT INTO @_TEMP_TR_COST_SHEET
        SELECT
            '12.D' AS row_key
            ,@comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept AS dept
            ,'T40' AS cost_sheet_id
            ,@main_item_code AS item_code
            ,0 AS cs_qty
            ,0 AS cs_amount
            ,0 AS cs_unit_qty
            ,0 AS cs_unit_price
            ,(SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '5.C') AS cs_unit_cost
            ,0 AS cs_exch_qty
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,0 AS process_cost

    INSERT INTO @_TEMP_TR_COST_SHEET
        SELECT
            '12.E' AS row_key
            ,@comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept AS dept
            ,'T50' AS cost_sheet_id
            ,@main_item_code AS item_code
            ,0 AS cs_qty
            ,0 AS cs_amount
            ,0 AS cs_unit_qty
            ,0 AS cs_unit_price
            ,(SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '12.C')+
             (SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '12.D') AS cs_unit_cost
            ,0 AS cs_exch_qty
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,0 AS process_cost

    INSERT INTO @_TEMP_TR_COST_SHEET
        SELECT
            '12.F' AS row_key
            ,@comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept AS dept
            ,'T60' AS cost_sheet_id
            ,@main_item_code AS item_code
            ,0 AS cs_qty
            ,0 AS cs_amount
            ,0 AS cs_unit_qty
            ,0 AS cs_unit_price
            ,(SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '12.E')-
             (SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '5.A' AND cost_sheet_id = 'G15') AS cs_unit_cost
            ,0 AS cs_exch_qty
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,0 AS process_cost
    INSERT INTO @_TEMP_TR_COST_SHEET
        SELECT
            '12.G' AS row_key
            ,@comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept AS dept
            ,'T70' AS cost_sheet_id
            ,@main_item_code AS item_code
            ,0 AS cs_qty
            ,0 AS cs_amount
            ,0 AS cs_unit_qty
            ,0 AS cs_unit_price
            ,(SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '12.F')-
             (SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '5.A' AND cost_sheet_id = 'H20') AS cs_unit_cost
            ,0 AS cs_exch_qty
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,0 AS process_cost
    INSERT INTO @_TEMP_TR_COST_SHEET
        SELECT
            '12.H' AS row_key
            ,@comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept AS dept
            ,'T99' AS cost_sheet_id
            ,@main_item_code AS item_code
            ,0 AS cs_qty
            ,0 AS cs_amount
            ,0 AS cs_unit_qty
            ,0 AS cs_unit_price
            ,SUM(cs_unit_cost) AS cs_unit_cost
            ,0 AS cs_exch_qty
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,0 AS process_cost
        FROM @_TEMP_TR_COST_SHEET
        WHERE row_key IN ('12.A','12.B', '12.E')

    INSERT INTO @_TEMP_TR_COST_SHEET
        SELECT
            '13' AS row_key
            ,@comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept AS dept
            ,'R10' AS cost_sheet_id
            ,@main_item_code AS item_code
            ,0 AS cs_qty
            ,0 AS cs_amount
            ,0 AS cs_unit_qty
            ,0 AS cs_unit_price
            ,A.opr_day AS cs_unit_cost
            ,0 AS cs_exch_qty
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,0 AS process_cost
        FROM (
            SELECT DISTINCT opr_day
            FROM istem_costing.dbo.tr_dept_header
            WHERE
                    comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept=@dept
                AND stock_take_date=(SELECT MAX(stock_take_date) FROM istem_costing.dbo.tr_dept_header WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept)
        ) AS A


        INSERT INTO istem_costing.dbo.tr_cost_sheet_hist SELECT * FROM istem_costing.dbo.tr_cost_sheet WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND ISNULL(item_code, '') <> '';
        DELETE FROM istem_costing.dbo.tr_cost_sheet WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND ISNULL(item_code, '') <> '';
        INSERT INTO istem_costing.dbo.tr_cost_sheet
                   (
                       comp_id
                       ,f_year
                       ,f_month
                       ,dept
                       ,cost_sheet_id
                       ,item_code
                       ,cs_qty
                       ,cs_amount
                       ,cs_unit_qty
                       ,cs_unit_price
                       ,cs_unit_cost
                       ,cs_exch_qty
                       ,proc_time
                       ,user_id
                       ,client_ip
                       ,rec_sts
                       ,proc_no
                       ,process_cost
                   )
                   SELECT
                        comp_id
                        ,f_year
                        ,f_month
                        ,dept
                        ,cost_sheet_id
                        ,item_code
                        ,cs_qty
                        ,cs_amount
                        ,cs_unit_qty
                        ,cs_unit_price
                        ,cs_unit_cost
                        ,cs_exch_qty
                        ,proc_time
                        ,user_id
                        ,client_ip
                        ,rec_sts
                        ,proc_no
                        ,process_cost
                FROM @_TEMP_TR_COST_SHEET

END
