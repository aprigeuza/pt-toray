
ALTER PROCEDURE  sp_WeavingProcessWIPCALC
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;
    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=7
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept_10 INT
    DECLARE @dept_30 INT
    DECLARE @mc_loc VARCHAR(MAX)
    DECLARE @tr_code VARCHAR(MAX)
    DECLARE @proc_code VARCHAR(MAX)
    DECLARE @proc_time AS DATETIME
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_no INT
    DECLARE @item_code VARCHAR(MAX)
    DECLARE @proc_type VARCHAR(MAX)
    DECLARE @mat_qty_unit VARCHAR(MAX)
    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT

    SET @dept_10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id) AND (dept_seq=10))
    SET @dept_30=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id) AND (dept_seq=30))

    IF @f_month = 1
        BEGIN
            SET @lp_f_month = 12
            SET @lp_f_year = @f_year - 1
        END
        ELSE
        BEGIN
            SET @lp_f_month = @f_month - 1
            SET @lp_f_year = @f_year
        END

    DECLARE @_TEMP TABLE(
         res VARCHAR(MAX)
        ,mat_code VARCHAR(MAX)
        ,mat_name VARCHAR(MAX)
        ,product_item VARCHAR(MAX)
        ,cost_sheet_id VARCHAR(MAX)
        ,bf_qty NUMERIC(14,5) NOT NULL DEFAULT 0
        ,bf_price NUMERIC(14,5) NOT NULL DEFAULT 0
        ,bf_amount NUMERIC(14,5) NOT NULL DEFAULT 0
        ,rc_qty NUMERIC(14,5) NOT NULL DEFAULT 0
        ,rc_price NUMERIC(14,5) NOT NULL DEFAULT 0
        ,rc_amount NUMERIC(14,5) NOT NULL DEFAULT 0
        ,co_qty NUMERIC(14,5) NOT NULL DEFAULT 0
        ,co_price NUMERIC(14,5) NOT NULL DEFAULT 0
        ,co_amount NUMERIC(14,5) NOT NULL DEFAULT 0
        ,adj_qty NUMERIC(14,5) NOT NULL DEFAULT 0
        ,adj_price NUMERIC(14,5) NOT NULL DEFAULT 0
        ,adj_amount NUMERIC(14,5) NOT NULL DEFAULT 0
        ,cf_qty NUMERIC(14,5) NOT NULL DEFAULT 0
        ,cf_price NUMERIC(14,5) NOT NULL DEFAULT 0
        ,cf_amount NUMERIC(14,5) NOT NULL DEFAULT 0
        ,wh_conv_qty NUMERIC(14,5) NOT NULL DEFAULT 0
        ,odt VARCHAR(MAX)
        ,leno INTEGER NOT NULL DEFAULT 0
    )

    -- Insert Header
    INSERT INTO @_TEMP
        (res,mat_code,mat_name,product_item, cost_sheet_id, odt)
        SELECT
            'H' AS res
            ,MT.mat_code AS mat_code
            ,M.FrgnName AS mat_name
            ,'' AS product_item
            ,M.U_Cost_ID AS cost_sheet_id
            ,'A' AS odt
        FROM (
            SELECT mat_code
            FROM istem_costing.dbo.tr_bal_wip AS A
            WHERE (A.comp_id=@comp_id) AND (A.f_year=@lp_f_year) AND (A.f_month=@lp_f_month) AND (A.dept=@dept_30)
            GROUP BY mat_code
            UNION ALL
            SELECT mat_code
            FROM istem_costing.dbo.tr_dept_calc AS A
            WHERE (A.comp_id=@comp_id) AND (A.f_year=@f_year) AND (A.f_month=@f_month) AND (A.dept=@dept_30)
            GROUP BY mat_code
            UNION ALL
            SELECT mat_code
            FROM istem_costing.dbo.tr_inv_out_detail AS A
            WHERE (A.comp_id=@comp_id) AND (A.f_year=@f_year) AND (A.f_month=@f_month) AND (A.out_dest=@dept_30)
            GROUP BY mat_code
        ) MT
        LEFT JOIN istem_costing.dbo.v_SAPItem AS M ON (M.ItemCode COLLATE SQL_Latin1_General_CP1_CI_AS = MT.mat_code COLLATE SQL_Latin1_General_CP1_CI_AS)
        GROUP BY MT.mat_code, M.FrgnName, M.U_Cost_ID

    -- Insert Detail 1
    INSERT INTO @_TEMP
        (res,mat_code,mat_name,product_item, cost_sheet_id, odt)
        SELECT
            'D1' AS res
            ,H.mat_code AS mat_code
            ,H.mat_name AS mat_name
            ,D1.item_code AS product_item
            -- ,CASE WHEN D1.item_code='' THEN 'RCVRM' ELSE D1.item_code END AS product_item
            ,H.cost_sheet_id AS cost_sheet_id
            ,'B' AS odt
        FROM @_TEMP AS H
        INNER JOIN (
            SELECT
                mat_code AS mat_code,
                ISNULL(item_code, '') AS item_code
            FROM istem_costing.dbo.tr_bal_wip
            WHERE
                (comp_id=@comp_id)
                AND (f_year=@lp_f_year)
                AND (f_month=@lp_f_month)
                AND (dept=@dept_30)
            GROUP BY mat_code, item_code
            UNION ALL
            SELECT
                mat_code AS mat_code,
                ISNULL(item_code, '') AS item_code
            FROM istem_costing.dbo.tr_dept_calc
                WHERE
                    (comp_id=@comp_id)
                    AND (f_year=@f_year)
                    AND (f_month=@f_month)
                    AND (dept=@dept_30)
                    -- AND tr_code LIKE '%WIP%'
                GROUP BY mat_code, item_code
        ) AS D1 ON (D1.mat_code=H.mat_code)
        WHERE H.res = 'H' AND D1.item_code <> ''
        GROUP BY H.mat_code,H.mat_name,D1.item_code,H.cost_sheet_id

    -- Update Detail 1 - WH Conv Qty
    UPDATE T
        SET
            T.wh_conv_qty=ISNULL(S.wh_conv_qty, 0)
        FROM @_TEMP AS T
        INNER JOIN (
            SELECT
                A.res AS res
                ,ISNULL(A.mat_code, '') AS mat_code
                ,ISNULL(A.product_item, '') AS product_item
                ,B.wh_conv_qty AS wh_conv_qty
            FROM @_TEMP AS A
            LEFT JOIN (
                SELECT
                     mat_code
                     ,item_code AS product_item
                     ,SUM(A.mat_qty) AS wh_conv_qty
                FROM istem_costing.dbo.tr_dept_calc AS A
                WHERE
                        (A.comp_id=@comp_id)
                    AND (A.f_year=@f_year)
                    AND (A.f_month=@f_month)
                    AND (A.dept=@dept_30)
                    AND (A.tr_code LIKE 'WH%')
                GROUP BY A.mat_code,A.item_code
            ) AS B ON (B.mat_code=A.mat_code AND B.product_item=A.product_item)
            WHERE A.res='D1'
        ) AS S ON (S.res=T.res AND S.mat_code=T.mat_code AND S.product_item=T.product_item)
        WHERE T.res='D1'

    -- Update Header - WH Conv Qty
    UPDATE T
        SET
         T.wh_conv_qty=ISNULL(S.wh_conv_qty, 0)
        FROM @_TEMP AS T
        INNER JOIN (
         SELECT
              ISNULL(A.mat_code, '') AS mat_code
             ,SUM(A.wh_conv_qty) AS wh_conv_qty
         FROM @_TEMP AS A
         WHERE A.res='D1'
         GROUP BY A.mat_code
        ) AS S ON (S.mat_code=T.mat_code)
        WHERE T.res='H'

    -- Update Detail 1 - bf_qty, bf_amount
    UPDATE T SET
             T.bf_qty=ISNULL((S.bf_qty),0)
            ,T.bf_price=ISNULL((NULLIF(S.bf_amount, 0)/NULLIF(S.bf_qty, 0)), 0)
            ,T.bf_amount=ISNULL(S.bf_amount,0)
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT
                 A.mat_code AS mat_code
                ,A.item_code AS product_item
                ,A.cost_sheet_id AS cost_sheet_id
                ,A.cf_wip_qty AS bf_qty
                ,A.cf_wip_amount AS bf_amount
            FROM istem_costing.dbo.tr_bal_wip AS A
            WHERE
                    (A.comp_id=@comp_id)
                AND (A.f_year=@lp_f_year)
                AND (A.f_month=@lp_f_month)
                AND (A.dept=@dept_30)
        ) AS S ON (S.mat_code=T.mat_code AND S.product_item=T.product_item AND S.cost_sheet_id=T.cost_sheet_id)
        WHERE
        T.res = 'D1'

    -- Update Header - bf_qty, bf_price, bf_amount
    UPDATE T SET
         T.bf_qty=ISNULL((S.bf_qty), 0)
        ,T.bf_price=ISNULL((NULLIF(S.bf_amount, 0)/NULLIF(S.bf_qty, 0)), 0)
        ,T.bf_amount=ISNULL((S.bf_amount), 0)
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT
                 A.mat_code AS mat_code
                ,A.cost_sheet_id AS cost_sheet_id
                ,SUM(A.bf_qty) AS bf_qty
                ,SUM(A.bf_amount) AS bf_amount
            FROM @_TEMP AS A
            WHERE
                A.res='D1'
            GROUP BY A.mat_code, A.cost_sheet_id
        ) AS S ON (S.mat_code=T.mat_code AND S.cost_sheet_id=T.cost_sheet_id)
        WHERE
        T.res = 'H'

    -- Update Header - rc_qty, rc_price, rc_amount
    UPDATE T SET
             T.rc_qty=ISNULL((S.rc_qty), 0)
            ,T.rc_price=ISNULL((NULLIF(S.rc_amount, 0)/NULLIF(S.rc_qty, 0)), 0)
            ,T.rc_amount=ISNULL((S.rc_amount), 0)
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT
                A.mat_code AS mat_code
                ,SUM(A.out_qty) AS rc_qty
                ,SUM(A.out_amount) AS rc_amount
            FROM istem_costing.dbo.tr_inv_out_detail AS A
            WHERE
                    (A.comp_id=@comp_id)
                AND (A.f_year=@f_year)
                AND (A.f_month=@f_month)
                AND (A.out_dest=@dept_30)
            GROUP BY A.mat_code
        ) AS S ON (S.mat_code=T.mat_code)
        WHERE T.res = 'H'

    -- Update Detail 1 - rc_price
    -- Tidak di tampilkan per tanggal : 07/01/2021
    -- UPDATE T SET
    --     T.rc_price=ISNULL(S.rc_price, 0)
    --     FROM @_TEMP AS T
    --     LEFT JOIN @_TEMP AS S ON (S.res = 'H' AND S.mat_code=T.mat_code AND S.cost_sheet_id=T.cost_sheet_id)
    --     WHERE T.res = 'D1'

    -- Update Detail 1 - rc_qty, rc_amount
    -- Tidak di tampilkan per tanggal : 07/01/2021
    -- UPDATE T SET
    --      T.rc_qty=ISNULL((NULLIF(T.wh_conv_qty, 0)/NULLIF(S.wh_conv_qty, 0)) * S.rc_qty, 0)
    --     ,T.rc_amount=ISNULL((T.rc_price * ((NULLIF(T.wh_conv_qty, 0)/NULLIF(S.wh_conv_qty, 0)) * S.rc_qty)), 0)
    --     FROM @_TEMP AS T
    --     LEFT JOIN @_TEMP AS S ON (S.res='H' AND S.mat_code=T.mat_code)
    --     WHERE T.res = 'D1'

    -- Update Header & Detail - co_price, adj_price, cf_price
    UPDATE T SET
            T.co_price=S.cf_price
            ,T.adj_price=S.cf_price
            ,T.cf_price=S.cf_price
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT
                mat_code,
                ISNULL((NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0)), 0) AS cf_price
            FROM @_TEMP AS T
            WHERE T.res = 'H'
        ) AS S ON (S.mat_code=T.mat_code)

    -- Update Detail 1 - cf_qty, cf_amount
    UPDATE T SET
         T.cf_qty=ISNULL((S.cf_qty), 0)
        ,T.cf_amount=ISNULL((S.cf_qty*T.cf_price), 0)
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT
                 A.mat_code AS mat_code
                ,A.item_code AS product_item
                ,SUM(A.mat_qty) AS cf_qty
            FROM istem_costing.dbo.tr_dept_calc AS A
            WHERE
                    (A.comp_id=@comp_id)
                AND (A.f_year=@f_year)
                AND (A.f_month=@f_month)
                AND (A.dept=@dept_30)
                AND (A.tr_code LIKE '%WIP%')
            GROUP BY A.mat_code, A.item_code
        ) AS S ON (S.mat_code=T.mat_code AND S.product_item=T.product_item)
        WHERE T.res = 'D1'

    -- Update Header - cf_qty, cf_amount
    UPDATE T SET
        T.cf_qty=ISNULL(S.cf_qty, 0)
        ,T.cf_amount=ISNULL(S.cf_qty*T.cf_price, 0)
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT
            mat_code
            ,SUM(cf_qty) AS cf_qty
            FROM @_TEMP AS A
            WHERE res='D1'
            GROUP BY A.mat_code
        ) AS S ON (S.mat_code=T.mat_code)
        WHERE T.res = 'H'

    -- Update Header - co_qty
    UPDATE T SET
        T.co_qty=(T.bf_qty+T.rc_qty)-(T.adj_qty+T.cf_qty)
        ,T.co_amount=(T.bf_amount+T.rc_amount)-(T.adj_amount+T.cf_amount)
        FROM @_TEMP AS T
        WHERE T.res = 'H'

    -- Update Detail 1 - co_qty, co_amount
    UPDATE T SET
        T.co_qty=ISNULL(NULLIF(T.wh_conv_qty, 0) / NULLIF(S.wh_conv_qty, 0)  * NULLIF(S.co_qty, 0), 0)
        ,T.co_amount=ISNULL(NULLIF(T.wh_conv_qty, 0) / NULLIF(S.wh_conv_qty, 0)  * NULLIF(S.co_qty, 0), 0) * T.co_price
        FROM @_TEMP AS T
        LEFT JOIN (SELECT * FROM @_TEMP WHERE res='H') AS S ON (S.mat_code=T.mat_code)
        WHERE T.res = 'D1'

    -- Update Header - adj_qty
    UPDATE T SET
            T.adj_qty=CASE WHEN T.wh_conv_qty =  0 THEN (T.bf_qty+T.rc_qty)-(T.cf_qty) ELSE 0 END
        FROM @_TEMP AS T
        WHERE T.res = 'H'

    -- Update Header - adj_amount
    UPDATE T SET
            T.adj_amount=T.adj_qty*T.adj_price
        FROM @_TEMP AS T
        WHERE T.res = 'H'

    -- Cek tr_wip
    -- Find Data
    SET @proc_time=GETDATE()
    SET @proc_no=1
    SET @rec_sts='A'

    DECLARE @c INT
    SET @c = (SELECT COUNT(*) FROM istem_costing.dbo.tr_wip WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept_30))

    IF @c >= 1
    BEGIN

        -- Save History
        INSERT INTO istem_costing.dbo.tr_wip_hist
                (comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, item_category, item_code, qty_unit, rcv_mat_qty, rcv_mat_amount, cons_mat_qty, cons_mat_amount, cf_wip_qty, cf_wip_amount, adj_wip_qty, adj_wip_amount, proc_time, user_id, client_ip, rec_sts, proc_no)
                SELECT
                    comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, item_category, item_code, qty_unit, rcv_mat_qty, rcv_mat_amount, cons_mat_qty, cons_mat_amount, cf_wip_qty, cf_wip_amount, adj_wip_qty, adj_wip_amount, proc_time, user_id, client_ip, rec_sts, proc_no
                FROM istem_costing.dbo.tr_wip
                WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept_30)

        -- Jika sudah ada
        -- Jadikan rec_sts = T
        SET @rec_sts = 'T'

        SET @proc_no = (SELECT MAX(ISNULL(proc_no, 0)) + 1 FROM istem_costing.dbo.tr_wip WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept_30))
    END

    DELETE
        FROM istem_costing.dbo.tr_wip
        WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept_30)

    -- Insert Header
    INSERT INTO istem_costing.dbo.tr_wip (comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, item_category, item_code, qty_unit, rcv_mat_qty, rcv_mat_amount, cons_mat_qty, cons_mat_amount, cf_wip_qty, cf_wip_amount, adj_wip_qty, adj_wip_amount, proc_time, user_id, client_ip, rec_sts, proc_no)
    SELECT
         @comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept_30 AS dept
        ,ISNULL(A.cost_sheet_id, '') AS cost_sheet_id
        ,ISNULL(A.mat_code, '') AS mat_code
        ,'' AS item_category
        ,ISNULL(A.product_item, '') AS item_code
        ,ISNULL(B.mat_qty_unit, 'LBS') AS qty_unit
        ,A.rc_qty AS rcv_mat_qty
        ,A.rc_amount AS rcv_mat_amount
        ,A.co_qty AS cons_mat_qty
        ,A.co_amount AS cons_mat_amount
        ,A.cf_qty AS cf_wip_qty
        ,A.cf_amount AS cf_wip_amount
        ,A.adj_qty AS adj_wip_qty
        ,A.adj_amount AS adj_wip_amount
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
    FROM @_TEMP AS A
    LEFT JOIN
    (
        SELECT * FROM (
            SELECT mat_code, mat_qty_unit FROM istem_costing.dbo.tr_dept_calc AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
                AND A.dept=@dept_30
                AND A.tr_code LIKE 'WIP%'
                AND ISNULL(A.item_code, '') <> ''
            UNION ALL
            SELECT mat_code, qty_unit FROM istem_costing.dbo.tr_inv_out_detail AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
                AND A.dept=@dept_30
            UNION ALL
            SELECT mat_code, qty_unit FROM istem_costing.dbo.tr_inv_out_detail AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
                AND A.dept=@dept_30
        ) AS A
        GROUP BY mat_code, mat_qty_unit
    )
     AS B ON (B.mat_code=A.mat_code)
    WHERE A.res = 'H'
    ORDER BY mat_code, odt ASC

    -- Insert Detail
    INSERT INTO istem_costing.dbo.tr_wip (comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, item_category, item_code, qty_unit, rcv_mat_qty, rcv_mat_amount, cons_mat_qty, cons_mat_amount, cf_wip_qty, cf_wip_amount, adj_wip_qty, adj_wip_amount, proc_time, user_id, client_ip, rec_sts, proc_no)
    SELECT
         @comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept_30 AS dept
        ,ISNULL(A.cost_sheet_id, '') AS cost_sheet_id
        ,ISNULL(A.mat_code, '') AS mat_code
        ,'' AS item_category
        ,ISNULL(A.product_item, '') AS item_code
        ,ISNULL(B.mat_qty_unit, 'LBS') AS qty_unit
        ,A.rc_qty AS rcv_mat_qty
        ,A.rc_amount AS rcv_mat_amount
        ,A.co_qty AS cons_mat_qty
        ,A.co_amount AS cons_mat_amount
        ,A.cf_qty AS cf_wip_qty
        ,A.cf_amount AS cf_wip_amount
        ,A.adj_qty AS adj_wip_qty
        ,A.adj_amount AS adj_wip_amount
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
    FROM @_TEMP AS A
    LEFT JOIN
    (
        SELECT mat_code, item_code, mat_qty_unit FROM istem_costing.dbo.tr_dept_calc AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept=@dept_30
            AND A.tr_code LIKE 'WIP%'
        GROUP BY mat_code, item_code, mat_qty_unit
    )
     AS B ON (B.mat_code=A.mat_code AND B.item_code=A.product_item)
    WHERE A.res = 'D1'
    ORDER BY mat_code, product_item, odt ASC

    DELETE FROM istem_costing.dbo.acct_wip_report WHERE comp_id=@comp_id AND dept=@dept_30 AND f_year=@f_year AND f_month=@f_month
    INSERT INTO istem_costing.dbo.acct_wip_report
               (comp_id
               ,dept
               ,f_year
               ,f_month
               ,mat_code
               ,mat_name
               ,product_item
               ,product_item2
               ,bf_qty
               ,bf_price
               ,bf_amount
               ,rc_qty
               ,rc_price
               ,rc_amount
               ,co_qty
               ,co_price
               ,co_amount
               ,cf_qty
               ,cf_price
               ,cf_amount
               ,wh_conv_qty
               ,var_num1
               ,var_num2
               ,var_num3
               ,var_num4
               ,var_num5
               ,remark1
               ,remark2
               ,remark3
               ,remark4
               ,remark5
               ,generate_date)
         SELECT
                @comp_id AS comp_id
               ,@dept_30 AS dept
               ,@f_year AS f_year
               ,@f_month AS f_month
               ,A.mat_code AS mat_code
               ,A.mat_name AS mat_name
               ,A.product_item AS product_item
               ,NULL AS product_item2
               ,NULLIF(A.bf_qty, 0) AS bf_qty
               ,NULLIF(A.bf_price, 0) AS bf_price
               ,NULLIF(A.bf_amount, 0) AS bf_amount
               ,NULLIF(A.rc_qty, 0) AS rc_qty
               ,NULLIF(A.rc_price, 0) AS rc_price
               ,NULLIF(A.rc_amount, 0) AS rc_amount
               ,NULLIF(A.co_qty, 0) AS co_qty
               ,NULLIF(A.co_price, 0) AS co_price
               ,NULLIF(A.co_amount, 0) AS co_amount
               ,NULLIF(A.cf_qty, 0) AS cf_qty
               ,NULLIF(A.cf_price, 0) AS cf_price
               ,NULLIF(A.cf_amount, 0) AS cf_amount
               ,NULLIF(A.wh_conv_qty, 0) AS wh_conv_qty
               ,NULL AS var_num1
               ,NULL AS var_num2
               ,NULL AS var_num3
               ,NULL AS var_num4
               ,NULL AS var_num5
               ,A.res AS remark1
               ,NULL AS remark2
               ,NULL AS remark3
               ,NULL AS remark4
               ,NULL AS remark5
               ,@proc_time
           FROM @_TEMP AS A ORDER BY mat_code, product_item, odt ASC



    -- Insert tr_slip
    --  Di pending sementara 19 Januari 2021
    -- SET @proc_time=GETDATE()
    -- SET @proc_no=1
    -- SET @rec_sts='A'
    --
    -- SET @proc_no = (SELECT ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_slip
    --                 WHERE (f_year = @f_year)
    --                     AND (f_month = @f_month)
    --                     AND (dept = @dept_30)
    --                     AND (jnl_code = 'JV08')
    --                     AND (jnl_detail_no = 10)
    --                     AND (jnl_description = 'WV WIP Adjustment'))
    -- IF @proc_no >= 2
    -- BEGIN
    --     -- Jika sudah ada
    --     -- Jadikan rec_sts = T
    --     SET @rec_sts = 'T'
    -- END
    -- SELECT
    --      @comp_id AS comp_id
    --     ,@f_year AS f_year
    --     ,@f_month AS f_month
    --     ,@dept_30 AS dept
    --     ,'JV08' AS jnl_code
    --     ,10 AS jnl_detail_no
    --     ,'WV WIP Adjustment' AS jnl_description
    --     ,SUM(adj_qty) AS jnl_amount
    --     ,@proc_time AS proc_time
    --     ,@user_id AS user_id
    --     ,@client_ip AS client_ip
    --     ,@rec_sts AS rec_sts
    --     ,@proc_no AS proc_no
    -- FROM @_TEMP AS A
    -- WHERE A.res = 'H'

    SELECT COUNT(*) AS C FROM @_TEMP
END
