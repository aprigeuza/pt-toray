ALTER PROCEDURE  sp_AcctDyeingFG
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;
    -- Acct - Dyeing
    -- Dyeing
    -- Acct - Dyeing FG


    -- DECLARE
    -- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=7
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    -- ********************************************************************
    -- ********************************************************************
    -- ********************************************************************
    -- ********************************************************************
    --              Query Dari Pak Win Belum
    -- ********************************************************************
    -- ********************************************************************
    -- ********************************************************************
    -- ********************************************************************
    -- ********************************************************************

    DECLARE @dept INT
    SET @dept = (SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40))

    DECLARE @_TEMP TABLE (
        -- ROW HEADER
    	comp_id numeric(1,0) NOT NULL,
    	f_year numeric(4,0) NOT NULL,
    	f_month numeric(2,0) NOT NULL,
    	dept numeric(3,0) NOT NULL,
    	grey_no varchar(6) NOT NULL,
    	ci_no varchar(20) NOT NULL,
    	dom_exp char(1) NOT NULL,
    	undelivery bit NOT NULL,
        qty_unit varchar(5) NOT NULL,

        -- COLUMN HEADER
        -- Opening Balance
        bf_qty numeric(16,4),
        bf_price numeric(16,4),
        bf_amount numeric(16,4),
        -- Transfer
        tf_qty numeric(16,4),
        tf_price numeric(16,4),
        tf_amount numeric(16,4),
        -- Production
        prod_qty numeric(16,4),
        prod_price numeric(16,4),
        prod_amount numeric(16,4),
        -- Average Price
        avg_price numeric(16,4),
        -- Sales A
        sales_a_qty numeric(16,4),
        sales_a_price numeric(16,4),
        sales_a_amount numeric(16,4),
        -- Sales C
        sales_c_qty numeric(16,4),
        sales_c_price numeric(16,4),
        sales_c_amount numeric(16,4),
        -- Sample
        samp_qty numeric(16,4),
        samp_price numeric(16,4),
        samp_amount numeric(16,4),
        -- Factory Use
        fac_qty numeric(16,4),
        fac_price numeric(16,4),
        fac_amount numeric(16,4),
        -- Return
        ret_qty numeric(16,4),
        ret_price numeric(16,4),
        ret_amount numeric(16,4),
        -- To Process
        to_proc_qty numeric(16,4),
        to_proc_price numeric(16,4),
        to_proc_amount numeric(16,4),
        -- Goods In transit
        gitr_qty numeric(16,4),
        gitr_price numeric(16,4),
        gitr_amount numeric(16,4),
        -- Total Issued
        tot_issue_qty numeric(16,4),
        tot_issue_price numeric(16,4),
        tot_issue_amount numeric(16,4),
        -- Ending Balance
        cf_qty numeric(16,4),
        cf_price numeric(16,4),
        cf_amount numeric(16,4),
        -- Impairment Price
        impairment_price numeric(16,4),

        sales_amount numeric(16,4),
        impairment_amount numeric(16,4),

        -- Ending Balance After impairment
        cf_final_qty numeric(16,4),
        cf_final_price numeric(16,4),
        cf_final_amount numeric(16,4)
    )

    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT

    IF @f_month = 1
    BEGIN
        SET @lp_f_month = 12
        SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
        SET @lp_f_month = @f_month - 1
        SET @lp_f_year = @f_year
    END

    -- Insert Data

    INSERT INTO @_TEMP
    (
        comp_id
        ,f_year
        ,f_month
        ,dept
        ,grey_no
        ,ci_no
        ,dom_exp
        ,undelivery
        ,qty_unit
    )
    SELECT
         @comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,A.grey_no AS grey_no
        ,A.ci_no AS ci_no
        ,A.dom_exp AS dom_exp
        ,A.undelivery AS undelivery
        ,A.qty_unit AS qty_unit
    FROM (
        SELECT
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
        FROM istem_costing.dbo.tr_bal_fg
        WHERE
                comp_id=@comp_id
            AND f_year=@lp_f_year
            AND f_month=@lp_f_month
            AND dept=@dept
            AND ISNULL(ci_no, '') <> ''
        GROUP BY
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
        UNION ALL
        SELECT
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
        FROM istem_costing.dbo.tr_prod_fg
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND ISNULL(ci_no, '') <> ''
        GROUP BY
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS A
    GROUP BY
        A.grey_no
        ,A.ci_no
        ,A.dom_exp
        ,A.undelivery
        ,A.qty_unit
    ORDER BY
        A.grey_no
        ,A.ci_no
        ,A.dom_exp
        ,A.undelivery
        ,A.qty_unit

    -- Beginning Balance
    UPDATE T SET
        T.bf_qty=S.bf_qty
        ,T.bf_price=S.bf_price
        ,T.bf_amount=S.bf_amount
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(cf_fg_qty) AS bf_qty
            ,NULLIF(SUM(cf_fg_amount), 0)/NULLIF(SUM(cf_fg_qty), 0) AS bf_price
            ,SUM(cf_fg_amount) AS bf_amount
        FROM istem_costing.dbo.tr_bal_fg
        WHERE
                comp_id=@comp_id
            AND f_year=@lp_f_year
            AND f_month=@lp_f_month
            AND dept=@dept
            AND undelivery=0
            AND ISNULL(ci_no, '') <> ''
            AND dom_exp IN ('D')
            AND qty_unit IN ('MTR')
        GROUP BY
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)


    -- Transfer
    UPDATE T SET
        T.tf_qty=S.tf_qty
        ,T.tf_price=NULLIF(S.tf_amount, 0)/NULLIF(S.tf_qty, 0)
        ,T.tf_amount=S.tf_amount
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(prod_qty) AS tf_qty
            ,ISNULL(SUM(material_cost), 0) + ISNULL(SUM(process_cost), 0) + ISNULL(SUM(pack_mat_cost), 0) AS tf_amount
        FROM istem_costing.dbo.tr_prod_fg
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND undelivery=0
            AND ISNULL(ci_no, '') <> ''
            AND wh_class IN ('7')
        GROUP BY
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)


    -- Production
    UPDATE T SET
        T.prod_qty=S.prod_qty
        ,T.prod_price=NULLIF(S.prod_amount, 0)/NULLIF(S.prod_qty, 0)
        ,T.prod_amount=S.prod_amount
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(prod_qty) AS prod_qty
            ,ISNULL(SUM(material_cost), 0) + ISNULL(SUM(process_cost), 0) + ISNULL(SUM(pack_mat_cost), 0) AS prod_amount
        FROM istem_costing.dbo.tr_prod_fg
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND ISNULL(ci_no, '') <> ''
            AND wh_class IN ('1', '6')
        GROUP BY
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)

    -- SALES A
    UPDATE T SET
        T.sales_a_price=NULLIF(( ISNULL(T.bf_amount, 0)+ISNULL(T.tf_amount, 0)+ISNULL(T.prod_amount, 0) ), 0)/NULLIF(( ISNULL(T.bf_qty, 0)+ISNULL(T.tf_qty, 0)+ISNULL(T.prod_qty, 0) ), 0)
        ,T.sales_c_price=NULLIF(( ISNULL(T.bf_amount, 0)+ISNULL(T.tf_amount, 0)+ISNULL(T.prod_amount, 0) ), 0)/NULLIF(( ISNULL(T.bf_qty, 0)+ISNULL(T.tf_qty, 0)+ISNULL(T.prod_qty, 0) ), 0)
        ,T.samp_price=NULLIF(( ISNULL(T.bf_amount, 0)+ISNULL(T.prod_amount, 0) ), 0)/NULLIF(( ISNULL(T.bf_qty, 0)+ISNULL(T.prod_qty, 0) ), 0)
        ,T.fac_price=NULLIF(( ISNULL(T.bf_amount, 0)+ISNULL(T.prod_amount, 0) ), 0)/NULLIF(( ISNULL(T.bf_qty, 0)+ISNULL(T.prod_qty, 0) ), 0)
        ,T.ret_price=NULLIF(( ISNULL(T.bf_amount, 0)+ISNULL(T.prod_amount, 0) ), 0)/NULLIF(( ISNULL(T.bf_qty, 0)+ISNULL(T.prod_qty, 0) ), 0)
        ,T.to_proc_price=NULLIF(( ISNULL(T.bf_amount, 0)+ISNULL(T.prod_amount, 0) ), 0)/NULLIF(( ISNULL(T.bf_qty, 0)+ISNULL(T.prod_qty, 0) ), 0)
        ,T.gitr_price=NULLIF(( ISNULL(T.bf_amount, 0)+ISNULL(T.prod_amount, 0) ), 0)/NULLIF(( ISNULL(T.bf_qty, 0)+ISNULL(T.prod_qty, 0) ), 0)
        ,T.tot_issue_price=NULLIF(( ISNULL(T.bf_amount, 0)+ISNULL(T.prod_amount, 0) ), 0)/NULLIF(( ISNULL(T.bf_qty, 0)+ISNULL(T.prod_qty, 0) ), 0)
    FROM @_TEMP AS T

    -- Sales A
    UPDATE T SET
        T.sales_a_qty=S.sales_a_qty
        ,T.sales_a_amount=S.sales_a_qty*T.sales_a_price
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(deliv_qty_mtr) AS sales_a_qty
        FROM istem_costing.dbo.tr_prod_fg_deliv
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND ISNULL(ci_no, '') <> ''
            AND dom_exp IN ('D')
            AND undelivery=0
            AND qty_unit IN ('MTR')
            AND deliv_class IN ('1')
        GROUP BY
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)

    -- Sales C
    UPDATE T SET
        T.sales_c_qty=S.sales_c_qty
        ,T.sales_c_amount=S.sales_c_qty*T.sales_c_price
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(deliv_qty_mtr) AS sales_c_qty
        FROM istem_costing.dbo.tr_prod_fg_deliv
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND ISNULL(ci_no, '') <> ''
            AND dom_exp IN ('D')
            AND undelivery=1
            AND qty_unit IN ('MTR')
            AND deliv_class IN ('2')
        GROUP BY
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)

    -- FACTORY
    UPDATE T SET
        T.fac_qty=S.fac_qty
        ,T.fac_amount=S.fac_qty*T.fac_price
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(deliv_qty_mtr) AS fac_qty
        FROM istem_costing.dbo.tr_prod_fg_deliv
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND ISNULL(ci_no, '') <> ''
            AND dom_exp IN ('D')
            AND undelivery=0
            AND qty_unit IN ('MTR')
            AND deliv_class IN ('4')
        GROUP BY
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)

    -- Return
    UPDATE T SET
        T.ret_qty=S.ret_qty
        ,T.ret_amount=S.ret_qty*T.ret_price
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(deliv_qty_mtr) AS ret_qty
        FROM istem_costing.dbo.tr_prod_fg_deliv
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND ISNULL(ci_no, '') <> ''
            AND dom_exp IN ('D')
            AND undelivery=0
            AND qty_unit IN ('MTR')
            AND deliv_class IN ('5')
        GROUP BY
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)

    -- CF
    UPDATE T SET
        T.cf_qty=ISNULL(T.bf_qty, 0)+ISNULL(T.tf_qty, 0)+ISNULL(T.prod_qty, 0)+ISNULL(T.tot_issue_qty, 0)
        ,T.cf_amount=ISNULL(T.bf_amount, 0)+ISNULL(T.tf_amount, 0)+ISNULL(T.prod_amount, 0)+ISNULL(T.tot_issue_amount, 0)
    FROM @_TEMP AS T

    UPDATE T SET
        T.cf_price=NULLIF(T.cf_amount, 0)/NULLIF(T.cf_qty, 0)
    FROM @_TEMP AS T

    DECLARE @im INTEGER
    SET @im = (
        SELECT
            COUNT(grey_no)
        FROM istem_costing.dbo.tr_fg_price
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND ISNULL(grey_no, '')='')

    IF @im >= 1
    BEGIN
        DECLARE @im_price INTEGER
        SET @im_price = (
            SELECT
                impair_fg_price
            FROM istem_costing.dbo.tr_fg_price
            WHERE
                comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept=@dept
                AND ISNULL(grey_no, '')='')

        UPDATE T SET
            T.impairment_price=@im_price
        FROM @_TEMP AS T
    END
    ELSE
    BEGIN
        UPDATE T SET
            T.impairment_price=CASE WHEN S.impairment_price IS NULL THEN T.cf_price ELSE S.impairment_price END
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT
                grey_no
                ,dom_exp
                ,undelivery
                ,impair_fg_price AS impairment_price
            FROM istem_costing.dbo.tr_fg_price
            WHERE
                comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept=@dept
                AND ISNULL(grey_no, '')=''
        ) AS S ON (S.grey_no=T.grey_no AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery)
    END

    UPDATE T SET
        T.sales_amount=NULLIF(T.cf_qty, 0)/NULLIF(T.impairment_price, 0)
        ,T.impairment_amount=ISNULL(T.cf_amount, 0)-ISNULL(NULLIF(T.cf_qty, 0)/NULLIF(T.impairment_price, 0), 0)
    FROM @_TEMP AS T


    UPDATE T SET
        T.cf_final_qty=NULLIF(T.cf_qty, 0)
        ,T.cf_final_price=NULLIF(ISNULL(T.cf_amount, 0)-ISNULL(T.impairment_amount, 0), 0)/NULLIF(T.cf_qty, 0)
        ,T.cf_final_amount=ISNULL(T.cf_amount, 0)-ISNULL(T.impairment_amount, 0)
    FROM @_TEMP AS T

    UPDATE T SET
        T.sales_a_price=CASE WHEN ISNULL(T.sales_a_qty, 0)=0 THEN NULL ELSE T.sales_a_price END
        ,T.sales_c_price=CASE WHEN ISNULL(T.sales_c_qty, 0)=0 THEN NULL ELSE T.sales_c_price END
        ,T.samp_price=CASE WHEN ISNULL(T.samp_qty, 0)=0 THEN NULL ELSE T.samp_price END
        ,T.fac_price=CASE WHEN ISNULL(T.fac_qty, 0)=0 THEN NULL ELSE T.fac_price END
        ,T.ret_price=CASE WHEN ISNULL(T.ret_qty, 0)=0 THEN NULL ELSE T.ret_price END
        ,T.to_proc_price=CASE WHEN ISNULL(T.to_proc_qty, 0)=0 THEN NULL ELSE T.to_proc_price END
        ,T.gitr_price=CASE WHEN ISNULL(T.gitr_qty, 0)=0 THEN NULL ELSE T.gitr_price END
        ,T.tot_issue_price=CASE WHEN ISNULL(T.tot_issue_qty, 0)=0 THEN NULL ELSE T.tot_issue_price END
    FROM @_TEMP AS T

    SELECT * FROM @_TEMP
    WHERE
    ISNULL(cf_qty, 0) <> 0

    DECLARE @proc_time AS DATETIME
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_no INT
    SET @proc_time = GETDATE()
    SET @rec_sts='A'
    SET @proc_no=(SELECT ISNULL(MAX(proc_no), 0)+1 FROM istem_costing.dbo.tr_bal_fg WHERE
            comp_id=@comp_id
        AND f_year=@f_year
        AND f_month=@f_month
        AND dept=@dept)

    IF @proc_no >= 2
    BEGIN
        SET @rec_sts='T'
    END
    DELETE FROM istem_costing.dbo.tr_bal_fg WHERE
            comp_id=@comp_id
        AND f_year=@f_year
        AND f_month=@f_month
        AND dept=@dept
    INSERT INTO istem_costing.dbo.tr_bal_fg
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,grey_no
            ,ci_no
            ,wh_class
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,cf_fg_qty
            ,cf_fg_amount
            ,cf_fg_amount_ori
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
             @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept AS dept
            ,A.grey_no AS grey_no
            ,A.ci_no AS ci_no
            ,'' AS wh_class
            ,A.dom_exp AS dom_exp
            ,A.undelivery AS undelivery
            ,A.qty_unit AS qty_unit
            ,A.cf_final_qty AS cf_fg_qty
            ,A.cf_final_amount AS cf_fg_amount
            ,A.cf_amount AS cf_fg_amount_ori
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
        FROM @_TEMP AS A
END
