

ALTER PROCEDURE  [sp_GetWarpYarnCodeByGreyNo]
	@GreyNo VARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	
	DECLARE @_TEPM TABLE(
		yarn_code VARCHAR(MAX)
		,yarn_name VARCHAR(MAX)
	)

	INSERT INTO @_TEPM
	SELECT 
	A.yarn_code
	,B.yarn_name
	FROM 
	(
		SELECT A.warp1_yarncode AS yarn_code
		FROM istem_sms.dbo.wv_fabric_analysis_master AS A
		WHERE (A.grey_no = @GreyNo)
		
		UNION ALL
		
		SELECT A.warp2_yarncode AS yarn_code
		FROM istem_sms.dbo.wv_fabric_analysis_master AS A
		WHERE (A.grey_no = @GreyNo)
		
		UNION ALL
		
		SELECT A.warp3_yarncode AS yarn_code
		FROM istem_sms.dbo.wv_fabric_analysis_master AS A
		WHERE (A.grey_no = @GreyNo)
		
		UNION ALL
		
		SELECT  A.warp4_yarncode AS yarn_code
		FROM istem_sms.dbo.wv_fabric_analysis_master AS A
		WHERE (A.grey_no = @GreyNo)
		
	) AS A
	LEFT JOIN istem_sms.dbo.sp_ms_yarn AS B ON (B.yarn_code=A.yarn_code)

	SELECT * FROM @_TEPM
END


