-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE  [sp_RptAcctWIPMonthly]
	@comp_id INT, @f_year INT, @f_month INT, @dept INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SELECT
	   comp_id
      ,dept
      ,f_year
      ,f_month
      ,mat_code
      ,mat_name
      ,product_item
      ,product_item2
      ,bf_qty
      ,CASE WHEN (ISNULL(bf_qty, 0) = 0 OR ISNULL(bf_amount, 0) = 0) THEN NULL ELSE bf_price END AS bf_price
      ,bf_amount
      ,rc_qty
      ,CASE WHEN (ISNULL(rc_qty, 0) = 0 OR ISNULL(rc_amount, 0) = 0) THEN NULL ELSE rc_price END AS rc_price
      ,rc_amount
      ,co_qty
      ,CASE WHEN (ISNULL(co_qty, 0) = 0 OR ISNULL(co_amount, 0) = 0) THEN NULL ELSE co_price END AS co_price
      ,co_amount
      ,cf_qty
      ,CASE WHEN (ISNULL(cf_qty, 0) = 0 OR ISNULL(cf_amount, 0) = 0) THEN NULL ELSE cf_price END AS cf_price
      ,cf_amount
      ,wh_conv_qty
      ,var_num1
      ,var_num2
      ,var_num3
      ,var_num4
      ,var_num5
      ,remark1
      ,remark2
      ,remark3
      ,remark4
      ,remark5
      ,generate_date
    FROM istem_costing.dbo.acct_wip_report
	WHERE
		comp_id = @comp_id
		AND f_year=@f_year
		AND f_month=@f_month
		AND dept=@dept
END
