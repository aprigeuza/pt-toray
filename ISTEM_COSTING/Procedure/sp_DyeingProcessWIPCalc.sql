ALTER PROCEDURE  sp_DyeingProcessWIPCalc
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;
    -- WIP CALC
    -- Dyeing- Balance WIP

    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=7
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'
    DECLARE @dept_10 INT
    DECLARE @dept_11 INT
    DECLARE @dept_40 INT
    DECLARE @dept_50 INT
    DECLARE @dept_99 INT
    DECLARE @proc_time AS DATETIME
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_no INT

    SET @dept_10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=10))
    SET @dept_11=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=11))
    SET @dept_40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40))
    SET @dept_50=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=50))
    SET @dept_99=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=99))
    SET @proc_time=GETDATE()
    SET @rec_sts='A'

    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT

    IF @f_month = 1
    BEGIN
        SET @lp_f_month = 12
        SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
        SET @lp_f_month = @f_month - 1
        SET @lp_f_year = @f_year
    END

    --Konversi period SAP ke CIM
    DECLARE @target_cim_f_year INT
    DECLARE @target_cim_f_month INT

    IF (@f_month + 3) > 12
    BEGIN
        SET @target_cim_f_month = (@f_month - 9)
        SET @target_cim_f_year = @f_year + 1
    END
    ELSE
    BEGIN
        SET @target_cim_f_month = @f_month + 3
        SET @target_cim_f_year = @f_year
    END

    DECLARE @_TEMP TABLE(
        status VARCHAR(MAX)
        ,item_category VARCHAR(MAX)
        ,cost_sheet_id VARCHAR(MAX)
        ,mat_usage VARCHAR(MAX)
        ,mat_code VARCHAR(MAX)
        ,item_code VARCHAR(MAX)
        ,mat_name VARCHAR(MAX)
        ,qty_unit VARCHAR(MAX)

        ,bf_qty_pcs NUMERIC(14,4)
        ,bf_qty_mtr NUMERIC(14,4)
        ,bf_price NUMERIC(14,4)
        ,bf_amount NUMERIC(14,4)

        ,rc_qty_pcs NUMERIC(14,4)
        ,rc_qty_mtr_wv NUMERIC(14,4)
        ,rc_qty_mtr_dy NUMERIC(14,4)
        ,rc_price NUMERIC(14,4)
        ,rc_amount NUMERIC(14,4)

        ,co_dy_qty_pcs NUMERIC(14,4)
        ,co_dy_qty_mtr NUMERIC(14,4)
        ,co_dy_price NUMERIC(14,4)
        ,co_dy_amount NUMERIC(14,4)

        ,ad_qty_pcs NUMERIC(14,4)
        ,ad_qty_mtr NUMERIC(14,4)
        ,ad_price NUMERIC(14,4)
        ,ad_amount NUMERIC(14,4)

        ,cf_qty_pcs NUMERIC(14,4)
        ,cf_qty_mtr NUMERIC(14,4)
        ,cf_price NUMERIC(14,4)
        ,cf_amount NUMERIC(14,4)

        ,r_flag VARCHAR(MAX)

        ,so VARCHAR(MAX) DEFAULT NULL
    )

    INSERT INTO @_TEMP (status, item_category, mat_code, mat_name, r_flag, so)
        SELECT
            CASE WHEN A.r_flag='R' THEN 'RETURN' ELSE 'NORMAL' END AS status
            ,B.item_category
            ,A.mat_code AS mat_code
            ,ISNULL(M.FrgnName, '') AS mat_name
            ,ISNULL(A.r_flag, '') AS r_flag
            ,CASE
                WHEN A.r_flag='R' THEN
                    CASE
                        WHEN B.item_category = 'TR' THEN 'B001'
                        WHEN B.item_category = 'SP' THEN 'B002'
                        ELSE 'B999'
                    END
                ELSE
                    CASE
                        WHEN B.item_category = 'TR' THEN 'A001'
                        WHEN B.item_category = 'SP' THEN 'A002'
                        ELSE 'A999'
                    END
            END AS so
        FROM (
            SELECT A.mat_code, A.r_flag
            FROM istem_costing.dbo.tr_bal_wip AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@lp_f_year
                AND A.f_month=@lp_f_month
                AND A.dept=@dept_40
            UNION ALL
            SELECT A.mat_code, '' AS r_flag
            FROM istem_costing.dbo.tr_dept_calc AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
                AND A.dept IN (@dept_40)
                AND A.tr_code IN ('RCVRM', 'WIP')
        ) AS A
        LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM AS M ON (M.ItemCode COLLATE SQL_Latin1_General_CP1_CI_AS = A.mat_code COLLATE SQL_Latin1_General_CP1_CI_AS)
        LEFT JOIN (
            SELECT grey_no, item_category
            FROM istem_sms.dbo.wv_fabric_analysis_master
            GROUP BY grey_no, item_category
        ) AS B ON (B.grey_no=A.mat_code)
        GROUP BY A.r_flag, A.mat_code, ISNULL(M.FrgnName, ''), B.item_category

    -- Balance
    UPDATE T SET
             T.bf_qty_pcs = S.bf_qty_pcs
            ,T.bf_qty_mtr = S.bf_qty_mtr
            ,T.bf_price = NULLIF(S.bf_amount, 0)/NULLIF(S.bf_qty_mtr, 0)
            ,T.bf_amount = S.bf_amount
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT A.mat_code, A.cf_wip_qty AS bf_qty_mtr, A.cf_wip_pcs AS bf_qty_pcs, A.cf_wip_amount AS bf_amount, A.r_flag AS r_flag
            FROM istem_costing.dbo.tr_bal_wip AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@lp_f_year
                AND A.f_month=@lp_f_month
                AND A.dept IN (@dept_40)
        ) AS S ON (S.mat_code=T.mat_code AND ISNULL(S.r_flag, '')=ISNULL(T.r_flag, ''))

    -- Receive
    UPDATE T SET
             T.rc_qty_pcs = S.rc_qty_pcs
            ,T.rc_qty_mtr_dy = S.rc_qty_mtr_dy
            ,T.rc_price = NULLIF(S.rc_amount, 0)/NULLIF(S.rc_qty_mtr_dy, 0)
            ,T.rc_amount = S.rc_amount
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT
                A.mat_code
                ,A.mat_qty AS rc_qty_mtr_dy
                ,A.qty_pcs AS rc_qty_pcs
                ,A.mat_amount AS rc_amount
                ,A.mc_loc AS r_flag
            FROM istem_costing.dbo.tr_dept_calc AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
                AND A.dept IN (@dept_40)
                AND A.tr_code = 'RCVRM'
        ) AS S ON (S.mat_code=T.mat_code AND ISNULL(S.r_flag, '')=ISNULL(T.r_flag, ''))

    UPDATE T SET
            T.rc_qty_mtr_wv = S.rc_qty_mtr_wv
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT
                A.mat_code
                ,A.out_invoice_qty AS rc_qty_mtr_wv
            FROM istem_costing.dbo.tr_inv_out_detail AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
                AND A.dept IN (@dept_11)
                AND A.out_dest IN (@dept_40)
        ) AS S ON (S.mat_code=T.mat_code)
        WHERE ISNULL(T.r_flag, '')=''

    -- Update price
    UPDATE T SET
            T.co_dy_price = NULLIF((ISNULL(T.bf_amount, 0) + ISNULL(T.rc_amount, 0) ), 0)/NULLIF((ISNULL(T.bf_qty_mtr, 0) + ISNULL(T.rc_qty_mtr_dy, 0)), 0)
            ,T.ad_price = NULLIF((ISNULL(T.bf_amount, 0) + ISNULL(T.rc_amount, 0) ), 0)/NULLIF((ISNULL(T.bf_qty_mtr, 0) + ISNULL(T.rc_qty_mtr_dy, 0)), 0)
            ,T.cf_price = NULLIF((ISNULL(T.bf_amount, 0) + ISNULL(T.rc_amount, 0) ), 0)/NULLIF((ISNULL(T.bf_qty_mtr, 0) + ISNULL(T.rc_qty_mtr_dy, 0)), 0)
        FROM @_TEMP AS T

    -- CF
    UPDATE T SET
             T.cf_qty_pcs = S.cf_qty_pcs
            ,T.cf_qty_mtr = S.cf_qty_mtr
            ,T.cf_amount = T.cf_price * S.cf_qty_mtr
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT
                A.mat_code
                ,A.qty_pcs AS cf_qty_pcs
                ,A.mat_qty AS cf_qty_mtr
                ,A.mat_amount AS cf_amount
                ,A.mc_loc AS r_flag
            FROM istem_costing.dbo.tr_dept_calc AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
                AND A.dept IN (@dept_40)
                AND A.tr_code = 'WIP'
        ) AS S ON (S.mat_code=T.mat_code AND ISNULL(S.r_flag, '')=ISNULL(T.r_flag, ''))

    -- Adjustment
    -- UPDATE T SET
    --      T.ad_qty_pcs = T.rc_qty_mtr_wv
    --     ,T.ad_qty_mtr = T.rc_qty_mtr_wv
    --     ,T.ad_amount = T.rc_qty_mtr_wv
    -- FROM @_TEMP AS T

    -- Consume
    UPDATE T SET
             T.co_dy_qty_pcs = NULLIF((ISNULL(T.bf_qty_pcs, 0) + ISNULL(T.rc_qty_pcs, 0)) - (ISNULL(T.ad_qty_pcs, 0) + ISNULL(T.cf_qty_pcs, 0)), 0)
            ,T.co_dy_qty_mtr = NULLIF(( (ISNULL(T.bf_qty_mtr, 0) + ISNULL(T.rc_qty_mtr_dy, 0)) - (ISNULL(T.ad_qty_mtr, 0) + ISNULL(T.cf_qty_mtr, 0)) ), 0)
            ,T.co_dy_amount = NULLIF(( (ISNULL(T.bf_qty_mtr, 0) + ISNULL(T.rc_qty_mtr_dy, 0)) - (ISNULL(T.ad_qty_mtr, 0) + ISNULL(T.cf_qty_mtr, 0)) ), 0) * T.co_dy_price
        FROM @_TEMP AS T

    UPDATE T SET
            T.cost_sheet_id=S.cost_sheet_id
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT
                 A.mat_code
                ,A.cost_sheet_id
            FROM (
                SELECT
                     A.mat_code
                    ,A.cost_sheet_id
                FROM istem_costing.dbo.tr_bal_wip AS A
                WHERE
                        A.comp_id=@comp_id
                    AND A.f_year=@lp_f_year
                    AND A.f_month=@lp_f_month
                    AND A.dept IN (@dept_40)
                UNION ALL
                SELECT
                     A.grey_no AS mat_code
                    ,CASE WHEN A.proc_by <> 'COM' THEN 'A30' ELSE 'A31' END AS cost_sheet_id
                FROM istem_sms.dbo.wv_fabric_analysis_master AS A
            ) AS A
            WHERE ISNULL(A.mat_code, '') <> '' AND ISNULL(A.cost_sheet_id, '') <> ''
            GROUP BY A.mat_code, A.cost_sheet_id
        ) AS S ON (S.mat_code = T.mat_code)

    UPDATE T SET
            T.item_code=T.mat_code
        FROM @_TEMP AS T

    UPDATE T SET
            T.qty_unit=S.qty_unit
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT
                 A.mat_code
                ,A.qty_unit
            FROM (
                SELECT
                     A.mat_code
                    ,A.qty_unit
                FROM istem_costing.dbo.tr_bal_wip AS A
                WHERE
                        A.comp_id=@comp_id
                    AND A.f_year=@lp_f_year
                    AND A.f_month=@lp_f_month
                    AND A.dept IN (@dept_40)
                UNION ALL
                SELECT
                     A.mat_code
                    ,A.mat_qty_unit AS qty_unit
                FROM istem_costing.dbo.tr_dept_calc AS A
                WHERE
                        A.comp_id=@comp_id
                    AND A.f_year=@f_year
                    AND A.f_month=@f_month
                    AND A.dept IN (@dept_40)
            ) AS A
            WHERE ISNULL(A.mat_code, '') <> '' AND ISNULL(A.qty_unit, '') <> ''
            GROUP BY A.mat_code, A.qty_unit
        ) AS S ON (S.mat_code = T.mat_code)


    SET @rec_sts='A'
    SET @proc_no=(SELECT ISNULL(MAX(A.proc_no), 0) + 1 FROM istem_costing.dbo.tr_wip_hist AS A WHERE
            A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept IN (@dept_40))
    IF @proc_no >= 2
    BEGIN
        SET @rec_sts = 'T'
    END

    -- INSERT INTO istem_costing.dbo.tr_wip_hist
    --     SELECT * FROM istem_costing.dbo.tr_wip AS A
    --         WHERE
    --                 A.comp_id=@comp_id
    --             AND A.f_year=@f_year
    --             AND A.f_month=@f_month
    --             AND A.dept IN (@dept_40)

    -- DELETE FROM istem_costing.dbo.tr_wip
    --     WHERE
    --             comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (@dept_40)

    INSERT INTO istem_costing.dbo.tr_wip
        (
           comp_id
           ,f_year
           ,f_month
           ,dept
           ,cost_sheet_id
           ,mat_code
           ,item_category
           ,item_code
           ,qty_unit
           ,rcv_mat_qty
           ,rcv_mat_amount
           ,cons_mat_qty
           ,cons_mat_amount
           ,cf_wip_qty
           ,cf_wip_amount
           ,adj_wip_qty
           ,adj_wip_amount
           ,proc_time
           ,user_id
           ,client_ip
           ,rec_sts
           ,proc_no
           ,r_flag
        )
        SELECT
             @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept_40 AS dept
            ,A.cost_sheet_id AS cost_sheet_id
            ,A.mat_code AS mat_code
            ,A.item_category AS item_category
            ,A.item_code AS item_code
            ,A.qty_unit AS qty_unit
            ,A.rc_qty_mtr_dy AS rcv_mat_qty
            ,A.rc_amount AS rcv_mat_amount
            ,A.co_dy_qty_mtr AS cons_mat_qty
            ,A.co_dy_amount AS cons_mat_amount
            ,A.cf_qty_mtr AS cf_wip_qty
            ,A.cf_amount AS cf_wip_amount
            ,A.ad_qty_mtr AS adj_wip_qty
            ,A.ad_amount AS adj_wip_amount
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,A.r_flag
        FROM @_TEMP AS A
END
