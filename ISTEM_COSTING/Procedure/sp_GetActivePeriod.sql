ALTER PROCEDURE  [sp_GetActivePeriod]
	@comp_id INT
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    SELECT *
	FROM istem_costing.dbo.v_SAPPeriod
	WHERE
		(comp_id=1 AND period_status IN ('N', 'C'))
	ORDER BY f_year, f_month ASC
END
