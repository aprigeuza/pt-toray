
-- =============================================
-- Author:		Apri
-- Create date: 2020-08-28
-- Description:
--				Yarn Product Monthly
--				Report Untuk Logistic
-- =============================================
ALTER PROCEDURE  [sp_RptLogisticYarnProductMonthly]
	@COMP_ID INT, @F_YEAR INT, @F_MONTH INT
AS
BEGIN
	SET NOCOUNT ON;

	DECLARE @LP_f_year INT, @LP_f_month INT

	IF @F_Month = 1
	BEGIN
		SET @LP_f_month = 12
		SET @LP_f_year = @F_Year - 1
	END
	ELSE
	BEGIN
		SET @LP_f_month = @F_Month - 1
		SET @LP_f_year = @F_Year
	END

	DECLARE @dept INT

	SET @dept = (SELECT TOP 1 dept FROM ms_dept WHERE comp_id=@COMP_ID AND dept_seq=10)

	SELECT
		MT.mat_code  AS MATERIAL_CODE
		,ISNULL(M.FrgnName, '') AS MATERIAL_NAME

		,NULLIF(BF_L.BF_L, 0) AS BF_L -- A
		,NULLIF(BF_P.BF_P, 0) AS BF_P -- B
		,NULLIF((ISNULL(BF_L.BF_L, 0) + ISNULL(BF_P.BF_P, 0)), 0) AS BF_TOTAL -- C

		,NULLIF(RC_L.RC_L, 0) AS RC_L -- D
		,NULLIF(RC_P.RC_P, 0) AS RC_P -- E
		,NULLIF((ISNULL(RC_L.RC_L, 0) + ISNULL(RC_P.RC_P, 0) ), 0) AS RC_TOTAL -- F

		,NULLIF(CWV_L.CWV_L, 0) AS CWV_L -- G
		,NULLIF(CWV_P.CWV_P, 0) AS CWV_P -- H
		,NULLIF(CSALES.CSALES, 0) AS CSALES -- I
		,NULLIF(CDYING.CDYING, 0) AS CDYING -- I
		,NULLIF(COTHER.COTHER, 0) AS COTHER -- J
		,NULLIF((ISNULL(CWV_L.CWV_L, 0)+ISNULL(CWV_P.CWV_P, 0)+ISNULL(CSALES.CSALES, 0)+ISNULL(CDYING.CDYING, 0)+ISNULL(COTHER.COTHER, 0)), 0) AS C_TOTAL

		-- (A+D) - (G+J)
		,NULLIF((ISNULL(BF_L.BF_L, 0) + ISNULL(RC_L.RC_L, 0)) - (ISNULL(CWV_L.CWV_L, 0)+ISNULL(CSALES.CSALES, 0)+ISNULL(CDYING.CDYING, 0)+ISNULL(COTHER.COTHER, 0)), 0) AS CF_L
		,NULLIF((ISNULL(BF_P.BF_P, 0) + ISNULL(RC_P.RC_P, 0)) - (ISNULL(CWV_P.CWV_P, 0)), 0) AS CF_P
		,NULLIF((ISNULL(BF_L.BF_L, 0) + ISNULL(RC_L.RC_L, 0)) - (ISNULL(CWV_L.CWV_L, 0)+ISNULL(CSALES.CSALES, 0)+ISNULL(CDYING.CDYING, 0)+ISNULL(COTHER.COTHER, 0))
		+(ISNULL(BF_P.BF_P, 0) + ISNULL(RC_P.RC_P, 0)) - (ISNULL(CWV_P.CWV_P, 0)), 0) AS CF_TOTAL
	FROM
	(
		SELECT * FROM (
			SELECT mat_code FROM tr_bal_mat WHERE (comp_id = @COMP_ID) AND (f_year = @LP_f_year) AND (f_month = @LP_f_month) AND (dept = @dept) AND (cost_sheet_id IN ('A20')) GROUP BY mat_code, mat_usage
			UNION ALL
			SELECT mat_code FROM tr_inv_in WHERE (comp_id = @COMP_ID) AND (f_year = @F_YEAR) AND (f_month = @F_MONTH) AND (dept = @dept) AND (cost_sheet_id IN ('A20')) GROUP BY mat_code, mat_usage
			UNION ALL
			SELECT mat_code FROM tr_inv_out_detail WHERE (comp_id = @COMP_ID) AND (f_year = @F_YEAR) AND (f_month = @F_MONTH) AND (dept = @dept) AND (cost_sheet_id IN ('A20')) GROUP BY mat_code, mat_usage
		) AS A GROUP BY mat_code
	) AS MT
	INNER JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM AS M ON (M.ItemCode COLLATE SQL_Latin1_General_CP1_CI_AS = MT.mat_code COLLATE SQL_Latin1_General_CP1_CI_AS)

	-- BF L & P
	LEFT JOIN (
		SELECT mat_code, SUM(ISNULL(cf_qty, 0)) AS BF_L FROM tr_bal_mat WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@LP_f_year)AND(f_month=@LP_f_month)AND(cost_sheet_id IN ('A20'))AND(mat_usage='L') GROUP BY mat_code
	) AS BF_L ON (BF_L.mat_code=MT.mat_code)
	LEFT JOIN (
		SELECT mat_code, SUM(ISNULL(cf_qty, 0)) AS BF_P FROM tr_bal_mat WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@LP_f_year)AND(f_month=@LP_f_month)AND(cost_sheet_id IN ('A20'))AND(mat_usage='P') GROUP BY mat_code
	) AS BF_P ON (BF_P.mat_code=MT.mat_code)


	-- Receive L & P
	LEFT JOIN (
		SELECT mat_code, SUM(ISNULL(in_qty, 0)) AS RC_L FROM tr_inv_in WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@F_YEAR)AND(f_month=@F_MONTH)AND(cost_sheet_id IN ('A20'))AND(mat_usage='L') GROUP BY mat_code
	) AS RC_L ON (RC_L.mat_code=MT.mat_code)
	LEFT JOIN (
		SELECT mat_code, SUM(ISNULL(in_qty, 0)) AS RC_P FROM tr_inv_in WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@F_YEAR)AND(f_month=@F_MONTH)AND(cost_sheet_id IN ('A20'))AND(mat_usage='P') GROUP BY mat_code
	) AS RC_P ON (RC_P.mat_code=MT.mat_code)


	-- Consume
	LEFT JOIN (
		SELECT mat_code, SUM(ISNULL(out_qty, 0)) AS CWV_L FROM tr_inv_out_detail WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@F_YEAR)AND(f_month=@F_MONTH)AND(cost_sheet_id IN ('A20'))AND(mat_usage='L')AND(out_dest=800) GROUP BY mat_code
	) AS CWV_L ON (CWV_L.mat_code=MT.mat_code)
	LEFT JOIN (
		SELECT mat_code, SUM(ISNULL(out_qty, 0)) AS CWV_P FROM tr_inv_out_detail WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@F_YEAR)AND(f_month=@F_MONTH)AND(cost_sheet_id IN ('A20'))AND(mat_usage='P')AND(out_dest=800) GROUP BY mat_code
	) AS CWV_P ON (CWV_P.mat_code=MT.mat_code)
	LEFT JOIN (
		SELECT mat_code, SUM(ISNULL(out_qty, 0)) AS CSALES FROM tr_inv_out_detail WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@F_YEAR)AND(f_month=@F_MONTH)AND(cost_sheet_id IN ('A20'))AND(out_dest=300) GROUP BY mat_code
	) AS CSALES ON (CSALES.mat_code=MT.mat_code)
	LEFT JOIN (
		SELECT mat_code, SUM(ISNULL(out_qty, 0)) AS CDYING FROM tr_inv_out_detail WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@F_YEAR)AND(f_month=@F_MONTH)AND(cost_sheet_id IN ('A20'))AND(out_dest=900) GROUP BY mat_code
	) AS CDYING ON (CDYING.mat_code=MT.mat_code)
	LEFT JOIN (
		SELECT mat_code, SUM(ISNULL(out_qty, 0)) AS COTHER FROM tr_inv_out_detail WHERE (dept=@dept)AND(comp_id=@COMP_ID)AND(f_year=@F_YEAR)AND(f_month=@F_MONTH)AND(cost_sheet_id IN ('A20'))AND(out_dest NOT IN (800, 300, 900)) GROUP BY mat_code
	) AS COTHER ON (COTHER.mat_code=MT.mat_code)
	ORDER BY M.ItmsGrpCod, MT.mat_code

END
