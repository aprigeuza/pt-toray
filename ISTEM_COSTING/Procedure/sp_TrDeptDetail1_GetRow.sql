

ALTER PROCEDURE  [sp_TrDeptDetail1_GetRow]
	@comp_id INT, 
	@f_year INT,
	@f_month INT,
	@tr_code NVARCHAR,
	@mc_loc NVARCHAR,
	@mc_proc_code NVARCHAR,
	@stock_take_date Date,
	@mc_dtl_code NVARCHAR,
	@item_code NVARCHAR
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT
	  *
	FROM [istem_costing].[dbo].[tr_dept_detail1] AS d
	INNER JOIN tr_dept_header AS h 
		ON (h.comp_id = d.comp_id)
		AND (h.f_year = d.f_year)
		AND (h.f_month = d.f_month)
		AND (h.dept = d.dept)
		AND (h.tr_code = d.tr_code)
		AND (h.mc_loc = d.mc_loc)
		AND (h.mc_proc_code = d.mc_proc_code)
	WHERE 
		(h.comp_id=@comp_id)
		AND
		(h.f_year=@f_year)
		AND
		(h.f_month=@f_month)
		AND
		(h.tr_code=@tr_code)
		AND
		(h.mc_loc=@mc_loc)
		AND
		(h.mc_proc_code=@mc_proc_code)
		AND
		(h.stock_take_date=@stock_take_date)
		AND
		(d.mc_dtl_code=@mc_dtl_code)
		AND 
		(d.item_code=@item_code)
END


