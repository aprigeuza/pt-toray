ALTER PROCEDURE  sp_DyeingProcessGreyCostLogtWeavingGreyBal
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;

    -- Grey Cost
    -- SUB2 : Logistic Weaving Grey Balance
    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=7
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept_10 INT
    DECLARE @dept_11 INT
    DECLARE @dept_40 INT
    DECLARE @dept_50 INT
    DECLARE @dept_99 INT
    DECLARE @proc_time AS DATETIME
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_no INT

    SET @dept_10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=10))
    SET @dept_11=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=11))
    SET @dept_40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40))
    SET @dept_50=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=50))
    SET @dept_99=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=99))
    SET @proc_time=GETDATE()
    SET @rec_sts='A'


    DECLARE @_TEMP TABLE(
        item_category VARCHAR(MAX)
        ,cost_sheet_id VARCHAR(MAX)
        ,mat_code VARCHAR(MAX)
        ,mat_name VARCHAR(MAX)
        ,bf_qty_pcs NUMERIC(14,4)
        ,bf_qty_mtr NUMERIC(14,4)
        ,bf_price NUMERIC(14,4)
        ,bf_amount NUMERIC(14,4)
        ,rc_qty_pcs NUMERIC(14,4)
        ,rc_qty_mtr NUMERIC(14,4)
        ,rc_price NUMERIC(14,4)
        ,rc_amount NUMERIC(14,4)
        ,co_dy_qty_pcs NUMERIC(14,4)
        ,co_dy_qty_mtr_wv NUMERIC(14,4)
        ,co_dy_qty_mtr_dy NUMERIC(14,4)
        ,co_dy_price NUMERIC(14,4)
        ,co_dy_amount NUMERIC(14,4)

        ,co_exp_qty_pcs NUMERIC(14,4)
        ,co_exp_qty_mtr_wv NUMERIC(14,4)
        ,co_exp_price NUMERIC(14,4)
        ,co_exp_amount NUMERIC(14,4)

        ,co_dom_qty_pcs NUMERIC(14,4)
        ,co_dom_qty_mtr_wv NUMERIC(14,4)
        ,co_dom_price NUMERIC(14,4)
        ,co_dom_amount NUMERIC(14,4)

        ,co_test_qty_pcs NUMERIC(14,4)
        ,co_test_qty_mtr_wv NUMERIC(14,4)
        ,co_test_price NUMERIC(14,4)
        ,co_test_amount NUMERIC(14,4)

        ,co_pengantar_qty_pcs NUMERIC(14,4)
        ,co_pengantar_qty_mtr_wv NUMERIC(14,4)
        ,co_pengantar_price NUMERIC(14,4)
        ,co_pengantar_amount NUMERIC(14,4)

        ,cf_qty_pcs NUMERIC(14,4)
        ,cf_qty_mtr NUMERIC(14,4)
        ,cf_price NUMERIC(14,4)
        ,cf_amount NUMERIC(14,4)
        ,so VARCHAR(MAX) DEFAULT NULL
    )

    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT

    IF @f_month = 1
    BEGIN
        SET @lp_f_month = 12
        SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
        SET @lp_f_month = @f_month - 1
        SET @lp_f_year = @f_year
    END

    INSERT INTO @_TEMP (item_category, cost_sheet_id, mat_code, mat_name)
        SELECT
            B.item_category
            ,A.cost_sheet_id
            ,A.mat_code AS mat_code
            ,ISNULL(M.FrgnName, '') AS mat_name
        FROM (
            SELECT A.cost_sheet_id, A.mat_code
            FROM istem_costing.dbo.tr_bal_mat AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@lp_f_year
                AND A.f_month=@lp_f_month
                AND A.dept=@dept_11
                AND A.cost_sheet_id IN ('A30', 'A31')
            UNION ALL
            SELECT A.cost_sheet_id, A.mat_code
            FROM istem_costing.dbo.tr_inv_in AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
                AND A.dept IN (@dept_11)
                AND A.cost_sheet_id IN ('A30', 'A31')
            UNION ALL
            SELECT A.cost_sheet_id, A.mat_code
            FROM istem_costing.dbo.tr_inv_out_detail AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
                AND A.dept IN (@dept_11)
                AND A.cost_sheet_id IN ('A30', 'A31')
        ) AS A
        LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM AS M ON (M.ItemCode COLLATE SQL_Latin1_General_CP1_CI_AS = A.mat_code COLLATE SQL_Latin1_General_CP1_CI_AS)
        LEFT JOIN (
            SELECT grey_no, item_category
            FROM istem_sms.dbo.wv_fabric_analysis_master
            GROUP BY grey_no, item_category
        ) AS B ON (B.grey_no=A.mat_code)
        GROUP BY A.mat_code, ISNULL(M.FrgnName, ''), A.cost_sheet_id, B.item_category

    UPDATE T SET
            bf_qty_pcs = S.bf_qty_pcs
            ,bf_qty_mtr = S.bf_qty_mtr
            ,bf_price = NULLIF(S.bf_amount, 0)/NULLIF(S.bf_qty_mtr, 0)
            ,bf_amount = S.bf_amount
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT A.mat_code, A.cf_qty AS bf_qty_mtr, A.cf_qty_pcs AS bf_qty_pcs, A.cf_amount AS bf_amount
            FROM istem_costing.dbo.tr_bal_mat AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@lp_f_year
                AND A.f_month=@lp_f_month
                AND A.dept IN (@dept_11)
                AND A.cost_sheet_id IN ('A30', 'A31')
        ) AS S ON (S.mat_code=T.mat_code)

    UPDATE T SET
        rc_qty_pcs = S.rc_qty_pcs
        ,rc_qty_mtr = S.rc_qty_mtr
        ,rc_price = NULLIF(S.rc_amount, 0)/NULLIF(S.rc_qty_mtr, 0)
        ,rc_amount = S.rc_amount
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT A.mat_code, A.in_qty AS rc_qty_mtr, A.in_qty_pcs AS rc_qty_pcs, A.in_amount AS rc_amount
        FROM istem_costing.dbo.tr_inv_in AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_11)
            AND A.cost_sheet_id IN ('A30', 'A31')
    ) AS S ON (S.mat_code=T.mat_code)

    UPDATE T SET
         co_dy_qty_pcs = S.co_dy_qty_pcs
        ,co_dy_qty_mtr_wv = S.co_dy_qty_mtr_wv
        ,co_dy_qty_mtr_dy = CASE WHEN T.item_category = 'SP' THEN (S.co_dy_qty_pcs * 47.5) ELSE (S.co_dy_qty_pcs * 50) END
        ,co_dy_price = NULLIF(( ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0) ), 0)/NULLIF(( ISNULL(T.bf_qty_mtr, 0)+ISNULL(T.rc_qty_mtr, 0) ), 0)
        ,co_dy_amount = S.co_dy_qty_mtr_wv * NULLIF(( ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0) ), 0)/NULLIF(( ISNULL(T.bf_qty_mtr, 0)+ISNULL(T.rc_qty_mtr, 0) ), 0)
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            A.mat_code
            ,A.out_qty AS co_dy_qty_mtr_wv
            ,A.out_qty_pcs AS co_dy_qty_pcs
            ,A.out_amount AS rc_amount
        FROM istem_costing.dbo.tr_inv_out_detail AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_11)
            AND A.out_dest IN (@dept_40)
            AND A.cost_sheet_id IN ('A30', 'A31')
    ) AS S ON (S.mat_code=T.mat_code)

    -- Export
    UPDATE T SET
         T.co_exp_qty_pcs = S.co_exp_qty_pcs
        ,T.co_exp_qty_mtr_wv = S.co_exp_qty_mtr_wv
        ,T.co_exp_price = T.co_dy_price
        ,T.co_exp_amount = S.co_exp_qty_mtr_wv * T.co_dy_price
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            A.mat_code
            ,A.out_qty AS co_exp_qty_mtr_wv
            ,A.out_qty_pcs AS co_exp_qty_pcs
            ,A.out_amount AS rc_amount
        FROM istem_costing.dbo.tr_inv_out_detail AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_11)
            AND A.out_dest IN (@dept_50)
            AND A.mat_usage IN ('E')
            AND A.cost_sheet_id IN ('A30', 'A31')
    ) AS S ON (S.mat_code=T.mat_code)

    -- Domestic
    UPDATE T SET
         co_dom_qty_pcs = S.co_dom_qty_pcs
        ,co_dom_qty_mtr_wv = S.co_dom_qty_mtr_wv
        ,co_dom_price = T.co_dy_price
        ,co_dom_amount = S.co_dom_qty_mtr_wv * T.co_dy_price
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            A.mat_code
            ,A.out_qty AS co_dom_qty_mtr_wv
            ,A.out_qty_pcs AS co_dom_qty_pcs
            ,A.out_amount AS rc_amount
        FROM istem_costing.dbo.tr_inv_out_detail AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_11)
            AND A.out_dest IN (@dept_50)
            AND A.mat_usage IN ('D')
            AND A.cost_sheet_id IN ('A30', 'A31')
    ) AS S ON (S.mat_code=T.mat_code)

    -- Test
    UPDATE T SET
         co_test_qty_pcs = S.co_test_qty_pcs
        ,co_test_qty_mtr_wv = S.co_test_qty_mtr_wv
        ,co_test_price = T.co_dy_price
        ,co_test_amount = S.co_test_qty_mtr_wv * T.co_dy_price
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            A.mat_code
            ,A.out_qty AS co_test_qty_mtr_wv
            ,A.out_qty_pcs AS co_test_qty_pcs
            ,A.out_amount AS rc_amount
        FROM istem_costing.dbo.tr_inv_out_detail AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_11)
            AND A.out_dest IN (@dept_99)
            AND A.mat_usage IN ('T')
            AND A.cost_sheet_id IN ('A30', 'A31')
    ) AS S ON (S.mat_code=T.mat_code)

    -- Pengantar
    UPDATE T SET
         T.co_pengantar_qty_pcs = S.co_pengantar_qty_pcs
        ,T.co_pengantar_qty_mtr_wv = S.co_pengantar_qty_mtr_wv
        ,T.co_pengantar_price = T.co_dy_price
        ,T.co_pengantar_amount = S.co_pengantar_qty_mtr_wv * T.co_dy_price
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            A.mat_code
            ,A.out_qty AS co_pengantar_qty_mtr_wv
            ,A.out_qty_pcs AS co_pengantar_qty_pcs
            ,A.out_amount AS rc_amount
        FROM istem_costing.dbo.tr_inv_out_detail AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_11)
            AND A.out_dest IN (@dept_99)
            AND A.mat_usage IN ('P')
            AND A.cost_sheet_id IN ('A30', 'A31')
    ) AS S ON (S.mat_code=T.mat_code)


    -- CF
    UPDATE T SET
         T.cf_qty_pcs = NULL
        ,T.cf_qty_mtr = (ISNULL(T.bf_qty_mtr, 0)+ISNULL(T.rc_qty_mtr, 0)) - (ISNULL(T.co_dy_qty_mtr_wv, 0)+ISNULL(T.co_exp_qty_mtr_wv, 0)+ISNULL(T.co_dom_qty_mtr_wv, 0)+ISNULL(T.co_test_qty_mtr_wv, 0)+ISNULL(T.co_pengantar_qty_mtr_wv, 0))
        ,T.cf_price = T.co_dy_price
        ,T.cf_amount = (ISNULL(T.bf_qty_mtr, 0)+ISNULL(T.rc_qty_mtr, 0)) - (ISNULL(T.co_dy_qty_mtr_wv, 0)+ISNULL(T.co_exp_qty_mtr_wv, 0)+ISNULL(T.co_dom_qty_mtr_wv, 0)+ISNULL(T.co_test_qty_mtr_wv, 0)+ISNULL(T.co_pengantar_qty_mtr_wv, 0)) * T.co_dy_price
    FROM @_TEMP AS T


    UPDATE T SET
         T.bf_price = CASE WHEN ISNULL(T.bf_qty_mtr, 0) > 0 THEN T.bf_price ELSE NULL END
        ,T.rc_price = CASE WHEN ISNULL(T.rc_qty_mtr, 0) > 0 THEN T.rc_price ELSE NULL END
        ,T.co_dy_price = CASE WHEN ISNULL(T.co_dy_qty_mtr_wv, 0) > 0 THEN T.co_dy_price ELSE NULL END
        ,T.co_exp_price = CASE WHEN ISNULL(T.co_exp_qty_mtr_wv, 0) > 0 THEN T.co_exp_price ELSE NULL END
        ,T.co_dom_price = CASE WHEN ISNULL(T.co_dom_qty_mtr_wv, 0) > 0 THEN T.co_dom_price ELSE NULL END
        ,T.co_test_price = CASE WHEN ISNULL(T.co_test_qty_mtr_wv, 0) > 0 THEN T.co_test_price ELSE NULL END
        ,T.co_pengantar_price = CASE WHEN ISNULL(T.co_pengantar_qty_mtr_wv, 0) > 0 THEN T.co_pengantar_price ELSE NULL END
        ,T.cf_price = CASE WHEN ISNULL(T.cf_qty_mtr, 0) > 0 THEN T.cf_price ELSE NULL END
    FROM @_TEMP AS T

    UPDATE T SET
         T.so = CASE
                    WHEN T.item_category = 'TR' THEN 'A1'
                    WHEN T.item_category = 'SP' THEN 'A2'
                    ELSE 'A99'
                END
    FROM @_TEMP AS T


    -- INSERT INTO istem_costing.dbo.tr_inv_out_detail_hist
    --     SELECT * FROM istem_costing.dbo.tr_inv_out_detail AS A
    --         WHERE
    --             A.comp_id=@comp_id
    --         AND A.f_year=@f_year
    --         AND A.f_month=@f_month
    --         AND A.dept IN (@dept_11)
    --         AND A.cost_sheet_id IN ('A30','A31');
    --
    -- UPDATE T SET
    --         T.out_amount=S.co_dy_amount
    --         ,T.proc_time=@proc_time
    --         ,T.user_id=@user_id
    --         ,T.client_ip=@client_ip
    --         ,T.proc_no=T.proc_no+1
    --     FROM istem_costing.dbo.tr_inv_out_detail AS T
    --     LEFT JOIN @_TEMP AS S ON (S.mat_code=T.mat_code)
    --     WHERE
    --             A.comp_id=@comp_id
    --         AND A.f_year=@f_year
    --         AND A.f_month=@f_month
    --         AND A.dept IN (@dept_11)
    --         AND A.cost_sheet_id IN ('A30','A31');


    -- SUB3: Create tr_dept_calc
    -- Untuk Item yang ada consume ke Dyeing saja

    SET @rec_sts='A'
    SET @proc_no=(SELECT ISNULL(MAX(A.proc_no), 0) + 1 FROM istem_costing.dbo.tr_dept_calc_hist AS A WHERE
            A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept IN (@dept_40)
        AND ISNULL(A.mc_loc, '')=''
        AND A.tr_code='RCVRM'
        AND A.proc_code='RCVRM'
        AND A.proc_type='RCVRM')
    IF @proc_no >= 2
    BEGIN
        SET @rec_sts = 'T'
    END

    -- INSERT INTO istem_costing.dbo.tr_dept_calc_hist
    --     SELECT * FROM istem_costing.dbo.tr_dept_calc AS A
    --         WHERE
    --                 A.comp_id=@comp_id
    --             AND A.f_year=@f_year
    --             AND A.f_month=@f_month
    --             AND A.dept IN (@dept_40)
    --             AND ISNULL(A.mc_loc, '')=''
    --             AND A.tr_code='RCVRM'
    --             AND A.proc_code='RCVRM'
    --             AND A.proc_type='RCVRM'
    --
    -- DELETE FROM istem_costing.dbo.tr_dept_calc
    --     WHERE
    --             comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (@dept_40)
    --         AND ISNULL(mc_loc, '')=''
    --         AND tr_code='RCVRM'
    --         AND proc_code='RCVRM'
    --         AND proc_type='RCVRM'

    INSERT INTO istem_costing.dbo.tr_dept_calc
        (
           comp_id
          ,f_year
          ,f_month
          ,dept
          ,tr_code
          ,mc_loc
          ,proc_code
          ,item_code
          ,mat_code
          ,mat_qty
          ,mat_amount
          ,mat_qty_unit
          ,proc_type
          ,proc_time
          ,user_id
          ,client_ip
          ,rec_sts
          ,proc_no
          ,qty_pcs
        )
        SELECT
            @comp_id AS comp_id
           ,@f_year AS f_year
           ,@f_month AS f_month
           ,@dept_40 AS dept
           ,'RCVRM' AS tr_code
           ,'' AS mc_loc
           ,'RCVRM' AS proc_code
           ,A.mat_code AS item_code
           ,A.mat_code AS mat_code
           ,A.co_dy_qty_mtr_dy AS mat_qty
           ,A.co_dy_amount AS mat_amount
           ,'MTR' AS mat_qty_unit
           ,'RCVRM' AS proc_type
           ,@proc_time AS proc_time
           ,@user_id AS user_id
           ,@client_ip AS client_ip
           ,@rec_sts AS rec_sts
           ,@proc_no AS proc_no
           ,A.co_dy_qty_pcs AS qty_pcs
        FROM @_TEMP AS A
        WHERE A.co_dy_qty_mtr_dy IS NOT NULL
        ORDER BY A.so, A.mat_code


    -- SUB4: Update ke tr_inv_out_detail - out-amount
    -- INSERT INTO istem_costing.dbo.tr_inv_out_detail_hist
    SET @proc_no = (SELECT ISNULL(MAX(A.proc_no), 0) + 1 FROM istem_costing.dbo.tr_inv_out_detail AS A
        WHERE
            A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept IN (@dept_11)
        AND A.cost_sheet_id IN ('A30','A31'))

    UPDATE T SET
            T.out_amount=CASE
                WHEN T.out_dest=@dept_40 THEN S.co_dy_amount
                WHEN T.out_dest=@dept_50 AND T.mat_usage='E' THEN S.co_exp_amount
                WHEN T.out_dest=@dept_50 AND T.mat_usage='D' THEN S.co_dom_amount
                WHEN T.out_dest=@dept_99 AND T.mat_usage='T' THEN S.co_test_amount
                WHEN T.out_dest=@dept_99 AND T.mat_usage='P' THEN S.co_pengantar_amount
                ELSE 0
            END
            ,T.out_invoice_qty=CASE
                WHEN T.out_dest=@dept_40 THEN S.co_dy_qty_mtr_wv
                ELSE 0
            END
            ,T.rec_sts='T'
            ,T.proc_no=@proc_no
        FROM istem_costing.dbo.tr_inv_out_detail AS T
        LEFT JOIN (
            SELECT
                A.mat_code
                ,A.co_dy_amount
                ,A.co_exp_amount
                ,A.co_dom_amount
                ,A.co_test_amount
                ,A.co_pengantar_amount
                ,A.co_dy_qty_mtr_wv
            FROM @_TEMP AS A
        ) AS S ON (S.mat_code=T.mat_code)
        WHERE
                T.comp_id=@comp_id
            AND T.f_year=@f_year
            AND T.f_month=@f_month
            AND T.dept IN (@dept_11)
            AND T.cost_sheet_id IN ('A30','A31');
END
