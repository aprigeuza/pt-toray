ALTER PROCEDURE  [sp_AcctProcess]
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;

    -- Run Logistic Download
    PRINT 'EXEC dbo.sp_LogDownMatProcess';
    EXEC dbo.sp_LogDownMatProcess @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Run Acccounting Process
    --SUB A
    PRINT 'EXEC dbo.sp_AcctProdWIPEquivalent';
    EXEC dbo.sp_AcctProdWIPEquivalent @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Clear tr_manex
    PRINT 'Clear tr_manex';
    INSERT INTO [istem_costing].[dbo].[tr_manex_hist]
            SELECT * FROM istem_costing.dbo.tr_manex AS A
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month;
    DELETE
        FROM istem_costing.dbo.tr_manex
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND tr_code IN ('AUPC', 'PCD', 'AUFC', 'FCD', 'ACGA', 'ACEN');
	--SUB B
    PRINT 'EXEC dbo.sp_AcctDownSME';
    EXEC dbo.sp_AcctDownSME @comp_id, @f_year, @f_month, @user_id, @client_ip;

	--SUB C
    PRINT 'EXEC dbo.sp_AcctDownDCA';
    EXEC dbo.sp_AcctDownDCA @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Download Balance
    -- 25-02-2021
    PRINT 'EXEC dbo.sp_AcctDownBalance';
    EXEC dbo.sp_AcctDownBalance @comp_id, @f_year, @f_month, @user_id, @client_ip;

	--SUB 1 ---> AUPC
    PRINT 'EXEC dbo.sp_AcctCalcUtPropCost';
    EXEC dbo.sp_AcctCalcUtPropCost @comp_id, @f_year, @f_month, @user_id, @client_ip;

	--SUB 2 ---> PCD
    PRINT 'EXEC dbo.sp_AcctCalcPropCostDept';
    EXEC dbo.sp_AcctCalcPropCostDept @comp_id, @f_year, @f_month, @user_id, @client_ip;

	--SUB 3 ---> AUFC
    PRINT 'EXEC dbo.sp_AcctCalcUtFixCost';
    EXEC dbo.sp_AcctCalcUtFixCost @comp_id, @f_year, @f_month, @user_id, @client_ip;

	--SUB 4 ---> FCD & ACGA
    PRINT 'EXEC dbo.sp_AcctCalcGAAcllocFixCost';
    EXEC dbo.sp_AcctCalcGAAcllocFixCost @comp_id, @f_year, @f_month, @user_id, @client_ip;

	--SUB 5 ---> FCD & ACEN
    PRINT 'EXEC dbo.sp_AcctCalcEngAllocFixCost';
    EXEC dbo.sp_AcctCalcEngAllocFixCost @comp_id, @f_year, @f_month, @user_id, @client_ip;

	--SUB 6 ---> FCD
    PRINT 'EXEC dbo.sp_AcctCalcFixCostDeptProd';
    EXEC dbo.sp_AcctCalcFixCostDeptProd @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Run Spinning Process
    PRINT 'EXEC dbo.sp_SpinningProcessWIP';
    EXEC dbo.sp_SpinningProcessWIP @comp_id, @f_year, @f_month, @user_id, @client_ip;

    PRINT 'EXEC dbo.sp_SpinningProcessStapleCost';
    EXEC dbo.sp_SpinningProcessStapleCost @comp_id, @f_year, @f_month, @user_id, @client_ip;

    PRINT 'EXEC dbo.sp_SpinningProcessWIPCALC';
    EXEC dbo.sp_SpinningProcessWIPCALC @comp_id, @f_year, @f_month, @user_id, @client_ip

    PRINT 'EXEC dbo.sp_SpinningProcessConsMat';
    EXEC dbo.sp_SpinningProcessConsMat @comp_id, @f_year, @f_month, @user_id, @client_ip

    -- Generate Balance Spinning
    PRINT 'EXEC dbo.sp_SpinningGenerateBalWIP';
    EXEC dbo.sp_SpinningGenerateBalWIP @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Balance Cost Spinning
    PRINT 'EXEC dbo.sp_AcctBalanceCostSpinning';
    EXEC dbo.sp_AcctBalanceCostSpinning @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Total Cost Spinning
    PRINT 'EXEC dbo.sp_AcctTotalCostSpinning';
    EXEC dbo.sp_AcctTotalCostSpinning @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Run Weaving Process
    PRINT 'EXEC dbo.sp_WeavingProcessYarnCost';
    EXEC dbo.sp_WeavingProcessYarnCost @comp_id, @f_year, @f_month, @user_id, @client_ip

    PRINT 'EXEC dbo.sp_WeavingProcessWIPCALC';
    EXEC dbo.sp_WeavingProcessWIPCALC @comp_id, @f_year, @f_month, @user_id, @client_ip

    PRINT 'EXEC dbo.sp_WeavingProcessConsMat';
    EXEC dbo.sp_WeavingProcessConsMat @comp_id, @f_year, @f_month, @user_id, @client_ip

    -- Generate Balance Spinning
    PRINT 'EXEC dbo.sp_WeavingGenerateBalWIP';
    EXEC dbo.sp_WeavingGenerateBalWIP @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Balance Cost Weaving
    PRINT 'EXEC dbo.sp_AcctBalanceCostWeaving';
    EXEC dbo.sp_AcctBalanceCostWeaving @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Total Cost Weaving
    PRINT 'EXEC dbo.sp_AcctTotalCostWeaving';
    EXEC dbo.sp_AcctTotalCostWeaving @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Generate Convert Item
    PRINT 'EXEC dbo.sp_GenerateConvertItem';
    EXEC dbo.sp_GenerateConvertItem @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Generate Convert Item
    PRINT 'EXEC dbo.sp_DyeingProcess';
    EXEC dbo.sp_DyeingProcess @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Total Cost Dyeing
    PRINT 'EXEC dbo.sp_AcctBalanceCostDyeing';
    EXEC dbo.sp_AcctBalanceCostDyeing @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Total Cost Dyeing
    PRINT 'EXEC dbo.sp_AcctTotalCostDyeing';
    EXEC dbo.sp_AcctTotalCostDyeing @comp_id, @f_year, @f_month, @user_id, @client_ip;
    -- Grey Balance Dyeing
    PRINT 'EXEC dbo.sp_AcctDyeingFG';
    EXEC dbo.sp_AcctDyeingFG @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Generate Distribution Cost
    PRINT 'EXEC dbo.sp_AcctUpdateProdDistCost';
    EXEC dbo.sp_AcctUpdateProdDistCost @comp_id, @f_year, @f_month, @user_id, @client_ip;

END
