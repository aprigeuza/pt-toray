-- =============================================
-- Proses Logistic Generate Balance Data
-- table : tr_bal_mat
-- =============================================
ALTER PROCEDURE  [sp_LogtGenerateBalance]
    @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
    SET NOCOUNT ON;
	-- DECLARE
	--     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24);
	--
	-- SET @comp_id=1
	-- SET @f_year=2020
	-- SET @f_month=7
	-- SET @user_id='SYS'
	-- SET @client_ip='192.168.1.1'


	DECLARE @_TEMP_TR_BAL_MAT TABLE (
		comp_id NUMERIC(1, 0) NOT NULL,
		f_year NUMERIC(4, 0) NOT NULL,
		f_month NUMERIC(2, 0) NOT NULL,
		dept NUMERIC(3, 0) NOT NULL,
		mat_code VARCHAR(45) NOT NULL,
		mat_usage CHAR(1) NULL,
		cost_sheet_id VARCHAR(4) NOT NULL,
		qty_unit VARCHAR(5) NULL,
		cf_bale_qty NUMERIC(4, 0) NULL,
		cf_qty NUMERIC(14, 2) NULL,
		cf_amount NUMERIC(14, 2) NULL,
		cf_invoice_qty NUMERIC(14, 2) NULL,
		proc_time DATETIME NULL,
		user_id VARCHAR(6) NULL,
		client_ip VARCHAR(15) NULL,
		rec_sts CHAR(1) NULL,
		proc_no TINYINT NULL
	);


	INSERT INTO @_TEMP_TR_BAL_MAT EXEC istem_costing.dbo.sp_LogtGenerateBalanceData @comp_id, @f_year, @f_month, @user_id, @client_ip;

    -- Insert History
    INSERT INTO istem_costing.dbo.tr_bal_mat_hist
        SELECT *
        FROM istem_costing.dbo.tr_bal_mat
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month;

    -- Delete Existing
	DELETE FROM istem_costing.dbo.tr_bal_mat
		WHERE
			comp_id=@comp_id
			AND f_year=@f_year
			AND f_month=@f_month

    -- Insert New Data
	INSERT INTO istem_costing.dbo.tr_bal_mat
		(
			comp_id
			,f_year
			,f_month
			,dept
			,mat_code
			,mat_usage
			,cost_sheet_id
			,qty_unit
			,cf_bale_qty
			,cf_qty
			,cf_amount
			,cf_invoice_qty
			,proc_time
			,user_id
			,client_ip
			,rec_sts
			,proc_no
		)
		SELECT
			comp_id
			,f_year
			,f_month
			,dept
			,mat_code
			,mat_usage
			,cost_sheet_id
			,qty_unit
			,cf_bale_qty
			,cf_qty
			,cf_amount
			,cf_invoice_qty
			,proc_time
			,user_id
			,client_ip
			,rec_sts
			,proc_no
		FROM @_TEMP_TR_BAL_MAT;


		SELECT *
		FROM @_TEMP_TR_BAL_MAT;
END
