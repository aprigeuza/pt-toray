-- =============================================
-- Acct-Download Balance
-- Download
-- =============================================
ALTER PROCEDURE  [sp_AcctDownBalance]
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;
	-- DECLARE
	-- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
	--
	-- SET @comp_id=1
	-- SET @f_year=2020
	-- SET @f_month=7
	-- SET @user_id='SYS'
	-- SET @client_ip='192.168.1.1'


	DECLARE @FromDate Date, @ToDate Date
    DECLARE @Date1 DATE
    DECLARE @Date2 DATE
    DECLARE @lastmonthdate1 DATE
	DECLARE @lastmonthdate2 DATE


	-- Sap Period
	SELECT
		@FromDate = F_RefDate
		,@ToDate = T_RefDate
	FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR
	WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)

    SET @lastmonthdate2 = DATEADD(day,-1,@FromDate)
    SET @lastmonthdate1 = CONVERT(DATE,CONVERT(VARCHAR,YEAR(@FromDate)) + '-'+CONVERT(VARCHAR,(MONTH(DATEADD(MONTH,-1,@FromDate))))+'-'+'01')

	SELECT @Date1=T0.RateDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.ORTT T0 WHERE T0.RateDate = @FromDate
	SELECT @Date2=T1.RateDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.ORTT T1 WHERE T1.RateDate = @ToDate

	DECLARE @proc_time AS DATETIME
	DECLARE @rec_sts VARCHAR(MAX)
	DECLARE @proc_no INT

	SET @rec_sts='A'
	SET @proc_time=GETDATE()
	SET @proc_no=1


	-- Cek tr_acct_balance
	-- Find Data
	DECLARE @c INT
	SET @c = (SELECT COUNT(*) FROM istem_costing.dbo.tr_acct_balance WHERE (comp_id=@comp_id) AND (f_year = @f_year) AND (f_month = @f_month))

	IF @c >= 1
	BEGIN
		-- Save History
		INSERT INTO istem_costing.dbo.tr_acct_balance_hist
				SELECT
                *
				FROM istem_costing.dbo.tr_acct_balance
				WHERE (comp_id=@comp_id) AND (f_year = @f_year) AND (f_month = @f_month)

		-- Jika sudah ada
		-- Jadikan rec_sts = T
		SET @rec_sts = 'T'

		SET @proc_no = (SELECT MAX(proc_no) + 1 FROM istem_costing.dbo.tr_acct_balance WHERE (comp_id=@comp_id) AND (f_year = @f_year) AND (f_month = @f_month))
	END

	DELETE FROM istem_costing.dbo.tr_acct_balance WHERE (comp_id=@comp_id) AND (f_year=@f_year) AND (f_month=@f_month)


    INSERT INTO istem_costing.dbo.tr_acct_balance
        (
            comp_id
            ,f_year
            ,f_month
            ,acct_code
            ,acct_name
            ,balance_amount
            ,bs_grp_id
            ,exp1_grp_id
            ,exp2_grp_id
            ,pl1_grp_id
            ,pl2_grp_id
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
		)
        SELECT
             @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
			,A.Account AS acct_code
			,B.AcctName AS acct_name
            ,A.BalanceAmount AS balance_amount
            ,''  AS bs_grp_id
            ,''  AS exp1_grp_id
            ,''  AS exp2_grp_id
            ,''  AS pl1_grp_id
            ,''  AS pl2_grp_id
			,@proc_time AS proc_time
			,@user_id AS user_id
			,@client_ip AS client_ip
			,@rec_sts AS rec_sts
			,@proc_no AS proc_no
		FROM (
            SELECT
                TT0.Account,
                SUM(TT0.lastmonth) AS lastperiod,
                SUM(TT0.Debit)as perioddebit,
                SUM(TT0.Credit) AS periodcredit,
                (SUM(TT0.Debit)-SUM(TT0.Credit)) AS BalanceAmount,
                SUM(cf) AS cf
            FROM
            (
                ---calculate until current
                SELECT T1.Account, 0 AS lastmonth, 0 AS debit, 0 AS credit, (SUM(T1.Debit) - SUM(T1.Credit)) AS cf
                FROM SAP_ISM.SBO_ISM_LIVE.DBO.OJDT T0
                INNER JOIN SAP_ISM.SBO_ISM_LIVE.DBO.JDT1 T1 ON T0.TransId =T1.TransId
                WHERE T0.RefDate < @FromDate
                GROUP BY T1.Account
                UNION
                ---calculate current period
                SELECT T1.Account, 0 AS lastmonth, SUM(T1.Debit) AS Debit, SUM(T1.Credit) AS Credit, 0 AS cf
                FROM SAP_ISM.SBO_ISM_LIVE.DBO.OJDT T0
                INNER JOIN SAP_ISM.SBO_ISM_LIVE.DBO.JDT1 T1 ON T0.Transid=T1.Transid
                WHERE T0.Refdate >= @FromDate AND T0.Refdate <= @ToDate
                GROUP BY T1.Account
                UNION
                ---calculate last month
                SELECT T1.Account, (SUM(T1.Debit) - SUM(T1.Credit)) AS lastmonth, 0 AS Debit, 0 AS Credit, 0 AS cf
                FROM SAP_ISM.SBO_ISM_LIVE.DBO.OJDT T0
                INNER JOIN SAP_ISM.SBO_ISM_LIVE.DBO.JDT1 T1 ON T0.Transid=T1.Transid
                WHERE T0.Refdate >= @lastmonthdate1 AND T0.Refdate <= @lastmonthdate2
                GROUP BY T1.Account
            ) TT0
            WHERE CONVERT(INT, LEFT(TT0.Account, 1)) <= 3
            GROUP BY TT0.Account
        ) AS A
        LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OACT B ON B.AcctCode = A.Account

	INSERT INTO istem_costing.dbo.tr_acct_balance
		(
            comp_id
            ,f_year
            ,f_month
            ,acct_code
            ,acct_name
            ,balance_amount
            ,bs_grp_id
            ,exp1_grp_id
            ,exp2_grp_id
            ,pl1_grp_id
            ,pl2_grp_id
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
		)
		SELECT
             @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
			,T11.Account AS acct_code
			,T11.AcctName AS acct_name
            ,T11.BalanceAmount AS balance_amount
            ,''  AS bs_grp_id
            ,''  AS exp1_grp_id
            ,''  AS exp2_grp_id
            ,''  AS pl1_grp_id
            ,''  AS pl2_grp_id
			,@proc_time AS proc_time
			,@user_id AS user_id
			,@client_ip AS client_ip
			,@rec_sts AS rec_sts
			,@proc_no AS proc_no
		FROM (
            SELECT
            	 T10.Account
            	,T10.AcctName
            	,T10.BalanceAmount
            FROM (
            	SELECT
            		T0.Account
            		,T1.AcctName
            		,SUM(T0.Debit) AS Debit
            		,SUM(T0.Debit)-SUM(T0.Credit) AS BalanceAmount
            	FROM SAP_ISM.SBO_ISM_LIVE.DBO.JDT1 T0
            	LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OACT T1 ON T0.Account = T1.AcctCode
                LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OJDT T2 ON T0.transid=T2.transid
            	WHERE
            			(T0.RefDate BETWEEN @FromDate AND @ToDate)
            		AND (CONVERT(INT, LEFT(T0.Account, 3)) >= 400)
            	GROUP BY
            		T0.Account, T1.AcctName
            ) T10
		) T11
		ORDER BY T11.Account

END
