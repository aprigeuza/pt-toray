
ALTER PROCEDURE  sp_WeavingProcessWH
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @dept INT
	DECLARE @tr_code VARCHAR(MAX)
	DECLARE @rec_sts VARCHAR(MAX)
	DECLARE @proc_time AS DATETIME
	DECLARE @proc_no INT
	DECLARE @proc_code VARCHAR(MAX)
	DECLARE @proc_type VARCHAR(MAX)

    SET @dept=(SELECT dept FROM ms_dept WHERE (comp_id=@comp_id AND dept_seq=30))
	SET @tr_code='WH'
	SET @proc_code='WH'
	SET @proc_time = GETDATE()
	SET @rec_sts='A'
	SET @proc_no=1
	SET @proc_type = (SELECT TOP 1 proc_type FROM istem_costing.dbo.ms_process WHERE (comp_id=@comp_id AND dept=@dept AND proc_code=@proc_code))

	DECLARE @f_refdate Date, @t_refdate Date, @p_year INT, @p_month INT
	SELECT
		@f_refdate = F_RefDate
		,@t_refdate = T_RefDate
		,@p_year = CAST(YEAR(T_RefDate) AS INT)
		,@p_month = CAST(MONTH(T_RefDate) AS INT)
	FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR
	WHERE
		(CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year)
		AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)

	-- Cek tr_dept_calc
	DECLARE @c INT
	SET @c = (SELECT COUNT(*) AS c FROM tr_dept_calc WHERE (comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND tr_code=@tr_code AND proc_code=@proc_code))

	IF @c >= 1
	BEGIN

		SET @proc_no = (SELECT (MAX(proc_no)+1) AS n FROM tr_dept_calc WHERE (comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND tr_code=@tr_code AND proc_code=@proc_code))

		INSERT INTO istem_costing.dbo.tr_dept_calc_hist
			   (comp_id
			   ,f_year
			   ,f_month
			   ,dept
			   ,tr_code
			   ,mc_loc
			   ,proc_code
			   ,item_code
			   ,mat_code
			   ,mat_qty
			   ,mat_qty_unit
			   ,proc_type
			   ,proc_time
			   ,user_id
			   ,client_ip
			   ,rec_sts
			   ,proc_no
               ,qty_pcs)
			 SELECT
				 comp_id
				,f_year
				,f_month
				,dept
				,tr_code
				,mc_loc
				,proc_code
				,item_code
				,mat_code
				,mat_qty
				,mat_qty_unit
				,proc_type
				,proc_time
				,user_id
				,client_ip
				,rec_sts
				,proc_no
                ,qty_pcs
			FROM istem_costing.dbo.tr_dept_calc
			WHERE (comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND tr_code=@tr_code)

		DELETE FROM tr_dept_calc WHERE (comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND tr_code=@tr_code AND proc_code=@proc_code)

		SET @rec_sts='T'
	END

	-- ################################### Warehouse/Production Data ##########################################
	-- ##################################### Data Grey WH dari CIM ###########################################
	DECLARE @_TEMP_GREY_WH TABLE (
		grey_no VARCHAR(MAX)
		,received_piece DECIMAL(16,5)
		,received_meter DECIMAL(16,5)
	)
	INSERT INTO @_TEMP_GREY_WH
        select
            R.grey_no
            ,SUM(R.RECEIVED_PIECE) as received_piece
            ,SUM(R.RECEIVED_METER) as received_meter
        from (
            select
                h.grey_no,
                sum(Case When h.status_pcs='H' then 0.5*h.tot_pcs else Case When h.status_pcs='Q' then 0.25*h.tot_pcs else 1*h.tot_pcs end end) as RECEIVED_PIECE,
                sum(Case When h.u_ms='YDS' then h.length/1.0936 else h.length end) as RECEIVED_METER
            from istem_sms.dbo.wv_wh_header h
            where h.whs_date<>''
                and year(h.whs_date)=@p_year
                and month(h.whs_date)=@p_month
                -- and h.whs_date<='2020-10-31'
            group by h.grey_no
            union all
            select
                h.grey_no,
                -sum(Case When h.status_pcs='H' then 0.5*h.tot_pcs else Case When h.status_pcs='Q' then 0.25*h.tot_pcs else 1*h.tot_pcs end end) as RECEIVED_PIECE,
                -sum(Case When h.u_ms='YDS' then h.length/1.0936 else h.length end) as RECEIVED_METER
            from istem_sms.dbo.wv_wh_return h
            where
                h.rec_sts='A'
                and h.returnF=1
                and year(h.whs_date)=@p_year
                and month(h.whs_date)=@p_month
                -- and h.whs_date<='2020-10-31'
            group by h.grey_no
            union all
            select
                h.grey_no,
                sum(Case When h.status_pcs='H' then 0.5*h.tot_pcs else Case When h.status_pcs='Q' then 0.25*h.tot_pcs else 1*h.tot_pcs end end) as RECEIVED_PIECE,
                sum(Case When h.u_ms='YDS' then h.length/1.0936 else h.length end) as RECEIVED_METER
            from istem_sms.dbo.wv_wh_cancel h
            where
                h.rec_type='C'
                and h.rec_sts='C'
                and year(h.whs_date)=@p_year
                and month(h.whs_date)=@p_month
                -- and h.whs_date<='2020-10-31'
            group by h.grey_no
            union all
            select
                h.grey_no,
                -sum(Case When h.status_pcs='H' then 0.5*h.tot_pcs else Case When h.status_pcs='Q' then 0.25*h.tot_pcs else 1*h.tot_pcs end end) as RECEIVED_PIECE,
                -sum(Case When h.u_ms='YDS' then h.length/1.0936 else h.length end) as RECEIVED_METER
            from istem_sms.dbo.wv_wh_cancel h
            where
                h.rec_type='C'
                and h.rec_sts='C'
                and year(h.proc_date)=@p_year
                and month(h.proc_date)=@p_month
                -- and h.proc_date<='2020-10-31'
            group by h.grey_no
        )R group by R.grey_no order by R.grey_no

	-- #############################################################################

	-- ##################################  Create pada tr_inv_in ###########################################
	-- Insert tr_inv_in
	-- Cek dulu
	DECLARE @dept_11 INT, @rec_sts2 VARCHAR(1), @proc_no2 INT
	SET @dept_11=(SELECT dept FROM ms_dept WHERE (comp_id=@comp_id AND dept_seq=11))
	SET @rec_sts2 = 'A'
	SET @proc_no2 = 1

	DECLARE @cc INT
	SET @cc = (SELECT COUNT(*) FROM istem_costing.dbo.tr_inv_in
				WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept_11 AND cost_sheet_id IN ('A30', 'A31') AND mat_usage='W')
	IF @cc >= 1
	BEGIN
		INSERT INTO istem_costing.dbo.tr_inv_in_hist
			SELECT * FROM istem_costing.dbo.tr_inv_in WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept_11 AND cost_sheet_id IN ('A30', 'A31') AND mat_usage='W'

		SET @rec_sts2 = 'T'
		SET @proc_no2 = (SELECT MAX(proc_no) + 1 FROM istem_costing.dbo.tr_inv_in WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept_11 AND cost_sheet_id IN ('A30', 'A31') AND mat_usage='W')
	END

	DELETE FROM istem_costing.dbo.tr_inv_in WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept_11 AND cost_sheet_id IN ('A30', 'A31') AND mat_usage='W'

	INSERT INTO istem_costing.dbo.tr_inv_in
		(
			 comp_id
			,f_year
			,f_month
			,dept
			,cost_sheet_id
			,mat_code
			,mat_usage
			,in_bale_qty
			,in_qty
			,in_amount
			,in_invoice_qty
			,qty_unit
			,proc_time
			,user_id
			,client_ip
			,rec_sts
			,proc_no
            ,in_qty_pcs
        )
		SELECT
			@comp_id            AS comp_id
		   ,@f_year             AS f_year
		   ,@f_month            AS f_month
		   ,@dept_11            AS dept
		   ,CASE
				--WHEN LEFT(A.grey_no, 1) = 'G' THEN 'A31'
				WHEN C.proc_by = 'COM' THEN 'A31'
				ELSE 'A30'
		   END                  AS cost_sheet_id
		   ,A.grey_no           AS mat_code
		   ,'W'                 AS mat_usage
		   ,0                   AS in_bale_qty
		   ,A.received_meter    AS in_qty
		   ,CASE
				WHEN LEFT(A.grey_no, 1) = 'G' THEN (ISNULL(B.trans_amount, 0)+ISNULL(B.landed_cost, 0))
				ELSE 0
		   END                  AS in_amount
		   ,A.received_piece    AS in_invoice_qty
		   ,'MTR'               AS qty_unit
		   ,@proc_time          AS proc_time
		   ,@user_id            AS user_id
		   ,@client_ip          AS client_ip
		   ,@rec_sts2           AS rec_sts
		   ,@proc_no2           AS proc_no
           ,A.received_piece    AS in_qty_pcs
		FROM @_TEMP_GREY_WH     AS A
		LEFT JOIN istem_costing.dbo.tr_sap_purchase AS B ON
		(
				(B.comp_id=@comp_id)
			AND (B.f_year=@f_year)
			AND (B.f_month=@f_month)
			AND (B.item_code=A.grey_no)
		)
		INNER JOIN istem_sms.dbo.wv_fabric_analysis_master AS C ON (C.grey_no=A.grey_no)

	-- #############################################################################

	DECLARE @_TEMP_GREY_YARN TABLE (
		grey_no VARCHAR(max)
		,length_gr DECIMAL(16,5)
		,warp_ln DECIMAL(16,5)
		,yarn_no VARCHAR(max)
		,yarn_code VARCHAR(max)
		,yarn_kind VARCHAR(max)
		,yarn_wg DECIMAL(16,5)
	)
	INSERT INTO @_TEMP_GREY_YARN
		SELECT
			grey_no
			,length_gr
			,warp_ln
			,yarn_no
			,yarn_code
			,yarn_kind
			,yarn_wg
		FROM (
			SELECT
				 grey_no
				,length_gr
				,warp_ln
				,'WARP1' AS yarn_no
				,warp1_yarncode AS yarn_code
				,warp1_kind AS yarn_kind
				,warp1_wg AS yarn_wg
			FROM istem_sms.dbo.wv_fabric_analysis_master
			WHERE ISNULL(warp1_yarncode, '') <> ''
			UNION ALL
			SELECT
				 grey_no
				,length_gr
				,warp_ln
				,'WARP2' AS yarn_no
				,warp2_yarncode AS yarn_code
				,warp2_kind AS yarn_kind
				,warp2_wg AS yarn_wg
			FROM istem_sms.dbo.wv_fabric_analysis_master
			WHERE ISNULL(warp2_yarncode, '') <> ''
			UNION ALL
			SELECT
				 grey_no
				,length_gr
				,warp_ln
				,'WARP3' AS yarn_no
				,warp3_yarncode AS yarn_code
				,warp3_kind AS yarn_kind
				,warp3_wg AS yarn_wg
			FROM istem_sms.dbo.wv_fabric_analysis_master
			WHERE ISNULL(warp3_yarncode, '') <> ''
			UNION ALL
			SELECT
				 grey_no
				,length_gr
				,warp_ln
				,'WARP4' AS yarn_no
				,warp4_yarncode AS yarn_code
				,warp4_kind AS yarn_kind
				,warp4_wg AS yarn_wg
			FROM istem_sms.dbo.wv_fabric_analysis_master
			WHERE ISNULL(warp4_yarncode, '') <> ''
			UNION ALL
			SELECT
				 grey_no
				,length_gr
				,warp_ln
				,'WEFT1' AS yarn_no
				,weft1_yarncode AS yarn_code
				,weft1_kind AS yarn_kind
				,weft1_wg AS yarn_wg
			FROM istem_sms.dbo.wv_fabric_analysis_master
			WHERE ISNULL(weft1_yarncode, '') <> ''
			UNION ALL
			SELECT
				 grey_no
				,length_gr
				,warp_ln
				,'WEFT2' AS yarn_no
				,weft2_yarncode AS yarn_code
				,weft2_kind AS yarn_kind
				,weft2_wg AS yarn_wg
			FROM istem_sms.dbo.wv_fabric_analysis_master
			WHERE ISNULL(weft2_yarncode, '') <> ''
			UNION ALL
			SELECT
				 grey_no
				,length_gr
				,warp_ln
				,'WEFT3' AS yarn_no
				,weft3_yarncode AS yarn_code
				,weft3_kind AS yarn_kind
				,weft3_wg AS yarn_wg
			FROM istem_sms.dbo.wv_fabric_analysis_master
			WHERE ISNULL(weft3_yarncode, '') <> ''
			UNION ALL
			SELECT
				 grey_no
				,length_gr
				,warp_ln
				,'WEFT4' AS yarn_no
				,weft4_yarncode AS yarn_code
				,weft4_kind AS yarn_kind
				,weft4_wg AS yarn_wg
			FROM istem_sms.dbo.wv_fabric_analysis_master
			WHERE ISNULL(weft4_yarncode, '') <> ''

		) AS A

	-- TEMPORARY TABLE
	DECLARE @_TEMP_TR_DEPT_CALC TABLE (
		comp_id numeric(1, 0) NOT NULL,
		f_year numeric(4, 0) NOT NULL,
		f_month numeric(2, 0) NOT NULL,
		dept numeric(3, 0) NOT NULL,
		tr_code varchar(5) NOT NULL,
		mc_loc varchar(15) NOT NULL,
		proc_code varchar(7) NOT NULL,
		item_code varchar(45) NOT NULL,
		mat_code varchar(45) NOT NULL,
		mat_qty numeric(12, 2) NULL,
		mat_qty_unit varchar(5) NULL,
		proc_type varchar(20) NULL,
		proc_time datetime NULL,
		user_id varchar(6) NULL,
		client_ip varchar(15) NULL,
		rec_sts char(1) NULL,
		proc_no tinyint NULL,
        qty_pcs numeric(12, 2) NULL
	)
	-- Insert data ke tr_dept_calc dari tr_inv_in
	INSERT INTO @_TEMP_TR_DEPT_CALC (
			 comp_id
			,f_year
			,f_month
			,dept
			,tr_code
			,mc_loc
			,proc_code
			,item_code
			,mat_code
			,mat_qty
			,mat_qty_unit
			,proc_type
			,proc_time
			,user_id
			,client_ip
			,rec_sts
			,proc_no
            ,qty_pcs
		)
		SELECT
			 A.comp_id                                              AS comp_id
			,A.f_year                                               AS f_year
			,A.f_month                                              AS f_month
			,@dept                                                  AS dept
			,@tr_code                                               AS tr_code
			,''                                                     AS mc_loc
			,@tr_code                                               AS proc_code
			,C.grey_no                                              AS item_code
			,ISNULL(C.yarn_code, '')                                AS mat_code
			,SUM((D.received_meter / C.length_gr)*C.yarn_wg)        AS mat_qty
			-- ,B.qty_unit                                             AS mat_qty_unit
			-- Hardcore sementara (2020-11-10)
			,'LBS'                                                  AS mat_qty_unit
			,B.proc_type                                            AS proc_type
			,@proc_time                                             AS proc_time
			,@user_id                                               AS user_id
			,@client_ip                                             AS client_ip
			,@rec_sts                                               AS rec_sts
			,@proc_no                                               AS proc_no
            ,SUM(A.in_qty_pcs)                                      AS qty_pcs
		FROM istem_costing.dbo.tr_inv_in AS A
		LEFT JOIN istem_costing.dbo.ms_process AS B ON (
			B.comp_id=A.comp_id
			AND B.dept=@dept
			AND B.proc_code=@proc_code
		)
		LEFT JOIN @_TEMP_GREY_YARN AS C ON (C.grey_no=A.mat_code)
		LEFT JOIN @_TEMP_GREY_WH AS D ON (D.grey_no=A.mat_code)
		LEFT JOIN istem_costing.dbo.ms_mc_proc AS E ON (
			E.proc_code=@proc_code
			AND E.comp_id=A.comp_id
			AND E.dept=@dept
		)
		WHERE
				(A.comp_id=@comp_id)
			AND (A.f_year=@f_year)
			AND (A.f_month=@f_month)
			AND (A.dept=@dept_11)
			-- AND (A.cost_sheet_id IN ('A30', 'A31'))
			AND (A.cost_sheet_id IN ('A30')) -- Yang di tarik hanya yang A30 Saja (2020-10-27)
		GROUP BY
			 A.comp_id
			,A.f_year
			,A.f_month
			,C.grey_no
			,C.yarn_code
			,A.mat_code
			,B.qty_unit
			,B.proc_type
			,E.mat_qty_unit

	INSERT INTO istem_costing.dbo.tr_dept_calc
		(comp_id
			,f_year
			,f_month
			,dept
			,tr_code
			,mc_loc
			,proc_code
			,item_code
			,mat_code
			,mat_qty
			,mat_qty_unit
			,proc_type
			,proc_time
			,user_id
			,client_ip
			,rec_sts
			,proc_no
            ,qty_pcs)
		SELECT
			 comp_id
			,f_year
			,f_month
			,dept
			,tr_code
			,mc_loc
			,proc_code
			,item_code
			,mat_code
			,SUM(mat_qty)
			,mat_qty_unit
			,proc_type
			,proc_time
			,user_id
			,client_ip
			,rec_sts
			,proc_no
            ,qty_pcs
		FROM @_TEMP_TR_DEPT_CALC
		GROUP BY
			 comp_id
			,f_year
			,f_month
			,dept
			,tr_code
			,mc_loc
			,proc_code
			,item_code
			,mat_code
			,mat_qty_unit
			,proc_type
			,proc_time
			,user_id
			,client_ip
			,rec_sts
			,proc_no
            ,qty_pcs

END;
