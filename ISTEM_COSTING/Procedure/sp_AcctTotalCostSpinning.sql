ALTER PROCEDURE  sp_AcctTotalCostSpinning
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    -- Acct - Spinning Total Cost


    --DECLARE
    --@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)

    --SET @comp_id=1
    --SET @f_year=2020
    --SET @f_month=7
    --SET @user_id='SYS'
    --SET @client_ip='192.168.1.1'

    DECLARE @dept_seq10 INT
    DECLARE @dept10 INT
    DECLARE @dept_seq20 INT
    DECLARE @dept20 INT
    DECLARE @tr_code VARCHAR(MAX)
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_time AS DATETIME
    DECLARE @proc_no INT
    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT

    SET @dept_seq10 = 10
    SET @dept10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=@dept_seq10))
    SET @dept_seq20 = 20
    SET @dept20=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=@dept_seq20))
    SET @tr_code=''
    SET @proc_time = GETDATE()
    SET @rec_sts='A'
    SET @proc_no=1

    IF @f_month = 1
    BEGIN
    	SET @lp_f_month = 12
    	SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
    	SET @lp_f_month = @f_month - 1
    	SET @lp_f_year = @f_year
    END

    DECLARE @_TEMP TABLE(
         item_code     VARCHAR(MAX)
        ,item_desc     VARCHAR(MAX)
        ,product_qty   NUMERIC(16,4)
        ,cr_electric   NUMERIC(16,4)
        ,cr_water      NUMERIC(16,4)
        ,cr_steam      NUMERIC(16,4)
        ,cr_lng        NUMERIC(16,4)
        ,cr_production NUMERIC(16,4)
        ,cr_diestuff   NUMERIC(16,4)
        ,cr_chemical   NUMERIC(16,4)
        ,cr_resin      NUMERIC(16,4)
        ,cr_labor      NUMERIC(16,4)
        ,cr_repair     NUMERIC(16,4)
        ,eq_electric   NUMERIC(16,4)
        ,eq_water      NUMERIC(16,4)
        ,eq_steam      NUMERIC(16,4)
        ,eq_lng        NUMERIC(16,4)
        ,eq_production NUMERIC(16,4)
        ,eq_diestuff   NUMERIC(16,4)
        ,eq_chemical   NUMERIC(16,4)
        ,eq_resin      NUMERIC(16,4)
        ,eq_labor      NUMERIC(16,4)
        ,eq_repair     NUMERIC(16,4)
        ,cd_electric   NUMERIC(16,4)
        ,cd_water      NUMERIC(16,4)
        ,cd_steam      NUMERIC(16,4)
        ,cd_lng        NUMERIC(16,4)
        ,cd_production NUMERIC(16,4)
        ,cd_diestuff   NUMERIC(16,4)
        ,cd_chemical   NUMERIC(16,4)
        ,cd_resin      NUMERIC(16,4)
        ,cd_labor      NUMERIC(16,4)
        ,cd_repair     NUMERIC(16,4)
        ,leno          NUMERIC(16,4)
        ,material_cost NUMERIC(16,4)
        ,pc_fixed      NUMERIC(16,4)
        ,pc_labour     NUMERIC(16,4)
        ,pc_repair     NUMERIC(16,4)
        ,total_cost    NUMERIC(16,4)
        ,unit_cost     NUMERIC(16,4)
    )


    DECLARE @tot_product_qty   NUMERIC(16,4)
    DECLARE @tot_cr_electric   NUMERIC(16,4)
    DECLARE @tot_cr_water      NUMERIC(16,4)
    DECLARE @tot_cr_steam      NUMERIC(16,4)
    DECLARE @tot_cr_lng        NUMERIC(16,4)
    DECLARE @tot_cr_production NUMERIC(16,4)
    DECLARE @tot_cr_diestuff   NUMERIC(16,4)
    DECLARE @tot_cr_chemical   NUMERIC(16,4)
    DECLARE @tot_cr_resin      NUMERIC(16,4)
    DECLARE @tot_cr_labor      NUMERIC(16,4)
    DECLARE @tot_cr_repair     NUMERIC(16,4)
    DECLARE @tot_eq_electric   NUMERIC(16,4)
    DECLARE @tot_eq_water      NUMERIC(16,4)
    DECLARE @tot_eq_steam      NUMERIC(16,4)
    DECLARE @tot_eq_lng        NUMERIC(16,4)
    DECLARE @tot_eq_production NUMERIC(16,4)
    DECLARE @tot_eq_diestuff   NUMERIC(16,4)
    DECLARE @tot_eq_chemical   NUMERIC(16,4)
    DECLARE @tot_eq_resin      NUMERIC(16,4)
    DECLARE @tot_eq_labor      NUMERIC(16,4)
    DECLARE @tot_eq_repair     NUMERIC(16,4)
    DECLARE @tot_cd_electric   NUMERIC(16,4)
    DECLARE @tot_cd_water      NUMERIC(16,4)
    DECLARE @tot_cd_steam      NUMERIC(16,4)
    DECLARE @tot_cd_lng        NUMERIC(16,4)
    DECLARE @tot_cd_production NUMERIC(16,4)
    DECLARE @tot_cd_diestuff   NUMERIC(16,4)
    DECLARE @tot_cd_chemical   NUMERIC(16,4)
    DECLARE @tot_cd_resin      NUMERIC(16,4)
    DECLARE @tot_cd_labor      NUMERIC(16,4)
    DECLARE @tot_cd_repair     NUMERIC(16,4)
    DECLARE @tot_leno          NUMERIC(16,4)
    DECLARE @tot_material_cost NUMERIC(16,4)
    DECLARE @tot_pc_fixed      NUMERIC(16,4)
    DECLARE @tot_pc_labour     NUMERIC(16,4)
    DECLARE @tot_pc_repair     NUMERIC(16,4)
    DECLARE @tot_total_cost    NUMERIC(16,4)
    DECLARE @tot_unit_cost     NUMERIC(16,4)


    -- Insert Item
    PRINT 'Proc : 1';

    INSERT INTO @_TEMP
    (
        item_code
        ,item_desc
        ,product_qty
    )
    SELECT
         A.item_code
        ,B.yarn_prev_name
        ,A.prod_qty
    FROM istem_costing.dbo.tr_prod AS A
    INNER JOIN istem_sms.dbo.sp_ms_yarn AS B ON (B.yarn_code = A.item_code)
    WHERE
        A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept=@dept20



    DECLARE @_TEMP_CONVERT_ITEM TABLE(
        item_code VARCHAR(50)
        ,convert_type_code VARCHAR(12)
        ,convert_ratio DECIMAL(16,5)
    )

    PRINT 'Proc : 2';
    INSERT INTO @_TEMP_CONVERT_ITEM
        SELECT
            item_code
            ,convert_type_code
            ,convert_ratio
        FROM istem_costing.dbo.tr_convert_item
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept20


    PRINT 'Proc : 3';
    UPDATE T SET
        T.cr_electric       = electric.convert_ratio
        ,T.cr_water         = water.convert_ratio
        ,T.cr_production    = production.convert_ratio
        ,T.cr_labor         = labor.convert_ratio
        ,T.cr_repair        = repair.convert_ratio
        ,T.eq_electric      = T.product_qty * electric.convert_ratio
        ,T.eq_water         = T.product_qty * water.convert_ratio
        ,T.eq_production    = T.product_qty * production.convert_ratio
        ,T.eq_labor         = T.product_qty * labor.convert_ratio
        ,T.eq_repair        = T.product_qty * repair.convert_ratio
    FROM @_TEMP AS T
    LEFT JOIN @_TEMP_CONVERT_ITEM AS electric    ON (electric.item_code=T.item_code AND electric.convert_type_code='R01')
    LEFT JOIN @_TEMP_CONVERT_ITEM AS water       ON (water.item_code=T.item_code AND water.convert_type_code='R02')
    LEFT JOIN @_TEMP_CONVERT_ITEM AS production  ON (production.item_code=T.item_code AND production.convert_type_code='R07')
    LEFT JOIN @_TEMP_CONVERT_ITEM AS labor       ON (labor.item_code=T.item_code AND labor.convert_type_code='R10')
    LEFT JOIN @_TEMP_CONVERT_ITEM AS repair      ON (repair.item_code=T.item_code AND repair.convert_type_code='R11')

    PRINT 'Proc : 4';
    SELECT
        @tot_eq_electric = SUM(ISNULL(eq_electric, 0))
        ,@tot_eq_production = SUM(ISNULL(eq_production, 0))
        ,@tot_eq_labor = SUM(ISNULL(eq_labor, 0))
        ,@tot_eq_repair = SUM(ISNULL(eq_repair, 0))
    FROM @_TEMP


    PRINT 'Proc : 5';
    SELECT @tot_cd_electric = SUM(cons_wip_cost_amount) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=1 AND f_year=@f_year AND f_month=@f_month AND dept=700 AND cost_sheet_id='C02'
    SELECT @tot_cd_water = SUM(cons_wip_cost_amount) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=1 AND f_year=@f_year AND f_month=@f_month AND dept=700 AND cost_sheet_id='C03'
    SELECT @tot_cd_steam = SUM(cons_wip_cost_amount) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=1 AND f_year=@f_year AND f_month=@f_month AND dept=700 AND cost_sheet_id='C04'
    SELECT @tot_cd_lng = SUM(cons_wip_cost_amount) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=1 AND f_year=@f_year AND f_month=@f_month AND dept=700 AND cost_sheet_id='C01'
    SELECT @tot_cd_chemical = SUM(cf_dca_amount) FROM istem_costing.dbo.tr_dca WHERE comp_id=1 AND f_year=@f_year AND f_month=@f_month AND dept=700 AND cost_sheet_id='B31'
    SELECT @tot_cd_diestuff = SUM(cf_dca_amount) FROM istem_costing.dbo.tr_dca WHERE comp_id=1 AND f_year=@f_year AND f_month=@f_month AND dept=700 AND cost_sheet_id='B30'


    PRINT 'Proc : 6';
    UPDATE T SET
        T.cd_electric = NULLIF(@tot_cd_electric, 0) / NULLIF(@tot_eq_electric, 0) * NULLIF(T.eq_electric, 0)
        ,T.cd_water = NULLIF(@tot_cd_water, 0) / NULLIF(@tot_eq_water, 0) * NULLIF(T.eq_water, 0)
        ,T.cd_steam = NULLIF(@tot_cd_steam, 0) / NULLIF(@tot_eq_steam, 0) * NULLIF(T.eq_steam, 0)
        ,T.cd_lng = NULLIF(@tot_cd_lng, 0) / NULLIF(@tot_eq_lng, 0) * NULLIF(T.eq_lng, 0)
        ,T.cd_chemical = NULLIF(@tot_cd_chemical, 0) / NULLIF(@tot_eq_chemical, 0) * NULLIF(T.eq_chemical, 0)
        ,T.cd_diestuff = NULLIF(@tot_cd_diestuff, 0) / NULLIF(@tot_eq_diestuff, 0) * NULLIF(T.eq_diestuff, 0)
    FROM @_TEMP AS T


    PRINT 'Proc : 7';
    UPDATE T SET
        T.material_cost = S1.material_cost
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            item_code,
            material_cost
        FROM istem_costing.dbo.tr_prod
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept20
    ) AS S1 ON (S1.item_code=T.item_code)


    PRINT 'Proc : 8';
    SELECT @tot_material_cost=SUM(ISNULL(material_cost, 0)) FROM @_TEMP

    -- Total Fixed Cost per Dept
    PRINT 'Proc : 9';
    SELECT
        @tot_pc_labour=SUM(A.manex_amount)
    FROM istem_costing.dbo.tr_manex AS A
    LEFT JOIN ms_cost_group AS B ON (B.comp_id=A.comp_id AND B.cost_sheet_id=A.cost_sheet_id)
    WHERE
        A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept=@dept20
        AND A.tr_code='FCD'
        AND B.fix_cost_group='R10'

    -- Total Fixed Cost per Dept
    PRINT 'Proc : 10';
    SELECT
        @tot_pc_repair=SUM(A.manex_amount)
    FROM istem_costing.dbo.tr_manex AS A
    LEFT JOIN ms_cost_group AS B ON (B.comp_id=A.comp_id AND B.cost_sheet_id=A.cost_sheet_id)
    WHERE
        A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept=@dept20
        AND A.tr_code='FCD'
        AND B.fix_cost_group='R11'

    -- Total Fixed Cost per Dept
    PRINT 'Proc : 11';
    SELECT
        @tot_pc_fixed=ISNULL(SUM(A.cons_wip_cost_amount), 0) - ISNULL((ISNULL(@tot_pc_labour, 0)+ISNULL(@tot_pc_repair, 0)), 0)
    FROM istem_costing.dbo.tr_wip_cost AS A
    WHERE
        A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept=@dept20
        AND A.cost_sheet_id = 'H99'

    PRINT 'Proc : 12';
    UPDATE T SET
        T.pc_fixed = NULLIF(@tot_pc_fixed, 0)/NULLIF(@tot_eq_production, 0) * NULLIF(T.eq_production, 0)
        ,T.pc_labour = NULLIF(@tot_pc_labour, 0)/NULLIF(@tot_eq_labor, 0) * NULLIF(T.eq_labor, 0)
        ,T.pc_repair = NULLIF(@tot_pc_repair, 0)/NULLIF(@tot_eq_repair, 0) * NULLIF(T.eq_repair, 0)
    FROM @_TEMP AS T

    PRINT 'Proc : 13';
    UPDATE T SET
        T.total_cost = NULLIF(ISNULL(T.pc_fixed, 0) + ISNULL(T.pc_labour, 0) + ISNULL(T.pc_repair, 0) + ISNULL(T.leno, 0) + ISNULL(T.material_cost, 0) + ISNULL(T.cd_electric, 0), 0)
    FROM @_TEMP AS T


    PRINT 'Proc : 14';
    UPDATE T SET
        T.unit_cost = NULLIF(NULLIF(T.total_cost, 0)/NULLIF(T.product_qty, 0), 0)
    FROM @_TEMP AS T

    PRINT 'Proc : 15';
    SELECT @proc_no=ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_inv_in WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept10 AND cost_sheet_id='A20';
    INSERT INTO istem_costing.dbo.tr_inv_in_hist SELECT * FROM istem_costing.dbo.tr_inv_in WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept10 AND cost_sheet_id='A20';
    UPDATE T
        SET
            T.in_amount = NULLIF(T.in_qty, 0) / NULLIF(S.product_qty, 0) * NULLIF(S.total_cost, 0)
            ,T.rec_sts='T'
            ,T.proc_no=@proc_no
            ,T.proc_time=@proc_time
            ,T.user_id=@user_id
            ,T.client_ip=@client_ip
    FROM istem_costing.dbo.tr_inv_in AS T
    LEFT JOIN @_TEMP AS S ON (S.item_code=T.mat_code)
    WHERE
        T.comp_id=@comp_id
        AND T.f_year=@f_year
        AND T.f_month=@f_month
        AND T.dept=@dept10
        AND T.cost_sheet_id='A20'


    PRINT 'Proc : 16';
    SELECT @proc_no=ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_prod_detail2 WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept20;
    INSERT INTO istem_costing.dbo.tr_prod_detail2_hist SELECT * FROM istem_costing.dbo.tr_prod_detail2 WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept20;
    UPDATE T
        SET
        T.prod_cost_distr = S.prod_cost_distr
        ,T.rec_sts='T'
        ,T.proc_no=@proc_no
        ,T.proc_time=@proc_time
        ,T.user_id=@user_id
        ,T.client_ip=@client_ip
    FROM istem_costing.dbo.tr_prod_detail2 AS T
    LEFT JOIN (
        SELECT * FROM (
            SELECT item_code, 'R01' AS convert_type_code, cd_electric AS prod_cost_distr FROM @_TEMP
            UNION ALL
            SELECT item_code, 'R02' AS convert_type_code, cd_water AS prod_cost_distr FROM @_TEMP
            UNION ALL
            SELECT item_code, 'R07' AS convert_type_code, cd_production AS prod_cost_distr FROM @_TEMP
            UNION ALL
            SELECT item_code, 'R10' AS convert_type_code, cd_labor AS prod_cost_distr FROM @_TEMP
            UNION ALL
            SELECT item_code, 'R11' AS convert_type_code, cd_repair AS prod_cost_distr FROM @_TEMP
        ) AS A
        WHERE A.prod_cost_distr IS NOT NULL
    )
    AS S ON (S.item_code=T.item_code AND S.convert_type_code=T.convert_type_code)
    WHERE
        T.comp_id=@comp_id
        AND T.f_year=@f_year
        AND T.f_month=@f_month
        AND T.dept=@dept20

    PRINT 'Proc : 17';
    SELECT @proc_no=ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_prod_detail1 WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept20;
    INSERT INTO istem_costing.dbo.tr_prod_detail1_hist SELECT * FROM istem_costing.dbo.tr_prod_detail1 WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept20;
    UPDATE T SET
        T.tot_prod_cost_distr = S.sum_of_prod_cost_distr
        ,T.rec_sts='T'
        ,T.proc_no=@proc_no
        ,T.proc_time=@proc_time
        ,T.user_id=@user_id
        ,T.client_ip=@client_ip
    FROM istem_costing.dbo.tr_prod_detail1 AS T
    LEFT JOIN (
            SELECT
                A.cost_sheet_id,
                SUM(A.prod_cost_distr) AS sum_of_prod_cost_distr
            FROM istem_costing.dbo.tr_prod_detail2 AS A
            WHERE
                A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
                AND A.dept=@dept20
            GROUP BY A.cost_sheet_id
    ) AS S ON (S.cost_sheet_id=T.cost_sheet_id)
    WHERE
        T.comp_id=@comp_id
        AND T.f_year=@f_year
        AND T.f_month=@f_month
        AND T.dept=@dept20

    PRINT 'Proc : 18';
    SELECT @proc_no=ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_prod_detail3 WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept20;

    IF @proc_no > 1
    BEGIN
        SET @rec_sts='T'
    END
    ELSE
    BEGIN
        SET @rec_sts='A'
    END

    DELETE FROM istem_costing.dbo.tr_prod_detail3 WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept20;

    INSERT INTO istem_costing.dbo.tr_prod_detail3
               (
                   comp_id
                   ,f_year
                   ,f_month
                   ,dept
                   ,cost_sheet_id
                   ,item_code
                   ,fix_cost_group
                   ,fix_cost_amount
                   ,proc_time
                   ,user_id
                   ,client_ip
                   ,rec_sts
                   ,proc_no
               )
               SELECT *
               FROM (
                   SELECT
                        @comp_id AS comp_id
                       ,@f_year AS f_year
                       ,@f_month AS f_month
                       ,@dept20 AS dept
                       ,A.cost_sheet_id AS cost_sheet_id
                       ,A.item_code AS item_code
                       ,'R10' AS fix_cost_group
                       ,B.pc_labour AS fix_cost_amount
                       ,@proc_time AS proc_time
                       ,@user_id AS user_id
                       ,@client_ip AS client_ip
                       ,@rec_sts AS rec_sts
                       ,@proc_no AS proc_no
                   FROM istem_costing.dbo.tr_prod AS A
                   INNER JOIN @_TEMP AS B ON (B.item_code=A.item_code)
                   WHERE
                       A.comp_id=@comp_id
                       AND A.f_year=@f_year
                       AND A.f_month=@f_month
                       AND A.dept=@dept20
                   UNION ALL
                   SELECT
                        @comp_id AS comp_id
                       ,@f_year AS f_year
                       ,@f_month AS f_month
                       ,@dept20 AS dept
                       ,A.cost_sheet_id AS cost_sheet_id
                       ,A.item_code AS item_code
                       ,'R11' AS fix_cost_group
                       ,B.pc_repair AS fix_cost_amount
                       ,@proc_time AS proc_time
                       ,@user_id AS user_id
                       ,@client_ip AS client_ip
                       ,@rec_sts AS rec_sts
                       ,@proc_no AS proc_no
                   FROM istem_costing.dbo.tr_prod AS A
                   INNER JOIN @_TEMP AS B ON (B.item_code=A.item_code)
                   WHERE
                       A.comp_id=@comp_id
                       AND A.f_year=@f_year
                       AND A.f_month=@f_month
                       AND A.dept=@dept20
                   UNION ALL
                   SELECT
                        @comp_id AS comp_id
                       ,@f_year AS f_year
                       ,@f_month AS f_month
                       ,@dept20 AS dept
                       ,A.cost_sheet_id AS cost_sheet_id
                       ,A.item_code AS item_code
                       ,'R07' AS fix_cost_group
                       ,B.pc_fixed AS fix_cost_amount
                       ,@proc_time AS proc_time
                       ,@user_id AS user_id
                       ,@client_ip AS client_ip
                       ,@rec_sts AS rec_sts
                       ,@proc_no AS proc_no
                   FROM istem_costing.dbo.tr_prod AS A
                   INNER JOIN @_TEMP AS B ON (B.item_code=A.item_code)
                   WHERE
                       A.comp_id=@comp_id
                       AND A.f_year=@f_year
                       AND A.f_month=@f_month
                       AND A.dept=@dept20
               ) AS A
               WHERE NULLIF(A.fix_cost_amount, 0) IS NOT NULL


   PRINT 'Proc : 19';
   SELECT @proc_no=ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_prod WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept20;
   INSERT INTO istem_costing.dbo.tr_prod_hist SELECT * FROM istem_costing.dbo.tr_prod WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept20;
   UPDATE T
       SET
       T.process_cost = ISNULL(S.pc_fixed, 0) + ISNULL(S.pc_labour, 0) + ISNULL(S.pc_repair, 0)
       ,T.rec_sts='T'
       ,T.proc_no=@proc_no
       ,T.proc_time=@proc_time
       ,T.user_id=@user_id
       ,T.client_ip=@client_ip
   FROM istem_costing.dbo.tr_prod AS T
   LEFT JOIN @_TEMP AS S ON (S.item_code=T.item_code)
   WHERE
       T.comp_id=@comp_id
       AND T.f_year=@f_year
       AND T.f_month=@f_month
       AND T.dept=@dept20
       AND ISNULL(S.pc_fixed, 0) <> 0
       AND ISNULL(S.pc_labour, 0) <> 0
       AND ISNULL(S.pc_repair, 0) <> 0
END
