
ALTER PROCEDURE  [sp_WeavingProcess]
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN

EXEC dbo.sp_WeavingProcessRCVRM @comp_id, @f_year, @f_month, @user_id, @client_ip
EXEC dbo.sp_WeavingProcessWIP1 @comp_id, @f_year, @f_month, @user_id, @client_ip
EXEC dbo.sp_WeavingProcessWIP2 @comp_id, @f_year, @f_month, @user_id, @client_ip
EXEC dbo.sp_WeavingProcessWIP3 @comp_id, @f_year, @f_month, @user_id, @client_ip
EXEC dbo.sp_WeavingProcessWIPRM @comp_id, @f_year, @f_month, @user_id, @client_ip
EXEC dbo.sp_WeavingProcessWH @comp_id, @f_year, @f_month, @user_id, @client_ip
EXEC dbo.sp_WeavingProcessYarnCost @comp_id, @f_year, @f_month, @user_id, @client_ip
EXEC dbo.sp_WeavingProcessWIPCALC @comp_id, @f_year, @f_month, @user_id, @client_ip
EXEC dbo.sp_WeavingProcessConsMat @comp_id, @f_year, @f_month, @user_id, @client_ip
END
