

-- =============================================
-- Author:		Apri
-- Create date: 2020-10-01
-- Description:	Proses Data untuk Spinning
--				[RCVRM]
-- =============================================
ALTER PROCEDURE  [sp_SpinningProcessRCVRM]
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;
	-- DECLARE
	-- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
	-- 
	-- SET @comp_id=1
	-- SET @f_year=2020
	-- SET @f_month=7
	-- SET @user_id='SYS'
	-- SET @client_ip='192.168.1.1'

	DECLARE @dept INT
	DECLARE @out_dest INT
	DECLARE @mc_loc VARCHAR(MAX)
	DECLARE @tr_code VARCHAR(MAX)
	DECLARE @proc_code VARCHAR(MAX)
	DECLARE @proc_time AS DATETIME
	DECLARE @rec_sts VARCHAR(MAX)
	DECLARE @proc_no INT
	DECLARE @item_code VARCHAR(MAX)

	SET @out_dest=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id) AND (dept_seq=20))
	SET @dept=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id) AND (dept_seq=20))
	SET @mc_loc=''
	SET @tr_code='RCVRM'
	SET @proc_code='RCVRM'
	SET @item_code=''

	SET @proc_time = GETDATE()
	SET @rec_sts='A'
	SET @proc_no=1

	-- Cek tr_dept_calc
	DECLARE @c INT
	SET @c = (SELECT COUNT(*) AS c FROM istem_costing.dbo.tr_dept_calc
				WHERE ((comp_id=@comp_id) AND (f_year=@f_year) AND (f_month=@f_month) AND (dept=@dept) AND (tr_code=@tr_code) AND (mc_loc=@mc_loc) AND (proc_code=@proc_code) AND (item_code=@item_code)))

	IF @c >= 1
	BEGIN
		-- Set Proc No
		SELECT @proc_no =(MAX(proc_no)+1) FROM istem_costing.dbo.tr_dept_calc
			WHERE ((comp_id=@comp_id) AND (f_year=@f_year) AND (f_month=@f_month) AND (dept=@dept) AND (tr_code=@tr_code) AND (mc_loc=@mc_loc) AND (proc_code=@proc_code) AND (item_code=@item_code))

		-- Simpan History
		INSERT INTO istem_costing.dbo.tr_dept_calc_hist (comp_id, f_year, f_month, dept, tr_code, mc_loc, proc_code, item_code, mat_code, mat_qty, mat_qty_unit, proc_type, proc_time, user_id, client_ip, rec_sts, proc_no)
			SELECT comp_id, f_year, f_month, dept, tr_code, mc_loc, proc_code, item_code, mat_code, mat_qty, mat_qty_unit, proc_type, proc_time, user_id, client_ip, rec_sts, proc_no
			FROM istem_costing.dbo.tr_dept_calc
			WHERE ((comp_id=@comp_id) AND (f_year=@f_year) AND (f_month=@f_month) AND (dept=@dept) AND (tr_code=@tr_code) AND (mc_loc=@mc_loc) AND (proc_code=@proc_code) AND (item_code=@item_code))

		-- Delete exisiting
		DELETE FROM istem_costing.dbo.tr_dept_calc
			WHERE ((comp_id=@comp_id) AND (f_year=@f_year) AND (f_month=@f_month) AND (dept=@dept) AND (tr_code=@tr_code) AND (mc_loc=@mc_loc) AND (proc_code=@proc_code) AND (item_code=@item_code))

		-- Set Status
		SET @rec_sts='T'
	END

	DECLARE @_TEMP_TR_DEPT_CALC TABLE (
		[comp_id] [numeric](1, 0) NOT NULL,
		[f_year] [numeric](4, 0) NOT NULL,
		[f_month] [numeric](2, 0) NOT NULL,
		[dept] [numeric](3, 0) NOT NULL,
		[tr_code] [varchar](5) NOT NULL,
		[mc_loc] [varchar](15) NOT NULL,
		[proc_code] [varchar](7) NOT NULL,
		[item_code] [varchar](45) NOT NULL,
		[mat_code] [varchar](45) NOT NULL,
		[mat_qty] [numeric](12, 2) NULL,
		[mat_amount] [numeric](16, 2) NULL,
		[mat_qty_unit] [varchar](5) NULL,
		[proc_type] [varchar](20) NULL,
		[proc_time] [datetime] NULL,
		[user_id] [varchar](6) NULL,
		[client_ip] [varchar](15) NULL,
		[rec_sts] [char](1) NULL,
		[proc_no] [tinyint] NULL
	)

	INSERT INTO @_TEMP_TR_DEPT_CALC
	SELECT
		 @comp_id AS comp_id
		,@f_year AS f_year
		,@f_month AS f_month
		,@dept AS dept
		,@tr_code AS tr_code
		,@mc_loc AS mc_loc
		,@proc_code AS proc_code
		,@item_code AS item_code
		,A.mat_code AS mat_code
		,A.out_qty AS mat_qty
		,A.out_amount AS mat_amount
		,A.qty_unit AS mat_qty_unit
		,B.proc_type AS proc_type
		,@proc_time AS proc_time
		,@user_id AS user_id
		,@client_ip AS client_ip
		,@rec_sts AS rec_sts
		,@proc_no AS proc_no
	FROM istem_costing.dbo.tr_inv_out_detail AS A
	LEFT JOIN istem_costing.dbo.ms_process AS B ON (
			B.comp_id=A.comp_id
		AND B.dept=@dept
		AND B.proc_code=@proc_code
	)
	WHERE
			(A.comp_id=@comp_id)
		AND (A.f_year=@f_year)
		AND (A.f_month=@f_month)
		AND (A.out_dest=@out_dest)
		AND (A.cost_sheet_id IN ('A10', 'A11', 'A12', 'A13'))
		AND (A.f_year=@f_year)

	INSERT INTO istem_costing.dbo.tr_dept_calc ( comp_id, f_year, f_month, dept, tr_code, mc_loc, proc_code, item_code, mat_code, mat_qty, mat_amount, mat_qty_unit, proc_type, proc_time, user_id, client_ip, rec_sts, proc_no )
		SELECT comp_id, f_year, f_month, dept, tr_code, mc_loc, proc_code, item_code, mat_code, mat_qty, mat_amount, mat_qty_unit, proc_type, proc_time, user_id, client_ip, rec_sts, proc_no
			FROM @_TEMP_TR_DEPT_CALC

END


