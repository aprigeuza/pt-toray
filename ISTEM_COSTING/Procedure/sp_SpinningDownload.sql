-- =============================================
-- Author:		Apri
-- Create date: 2020-10-07
-- Description:	Spinning Download
-- =============================================
ALTER PROCEDURE  [sp_SpinningDownload]
	@comp_id INT, @f_year AS INT, @f_month AS INT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @dept INT
			,@dept_seq INT
			,@max_date DATE

	SET @dept_seq = 20

	SELECT @dept = dept FROM istem_costing.dbo.ms_dept WHERE (dept_seq=@dept_seq)

	SELECT @max_date=MAX(stock_take_date) FROM istem_costing.dbo.tr_dept_detail1 AS A
		WHERE
			(A.comp_id=@comp_id)
		AND (A.f_year=@f_year)
		AND (A.f_month=@f_month)
		AND (A.tr_code IN ('WIP'))
		AND (A.dept=@dept)

	SELECT
		 B.[stock_take_date]
		,B.[tr_code]
		,B.[mc_loc]
		,A.[mc_proc_code]
		,A.[mc_proc_name]
		,B.[mc_dtl_code]
		,C.[mc_dtl_name]
		,B.[item_code]
		,B.[mat_code]
		,NULLIF(B.[input_qty], 0) AS input_qty
		,B.[input_qty_unit]
	FROM [istem_costing].[dbo].[ms_mc_proc] AS A
	LEFT JOIN [istem_costing].[dbo].[tr_dept_detail1] AS B ON (
			B.comp_id=A.comp_id
		AND B.dept=A.dept
		AND B.mc_proc_code=A.mc_proc_code
	)
	LEFT JOIN [istem_costing].[dbo].[ms_mc_proc_dtl] AS C ON (
			C.comp_id=B.comp_id
		AND C.dept=B.dept
		AND C.tr_code=B.tr_code
		AND C.mc_dtl_code=B.mc_dtl_code
	)
	WHERE
			(A.comp_id=@comp_id)
		AND (A.dept=@dept)
		AND (B.f_year=@f_year)
		AND (B.f_month=@f_month)
		AND (B.stock_take_date=@max_date)
	ORDER BY B.mc_loc DESC
END
