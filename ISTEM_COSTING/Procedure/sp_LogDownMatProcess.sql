ALTER PROCEDURE  sp_LogDownMatProcess
    @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
    SET NOCOUNT ON;

    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=7
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @_TEMP_INFO TABLE( info VARCHAR(100), affected_rows INT);
    DECLARE @_RowCounts TABLE( [row_count] INT);
    DECLARE @info VARCHAR(100), @row_count INT, @proc_no INT;
    DECLARE @tr_sap_purchase_proc_no INT;
    DECLARE @tr_inv_in_proc_no INT;
    DECLARE @tr_inv_out_header_proc_no INT;

    DECLARE @_tb_proc_no TABLE(
         tr_sap_purchase_proc_no INT
        ,tr_inv_in_proc_no INT
        ,tr_inv_out_header_proc_no INT
    );

    IF @comp_id=1
    BEGIN
        INSERT INTO @_tb_proc_no EXEC istem_costing.dbo.sp_LogDownMatPrepareImport @comp_id, @f_year, @f_month;

        SELECT
            @tr_sap_purchase_proc_no=tr_sap_purchase_proc_no
            ,@tr_inv_in_proc_no=tr_inv_in_proc_no
            ,@tr_inv_out_header_proc_no=tr_inv_out_header_proc_no
        FROM @_tb_proc_no


        -- ================================================================================================
        -- Process Sub 1
        SET @info = 'Process : GR PO - Data Purchase Material, ItmsGrpCod = 106 s/d 111 ';
        PRINT @info;
        EXEC @row_count =  istem_costing.dbo.sp_LogDownMatGRPO @comp_id, @f_year, @f_month, @user_id, @client_ip, @tr_sap_purchase_proc_no
        INSERT INTO @_TEMP_INFO (info, affected_rows) VALUES (@info, @row_count)
        SET @info = 'Process : GR PO - Data Purchase Material, ItmsGrpCod = 106 s/d 111 Summary ';
        PRINT @info;
        EXEC @row_count =  istem_costing.dbo.sp_LogDownMatGRPO2 @comp_id, @f_year, @f_month, @user_id, @client_ip, @tr_inv_in_proc_no
        INSERT INTO @_TEMP_INFO (info, affected_rows) VALUES (@info, @row_count)

        -- ================================================================================================
        -- Process Sub2
        SET @info = 'Process : GI Staple - Delivery Staple to Spinning/other Dept ';
        PRINT @info;
        EXEC @row_count =  istem_costing.dbo.sp_LogDownMatGIStaple @comp_id, @f_year, @f_month, @user_id, @client_ip, @tr_inv_out_header_proc_no
        INSERT INTO @_TEMP_INFO (info, affected_rows) VALUES (@info, @row_count)
        -- ================================================================================================
        -- Process Sub3
        SET @info = 'Process : GR Yarn Product - Receive Yarn Production Spinning ';
        PRINT @info;
        EXEC @row_count =  istem_costing.dbo.sp_LogDownMatGRYarnProd @comp_id, @f_year, @f_month, @user_id, @client_ip, @tr_inv_in_proc_no
        INSERT INTO @_TEMP_INFO (info, affected_rows) VALUES (@info, @row_count)
        -- ================================================================================================
        -- Process Sub4
        SET @info = 'Process : GI Yarn - Delivery Yarn to Weaving or other Dept ';
        PRINT @info;
        EXEC @row_count =  istem_costing.dbo.sp_LogDownMatGIYarn @comp_id, @f_year, @f_month, @user_id, @client_ip, @tr_inv_out_header_proc_no
        INSERT INTO @_TEMP_INFO (info, affected_rows) VALUES (@info, @row_count)
        -- ================================================================================================
        -- Process Sub5
        SET @info = 'Process : Data Consume/Delivery ';
        PRINT @info;
        EXEC @row_count =  istem_costing.dbo.sp_LogDownMatConsumeDelivery @comp_id, @f_year, @f_month, @user_id, @client_ip, @tr_inv_out_header_proc_no
        INSERT INTO @_TEMP_INFO (info, affected_rows) VALUES (@info, @row_count)
    END

    --IF @comp_id=2
    --BEGIN
    --    INSERT INTO @_tb_proc_no EXEC istem_costing.dbo.sp_LogDownMatPrepareImport @comp_id, @f_year, @f_month;
    --    SELECT
    --        @tr_sap_purchase_proc_no=tr_sap_purchase_proc_no
    --        ,@tr_inv_in_proc_no=tr_inv_in_proc_no
    --        ,@tr_inv_out_header_proc_no=tr_inv_out_header_proc_no
    --    FROM @_tb_proc_no
    --    -- ================================================================================================
    --    -- Process Sub 1
    --    SET @info = 'Process : GR PO - Data Purchase Material, ItmsGrpCod = 106 s/d 111 ';
    --    PRINT @info;
    --    EXEC @row_count = istem_costing.dbo.sp_LogDownMatGRPO @comp_id, @f_year, @f_month, @user_id, @client_ip, @tr_sap_purchase_proc_no
    --    INSERT INTO @_TEMP_INFO (info, affected_rows) VALUES (@info, @row_count)
    --END


    SELECT * FROM @_TEMP_INFO;



END
