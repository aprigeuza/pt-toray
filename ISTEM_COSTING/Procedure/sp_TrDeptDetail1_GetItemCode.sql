

ALTER PROCEDURE  [sp_TrDeptDetail1_GetItemCode]
	@comp_id INT, 
	@f_year INT,
	@f_month INT,
	@tr_code VARCHAR(MAX),
	@mc_loc VARCHAR(MAX),
	@mc_proc_code VARCHAR(MAX),
	@stock_take_date Date
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	SELECT
	  d.item_code
	FROM [istem_costing].[dbo].[tr_dept_detail1] AS d
	WHERE 
		(d.comp_id=@comp_id)
		AND
		(d.f_year=@f_year)
		AND
		(d.f_month=@f_month)
		AND
		(d.tr_code=@tr_code)
		AND
		(d.mc_loc=@mc_loc)
		AND
		(d.mc_proc_code=@mc_proc_code)
		AND
		(d.stock_take_date=@stock_take_date)
	GROUP BY 
	  d.item_code
END


