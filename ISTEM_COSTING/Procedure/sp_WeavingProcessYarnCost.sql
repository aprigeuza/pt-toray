-- Weaving Yarn Cost
ALTER PROCEDURE  sp_WeavingProcessYarnCost
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;

    PRINT 'Weaving Yarn Cost';

    -- DECLARE
    -- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=7
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept10 INT
    DECLARE @dept30 INT
    DECLARE @tr_code VARCHAR(MAX)
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_time AS DATETIME
    DECLARE @proc_no INT
    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT

    SET @dept10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=10))
    SET @dept30=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=30))

    SET @tr_code=''
    SET @proc_time = GETDATE()
    SET @rec_sts='A'
    SET @proc_no=1

    IF @f_month = 1
    BEGIN
    	SET @lp_f_month = 12
    	SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
    	SET @lp_f_month = @f_month - 1
    	SET @lp_f_year = @f_year
    END

    -- Get All Material
    DECLARE @_TEMP_MATERIAL TABLE(
    	cost_sheet_id VARCHAR(MAX)
    	,mat_code VARCHAR(MAX)
    )
    INSERT INTO @_TEMP_MATERIAL (cost_sheet_id, mat_code)
    	SELECT A.cost_sheet_id, A.mat_code FROM (
    		-- BF
    		SELECT
    			 cost_sheet_id
    			,mat_code
    		FROM istem_costing.dbo.tr_bal_mat
    		WHERE
    			comp_id=@comp_id
    			AND f_year=@lp_f_year
    			AND f_month=@lp_f_month
    			AND dept IN (@dept10)
    			AND cost_sheet_id IN ('A20', 'A21')
    		UNION ALL
    		-- Receive
    		SELECT
    			cost_sheet_id
    		   ,mat_code
    		FROM istem_costing.dbo.tr_inv_in
    		WHERE
    			comp_id=@comp_id
    			AND f_year=@f_year
    			AND f_month=@f_month
    			AND dept IN (@dept10)
    			AND cost_sheet_id IN ('A20', 'A21')
    		UNION ALL
    		-- Consume
    		SELECT
    			 cost_sheet_id
    			,mat_code
    		FROM istem_costing.dbo.tr_inv_out_detail
    		WHERE
    			comp_id=@comp_id
    			AND f_year=@f_year
    			AND f_month=@f_month
    			AND dept IN (@dept10)
    			AND cost_sheet_id IN ('A20', 'A21')
    	) AS A
    	GROUP BY cost_sheet_id, mat_code



    -- Create Logistic Balance Staple based on Nett Qty
    DECLARE @_TEMP_BALANCE_TABLE TABLE(
    	 cost_sheet_id VARCHAR(MAX)
    	,mat_code VARCHAR(MAX)
    	,mat_name VARCHAR(MAX)
    	,bf_qty DECIMAL(16,4)
    	,bf_price DECIMAL(16,4)
    	,bf_amount DECIMAL(16,4)
    	,rec_qty DECIMAL(16,4)
    	,rec_price DECIMAL(16,4)
    	,rec_amount DECIMAL(16,4)
    	,con_wv_qty DECIMAL(16,4)
    	,con_wv_price DECIMAL(16,4)
    	,con_wv_amount DECIMAL(16,4)
    	,con_ot_qty DECIMAL(16,4)
    	,con_ot_price DECIMAL(16,4)
    	,con_ot_amount DECIMAL(16,4)
    	,cf_qty DECIMAL(16,4)
    	,cf_price DECIMAL(16,4)
    	,cf_amount DECIMAL(16,4)
    )
    INSERT INTO @_TEMP_BALANCE_TABLE (
    		cost_sheet_id
    		,mat_code
    		,mat_name
    		,bf_qty
    		,bf_price
    		,bf_amount
    		,rec_qty
    		,rec_price
    		,rec_amount
    		,con_wv_qty
    		,con_wv_price
    		,con_wv_amount
    		,con_ot_qty
    		,con_ot_price
    		,con_ot_amount
    		,cf_qty
    		,cf_price
    		,cf_amount
    	)

    	SELECT
    		 A.cost_sheet_id AS cost_sheet_id
    		,A.mat_code AS mat_code
    		,A.mat_name AS mat_name
    		,ISNULL(A.bf_qty,0) AS bf_qty -- A
    		,ISNULL(A.bf_price,0) AS bf_price -- B
    		,ISNULL(A.bf_amount,0) AS bf_amount -- C
    		,ISNULL(A.rec_qty,0) AS rec_qty -- D
    		,ISNULL(A.rec_price,0) AS rec_price -- E
    		,ISNULL(A.rec_amount,0) AS rec_amount -- F
    		,ISNULL(A.con_wv_qty,0) AS con_wv_qty -- G
    		,ISNULL(A.con_wv_price,0) AS con_wv_price -- H
    		,ISNULL(A.con_wv_amount,0) AS con_wv_amount -- I
    		,ISNULL(A.con_ot_qty,0) AS con_ot_qty -- J
    		,ISNULL(A.con_ot_price,0) AS con_ot_price -- K
    		,ISNULL(A.con_ot_amount,0) AS con_ot_amount -- L
    		,ISNULL( ((ISNULL(A.bf_qty,0)+ISNULL(A.rec_qty,0))-(ISNULL(A.con_wv_qty,0)+ISNULL(A.con_ot_qty,0))) ,0) AS cf_qty -- M
    		,ISNULL(A.con_wv_price,0) AS cf_price -- N
    		,ISNULL( (((ISNULL(A.bf_qty,0)+ISNULL(A.rec_qty,0))-(ISNULL(A.con_wv_qty,0)+ISNULL(A.con_ot_qty,0)))*ISNULL(A.con_wv_price,0)) ,0) AS cf_amount -- P
    	FROM (
    		SELECT
    			 MAT.cost_sheet_id AS cost_sheet_id
    			,MAT.mat_code AS mat_code
    			,ISNULL(M.FrgnName, '') AS mat_name
    			,BF.qty AS bf_qty -- A
    			,(NULLIF(BF.amount, 0)/NULLIF(BF.qty, 0)) AS bf_price -- B
    			,BF.amount AS bf_amount -- C
    			,REC.qty AS rec_qty -- D
    			,(NULLIF(REC.amount, 0)/NULLIF(REC.qty, 0)) AS rec_price -- E
    			,REC.amount AS rec_amount -- F
    			,CON_WV.qty AS con_wv_qty -- G
    			,
    			(NULLIF((ISNULL(BF.amount, 0)+ISNULL(REC.amount, 0)), 0) /
    			NULLIF((ISNULL(BF.qty, 0)+ISNULL(REC.qty, 0)), 0)) AS con_wv_price -- H : (C + F) / (A + D)
    			,(
    				NULLIF(CON_WV.qty, 0) *
    				(NULLIF((ISNULL(BF.amount, 0)+ISNULL(REC.amount, 0)), 0) /
    				NULLIF((ISNULL(BF.qty, 0)+ISNULL(REC.qty, 0)), 0))
    			) AS con_wv_amount -- I
    			,CON_OT.qty AS con_ot_qty -- J
    			,NULLIF((ISNULL(BF.amount, 0)+ISNULL(REC.amount, 0)), 0)/NULLIF((ISNULL(BF.qty, 0)+ISNULL(REC.qty, 0)), 0) AS con_ot_price -- K : (C + F) / (A + D)
    			,(
    				NULLIF(CON_OT.qty, 0) *
    				(
    					NULLIF((ISNULL(BF.amount, 0)+ISNULL(REC.amount, 0)), 0)/NULLIF((ISNULL(BF.qty, 0)+ISNULL(REC.qty, 0)), 0)
    				)
    			) AS con_ot_amount -- L
    		FROM @_TEMP_MATERIAL AS MAT
    		LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM AS M ON (M.ItemCode COLLATE SQL_Latin1_General_CP1_CI_AS = MAT.mat_code COLLATE SQL_Latin1_General_CP1_CI_AS)
    		-- BF
    		LEFT JOIN (
    			-- BF
    			SELECT
    				 cost_sheet_id
    				,mat_code AS mat_code
    				,NULLIF(SUM(cf_qty), 0) AS qty
    				,NULLIF(SUM(cf_amount), 0) AS amount
    			FROM istem_costing.dbo.tr_bal_mat
    			WHERE
    				comp_id=@comp_id
    				AND f_year=@lp_f_year
    				AND f_month=@lp_f_month
    				AND dept=@dept10
    				AND cost_sheet_id IN ('A20', 'A21')
                GROUP BY cost_sheet_id, mat_code
    		) AS BF ON (BF.cost_sheet_id=MAT.cost_sheet_id AND BF.mat_code=MAT.mat_code)
    		-- Receive
    		LEFT JOIN (
    			-- Receive
    			SELECT
    				 cost_sheet_id
    				,mat_code AS mat_code
    				,NULLIF(SUM(in_qty), 0) AS qty
    				,NULLIF(SUM(in_amount), 0) AS amount
    			FROM istem_costing.dbo.tr_inv_in
    			WHERE
    				comp_id=@comp_id
    				AND f_year=@f_year
    				AND f_month=@f_month
    				AND dept=@dept10
    				AND cost_sheet_id IN ('A20', 'A21')
                GROUP BY cost_sheet_id, mat_code
    		) AS REC ON (REC.cost_sheet_id=MAT.cost_sheet_id AND REC.mat_code=MAT.mat_code)
    		-- Consume Weaving
    		LEFT JOIN (
    			-- Consume
    			SELECT
    				 cost_sheet_id
    				,mat_code AS mat_code
    				,NULLIF(SUM(out_qty), 0) AS qty
    				,NULLIF(SUM(out_amount), 0) AS amount
    			FROM istem_costing.dbo.tr_inv_out_detail
    			WHERE
    				comp_id=@comp_id
    				AND f_year=@f_year
    				AND f_month=@f_month
    				AND dept=@dept10
    				AND out_dest=@dept30
    				AND cost_sheet_id IN ('A20', 'A21')
                GROUP BY cost_sheet_id, mat_code
    		) AS CON_WV ON (CON_WV.cost_sheet_id=MAT.cost_sheet_id AND CON_WV.mat_code=MAT.mat_code)
    		-- Consume Other
    		LEFT JOIN (
    			-- Consume
    			SELECT
    				 cost_sheet_id
    				,mat_code AS mat_code
    				,NULLIF(SUM(out_qty), 0) AS qty
    				,NULLIF(SUM(out_amount), 0) AS amount
    			FROM istem_costing.dbo.tr_inv_out_detail
    			WHERE
    				comp_id=@comp_id
    				AND f_year=@f_year
    				AND f_month=@f_month
    				AND dept=@dept10
    				AND out_dest<>@dept30
    				AND cost_sheet_id IN ('A20', 'A21')
                GROUP BY cost_sheet_id, mat_code
    		) AS CON_OT ON (CON_OT.cost_sheet_id=MAT.cost_sheet_id AND CON_OT.mat_code=MAT.mat_code)
    	) AS A

    UPDATE T SET
        T.out_amount = NULLIF(T.out_qty, 0) / NULLIF(S.con_wv_qty, 0) * NULLIF(S.con_wv_amount, 0)
    FROM istem_costing.dbo.tr_inv_out_detail AS T
    INNER JOIN @_TEMP_BALANCE_TABLE AS S
    ON (
       T.comp_id=@comp_id
       AND T.f_year=@f_year
       AND T.f_month=@f_month
       AND T.dept=@dept10
       AND T.cost_sheet_id=S.cost_sheet_id
       AND T.mat_code=S.mat_code
    )

END
