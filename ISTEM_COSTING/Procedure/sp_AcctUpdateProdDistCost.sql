
ALTER PROCEDURE  sp_AcctUpdateProdDistCost
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;
    DECLARE @dept20 INT;
    DECLARE @dept30 INT;
    DECLARE @dept40 INT;
    SET @dept20=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=20));
    SET @dept30=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=30));
    SET @dept40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40));

    UPDATE T SET
        T.distribution_cost = S.sum_prod_cost_distr
    FROM istem_costing.dbo.tr_prod AS T
    LEFT JOIN (
        SELECT
            comp_id
            , f_year
            , f_month
            , dept
            , item_code
            , SUM(prod_cost_distr) AS sum_prod_cost_distr
        FROM tr_prod_detail2
        WHERE prod_cost_distr IS NOT NULL
        GROUP BY

            comp_id
            , f_year
            , f_month
            , dept
            , item_code
    ) AS S ON (
        S.comp_id=T.comp_id
        AND S.f_year=T.f_year
        AND S.f_month=T.f_month
        AND S.dept=T.dept
        AND S.item_code=T.item_code
    )
    WHERE
        T.comp_id=@comp_id
        AND T.f_year=@f_year
        AND T.f_month=@f_month
        AND T.dept IN (@dept20, @dept30)

    UPDATE T SET
        T.distribution_cost = (NULLIF(T.prod_qty_mtr, 0)/NULLIF(A.sum_prod_qty_mtr, 0)) * S.sum_prod_cost_distr
    FROM istem_costing.dbo.tr_prod_fg AS T
    LEFT JOIN (
        SELECT
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,grey_no
            ,ci_no
            ,item_category
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
            ,SUM(prod_qty_mtr) AS sum_prod_qty_mtr
        FROM istem_costing.dbo.tr_prod_fg
        GROUP BY
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,grey_no
            ,ci_no
            ,item_category
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
    ) AS A ON (
        A.comp_id=T.comp_id
        AND A.f_year=T.f_year
        AND A.f_month=T.f_month
        AND A.dept=T.dept
        AND A.grey_no=T.grey_no
        AND A.item_category=T.item_category
        AND A.dy_proc_type=T.dy_proc_type
        AND A.dy_proc_type=T.dy_proc_type
        AND A.dyes_type_code=T.dyes_type_code
        AND A.color_shade_code=T.color_shade_code
    )
    LEFT JOIN (
        SELECT
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,item_code AS grey_no
            ,ci_no
            ,item_category
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
            ,SUM(prod_cost_distr) AS sum_prod_cost_distr
        FROM tr_prod_detail2
        WHERE prod_cost_distr IS NOT NULL
        GROUP BY
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,item_code
            ,ci_no
            ,item_category
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
    ) AS S ON (
        S.comp_id=T.comp_id
        AND S.f_year=T.f_year
        AND S.f_month=T.f_month
        AND S.dept=T.dept
        AND S.grey_no=T.grey_no
        AND S.item_category=T.item_category
        AND S.dy_proc_type=T.dy_proc_type
        AND S.dy_proc_type=T.dy_proc_type
        AND S.dyes_type_code=T.dyes_type_code
        AND S.color_shade_code=T.color_shade_code
    )
    WHERE
        T.comp_id=@comp_id
        AND T.f_year=@f_year
        AND T.f_month=@f_month
        AND T.dept=@dept40
END
