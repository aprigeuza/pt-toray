ALTER PROCEDURE  sp_AcctCS2Spinning
    @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    PRINT 'SUB1 : sp_AcctCS2Spinning'
    --
    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=7
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'



    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT
    IF @f_month = 1
    BEGIN
        SET @lp_f_month = 12
        SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
        SET @lp_f_month = @f_month - 1
        SET @lp_f_year = @f_year
    END

    DECLARE @dept INT
    DECLARE @prod_dept INT
    DECLARE @proc_time DATETIME
    DECLARE @rec_sts VARCHAR
    DECLARE @proc_no INT

    -- 1	Dept --> ms_dept where prod_dept = 1 & dept_seq=20
    SET @dept = (SELECT dept FROM istem_costing.dbo.ms_dept WHERE comp_id=@comp_id AND dept_seq=20)
    SET @prod_dept = 1
    SET @proc_time=GETDATE()
    SET @rec_sts='A'
    SET @proc_no=(
        SELECT ISNULL(MAX(proc_no), 0) + 1
        FROM istem_costing.dbo.tr_cost_sheet_hist
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
    )

    IF @proc_no >= 1
    BEGIN
        SET @rec_sts='T'
    END

    DECLARE @_TEMP_TR_COST_SHEET TABLE (
        row_key varchar(5) NOT NULL,
    	comp_id numeric(1,0) NOT NULL,
    	f_year numeric(4,0) NOT NULL,
    	f_month numeric(2,0) NOT NULL,
    	dept numeric(3,0) NOT NULL,
    	cost_sheet_id varchar(4) NOT NULL,
    	item_code varchar(45) NOT NULL,
    	cs_qty numeric(19,2) DEFAULT 0 NULL,
    	cs_amount numeric(19,2) DEFAULT 0 NULL,
    	cs_unit_qty numeric(12,4) DEFAULT 0 NULL,
    	cs_unit_price numeric(12,4) DEFAULT 0 NULL,
    	cs_unit_cost numeric(12,4) DEFAULT 0 NULL,
    	cs_exch_qty numeric(19,2) DEFAULT 0 NULL,
    	proc_time datetime NULL,
    	user_id varchar(6) NULL,
    	client_ip varchar(15) NULL,
    	rec_sts char(1) NULL,
    	proc_no tinyint NULL,
    	process_cost numeric(18,2) NULL
    )


    -- 2	Hitung Working Product

    DECLARE @2_cs_exch_qty DECIMAL(16, 4)
    SELECT @2_cs_exch_qty = SUM(rcv_wip_cost_qty)
    FROM istem_costing.dbo.tr_wip_cost
    WHERE
        comp_id=@comp_id
        AND f_year=@f_year
        AND f_month=@f_month
        AND dept=@dept
        AND cost_sheet_id='H99'

    INSERT INTO @_TEMP_TR_COST_SHEET
    VALUES ('2', @comp_id, @f_year, @f_month, @dept, 'P10', '', (
        (
            -- (A) This Month WIP Qty --> dari tr_wip
            ISNULL((SELECT SUM(cf_wip_qty) FROM istem_costing.dbo.tr_wip WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND ISNULL(item_code, '')=''), 0) +
            -- (B) This Month Production Qty -->  tr_prod.prod_qty
            ISNULL((SELECT SUM(prod_qty) FROM istem_costing.dbo.tr_prod WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept), 0)
        ) -
        -- (C) Last Month WIP Qty --> dari tr_bal_wip
        ISNULL((SELECT SUM(cf_wip_qty) FROM istem_costing.dbo.tr_bal_wip WHERE comp_id=@comp_id AND f_year=@lp_f_year AND f_month=@lp_f_month AND dept=@dept AND ISNULL(item_code, '')=''), 0)
    ), 0, 0, 0, 0, @2_cs_exch_qty, @proc_time, @user_id, @client_ip, @rec_sts, @proc_no, 0)

    -- 3	Raw Material
    -- 3.a	(A) Dari ms_dept. main_item_code link ke v_sp_ms_yarn_detail untuk ambil komposisi material apa saja yang dipakai
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '3.A' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,A.cost_sheet_id AS cost_sheet_id
        ,'' AS item_code
        ,NULLIF(A.sum_out_qty, 0) AS cs_qty
        ,NULLIF(A.sum_out_amount, 0) AS cs_amount
        ,NULLIF(A.sum_out_qty, 0)/NULLIF((SELECT cs_qty FROM @_TEMP_TR_COST_SHEET WHERE row_key='2' AND cost_sheet_id='P10'), 0) AS cs_unit_qty
        ,NULLIF(A.sum_out_amount, 0)/NULLIF(A.sum_out_qty, 0) AS cs_unit_price
        ,NULLIF(A.sum_out_qty, 0)/NULLIF((SELECT cs_qty FROM @_TEMP_TR_COST_SHEET WHERE row_key='2' AND cost_sheet_id='P10'), 0) * (NULLIF(A.sum_out_amount, 0)/NULLIF(A.sum_out_qty, 0))  AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM (
        SELECT
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,SUM(out_qty) AS sum_out_qty
            ,SUM(out_amount) AS sum_out_amount
        FROM istem_costing.dbo.tr_inv_out_detail
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND out_dest=@dept
        GROUP BY
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
    ) AS A

    -- 3.b	SubTotal Raw Material
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key ,comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
         '3.B' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'A99' AS cost_sheet_id
        ,'' AS item_code
        ,SUM(cs_qty) AS cs_qty
        ,SUM(cs_amount) AS cs_amount
        ,SUM(cs_unit_qty) AS cs_unit_qty
        ,0 AS cs_unit_price
        ,0 AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM @_TEMP_TR_COST_SHEET
    WHERE row_key = '3.A'

    -- 4	Utility Prop Cost
    -- 	Dari hasil Query no. 2.c
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key ,comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
         '4.A' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,A.cost_sheet_id AS cost_sheet_id
        ,'' AS item_code
        ,0 AS cs_qty
        ,A.sum_manex_amount AS cs_amount
        ,0 AS cs_unit_qty
        ,0 AS cs_unit_price
        ,0 AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM (
        SELECT cost_sheet_id,sum(manex_amount) AS sum_manex_amount
        FROM istem_costing.dbo.tr_manex
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND (cost_sheet_id BETWEEN 'C02' AND 'C99')
        GROUP BY cost_sheet_id
    ) AS A

    UPDATE T SET
        T.cs_qty=NULLIF(S.cs_qty, 0)
        ,T.cs_unit_qty=NULLIF(S.cs_qty, 0)/NULLIF((@2_cs_exch_qty), 0)
        ,T.cs_unit_price=NULLIF(T.cs_amount, 0)/NULLIF(S.cs_qty, 0)
        ,T.cs_unit_cost=(NULLIF(S.cs_qty, 0)/NULLIF((@2_cs_exch_qty), 0)) * (NULLIF(T.cs_amount, 0)/NULLIF(S.cs_qty, 0))
    FROM @_TEMP_TR_COST_SHEET AS T
    LEFT JOIN (
        SELECT
            A.cost_sheet_id
            ,C.utility_cons_qty AS cs_qty
        FROM (
            SELECT
                cost_sheet_id
                ,proc_code
                ,ori_dept
            FROM istem_costing.dbo.tr_manex
            WHERE
                    comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept=@dept
                AND (cost_sheet_id BETWEEN 'C02' AND 'C99')
            GROUP BY
                cost_sheet_id
                ,proc_code
                ,ori_dept
        ) AS A
        LEFT JOIN istem_costing.dbo.ms_convert_type AS B ON (B.proc_code=A.proc_code AND B.ut_cost_center=A.ori_dept)
        LEFT JOIN (
            SELECT
                convert_type_code
                ,utility_cons_qty
            FROM istem_costing.dbo.tr_utility_cons
            WHERE
                    comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept=@dept
        ) AS C ON (C.convert_type_code=B.convert_type_code)

    ) AS S ON (S.cost_sheet_id=T.cost_sheet_id)
    WHERE
        row_key='4.A'


    -- 5	Proportional Cost dan Fixed Cost
    -- 5.b	SubTotal Proportional Cost
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '5.B' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'E99' AS cost_sheet_id
        ,'' AS item_code
        ,0 AS cs_qty
        ,SUM(cs_amount) AS cs_amount
        ,NULLIF(( SELECT SUM(cs_qty) FROM @_TEMP_TR_COST_SHEET WHERE row_key='3.B'), 0)/NULLIF(@2_cs_exch_qty, 0) AS cs_unit_qty
        ,0 AS cs_unit_price
        ,SUM(cs_unit_cost) AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM @_TEMP_TR_COST_SHEET
    WHERE
        (
            cost_sheet_id LIKE 'C%'
            OR cost_sheet_id LIKE 'D%'
            OR cost_sheet_id LIKE 'A%'
        )
        AND row_key != '3.B'

    -- 5.a	tr_manex
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '5.A' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,A.cost_sheet_id AS cost_sheet_id
        ,'' AS item_code
        ,0 AS cs_qty
        ,A.sum_manex_amount AS cs_amount
        ,0 AS cs_unit_qty
        ,0 AS cs_unit_price
        ,NULLIF(A.sum_manex_amount, 0)/NULLIF(@2_cs_exch_qty, 0) AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM (
        SELECT
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,sum(manex_amount) AS sum_manex_amount
        FROM istem_costing.dbo.tr_manex
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND (cost_sheet_id ='C01' or cost_sheet_id >= 'D01')
        GROUP by comp_id, f_year,f_month,dept,cost_sheet_id
    ) AS A


    -- 5.c	SubTotal Fixed Cost
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '5.C' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'T40' AS cost_sheet_id
        ,'' AS item_code
        ,0 AS cs_qty
        ,SUM(cs_amount) AS cs_amount
        ,0 AS cs_unit_qty
        ,0 AS cs_unit_price
        ,NULLIF(SUM(cs_amount), 0)/NULLIF(@2_cs_exch_qty, 0)  AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM @_TEMP_TR_COST_SHEET
    WHERE row_key='5.A'


    -- 6	Warehousing Cost
    --      (A) Total Material Cost dan Process Cost dan total production Qty per Dept
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '6' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'M10' AS cost_sheet_id
        ,'' AS item_code
        ,0 AS cs_qty
        ,NULLIF(( ISNULL(mat_proc_cost, 0)+ISNULL(cost_dist,0) ), 0) AS cs_amount
        ,0 AS cs_unit_qty
        ,0 AS cs_unit_price
        ,NULLIF(( ISNULL(mat_proc_cost, 0)+ISNULL(cost_dist,0) ), 0)/NULLIF(A.sum_prod_qty, 0)  AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM (
        SELECT
            '' AS item_code
            ,SUM(prod_qty) AS sum_prod_qty
            ,SUM(material_cost)+SUM(process_cost) AS mat_proc_cost
        FROM istem_costing.dbo.tr_prod
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
    ) AS A
    LEFT JOIN (
        SELECT
            '' AS item_code
            ,SUM(tot_prod_cost_distr) AS cost_dist
        FROM istem_costing.dbo.tr_prod_detail1
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
    ) AS B ON (B.item_code=A.item_code)



    -- 7	Manufacturing Cost
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '7' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'J10' AS cost_sheet_id
        ,'' AS item_code
        ,0 AS cs_qty
        ,SUM(cs_amount) AS cs_amount
        ,0 AS cs_unit_qty
        ,0 AS cs_unit_price
        ,SUM(cs_unit_cost)  AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM @_TEMP_TR_COST_SHEET
    WHERE row_key IN ('5.B', '5.C')


    -- 8	Variance of WIP
    DECLARE @8d DECIMAL(16, 4)
    SET @8d = ( SELECT sum(cf_wip_cost_amount) FROM istem_costing.dbo.tr_bal_wip_cost WHERE comp_id=@comp_id AND f_year=@lp_f_year AND f_month=@lp_f_month AND dept=@dept) - (SELECT sum(cf_wip_cost_amount) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept)

    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '8' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'K10' AS cost_sheet_id
        ,'' AS item_code
        ,0 AS cs_qty
        ,@8d AS cs_amount
        ,0 AS cs_unit_qty
        ,0 AS cs_unit_price
        ,(SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '6')-(SELECT cs_unit_cost FROM @_TEMP_TR_COST_SHEET WHERE row_key = '7') AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost;

    -- 9	Transfer to Next Process
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '9' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'L10' AS cost_sheet_id
        ,'' AS item_code
        ,0 AS cs_qty
        ,cs_amount AS cs_amount
        ,0 AS cs_unit_qty
        ,0 AS cs_unit_price
        ,0 AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM @_TEMP_TR_COST_SHEET
    WHERE row_key='8'


    -- 10	Processing Cost (1)
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '10' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'T50' AS cost_sheet_id
        ,'' AS item_code
        ,0 AS cs_qty
        ,0 AS cs_amount
        ,0 AS cs_unit_qty
        ,0 AS cs_unit_price
        ,ISNULL(cs_unit_cost, 0)-ISNULL(( SELECT SUM(cs_unit_cost) FROM @_TEMP_TR_COST_SHEET WHERE row_key='3.A' ), 0) AS cs_unit_cost
        ,0 AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM @_TEMP_TR_COST_SHEET
    WHERE row_key='6'


    -- 11	Product Exchange
    INSERT INTO @_TEMP_TR_COST_SHEET
    (row_key, comp_id, f_year, f_month, dept, cost_sheet_id, item_code, cs_qty, cs_amount, cs_unit_qty, cs_unit_price, cs_unit_cost, cs_exch_qty, proc_time, user_id, client_ip, rec_sts, proc_no, process_cost)
    SELECT
        '11' AS row_key
        ,@comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,'N10' AS cost_sheet_id
        ,'' AS item_code
        ,A.sum_prod_qty AS cs_qty
        ,0 AS cs_amount
        ,0 AS cs_unit_qty
        ,0 AS cs_unit_price
        ,0 AS cs_unit_cost
        ,B.sum_cons_wip_cost_qty AS cs_exch_qty
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,0 AS process_cost
    FROM (
        SELECT
            '' AS item_code
            ,SUM(prod_qty) AS sum_prod_qty
        FROM istem_costing.dbo.tr_prod
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
    ) AS A
    LEFT JOIN (
        SELECT
            '' AS item_code
            ,SUM(cons_wip_cost_qty) AS sum_cons_wip_cost_qty
        FROM istem_costing.dbo.tr_wip_cost
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND cost_sheet_id='H99'
    ) AS B ON (B.item_code=A.item_code)


    INSERT INTO istem_costing.dbo.tr_cost_sheet_hist SELECT * FROM istem_costing.dbo.tr_cost_sheet WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND item_code='';
    DELETE FROM istem_costing.dbo.tr_cost_sheet WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND item_code='';
    INSERT INTO istem_costing.dbo.tr_cost_sheet
               (
                   comp_id
                   ,f_year
                   ,f_month
                   ,dept
                   ,cost_sheet_id
                   ,item_code
                   ,cs_qty
                   ,cs_amount
                   ,cs_unit_qty
                   ,cs_unit_price
                   ,cs_unit_cost
                   ,cs_exch_qty
                   ,proc_time
                   ,user_id
                   ,client_ip
                   ,rec_sts
                   ,proc_no
                   ,process_cost
               )
               SELECT
                    comp_id
                    ,f_year
                    ,f_month
                    ,dept
                    ,cost_sheet_id
                    ,item_code
                    ,cs_qty
                    ,cs_amount
                    ,cs_unit_qty
                    ,cs_unit_price
                    ,cs_unit_cost
                    ,cs_exch_qty
                    ,proc_time
                    ,user_id
                    ,client_ip
                    ,rec_sts
                    ,proc_no
                    ,process_cost
            FROM @_TEMP_TR_COST_SHEET
END
