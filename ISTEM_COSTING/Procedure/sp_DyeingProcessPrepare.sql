ALTER PROCEDURE  sp_DyeingProcessPrepare
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;
    -- Prepare
    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=7
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept_10 INT
    DECLARE @dept_11 INT
    DECLARE @dept_40 INT
    DECLARE @dept_50 INT
    DECLARE @dept_99 INT

    SET @dept_10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=10))
    SET @dept_11=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=11))
    SET @dept_40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40))
    SET @dept_50=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=50))
    SET @dept_99=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=99))

    -- tr_dept_calc
    INSERT INTO istem_costing.dbo.tr_dept_calc_hist
        SELECT *
        FROM istem_costing.dbo.tr_dept_calc
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept_40)
    DELETE FROM istem_costing.dbo.tr_dept_calc
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept_40)

    -- Update Only
    -- tr_inv_out_header
    INSERT INTO istem_costing.dbo.tr_inv_out_header_hist
        SELECT *
        FROM istem_costing.dbo.tr_inv_out_header AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_40, @dept_11)
            AND A.cost_sheet_id IN ('A30','A31');
    DELETE FROM istem_costing.dbo.tr_inv_out_header
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept_11)
            AND cost_sheet_id IN ('A30','A31');

    -- tr_inv_out_detail
    INSERT INTO istem_costing.dbo.tr_inv_out_detail_hist
        SELECT *
        FROM istem_costing.dbo.tr_inv_out_detail AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_40, @dept_11)
                AND A.cost_sheet_id IN ('A30','A31');
    DELETE FROM istem_costing.dbo.tr_inv_out_detail
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept_11)
            AND cost_sheet_id IN ('A30','A31');

    -- Update Only
    -- tr_inv_in_hist
    INSERT INTO istem_costing.dbo.tr_inv_in_hist
        SELECT *
        FROM istem_costing.dbo.tr_inv_in AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_11)
                AND A.cost_sheet_id='A31';

    -- tr_wip
    INSERT INTO istem_costing.dbo.tr_wip_hist
        SELECT *
        FROM istem_costing.dbo.tr_wip AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_40)
    DELETE FROM istem_costing.dbo.tr_wip
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept_40)

    -- tr_prod_fg
    INSERT INTO istem_costing.dbo.tr_prod_fg_hist
        SELECT *
        FROM istem_costing.dbo.tr_prod_fg AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_40)
    DELETE FROM istem_costing.dbo.tr_prod_fg
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept_40)

    -- tr_prod_fg_deliv
    INSERT INTO istem_costing.dbo.tr_prod_fg_deliv_hist
        SELECT *
        FROM istem_costing.dbo.tr_prod_fg_deliv AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_40)
    DELETE FROM istem_costing.dbo.tr_prod_fg_deliv
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept_40)
    -- tr_prod_detail2
    INSERT INTO istem_costing.dbo.tr_prod_detail2_hist
        SELECT *
        FROM istem_costing.dbo.tr_prod_detail2 AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_40)
    DELETE FROM istem_costing.dbo.tr_prod_detail2
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept_40)
    -- tr_prod_detail1
    INSERT INTO istem_costing.dbo.tr_prod_detail1_hist
        SELECT *
        FROM istem_costing.dbo.tr_prod_detail1 AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_40)
    DELETE FROM istem_costing.dbo.tr_prod_detail1
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept_40)

    -- tr_wip_eqv_detail
    INSERT INTO istem_costing.dbo.tr_wip_eqv_detail_hist
        SELECT *
        FROM istem_costing.dbo.tr_wip_eqv_detail AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_40)
    DELETE FROM istem_costing.dbo.tr_wip_eqv_detail
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept_40)
    -- tr_wip_eqv_header
    INSERT INTO istem_costing.dbo.tr_wip_eqv_header_hist
        SELECT *
        FROM istem_costing.dbo.tr_wip_eqv_header AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_40)
    DELETE FROM istem_costing.dbo.tr_wip_eqv_header
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept_40)

    -- tr_bal_wip
    INSERT INTO istem_costing.dbo.tr_bal_wip_hist
        SELECT *
        FROM istem_costing.dbo.tr_bal_wip AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_40)
    DELETE FROM istem_costing.dbo.tr_bal_wip
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept_40)

END
