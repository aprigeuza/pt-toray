ALTER PROCEDURE  sp_DyeingProcessWIPDownload
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;
    -- Download WIP from CIM
    -- Untuk data WIP normal dan Return from FG:
    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=7
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'
    DECLARE @dept_11 INT
    DECLARE @dept_40 INT
    DECLARE @proc_time AS DATETIME
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_no INT

    -- PERIOD
    DECLARE @from_date DATE, @to_date DATE
    SELECT TOP 1 @from_date = F_RefDate, @to_date = T_RefDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)

    --Konversi period SAP ke CIM
    DECLARE @target_cim_f_year INT
    DECLARE @target_cim_f_month INT

    IF (@f_month + 3) > 12
    BEGIN
        SET @target_cim_f_month = (@f_month - 9)
        SET @target_cim_f_year = @f_year + 1
    END
    ELSE
    BEGIN
        SET @target_cim_f_month = @f_month + 3
        SET @target_cim_f_year = @f_year
    END

    SET @dept_11=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=11))
    SET @dept_40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40))
    SET @proc_time=GETDATE()
    SET @rec_sts='A'
    SET @proc_no=1

    IF @proc_no >= 2
    BEGIN
        SET @rec_sts='T'
    END

    DECLARE @_TR_DEPT_CALC TABLE (
        comp_id numeric(1,0) NOT NULL,
    	f_year numeric(4,0) NOT NULL,
    	f_month numeric(2,0) NOT NULL,
    	dept numeric(3,0) NOT NULL,
    	tr_code varchar(5) NOT NULL,
    	mc_loc varchar(15) NOT NULL,
    	proc_code varchar(7) NOT NULL,
    	item_code varchar(45) NOT NULL,
    	mat_code varchar(45) NULL,
    	mat_qty numeric(12,2) DEFAULT 0 NULL,
    	mat_amount numeric(14,2) DEFAULT 0 NULL,
    	mat_qty_unit varchar(5) NULL,
    	proc_type varchar(20) NULL,
    	proc_time datetime NULL,
    	user_id varchar(6) NULL,
    	client_ip varchar(15) NULL,
    	rec_sts char(1) NULL,
    	proc_no tinyint NULL,
    	qty_pcs numeric(9,2) NULL
    )

    INSERT INTO @_TR_DEPT_CALC
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,tr_code
            ,mc_loc
            ,proc_code
            ,item_code
            ,mat_code
            ,mat_qty
            ,mat_amount
            ,mat_qty_unit
            ,proc_type
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
            ,qty_pcs
        )
        SELECT
             @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept_40 AS dept
            ,'WIP' AS tr_code
            ,A.r_flag AS mc_loc
            ,'WIP' AS proc_code
            ,A.grey_no AS item_code
            ,A.grey_no AS mat_code
            ,A.wip_length AS mat_qty
            ,NULL AS mat_amount
            ,'MTR' AS mat_qty_unit
            ,'WIP-P' AS proc_type
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,A.wip_pcs AS qty_pcs
        FROM (
            SELECT
                year
                ,month
                ,grey_no
                ,r_flag
                ,SUM(wip_pcs) AS wip_pcs
                ,SUM(wip_length) AS wip_length
            FROM istem_sms.dbo.dy_manufacturing_report
            WHERE month=@target_cim_f_month AND year=@target_cim_f_year
            GROUP BY year, month, grey_no, r_flag
        ) AS A

    -- Khusus untuk data Return from FG, perlu ambil data Receive juga
    INSERT INTO @_TR_DEPT_CALC
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,tr_code
            ,mc_loc
            ,proc_code
            ,item_code
            ,mat_code
            ,mat_qty
            ,mat_amount
            ,mat_qty_unit
            ,proc_type
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
            ,qty_pcs
        )
        SELECT
             @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept_40 AS dept
            ,'RCVRM' AS tr_code
            ,A.r_flag AS mc_loc
            ,'RCVRM' AS proc_code
            ,A.grey_no AS item_code
            ,A.grey_no AS mat_code
            ,A.received_length AS mat_qty
            ,NULL AS mat_amount
            ,'MTR' AS mat_qty_unit
            ,'RCVRM' AS proc_type
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,A.received_pcs AS qty_pcs
        FROM (
            SELECT
                year
                ,month
                ,grey_no
                ,r_flag
                ,SUM(received_pcs) AS received_pcs
                ,SUM(received_length) AS received_length
            FROM istem_sms.dbo.dy_manufacturing_report


            WHERE month=@target_cim_f_month AND year=@target_cim_f_year AND r_flag='R'
            GROUP BY year, month, grey_no, r_flag
        ) AS A

    SET @rec_sts='A'
    SET @proc_no=(SELECT ISNULL(MAX(A.proc_no), 0) + 1 FROM istem_costing.dbo.tr_dept_calc_hist AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_40)
            AND A.tr_code='WIP'
            AND A.proc_code='WIP'
            AND A.proc_type='WIP-P')
    IF @proc_no >= 2
    BEGIN
        SET @rec_sts = 'T'
    END

    -- INSERT INTO istem_costing.dbo.tr_dept_calc_hist
    --     SELECT * FROM istem_costing.dbo.tr_dept_calc AS A
    --         WHERE
    --                 A.comp_id=@comp_id
    --             AND A.f_year=@f_year
    --             AND A.f_month=@f_month
    --             AND A.dept IN (@dept_40)
    --             AND A.tr_code='WIP'
    --             AND A.proc_code='WIP'
    --             AND A.proc_type='WIP-P'
    --
    -- DELETE FROM istem_costing.dbo.tr_dept_calc
    --     WHERE
    --             comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (@dept_40)
    --         AND tr_code='WIP'
    --         AND proc_code='WIP'
    --         AND proc_type='WIP-P'

    INSERT INTO istem_costing.dbo.tr_dept_calc
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,tr_code
            ,mc_loc
            ,proc_code
            ,item_code
            ,mat_code
            ,mat_qty
            ,mat_amount
            ,mat_qty_unit
            ,proc_type
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
            ,qty_pcs
        )
        SELECT
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,tr_code
            ,mc_loc
            ,proc_code
            ,item_code
            ,mat_code
            ,mat_qty
            ,mat_amount
            ,mat_qty_unit
            ,proc_type
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
            ,qty_pcs
        FROM @_TR_DEPT_CALC
END
