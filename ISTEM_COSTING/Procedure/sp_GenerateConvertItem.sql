ALTER PROCEDURE  sp_GenerateConvertItem
    @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Next Period
    DECLARE @np_f_year INT
    DECLARE @np_f_month INT
    DECLARE @rec_sts VARCHAR(1)
    DECLARE @proc_no INT

    IF @f_month >= 12
    BEGIN
        SET @np_f_month = 1
        SET @np_f_year = @f_year + 1
    END
    ELSE
    BEGIN
        SET @np_f_month = @f_month + 1
        SET @np_f_year = @f_year
    END

    SET @rec_sts='A'
    SET @proc_no=(SELECT ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_convert_item WHERE comp_id=@comp_id AND f_year=@np_f_year AND f_month=@np_f_month)

    IF @proc_no >= 1
    BEGIN
        SET @rec_sts='T'

        INSERT INTO istem_costing.dbo.tr_convert_item_hist
            SELECT * FROM istem_costing.dbo.tr_convert_item WHERE comp_id=@comp_id AND f_year=@np_f_year AND f_month=@np_f_month;

        DELETE FROM istem_costing.dbo.tr_convert_item WHERE comp_id=@comp_id AND f_year=@np_f_year AND f_month=@np_f_month;
    END

    INSERT INTO istem_costing.dbo.tr_convert_item
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,item_code
            ,convert_type_code
            ,category_code
            ,category_detail_code
            ,convert_ratio
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
        )
        SELECT
             @comp_id AS comp_id
            ,@np_f_year AS f_year
            ,@np_f_month AS f_month
            ,dept AS dept
            ,A.item_code AS item_code
            ,A.convert_type_code AS convert_type_code
            ,A.category_code AS category_code
            ,A.category_detail_code AS category_detail_code
            ,A.convert_ratio AS convert_ratio
            ,GETDATE() AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,A.dy_proc_type
            ,A.dyes_type_code
            ,A.color_shade_code
        FROM istem_costing.dbo.tr_convert_item AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month

END
