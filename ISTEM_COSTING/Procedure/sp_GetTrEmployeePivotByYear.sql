ALTER PROCEDURE  sp_GetTrEmployeePivotByYear
    @comp_id INT, @f_year INT, @tr_flag AS VARCHAR(1)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
    SELECT *
    FROM (
        SELECT
            A.dept,
            A.dept_name,
            @tr_flag AS tr_flag,
            CONVERT(VARCHAR(25), C.f_month_cat) AS f_month_cat,
            C.v AS val
        FROM istem_costing.dbo.ms_dept AS A
        LEFT JOIN (
            SELECT comp_id, f_month, f_year
            FROM istem_costing.dbo.v_SAPPeriodAll
            WHERE comp_id=@comp_id AND f_year=@f_year
            GROUP BY comp_id, f_month, f_year
        ) AS B ON (B.comp_id=A.comp_id)
        LEFT JOIN (
            SELECT dept, f_month, CONVERT(VARCHAR(25), 'emp_' + CONVERT(VARCHAR(12), f_month)) AS f_month_cat, no_employee AS v
            FROM istem_costing.dbo.tr_employee
            WHERE comp_id=@comp_id AND f_year = @f_year AND tr_flag = @tr_flag
            UNION ALL
            SELECT dept, f_month, CONVERT(VARCHAR(25), 'ots_' + CONVERT(VARCHAR(12), f_month)) AS f_month_cat, no_outsource AS v
            FROM istem_costing.dbo.tr_employee
            WHERE comp_id=@comp_id AND f_year = @f_year AND tr_flag = @tr_flag
        ) AS C ON (C.f_month=B.f_month AND C.dept=A.dept)
        WHERE
            A.main_dept = 1
    ) AS t
    PIVOT (
        SUM(val)
        FOR f_month_cat IN (
            [emp_1], [emp_2], [emp_3], [emp_4], [emp_5], [emp_6], [emp_7], [emp_8], [emp_9], [emp_10], [emp_11], [emp_12]
            ,[ots_1], [ots_2], [ots_3], [ots_4], [ots_5], [ots_6], [ots_7], [ots_8], [ots_9], [ots_10], [ots_11], [ots_12]
        )
    ) AS Piv1
END
