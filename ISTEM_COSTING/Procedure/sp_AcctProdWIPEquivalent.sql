
ALTER PROCEDURE  [sp_AcctProdWIPEquivalent]
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;

    -- *****************************************************************************
    --                                 SUB A
    --                            UPDATE 2020-12-10
    -- *****************************************************************************

    -- SUB A1
    -- Spinning & Weaving
    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1;
    -- SET @f_year=2020;
    -- SET @f_month=7;
    -- SET @user_id='SYS';
    -- SET @client_ip='192.168.1.1';

    -- DECLARE @dept10 INT;
    -- DECLARE @dept20 INT;
    -- DECLARE @dept30 INT;
    -- DECLARE @dept40 INT;
    -- DECLARE @proc_time AS DATETIME;
    -- DECLARE @rec_sts VARCHAR(MAX);
    -- DECLARE @proc_no INT;
    --
    -- SET @dept10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=10));
    -- SET @dept20=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=20));
    -- SET @dept30=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=30));
    -- SET @dept40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40));
    -- SET @proc_time = GETDATE();
    -- SET @rec_sts='A';
    -- SET @proc_no=1;
    --
    -- DECLARE @_TEMP_TR_PROD TABLE (
    -- 	comp_id NUMERIC(1,0) NOT NULL,
    -- 	f_year NUMERIC(4,0) NOT NULL,
    -- 	f_month NUMERIC(2,0) NOT NULL,
    -- 	dept NUMERIC(3,0) NOT NULL,
    -- 	cost_sheet_id VARCHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    -- 	item_code VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    -- 	prod_qty NUMERIC(12,2) DEFAULT 0 NULL,
    -- 	material_cost NUMERIC(12,2) DEFAULT 0 NULL,
    -- 	wv_leno NUMERIC(12,2) DEFAULT 0 NULL,
    -- 	proc_time datetime NULL,
    -- 	user_id VARCHAR(6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    -- 	client_ip VARCHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    -- 	rec_sts CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    -- 	proc_no TINYINT NULL
    -- )
    -- DECLARE @_TEMP_TR_PROD_DETAIL1 TABLE (
    -- 	comp_id NUMERIC(1, 0) NOT NULL,
    -- 	f_year NUMERIC(4, 0) NOT NULL,
    -- 	f_month NUMERIC(2, 0) NOT NULL,
    -- 	dept NUMERIC(3, 0) NOT NULL,
    -- 	cost_sheet_id VARCHAR(4) NOT NULL,
    --     convert_type_code VARCHAR(3) NOT NULL,
    -- 	item_code VARCHAR(45) NOT NULL,
	--     fix_cost_group VARCHAR(3) NOT NULL,
    -- 	tot_prod_equiv_qty NUMERIC(12, 2) NULL,
    -- 	tot_prod_cost_distr NUMERIC(12, 2) NULL,
    -- 	proc_time datetime NULL,
    -- 	user_id VARCHAR(6) NULL,
    -- 	client_ip VARCHAR(15) NULL,
    -- 	rec_sts CHAR(1) NULL,
    -- 	proc_no TINYINT NULL
    -- )
    -- DECLARE @_TEMP_TR_PROD_DETAIL2 TABLE (
    -- 	 comp_id NUMERIC(1, 0) NOT NULL
    -- 	,f_year NUMERIC(4, 0) NOT NULL
    -- 	,f_month NUMERIC(2, 0) NOT NULL
    -- 	,dept NUMERIC(3, 0) NOT NULL
    -- 	,cost_sheet_id VARCHAR(4) NOT NULL
    -- 	,item_code VARCHAR(45) NOT NULL
    -- 	,convert_type_code VARCHAR(3) NOT NULL
    -- 	,fix_cost_group VARCHAR(4) NOT NULL
    -- 	,prod_equiv_qty NUMERIC(12, 2) NULL
    -- 	,prod_cost_distr NUMERIC(12, 2) NULL
    -- 	,proc_time DATETIME NULL
    -- 	,user_id VARCHAR(6) NULL
    -- 	,client_ip VARCHAR(15) NULL
    -- 	,rec_sts CHAR(1) NULL
    -- 	,proc_no TINYINT NULL
    -- )
    --
    -- -- ## Start SUB A1
    -- -- Proses SUB A1
    --
    -- -- Select : proc_no
    -- SELECT
    --     @proc_no=ISNULL(MAX(proc_no), 0) + 1
    --     FROM istem_costing.dbo.tr_prod_detail2
    --     WHERE
    --         comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL2 GROUP BY dept);
    -- SET @rec_sts = 'A';
    -- IF @proc_no >= 2
    -- BEGIN
    --     SET @rec_sts = 'T'
    -- END
    -- -- Ambil data untuk tr_prod_detail2
    -- INSERT INTO @_TEMP_TR_PROD_DETAIL2
    -- 	(
    -- 		 comp_id
    -- 		,f_year
    -- 		,f_month
    -- 		,dept
    -- 		,cost_sheet_id
    -- 		,item_code
    -- 		,convert_type_code
    -- 		,fix_cost_group
    -- 		,prod_equiv_qty
    -- 		,prod_cost_distr
    -- 		,proc_time
    -- 		,user_id
    -- 		,client_ip
    -- 		,rec_sts
    -- 		,proc_no
    -- 	)
    --     SELECT
    --         @comp_id AS comp_id
    --         ,@f_year AS f_year
    --         ,@f_month AS f_month
    --         ,B.v_dept AS dept
    --         ,D.cost_sheet_id AS cost_sheet_id
    --         ,A.mat_code AS item_code
    --         ,C.convert_type_code AS convert_type_code
    --         ,'' AS fix_cost_group
    --         ,A.v_tot_prod_qty * C.convert_ratio AS prod_equiv_qty
    --         ,NULL AS prod_cost_distr
    --         ,@proc_time AS proc_time
    --         ,@user_id AS user_id
    --         ,@client_ip AS client_ip
    --         ,@rec_sts AS rec_sts
    --         ,@proc_no AS proc_no
    --     FROM (
    --         SELECT
    --             cost_sheet_id
    --             ,mat_code
    --             ,SUM(in_qty) AS v_tot_prod_qty
    --         FROM istem_costing.dbo.tr_inv_in
    --         WHERE
    --             comp_id=@comp_id
    --             AND f_year=@f_year
    --             AND f_month=@f_month
    --             AND dept=@dept10
    --             AND cost_sheet_id IN ('A20', 'A30')
    --         GROUP BY
    --             cost_sheet_id
    --             ,mat_code
    --     ) AS A
    --     INNER JOIN (
    --         SELECT
    --         	cost_sheet_id,
    --         	dept AS v_dept
    --         FROM istem_costing.dbo.ms_cost_group_detail
    --         WHERE comp_id=@comp_id
    --     ) AS B ON (B.cost_sheet_id=A.cost_sheet_id)
    --     INNER JOIN (
    --         SELECT
    --         	convert_type_code
    --         	,dept AS v_dept
    --         	,item_code
    --         	,convert_ratio
    --         FROM istem_costing.dbo.tr_convert_item
    --         WHERE
    --             comp_id=@comp_id
    --             AND f_year=@f_year
    --             AND f_month=@f_month
    --             AND convert_type_code LIKE 'R%'
    --     ) AS C ON (C.v_dept=B.v_dept AND C.item_code=A.mat_code)
    --     INNER JOIN (
    --         SELECT
    --         	cost_sheet_id,
    --         	convert_type_code
    --         FROM istem_costing.dbo.ms_convert_type
    --         WHERE
    --         	comp_id=@comp_id
    --             AND convert_type_code LIKE 'R%'
    --     ) AS D ON (D.convert_type_code=C.convert_type_code)
    -- -- Insert : tr_prod_detail2_hist
    -- INSERT INTO istem_costing.dbo.tr_prod_detail2_hist
    --     SELECT * FROM istem_costing.dbo.tr_prod_detail2
    --     WHERE
    --         comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL2 GROUP BY dept);
    -- -- Delete : tr_prod_detail2
    -- DELETE FROM istem_costing.dbo.tr_prod_detail2
    --     WHERE
    --         comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL2 GROUP BY dept)
    -- -- Insert : tr_prod_detail2
    -- INSERT INTO istem_costing.dbo.tr_prod_detail2
    --     (
    --         comp_id
    --         ,f_year
    --         ,f_month
    --         ,dept
    --         ,cost_sheet_id
    --         ,item_code
    --         ,convert_type_code
    --         ,fix_cost_group
    --         ,prod_equiv_qty
    --         ,prod_cost_distr
    --         ,proc_time
    --         ,user_id
    --         ,client_ip
    --         ,rec_sts
    --         ,proc_no
    --     )
    --     SELECT
    --         comp_id
    --         ,f_year
    --         ,f_month
    --         ,dept
    --         ,cost_sheet_id
    --         ,item_code
    --         ,convert_type_code
    --         ,fix_cost_group
    --         ,prod_equiv_qty
    --         ,prod_cost_distr
    --         ,GETDATE()
    --         ,@user_id
    --         ,@client_ip
    --         ,@rec_sts
    --         ,@proc_no
    --     FROM @_TEMP_TR_PROD_DETAIL2
    --
    -- -- #############################################################################
    -- -- Create tr_prod_detail2
    -- SELECT
    --     @proc_no=ISNULL(MAX(proc_no), 0) + 1
    --     FROM istem_costing.dbo.tr_prod_detail1
    --     WHERE
    --         comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL2 GROUP BY dept);
    -- SET @rec_sts = 'A';
    -- IF @proc_no >= 2
    -- BEGIN
    --     SET @rec_sts = 'T'
    -- END
    --
    -- INSERT INTO @_TEMP_TR_PROD_DETAIL1
    -- 	(
    -- 		comp_id
    -- 		,f_year
    -- 		,f_month
    -- 		,dept
    -- 		,cost_sheet_id
    --         ,convert_type_code
    -- 		,item_code
    --         ,fix_cost_group
    -- 		,tot_prod_equiv_qty
    -- 		,tot_prod_cost_distr
    -- 		,proc_time
    -- 		,user_id
    -- 		,client_ip
    -- 		,rec_sts
    -- 		,proc_no
    -- 	)
    -- 	SELECT
    -- 		A.comp_id AS comp_id
    -- 		,A.f_year AS f_year
    -- 		,A.f_month AS f_month
    -- 		,A.dept AS dept
    -- 		,A.cost_sheet_id AS cost_sheet_id
    --         ,A.convert_type_code AS convert_type_code
    --         ,'' AS item_code
    --         ,A.fix_cost_group AS fix_cost_group
    -- 		,SUM(A.prod_equiv_qty) AS tot_prod_equiv_qty
    -- 		,NULL AS tot_prod_cost_distr
    -- 		,@proc_time AS proc_time
    -- 		,@user_id AS user_id
    -- 		,@client_ip AS client_ip
    -- 		,@rec_sts AS rec_sts
    -- 		,@proc_no AS proc_no
    -- 	FROM @_TEMP_TR_PROD_DETAIL2 AS A
    -- 	GROUP BY
    -- 		A.comp_id
    -- 		,A.f_year
    -- 		,A.f_month
    -- 		,A.dept
    -- 		,A.cost_sheet_id
    --         ,A.convert_type_code
    -- 		,A.fix_cost_group
    --
    -- INSERT INTO istem_costing.dbo.tr_prod_detail1_hist
    --     SELECT * FROM istem_costing.dbo.tr_prod_detail1
    --     WHERE
    --     comp_id=@comp_id
    --     AND f_year=@f_year
    --     AND f_month=@f_month
    --     AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL1 GROUP BY dept)
    --
    -- DELETE FROM istem_costing.dbo.tr_prod_detail1
    -- WHERE
    --     comp_id=@comp_id
    --     AND f_year=@f_year
    --     AND f_month=@f_month
    --     AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL1 GROUP BY dept)
    --
	-- INSERT INTO istem_costing.dbo.tr_prod_detail1
    -- 	(
    --         comp_id
    --         ,f_year
    --         ,f_month
    --         ,dept
    --         ,cost_sheet_id
    --         ,convert_type_code
    --         ,item_code
    --         ,fix_cost_group
    --         ,tot_prod_equiv_qty
    --         ,tot_prod_cost_distr
    --         ,proc_time
    --         ,user_id
    --         ,client_ip
    --         ,rec_sts
    --         ,proc_no
    --     )
    -- 	SELECT
    -- 	   comp_id
    --        ,f_year
    --        ,f_month
    --        ,dept
    --        ,cost_sheet_id
    --        ,convert_type_code
    --        ,item_code
    --        ,fix_cost_group
    --        ,tot_prod_equiv_qty
    --        ,tot_prod_cost_distr
    --        ,proc_time
    --        ,user_id
    --        ,@client_ip
    --        ,@rec_sts
    --        ,@proc_no
    --     FROM @_TEMP_TR_PROD_DETAIL1
    --
    -- -- #############################################################################
    -- -- 5	Create tr_prod
    -- SELECT
    --     @proc_no=ISNULL(MAX(proc_no), 0) + 1
    --     FROM istem_costing.dbo.tr_prod_detail1
    --     WHERE
    --         comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL2 GROUP BY dept);
    -- SET @rec_sts = 'A';
    -- IF @proc_no >= 2
    -- BEGIN
    --     SET @rec_sts = 'T'
    -- END
    -- INSERT INTO @_TEMP_TR_PROD
    --     (
    --         comp_id
    --         ,f_year
    --         ,f_month
    --         ,dept
    --         ,cost_sheet_id
    --         ,item_code
    --         ,prod_qty
    --         ,material_cost
    --         ,wv_leno
    --         ,proc_time
    --         ,user_id
    --         ,client_ip
    --         ,rec_sts
    --         ,proc_no
    --     )
    --     SELECT
    --         @comp_id
    --         ,@f_year
    --         ,@f_month
    --         ,B.v_dept AS dept
    --         ,A.cost_sheet_id AS cost_sheet_id
    --         ,A.mat_code AS item_code
    --         ,A.v_tot_prod_qty AS prod_qty
    --         ,NULL AS material_cost
    --         ,NULL AS wv_leno
    --         ,@proc_time AS proc_time
    --         ,@user_id AS user_id
    --         ,@client_ip AS client_ip
    --         ,@rec_sts AS rec_sts
    --         ,@proc_no AS proc_no
    --     FROM (
    --         SELECT
    --             cost_sheet_id
    --             ,mat_code
    --             ,SUM(in_qty) AS v_tot_prod_qty
    --         FROM istem_costing.dbo.tr_inv_in
    --         WHERE
    --             comp_id=@comp_id
    --             AND f_year=@f_year
    --             AND f_month=@f_month
    --             AND dept=@dept10
    --             AND cost_sheet_id IN ('A20', 'A30')
    --         GROUP BY
    --             cost_sheet_id
    --             ,mat_code
    --     ) AS A
    --     INNER JOIN (
    --         SELECT
    --             cost_sheet_id,
    --             dept AS v_dept
    --         FROM istem_costing.dbo.ms_cost_group_detail
    --         WHERE comp_id=@comp_id
    --     ) AS B ON (B.cost_sheet_id=A.cost_sheet_id)
    -- -- Delete
    -- DELETE FROM istem_costing.dbo.tr_prod
    --     WHERE
    --         comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (SELECT dept FROM @_TEMP_TR_PROD GROUP BY dept)
    -- -- Insert
	-- INSERT INTO istem_costing.dbo.tr_prod
    --     (
    --         comp_id
    --         ,f_year
    --         ,f_month
    --         ,dept
    --         ,cost_sheet_id
    --         ,item_code
    --         ,prod_qty
    --         ,material_cost
    --         ,wv_leno
    --         ,proc_time
    --         ,user_id
    --         ,client_ip
    --         ,rec_sts
    --         ,proc_no
    --     )
    --     SELECT
    --         comp_id
    --         ,f_year
    --         ,f_month
    --         ,dept
    --         ,cost_sheet_id
    --         ,item_code
    --         ,prod_qty
    --         ,material_cost
    --         ,wv_leno
    --         ,proc_time
    --         ,user_id
    --         ,client_ip
    --         ,rec_sts
    --         ,proc_no
    --     FROM @_TEMP_TR_PROD;




    -- SUB A1
    -- Spinning & Weaving
    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1;
    -- SET @f_year=2020;
    -- SET @f_month=7;
    -- SET @user_id='SYS';
    -- SET @client_ip='192.168.1.1';

    DECLARE @dept10 INT;
    DECLARE @dept20 INT;
    DECLARE @dept30 INT;
    DECLARE @dept40 INT;
    DECLARE @proc_time AS DATETIME;
    DECLARE @rec_sts VARCHAR(MAX);
    DECLARE @proc_no INT;

    SET @dept10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=10));
    SET @dept20=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=20));
    SET @dept30=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=30));
    SET @dept40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40));
    SET @proc_time = GETDATE();
    SET @rec_sts='A';
    SET @proc_no=1;

    -- DECLARE @_TEMP_TR_PROD TABLE (
    --     comp_id NUMERIC(1,0) NOT NULL,
    --     f_year NUMERIC(4,0) NOT NULL,
    --     f_month NUMERIC(2,0) NOT NULL,
    --     dept NUMERIC(3,0) NOT NULL,
    --     cost_sheet_id VARCHAR(4) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    --     item_code VARCHAR(45) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
    --     prod_qty NUMERIC(12,2) DEFAULT 0 NULL,
    --     material_cost NUMERIC(12,2) DEFAULT 0 NULL,
    --     wv_leno NUMERIC(12,2) DEFAULT 0 NULL,
    --     proc_time datetime NULL,
    --     user_id VARCHAR(6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    --     client_ip VARCHAR(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    --     rec_sts CHAR(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    --     proc_no TINYINT NULL
    -- )
    DECLARE @_TEMP_TR_PROD_DETAIL1 TABLE (
        comp_id NUMERIC(1, 0) NOT NULL,
        f_year NUMERIC(4, 0) NOT NULL,
        f_month NUMERIC(2, 0) NOT NULL,
        dept NUMERIC(3, 0) NOT NULL,
        cost_sheet_id VARCHAR(4) NOT NULL,
        convert_type_code VARCHAR(3) NOT NULL,
        item_code VARCHAR(45) NOT NULL,
        fix_cost_group VARCHAR(3) NOT NULL,
        tot_prod_equiv_qty NUMERIC(12, 2) NULL,
        tot_prod_cost_distr NUMERIC(12, 2) NULL,
        proc_time datetime NULL,
        user_id VARCHAR(6) NULL,
        client_ip VARCHAR(15) NULL,
        rec_sts CHAR(1) NULL,
        proc_no TINYINT NULL
    )
    DECLARE @_TEMP_TR_PROD_DETAIL2 TABLE (
        comp_id numeric(1,0) NOT NULL,
    	f_year numeric(4,0) NOT NULL,
    	f_month numeric(2,0) NOT NULL,
    	dept numeric(3,0) NOT NULL,
    	cost_sheet_id varchar(4) NOT NULL,
    	item_code varchar(45) NOT NULL,
    	ci_no varchar(20) NULL,
    	item_category varchar(2) NULL,
    	convert_type_code varchar(3) NOT NULL,
    	fix_cost_group varchar(4) NOT NULL,
    	prod_equiv_qty numeric(12,2) DEFAULT 0 NULL,
    	prod_cost_distr numeric(12,2) DEFAULT 0 NULL,
    	dy_proc_type varchar(5) DEFAULT '' NOT NULL,
    	dyes_type_code varchar(4) DEFAULT '' NOT NULL,
    	color_shade_code varchar(2) DEFAULT '' NOT NULL,
    	proc_time datetime NULL,
    	user_id varchar(6) NULL,
    	client_ip varchar(15) NULL,
    	rec_sts char(1) NULL,
    	proc_no tinyint NULL
    )

    -- ***********************************************
    --                   Spinning
    -- ***********************************************

    -- ## Start SUB A1
    -- Proses SUB A1

    -- Select : proc_no
    SELECT
        @proc_no=ISNULL(MAX(proc_no), 0) + 1
        FROM istem_costing.dbo.tr_prod_detail2
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept20);
    SET @rec_sts = 'A';
    IF @proc_no >= 2
        BEGIN
            SET @rec_sts = 'T'
        END
    -- Ambil data untuk tr_prod_detail2
    INSERT INTO @_TEMP_TR_PROD_DETAIL2
        (
             comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,item_code
            ,convert_type_code
            ,fix_cost_group
            ,prod_equiv_qty
            ,prod_cost_distr
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
            ,item_category
            ,ci_no
        )
        SELECT
            @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,B.v_dept AS dept
            ,D.cost_sheet_id AS cost_sheet_id
            ,A.mat_code AS item_code
            ,C.convert_type_code AS convert_type_code
            ,'' AS fix_cost_group
            ,A.v_tot_prod_qty * C.convert_ratio AS prod_equiv_qty
            ,NULL AS prod_cost_distr
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,E.item_category AS item_category
            ,'' AS ci_no
        FROM (
            SELECT
                cost_sheet_id
                ,mat_code
                ,SUM(in_qty) AS v_tot_prod_qty
            FROM istem_costing.dbo.tr_inv_in
            WHERE
                comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept=@dept10
                AND cost_sheet_id IN ('A20')
            GROUP BY
                cost_sheet_id
                ,mat_code
        ) AS A
        INNER JOIN (
            SELECT
                cost_sheet_id,
                dept AS v_dept
            FROM istem_costing.dbo.ms_cost_group_detail
            WHERE comp_id=@comp_id
        ) AS B ON (B.cost_sheet_id=A.cost_sheet_id)
        INNER JOIN (
            SELECT
                convert_type_code
                ,dept AS v_dept
                ,item_code
                ,convert_ratio
            FROM istem_costing.dbo.tr_convert_item
            WHERE
                comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND convert_type_code LIKE 'R%'
        ) AS C ON (C.v_dept=B.v_dept AND C.item_code=A.mat_code)
        INNER JOIN (
            SELECT
                cost_sheet_id,
                convert_type_code
            FROM istem_costing.dbo.ms_convert_type
            WHERE
                comp_id=@comp_id
                AND convert_type_code LIKE 'R%'
        ) AS D ON (D.convert_type_code=C.convert_type_code)
        LEFT JOIN istem_sms.dbo.wv_fabric_analysis_master AS E ON (E.grey_no=A.mat_code)
        WHERE
            B.v_dept = @dept20
    -- Insert : tr_prod_detail2_hist
    INSERT INTO istem_costing.dbo.tr_prod_detail2_hist
        SELECT * FROM istem_costing.dbo.tr_prod_detail2
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL2 GROUP BY dept);
    -- Delete : tr_prod_detail2
    DELETE FROM istem_costing.dbo.tr_prod_detail2
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL2 GROUP BY dept)
    -- Insert : tr_prod_detail2
    INSERT INTO istem_costing.dbo.tr_prod_detail2
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,item_code
            ,ci_no
            ,item_category
            ,convert_type_code
            ,fix_cost_group
            ,prod_equiv_qty
            ,prod_cost_distr
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,item_code
            ,ci_no
            ,item_category
            ,convert_type_code
            ,fix_cost_group
            ,prod_equiv_qty
            ,prod_cost_distr
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
            ,GETDATE()
            ,@user_id
            ,@client_ip
            ,@rec_sts
            ,@proc_no
        FROM @_TEMP_TR_PROD_DETAIL2

    -- Create tr_prod_detail1
    SELECT
        @proc_no=ISNULL(MAX(proc_no), 0) + 1
        FROM istem_costing.dbo.tr_prod_detail1
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL2 GROUP BY dept);
    SET @rec_sts = 'A';
    IF @proc_no >= 2
    BEGIN
        SET @rec_sts = 'T'
    END

    INSERT INTO @_TEMP_TR_PROD_DETAIL1
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,convert_type_code
            ,item_code
            ,fix_cost_group
            ,tot_prod_equiv_qty
            ,tot_prod_cost_distr
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
            A.comp_id AS comp_id
            ,A.f_year AS f_year
            ,A.f_month AS f_month
            ,A.dept AS dept
            ,A.cost_sheet_id AS cost_sheet_id
            ,A.convert_type_code AS convert_type_code
            ,'' AS item_code
            ,A.fix_cost_group AS fix_cost_group
            ,SUM(A.prod_equiv_qty) AS tot_prod_equiv_qty
            ,NULL AS tot_prod_cost_distr
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
        FROM @_TEMP_TR_PROD_DETAIL2 AS A
        GROUP BY
            A.comp_id
            ,A.f_year
            ,A.f_month
            ,A.dept
            ,A.cost_sheet_id
            ,A.convert_type_code
            ,A.fix_cost_group

    INSERT INTO istem_costing.dbo.tr_prod_detail1_hist
        SELECT * FROM istem_costing.dbo.tr_prod_detail1
        WHERE
        comp_id=@comp_id
        AND f_year=@f_year
        AND f_month=@f_month
        AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL1 GROUP BY dept)

    DELETE FROM istem_costing.dbo.tr_prod_detail1
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL1 GROUP BY dept)

    INSERT INTO istem_costing.dbo.tr_prod_detail1
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,convert_type_code
            ,item_code
            ,fix_cost_group
            ,tot_prod_equiv_qty
            ,tot_prod_cost_distr
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
           comp_id
           ,f_year
           ,f_month
           ,dept
           ,cost_sheet_id
           ,convert_type_code
           ,item_code
           ,fix_cost_group
           ,tot_prod_equiv_qty
           ,tot_prod_cost_distr
           ,proc_time
           ,user_id
           ,@client_ip
           ,@rec_sts
           ,@proc_no
        FROM @_TEMP_TR_PROD_DETAIL1

    -- SELECT
    --     @proc_no=ISNULL(MAX(proc_no), 0) + 1
    --     FROM istem_costing.dbo.tr_prod
    --     WHERE
    --         comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL1 GROUP BY dept);
    -- SET @rec_sts = 'A'
    -- IF @proc_no >= 2
    -- BEGIN
    --     SET @rec_sts = 'T'
    -- END
    --
    -- INSERT INTO @_TEMP_TR_PROD
    --     (
    --         comp_id
    --         ,f_year
    --         ,f_month
    --         ,dept
    --         ,cost_sheet_id
    --         ,item_code
    --         ,prod_qty
    --         ,material_cost
    --         ,wv_leno
    --         ,proc_time
    --         ,user_id
    --         ,client_ip
    --         ,rec_sts
    --         ,proc_no
    --     )
    --     SELECT
    --         @comp_id
    --         ,@f_year
    --         ,@f_month
    --         ,B.v_dept AS dept
    --         ,A.cost_sheet_id AS cost_sheet_id
    --         ,A.mat_code AS item_code
    --         ,A.v_tot_prod_qty AS prod_qty
    --         ,NULL AS material_cost
    --         ,NULL AS wv_leno
    --         ,@proc_time AS proc_time
    --         ,@user_id AS user_id
    --         ,@client_ip AS client_ip
    --         ,@rec_sts AS rec_sts
    --         ,@proc_no AS proc_no
    --     FROM (
    --         SELECT
    --             cost_sheet_id
    --             ,mat_code
    --             ,SUM(in_qty) AS v_tot_prod_qty
    --         FROM istem_costing.dbo.tr_inv_in
    --         WHERE
    --             comp_id=@comp_id
    --             AND f_year=@f_year
    --             AND f_month=@f_month
    --             AND dept=@dept10
    --             AND cost_sheet_id IN ('A20')
    --         GROUP BY
    --             cost_sheet_id
    --             ,mat_code
    --     ) AS A
    --     INNER JOIN (
    --         SELECT
    --             cost_sheet_id,
    --             dept AS v_dept
    --         FROM istem_costing.dbo.ms_cost_group_detail
    --         WHERE comp_id=@comp_id
    --     ) AS B ON (B.cost_sheet_id=A.cost_sheet_id)
    --
    -- UPDATE T SET
    --     T.material_cost=S.material_cost
    -- FROM @_TEMP_TR_PROD AS T
    -- LEFT JOIN (
    --     SELECT
    --          A.item_code
    --         ,SUM(A.cons_mat_amount) AS material_cost
    --     FROM istem_costing.dbo.tr_wip AS A
    --     WHERE
    --             (A.comp_id=@comp_id)
    --         AND (A.f_year=@f_year)
    --         AND (A.f_month=@f_month)
    --         AND (A.dept=@dept20)
    --     GROUP BY A.item_code
    -- ) AS S ON (S.item_code=T.item_code)
    --
    -- -- Delete
    -- DELETE FROM istem_costing.dbo.tr_prod
    --     WHERE
    --         comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (SELECT dept FROM @_TEMP_TR_PROD GROUP BY dept)
    -- -- Insert
    -- INSERT INTO istem_costing.dbo.tr_prod
    --         (
    --             comp_id
    --             ,f_year
    --             ,f_month
    --             ,dept
    --             ,cost_sheet_id
    --             ,item_code
    --             ,prod_qty
    --             ,material_cost
    --             ,wv_leno
    --             ,proc_time
    --             ,user_id
    --             ,client_ip
    --             ,rec_sts
    --             ,proc_no
    --         )
    --         SELECT
    --             comp_id
    --             ,f_year
    --             ,f_month
    --             ,dept
    --             ,cost_sheet_id
    --             ,item_code
    --             ,prod_qty
    --             ,material_cost
    --             ,wv_leno
    --             ,proc_time
    --             ,user_id
    --             ,client_ip
    --             ,rec_sts
    --             ,proc_no
    --         FROM @_TEMP_TR_PROD;


    DELETE FROM @_TEMP_TR_PROD_DETAIL2 WHERE comp_id IS NOT NULL;
    DELETE FROM @_TEMP_TR_PROD_DETAIL1 WHERE comp_id IS NOT NULL;
    -- DELETE FROM @_TEMP_TR_PROD WHERE comp_id IS NOT NULL;

    -- ***********************************************
    --                   Weaving
    -- ***********************************************

    -- ## Start SUB A1
    -- Proses SUB A1

    -- Select : proc_no
    SELECT
        @proc_no=ISNULL(MAX(proc_no), 0) + 1
        FROM istem_costing.dbo.tr_prod_detail2
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (@dept30);
    SET @rec_sts = 'A';
    IF @proc_no >= 2
        BEGIN
            SET @rec_sts = 'T'
        END
    -- Ambil data untuk tr_prod_detail2
    INSERT INTO @_TEMP_TR_PROD_DETAIL2
        (
             comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,item_code
            ,convert_type_code
            ,fix_cost_group
            ,prod_equiv_qty
            ,prod_cost_distr
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
            ,ci_no
        )
        SELECT
            @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,B.v_dept AS dept
            ,D.cost_sheet_id AS cost_sheet_id
            ,A.mat_code AS item_code
            ,C.convert_type_code AS convert_type_code
            ,'' AS fix_cost_group
            ,A.v_tot_prod_qty * C.convert_ratio AS prod_equiv_qty
            ,NULL AS prod_cost_distr
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,'' AS ci_no
        FROM (
            SELECT
                cost_sheet_id
                ,mat_code
                ,SUM(in_qty) AS v_tot_prod_qty
            FROM istem_costing.dbo.tr_inv_in
            WHERE
                comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept IN (@dept10)
                AND cost_sheet_id IN ('A30')
            GROUP BY
                cost_sheet_id
                ,mat_code
        ) AS A
        INNER JOIN (
            SELECT
                cost_sheet_id,
                dept AS v_dept
            FROM istem_costing.dbo.ms_cost_group_detail
            WHERE comp_id=@comp_id
        ) AS B ON (B.cost_sheet_id=A.cost_sheet_id)
        INNER JOIN (
            SELECT
                convert_type_code
                ,dept AS v_dept
                ,item_code
                ,convert_ratio
            FROM istem_costing.dbo.tr_convert_item
            WHERE
                comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND convert_type_code LIKE 'R%'
        ) AS C ON (C.v_dept=B.v_dept AND C.item_code=A.mat_code)
        INNER JOIN (
            SELECT
                cost_sheet_id,
                convert_type_code
            FROM istem_costing.dbo.ms_convert_type
            WHERE
                comp_id=@comp_id
                AND convert_type_code LIKE 'R%'
        ) AS D ON (D.convert_type_code=C.convert_type_code)
        WHERE
            B.v_dept = @dept30
    -- Insert : tr_prod_detail2_hist
    INSERT INTO istem_costing.dbo.tr_prod_detail2_hist
        SELECT * FROM istem_costing.dbo.tr_prod_detail2
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL2 GROUP BY dept);
    -- Delete : tr_prod_detail2
    DELETE FROM istem_costing.dbo.tr_prod_detail2
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL2 GROUP BY dept)
    -- Insert : tr_prod_detail2
    INSERT INTO istem_costing.dbo.tr_prod_detail2
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,item_code
            ,ci_no
            ,item_category
            ,convert_type_code
            ,fix_cost_group
            ,prod_equiv_qty
            ,prod_cost_distr
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,item_code
            ,ci_no
            ,item_category
            ,convert_type_code
            ,fix_cost_group
            ,prod_equiv_qty
            ,prod_cost_distr
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
            ,GETDATE()
            ,@user_id
            ,@client_ip
            ,@rec_sts
            ,@proc_no
        FROM @_TEMP_TR_PROD_DETAIL2

    -- Create tr_prod_detail1
    SELECT
        @proc_no=ISNULL(MAX(proc_no), 0) + 1
        FROM istem_costing.dbo.tr_prod_detail1
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL2 GROUP BY dept);
    SET @rec_sts = 'A';
    IF @proc_no >= 2
    BEGIN
        SET @rec_sts = 'T'
    END

    INSERT INTO @_TEMP_TR_PROD_DETAIL1
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,convert_type_code
            ,item_code
            ,fix_cost_group
            ,tot_prod_equiv_qty
            ,tot_prod_cost_distr
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
            A.comp_id AS comp_id
            ,A.f_year AS f_year
            ,A.f_month AS f_month
            ,A.dept AS dept
            ,A.cost_sheet_id AS cost_sheet_id
            ,A.convert_type_code AS convert_type_code
            ,'' AS item_code
            ,A.fix_cost_group AS fix_cost_group
            ,SUM(A.prod_equiv_qty) AS tot_prod_equiv_qty
            ,NULL AS tot_prod_cost_distr
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
        FROM @_TEMP_TR_PROD_DETAIL2 AS A
        GROUP BY
            A.comp_id
            ,A.f_year
            ,A.f_month
            ,A.dept
            ,A.cost_sheet_id
            ,A.convert_type_code
            ,A.fix_cost_group

    INSERT INTO istem_costing.dbo.tr_prod_detail1_hist
        SELECT * FROM istem_costing.dbo.tr_prod_detail1
        WHERE
        comp_id=@comp_id
        AND f_year=@f_year
        AND f_month=@f_month
        AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL1 GROUP BY dept)

    DELETE FROM istem_costing.dbo.tr_prod_detail1
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL1 GROUP BY dept)

    INSERT INTO istem_costing.dbo.tr_prod_detail1
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,convert_type_code
            ,item_code
            ,fix_cost_group
            ,tot_prod_equiv_qty
            ,tot_prod_cost_distr
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
           comp_id
           ,f_year
           ,f_month
           ,dept
           ,cost_sheet_id
           ,convert_type_code
           ,item_code
           ,fix_cost_group
           ,tot_prod_equiv_qty
           ,tot_prod_cost_distr
           ,proc_time
           ,user_id
           ,@client_ip
           ,@rec_sts
           ,@proc_no
        FROM @_TEMP_TR_PROD_DETAIL1

    -- SELECT
    --     @proc_no=ISNULL(MAX(proc_no), 0) + 1
    --     FROM istem_costing.dbo.tr_prod
    --     WHERE
    --         comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL1 GROUP BY dept);
    -- SET @rec_sts = 'A'
    -- IF @proc_no >= 2
    -- BEGIN
    --     SET @rec_sts = 'T'
    -- END
    -- INSERT INTO @_TEMP_TR_PROD
    --     (
    --         comp_id
    --         ,f_year
    --         ,f_month
    --         ,dept
    --         ,cost_sheet_id
    --         ,item_code
    --         ,prod_qty
    --         ,material_cost
    --         ,wv_leno
    --         ,proc_time
    --         ,user_id
    --         ,client_ip
    --         ,rec_sts
    --         ,proc_no
    --     )
    --     SELECT
    --         @comp_id
    --         ,@f_year
    --         ,@f_month
    --         ,B.v_dept AS dept
    --         ,A.cost_sheet_id AS cost_sheet_id
    --         ,A.mat_code AS item_code
    --         ,A.v_tot_prod_qty AS prod_qty
    --         ,NULL AS material_cost
    --         ,NULL AS wv_leno
    --         ,@proc_time AS proc_time
    --         ,@user_id AS user_id
    --         ,@client_ip AS client_ip
    --         ,@rec_sts AS rec_sts
    --         ,@proc_no AS proc_no
    --     FROM (
    --         SELECT
    --             cost_sheet_id
    --             ,mat_code
    --             ,SUM(in_qty) AS v_tot_prod_qty
    --         FROM istem_costing.dbo.tr_inv_in
    --         WHERE
    --             comp_id=@comp_id
    --             AND f_year=@f_year
    --             AND f_month=@f_month
    --             AND dept=@dept10
    --             AND cost_sheet_id IN ('A20')
    --         GROUP BY
    --             cost_sheet_id
    --             ,mat_code
    --     ) AS A
    --     INNER JOIN (
    --         SELECT
    --             cost_sheet_id,
    --             dept AS v_dept
    --         FROM istem_costing.dbo.ms_cost_group_detail
    --         WHERE comp_id=@comp_id
    --     ) AS B ON (B.cost_sheet_id=A.cost_sheet_id)
    --
    -- UPDATE T SET
    --     T.material_cost=S.material_cost
    -- FROM @_TEMP_TR_PROD AS T
    -- LEFT JOIN (
    --     SELECT
    --          A.item_code
    --         ,SUM(A.cons_mat_amount) AS material_cost
    --     FROM istem_costing.dbo.tr_wip AS A
    --     WHERE
    --             (A.comp_id=@comp_id)
    --         AND (A.f_year=@f_year)
    --         AND (A.f_month=@f_month)
    --         AND (A.dept=@dept30)
    --     GROUP BY A.item_code
    -- ) AS S ON (S.item_code=T.item_code)
    --
    --
    --
    -- -- Delete
    -- DELETE FROM istem_costing.dbo.tr_prod
    --     WHERE
    --         comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (SELECT dept FROM @_TEMP_TR_PROD GROUP BY dept)
    -- -- Insert
    -- INSERT INTO istem_costing.dbo.tr_prod
    --     (
    --         comp_id
    --         ,f_year
    --         ,f_month
    --         ,dept
    --         ,cost_sheet_id
    --         ,item_code
    --         ,prod_qty
    --         ,material_cost
    --         ,wv_leno
    --         ,proc_time
    --         ,user_id
    --         ,client_ip
    --         ,rec_sts
    --         ,proc_no
    --     )
    --     SELECT
    --         comp_id
    --         ,f_year
    --         ,f_month
    --         ,dept
    --         ,cost_sheet_id
    --         ,item_code
    --         ,prod_qty
    --         ,material_cost
    --         ,wv_leno
    --         ,proc_time
    --         ,user_id
    --         ,client_ip
    --         ,rec_sts
    --         ,proc_no
    --     FROM @_TEMP_TR_PROD;

    -- SELECT
    --     @proc_no=ISNULL(MAX(proc_no), 0) + 1
    --     FROM istem_costing.dbo.tr_prod
    --     WHERE
    --         comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (SELECT dept FROM @_TEMP_TR_PROD_DETAIL1 GROUP BY dept);
    -- SET @rec_sts = 'A'
    -- IF @proc_no >= 2
    -- BEGIN
    --     SET @rec_sts = 'T'
    -- END
    --
    -- INSERT INTO @_TEMP_TR_PROD
    --     (
    --         comp_id
    --         ,f_year
    --         ,f_month
    --         ,dept
    --         ,cost_sheet_id
    --         ,item_code
    --         ,prod_qty
    --         ,material_cost
    --         ,wv_leno
    --         ,proc_time
    --         ,user_id
    --         ,client_ip
    --         ,rec_sts
    --         ,proc_no
    --     )
    --     SELECT
    --         @comp_id
    --         ,@f_year
    --         ,@f_month
    --         ,B.v_dept AS dept
    --         ,A.cost_sheet_id AS cost_sheet_id
    --         ,A.mat_code AS item_code
    --         ,A.v_tot_prod_qty AS prod_qty
    --         ,NULL AS material_cost
    --         ,NULL AS wv_leno
    --         ,@proc_time AS proc_time
    --         ,@user_id AS user_id
    --         ,@client_ip AS client_ip
    --         ,@rec_sts AS rec_sts
    --         ,@proc_no AS proc_no
    --     FROM (
    --         SELECT
    --             cost_sheet_id
    --             ,mat_code
    --             ,SUM(in_qty) AS v_tot_prod_qty
    --         FROM istem_costing.dbo.tr_inv_in
    --         WHERE
    --             comp_id=@comp_id
    --             AND f_year=@f_year
    --             AND f_month=@f_month
    --             AND dept=@dept10
    --             AND cost_sheet_id IN ('A30')
    --         GROUP BY
    --             cost_sheet_id
    --             ,mat_code
    --     ) AS A
    --     INNER JOIN (
    --         SELECT
    --             cost_sheet_id,
    --             dept AS v_dept
    --         FROM istem_costing.dbo.ms_cost_group_detail
    --         WHERE comp_id=@comp_id
    --     ) AS B ON (B.cost_sheet_id=A.cost_sheet_id)
    --
    -- UPDATE T SET
    --     T.material_cost=S.material_cost
    -- FROM @_TEMP_TR_PROD AS T
    -- LEFT JOIN (
    --     SELECT
    --          A.item_code
    --         ,SUM(A.cons_mat_amount) AS material_cost
    --     FROM istem_costing.dbo.tr_wip AS A
    --     WHERE
    --             (A.comp_id=@comp_id)
    --         AND (A.f_year=@f_year)
    --         AND (A.f_month=@f_month)
    --         AND (A.dept=@dept20)
    --     GROUP BY A.item_code
    -- ) AS S ON (S.item_code=T.item_code)
    --
    -- -- Delete
    -- DELETE FROM istem_costing.dbo.tr_prod
    --     WHERE
    --         comp_id=@comp_id
    --         AND f_year=@f_year
    --         AND f_month=@f_month
    --         AND dept IN (SELECT dept FROM @_TEMP_TR_PROD GROUP BY dept)
    -- -- Insert
    -- INSERT INTO istem_costing.dbo.tr_prod
    --         (
    --             comp_id
    --             ,f_year
    --             ,f_month
    --             ,dept
    --             ,cost_sheet_id
    --             ,item_code
    --             ,prod_qty
    --             ,material_cost
    --             ,wv_leno
    --             ,proc_time
    --             ,user_id
    --             ,client_ip
    --             ,rec_sts
    --             ,proc_no
    --         )
    --         SELECT
    --             comp_id
    --             ,f_year
    --             ,f_month
    --             ,dept
    --             ,cost_sheet_id
    --             ,item_code
    --             ,prod_qty
    --             ,material_cost
    --             ,wv_leno
    --             ,proc_time
    --             ,user_id
    --             ,client_ip
    --             ,rec_sts
    --             ,proc_no
    --         FROM @_TEMP_TR_PROD;















    -- ## End SUB A1
    -- =============================================================================
    -- #############################################################################
    -- =============================================================================
    -- =============================================================================
    -- =============================================================================


    -- ## Start SUB A2
    -- SUB A2 - Kalkulasi WIP Qty and Equivalent Qty by Product Item
    -- A	ISM SP - Spinning
    DECLARE @v_tot_wip_equiv_qty AS DECIMAL(16,5) -- Convert Type Ratio
    DECLARE @cr_y01 AS DECIMAL(16,5) -- Convert Type Ratio
    DECLARE @cr_y02 AS DECIMAL(16,5) -- Convert Type Ratio
    SET @cr_y01 = (SELECT convert_ratio FROM istem_costing.dbo.tr_convert_item WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept20 AND convert_type_code='Y01')
    SET @cr_y02 = (SELECT convert_ratio FROM istem_costing.dbo.tr_convert_item WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept20 AND convert_type_code='Y02')
    SET @v_tot_wip_equiv_qty = (SELECT NULLIF(SUM(input_qty), 0) AS v_tot_wip_equiv_qty FROM istem_costing.dbo.tr_dept_detail1 WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept20 GROUP BY dept)
    SET @v_tot_wip_equiv_qty = @v_tot_wip_equiv_qty / @cr_y01 * @cr_y02
    SET @proc_no = (SELECT ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_wip_eqv_header WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept20 AND proc_type='WIP-P')
    SET @rec_sts='A'
    IF @proc_no >= 2
    BEGIN
        SET @rec_sts='T'
    END

    DECLARE @_TR_WIP_EQV_HEADER TABLE (
    	comp_id NUMERIC(1, 0) NOT NULL
    	,f_year NUMERIC(4, 0) NOT NULL
    	,f_month NUMERIC(2, 0) NOT NULL
    	,dept NUMERIC(3, 0) NOT NULL
    	,proc_type VARCHAR(20) NOT NULL
    	,cost_sheet_id VARCHAR(4) NOT NULL
    	,convert_type_code VARCHAR(3) NOT NULL
    	,tot_wip_equiv_qty NUMERIC(12, 2) NULL
    	,proc_time datetime NULL
    	,user_id VARCHAR(6) NULL
    	,client_ip VARCHAR(15) NULL
    	,rec_sts CHAR(1) NULL
    	,proc_no TINYINT NULL
    );

    INSERT INTO @_TR_WIP_EQV_HEADER
        (
             comp_id
            ,f_year
            ,f_month
            ,dept
            ,proc_type
            ,cost_sheet_id
            ,convert_type_code
            ,tot_wip_equiv_qty
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        VALUES
        (
            @comp_id
            ,@f_year
            ,@f_month
            ,@dept20
            ,'WIP-P'
            ,''
            ,''
            ,@v_tot_wip_equiv_qty
            ,@proc_time
            ,@user_id
            ,@client_ip
            ,@rec_sts
            ,@proc_no
        )

    DELETE FROM istem_costing.dbo.tr_wip_eqv_header WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept20 AND proc_type='WIP-P'
    INSERT INTO istem_costing.dbo.tr_wip_eqv_header
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,proc_type
            ,cost_sheet_id
            ,convert_type_code
            ,tot_wip_equiv_qty
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,proc_type
            ,cost_sheet_id
            ,convert_type_code
            ,tot_wip_equiv_qty
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        FROM @_TR_WIP_EQV_HEADER


    -- Clear Temp
    DELETE FROM @_TR_WIP_EQV_HEADER WHERE comp_id IS NOT NULL;
    -- =============================================================================

    -- B	ISM WV - Weaving
    SELECT @proc_no=ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_wip_eqv_detail
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept30
    SET @rec_sts='A'
    IF @proc_no >= 2
    BEGIN
        SET @rec_sts='T'
    END

    DECLARE @_TR_WIP_EQV_DETAIL TABLE(
    	comp_id NUMERIC(1, 0) NOT NULL,
    	f_year NUMERIC(4, 0) NOT NULL,
    	f_month NUMERIC(2, 0) NOT NULL,
    	dept NUMERIC(3, 0) NOT NULL,
    	proc_type VARCHAR(20) NOT NULL,
    	cost_sheet_id VARCHAR(4) NOT NULL,
    	item_code VARCHAR(45) NOT NULL,
    	convert_type_code VARCHAR(3) NOT NULL,
    	wip_equiv_qty NUMERIC(12, 2) NULL,
    	wip_cost_distr NUMERIC(12, 2) NULL,
    	proc_time datetime NULL,
    	user_id VARCHAR(6) NULL,
    	client_ip VARCHAR(15) NULL,
    	rec_sts CHAR(1) NULL,
    	proc_no TINYINT NULL
    )

    INSERT INTO @_TR_WIP_EQV_DETAIL
        (
        	comp_id
        	,f_year
        	,f_month
        	,dept
        	,proc_type
        	,cost_sheet_id
        	,item_code
        	,convert_type_code
        	,wip_equiv_qty
        	,wip_cost_distr
        	,proc_time
        	,user_id
        	,client_ip
        	,rec_sts
        	,proc_no
        )
        SELECT
             @comp_id
            ,@f_year
            ,@f_month
            ,@dept30
            ,proc_type
            ,cost_sheet_id
            ,item_code
            ,convert_type_code
            ,wip_equiv_qty
            ,wip_cost_distr
            ,GETDATE()
            ,@user_id
            ,@client_ip
            ,@rec_sts
            ,@proc_no
        FROM (
            SELECT
                A.proc_type AS proc_type
                ,D.cost_sheet_id AS cost_sheet_id
                ,A.item_code AS item_code
                ,C.convert_type_code AS convert_type_code
                ,CASE
                   WHEN A.proc_type LIKE '%WIP-SP' THEN (A.v_wip_qty/(B.yarn_wg/B.yarn_length_gr)) * 0.5 * C.convert_ratio
                   WHEN A.proc_type LIKE '%WIP-P' THEN (A.v_wip_qty/(B.yarn_wg/B.yarn_length_gr)) * C.convert_ratio
                   ELSE 0
                END wip_equiv_qty
                ,0 AS wip_cost_distr
            FROM (
                SELECT
                    item_code
                    ,proc_type
                    ,SUM(mat_qty) AS v_wip_qty
                FROM istem_costing.dbo.tr_dept_calc
                WHERE
                    comp_id=@comp_id
                    AND f_year=@f_year
                    AND f_month=@f_month
                    AND dept=@dept30
                    AND tr_code LIKE '%WIP%'
                GROUP BY
                    item_code
                    ,proc_type
            ) AS A
            INNER JOIN (
                SELECT
                    grey_no
                    ,yarn_length_gr
                    ,SUM(yarn_wg) AS yarn_wg
                FROM istem_costing.dbo.v_WVGreyYarn AS A
                GROUP BY
                    grey_no
                    ,yarn_length_gr
            ) AS B ON (B.grey_no=A.item_code)
            INNER JOIN (
                SELECT
                    convert_type_code
                    ,dept AS v_dept
                    ,item_code
                    ,convert_ratio
                FROM istem_costing.dbo.tr_convert_item
                WHERE
                    comp_id=@comp_id
                    AND f_year=@f_year
                    AND f_month=@f_month
                    AND dept=@dept30
            ) AS C ON (C.item_code=A.item_code)
            INNER JOIN (
                SELECT
                    cost_sheet_id,
                    convert_type_code
                FROM istem_costing.dbo.ms_convert_type
                WHERE
                    comp_id=@comp_id
            ) AS D ON (D.convert_type_code=C.convert_type_code)
        ) AS A
        GROUP BY
            proc_type
            ,cost_sheet_id
            ,item_code
            ,convert_type_code
            ,wip_equiv_qty
            ,wip_cost_distr

    DELETE FROM istem_costing.dbo.tr_wip_eqv_detail
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept30
        -- AND item_code NOT IN (SELECT item_code FROM @_TR_WIP_EQV_DETAIL GROUP BY item_code)

    INSERT INTO istem_costing.dbo.tr_wip_eqv_detail
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,proc_type
            ,cost_sheet_id
            ,item_code
            ,convert_type_code
            ,wip_equiv_qty
            ,wip_cost_distr
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,proc_type
            ,cost_sheet_id
            ,item_code
            ,convert_type_code
            ,wip_equiv_qty
            ,wip_cost_distr
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        FROM @_TR_WIP_EQV_DETAIL;

    SELECT @proc_no=ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_wip_eqv_header
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept30
    SET @rec_sts='A'
    IF @proc_no >= 2
    BEGIN
        SET @rec_sts='T'
    END

    INSERT INTO @_TR_WIP_EQV_HEADER
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,proc_type
            ,cost_sheet_id
            ,convert_type_code
            ,tot_wip_equiv_qty
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
             @comp_id
            ,@f_year
            ,@f_month
            ,@dept30
            ,'' AS proc_type
            ,cost_sheet_id
            ,convert_type_code
            ,SUM(wip_equiv_qty) AS tot_wip_equiv_qty
            ,GETDATE()
            ,@user_id
            ,@client_ip
            ,@rec_sts
            ,@proc_no
        FROM @_TR_WIP_EQV_DETAIL
        GROUP BY
            cost_sheet_id
            ,convert_type_code

    DELETE FROM istem_costing.dbo.tr_wip_eqv_header
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept30

    INSERT INTO istem_costing.dbo.tr_wip_eqv_header
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,proc_type
            ,cost_sheet_id
            ,convert_type_code
            ,tot_wip_equiv_qty
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,proc_type
            ,cost_sheet_id
            ,convert_type_code
            ,tot_wip_equiv_qty
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        FROM @_TR_WIP_EQV_HEADER
    -- ## End SUB A2

END
