

-- =============================================
-- Author:		Apri
-- Create date: 2020-10-01
-- Description:	Proses Data untuk Spinning
--				[WH]
-- =============================================
ALTER PROCEDURE  [sp_SpinningProcessWH]
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;

	-- DECLARE
	-- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
	--
	-- SET @comp_id=1
	-- SET @f_year=2020
	-- SET @f_month=6
	-- SET @user_id='SYS'
	-- SET @client_ip='192.168.1.1'

	DECLARE @dept_10 INT
	DECLARE @dept_20 INT
	DECLARE @mc_loc VARCHAR(MAX)
	DECLARE @tr_code VARCHAR(MAX)
	DECLARE @proc_code VARCHAR(MAX)
	DECLARE @proc_time AS DATETIME
	DECLARE @rec_sts VARCHAR(MAX)
	DECLARE @proc_no INT
	DECLARE @item_code VARCHAR(MAX)
	DECLARE @proc_type VARCHAR(MAX)
	DECLARE @mat_qty_unit VARCHAR(MAX)

	SET @dept_10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id) AND (dept_seq=10))
	SET @dept_20=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id) AND (dept_seq=20))
	SET @mc_loc=''
	SET @tr_code='WH'
	SET @proc_code='WH'
	SET @item_code=''
	SET @proc_type=(SELECT proc_type FROM istem_costing.dbo.ms_process WHERE comp_id=@comp_id AND dept=@dept_20 AND tr_code=@tr_code AND proc_code=@proc_code)
	SET @mat_qty_unit=(SELECT mat_qty_unit FROM istem_costing.dbo.ms_mc_proc WHERE comp_id=@comp_id AND dept=@dept_20 AND proc_code=@proc_code)

	SET @proc_time=GETDATE()
	SET @rec_sts='A'
	SET @proc_no=1


	-- Cek tr_dept_calc
	DECLARE @c INT
	SET @c = (SELECT COUNT(*) AS c FROM istem_costing.dbo.tr_dept_calc
				WHERE ((comp_id=@comp_id) AND (f_year=@f_year) AND (f_month=@f_month) AND (dept=@dept_20) AND (tr_code=@tr_code) AND (mc_loc=@mc_loc) AND (proc_code=@proc_code) AND (item_code=@item_code)))

	IF @c >= 1
	BEGIN
		-- Set Proc No
		SELECT @proc_no =(MAX(proc_no)+1) FROM istem_costing.dbo.tr_dept_calc
			WHERE ((comp_id=@comp_id) AND (f_year=@f_year) AND (f_month=@f_month) AND (dept=@dept_20) AND (tr_code=@tr_code) AND (mc_loc=@mc_loc) AND (proc_code=@proc_code) AND (item_code=@item_code))

		-- Simpan History
		INSERT INTO istem_costing.dbo.tr_dept_calc_hist (comp_id, f_year, f_month, dept, tr_code, mc_loc, proc_code, item_code, mat_code, mat_qty, mat_qty_unit, proc_type, proc_time, user_id, client_ip, rec_sts, proc_no)
			SELECT comp_id, f_year, f_month, dept, tr_code, mc_loc, proc_code, item_code, mat_code, mat_qty, mat_qty_unit, proc_type, proc_time, user_id, client_ip, rec_sts, proc_no
			FROM istem_costing.dbo.tr_dept_calc
			WHERE ((comp_id=@comp_id) AND (f_year=@f_year) AND (f_month=@f_month) AND (dept=@dept_20) AND (tr_code=@tr_code) AND (mc_loc=@mc_loc) AND (proc_code=@proc_code) AND (item_code=@item_code))

		-- Delete exisiting
		DELETE FROM istem_costing.dbo.tr_dept_calc
			WHERE ((comp_id=@comp_id) AND (f_year=@f_year) AND (f_month=@f_month) AND (dept=@dept_20) AND (tr_code=@tr_code) AND (mc_loc=@mc_loc) AND (proc_code=@proc_code) AND (item_code=@item_code))

		-- Set Status
		SET @rec_sts='T'
	END
	DECLARE @_TEMP_TR_DEPT_CALC TABLE (
		[comp_id] [numeric](1, 0) NOT NULL,
		[f_year] [numeric](4, 0) NOT NULL,
		[f_month] [numeric](2, 0) NOT NULL,
		[dept] [numeric](3, 0) NOT NULL,
		[tr_code] [varchar](5) NOT NULL,
		[mc_loc] [varchar](15) NOT NULL,
		[proc_code] [varchar](7) NOT NULL,
		[item_code] [varchar](45) NOT NULL,
		[mat_code] [varchar](45) NOT NULL,
		[mat_qty] [numeric](12, 2) NULL,
		[mat_amount] [numeric](16, 2) NULL,
		[mat_qty_unit] [varchar](5) NULL,
		[proc_type] [varchar](20) NULL,
		[proc_time] [datetime] NULL,
		[user_id] [varchar](6) NULL,
		[client_ip] [varchar](15) NULL,
		[rec_sts] [char](1) NULL,
		[proc_no] [tinyint] NULL
	)

	INSERT INTO @_TEMP_TR_DEPT_CALC
		(
			comp_id
			,f_year
			,f_month
			,dept
			,tr_code
			,mc_loc
			,proc_code
			,item_code
			,mat_code
			,mat_qty
			,mat_qty_unit
			,proc_type
			,proc_time
			,user_id
			,client_ip
			,rec_sts
			,proc_no
		)
		SELECT
			 @comp_id AS comp_id
			,@f_year AS f_year
			,@f_month AS f_month
			,@dept_20 AS dept
			,@tr_code AS tr_code
			,'' AS mc_loc
			,@proc_code AS proc_code
			,A.mat_code AS item_code
			,ISNULL(C.mat_code, '') AS mat_code
			,(A.sum_of_in_qty * C.mat_comp_actual)/100 AS mat_qty
			,@mat_qty_unit AS mat_qty_unit
			,@proc_type AS proc_type
			,@proc_time AS proc_time
			,@user_id AS user_id
			,@client_ip AS client_ip
			,@rec_sts AS rec_sts
			,@proc_no AS proc_no
		FROM (
			SELECT
				A.mat_code,
				SUM(A.in_qty) AS sum_of_in_qty
			FROM istem_costing.dbo.tr_inv_in AS A
			WHERE
					(A.comp_id=@comp_id)
				AND (A.f_year=@f_year)
				AND (A.f_month=@f_month)
				AND (A.dept=@dept_10)
				AND (A.cost_sheet_id IN ('A20'))
			GROUP BY
			A.mat_code
		) AS A
		LEFT JOIN istem_sms.dbo.sp_ms_yarn AS B ON (
			B.yarn_code = A.mat_code
		)
		LEFT JOIN istem_sms.dbo.sp_ms_yarn_detail AS C ON (
			C.yarn_code = B.yarn_code
		)

	-- Delete exisiting
	DELETE FROM istem_costing.dbo.tr_dept_calc
		WHERE ((comp_id=@comp_id) AND (f_year=@f_year) AND (f_month=@f_month) AND (dept=@dept_20) AND (tr_code=@tr_code) AND (mc_loc=@mc_loc) AND (proc_code=@proc_code))
	-- Import
	INSERT INTO istem_costing.dbo.tr_dept_calc ( comp_id, f_year, f_month, dept, tr_code, mc_loc, proc_code, item_code, mat_code, mat_qty, mat_amount, mat_qty_unit, proc_type, proc_time, user_id, client_ip, rec_sts, proc_no )
		SELECT comp_id, f_year, f_month, dept, tr_code, mc_loc, proc_code, item_code, mat_code, mat_qty, mat_amount, mat_qty_unit, proc_type, proc_time, user_id, client_ip, rec_sts, proc_no
			FROM @_TEMP_TR_DEPT_CALC


END
