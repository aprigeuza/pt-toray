
-- =============================================
-- Author:		Apri
-- Create date: 2020-11-24
-- Description:
--				Yarn Purchase Monthly
--				Report Untuk Accounting
-- =============================================
ALTER PROCEDURE  [sp_RptAcctYarnPurchaseMonthly]
	@COMP_ID INT, @F_YEAR INT, @F_MONTH INT
AS
BEGIN
	SET NOCOUNT ON;



-- DECLARE
--     @comp_id INT, @f_year INT, @f_month INT
--
-- SET @comp_id=1
-- SET @f_year=2020
-- SET @f_month=7


DECLARE @LP_f_year INT, @LP_f_month INT

IF @F_Month = 1
BEGIN
	SET @LP_f_month = 12
	SET @LP_f_year = @F_Year - 1
END
ELSE
BEGIN
	SET @LP_f_month = @F_Month - 1
	SET @LP_f_year = @F_Year
END

DECLARE @dept INT

SET @dept = (SELECT TOP 1 dept FROM ms_dept WHERE comp_id=@COMP_ID AND dept_seq=10)

SELECT
	MT.mat_code  AS material_code
	,ISNULL(M.FrgnName, '') AS material_name
    ,NULLIF(bf.bf_qty, 0) AS bf_qty
    ,NULLIF(bf.bf_price, 0) AS bf_price
	,NULLIF(bf.bf_amount, 0) AS bf_amount
    ,NULLIF(rc.rc_qty, 0) AS rc_qty
    ,NULLIF(rc.rc_price, 0) AS rc_price
	,NULLIF(rc.rc_amount, 0) AS rc_amount
    ,NULLIF(cs.weaving, 0) AS weaving_qty
    ,NULLIF(ISNULL(bf.bf_amount, 0)+ISNULL(rc.rc_amount, 0), 0)/NULLIF(ISNULL(bf.bf_qty, 0)+ISNULL(rc.rc_qty, 0), 0) AS weaving_price
	,NULLIF(cs.weaving, 0) * NULLIF(ISNULL(bf.bf_amount, 0)+ISNULL(rc.rc_amount, 0), 0)/NULLIF(ISNULL(bf.bf_qty, 0)+ISNULL(rc.rc_qty, 0), 0) AS weaving_amount
	,NULLIF(cs.dying, 0) AS dying_qty
    ,NULLIF(ISNULL(bf.bf_amount, 0)+ISNULL(rc.rc_amount, 0), 0)/NULLIF(ISNULL(bf.bf_qty, 0)+ISNULL(rc.rc_qty, 0), 0) AS dying_price
    ,NULLIF(cs.dying, 0) * NULLIF(ISNULL(bf.bf_amount, 0)+ISNULL(rc.rc_amount, 0), 0)/NULLIF(ISNULL(bf.bf_qty, 0)+ISNULL(rc.rc_qty, 0), 0) AS dying_amount
	,NULLIF(cs.other, 0) AS other_qty
    ,NULLIF(ISNULL(bf.bf_amount, 0)+ISNULL(rc.rc_amount, 0), 0)/NULLIF(ISNULL(bf.bf_qty, 0)+ISNULL(rc.rc_qty, 0), 0) AS other_price
    ,NULLIF(cs.other, 0) * NULLIF(ISNULL(bf.bf_amount, 0)+ISNULL(rc.rc_amount, 0), 0)/NULLIF(ISNULL(bf.bf_qty, 0)+ISNULL(rc.rc_qty, 0), 0) AS other_amount
    ,NULLIF((ISNULL(bf.bf_qty, 0) + ISNULL(rc.rc_qty, 0)) - ((ISNULL(cs.weaving, 0) + ISNULL(cs.dying, 0) + ISNULL(cs.other, 0))), 0) AS cf_qty
    ,NULLIF(ISNULL(bf.bf_amount, 0)+ISNULL(rc.rc_amount, 0), 0)/NULLIF(ISNULL(bf.bf_qty, 0)+ISNULL(rc.rc_qty, 0), 0) AS cf_price
	,NULLIF((ISNULL(bf.bf_qty, 0) + ISNULL(rc.rc_qty, 0)) - ((ISNULL(cs.weaving, 0) + ISNULL(cs.dying, 0) + ISNULL(cs.other, 0))), 0)
     * NULLIF(ISNULL(bf.bf_amount, 0)+ISNULL(rc.rc_amount, 0), 0)/NULLIF(ISNULL(bf.bf_qty, 0)+ISNULL(rc.rc_qty, 0), 0) AS cf_amount
FROM
(
	SELECT * FROM (
	SELECT mat_code FROM tr_bal_mat WHERE (comp_id = @COMP_ID) AND (f_year = @LP_f_year) AND (f_month = @LP_f_month) AND (cost_sheet_id IN ('A21')) AND (dept=@dept) GROUP BY mat_code
	UNION ALL
	SELECT mat_code FROM tr_inv_in WHERE (comp_id = @COMP_ID) AND (f_year = @F_YEAR) AND (f_month = @F_MONTH) AND (cost_sheet_id IN ('A21')) AND (dept=@dept) GROUP BY mat_code
	UNION ALL
	SELECT mat_code FROM tr_inv_out_detail WHERE (comp_id = @COMP_ID) AND (f_year = @F_YEAR) AND (f_month = @F_MONTH) AND (cost_sheet_id IN ('A21')) AND (dept=@dept) GROUP BY mat_code
	) AS A GROUP BY mat_code
) AS MT
INNER JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM AS M ON (M.ItemCode COLLATE SQL_Latin1_General_CP1_CI_AS = MT.mat_code COLLATE SQL_Latin1_General_CP1_CI_AS)
LEFT JOIN
(
	SELECT
		mat_code
        ,SUM(ISNULL(cf_qty, 0)) AS bf_qty -- C
        ,NULLIF(SUM(ISNULL(cf_amount, 0)), 0)/NULLIF(SUM(ISNULL(cf_qty, 0)), 0) AS bf_price -- C
		,SUM(ISNULL(cf_amount, 0)) AS bf_amount -- C
	FROM tr_bal_mat
	WHERE
		(comp_id = @COMP_ID)
		AND
		(f_year = @LP_f_year)
		AND
		(f_month = @LP_f_month)
		AND
		(dept=@dept)
	GROUP BY mat_code
) AS bf ON
(bf.mat_code= MT.mat_code)
LEFT JOIN
(
	SELECT
		mat_code
        ,SUM(ISNULL(in_qty, 0)) AS rc_qty -- E
        ,NULLIF(SUM(ISNULL(in_amount, 0)), 0)/NULLIF(SUM(ISNULL(in_qty, 0)), 0) AS rc_price -- E
		,SUM(ISNULL(in_amount, 0)) AS rc_amount -- E
	FROM tr_inv_in
	WHERE
		(comp_id = @COMP_ID)
		AND
		(f_year = @F_YEAR)
		AND
		(f_month = @F_MONTH)
		AND
		(dept=@dept)
	GROUP BY mat_code
) AS rc ON
(rc.mat_code=MT.mat_code)
LEFT JOIN
(
  SELECT
	A.mat_code
	,SUM(B.out_qty) AS weaving
	,SUM(C.out_qty) AS dying
	,SUM(D.out_qty) AS other
  FROM tr_inv_out_header AS A
  LEFT JOIN tr_inv_out_detail AS B ON
	(
		(B.mat_code=A.mat_code)
		AND
		(B.out_dest=800)
		AND
		(B.cost_sheet_id=A.cost_sheet_id)
		AND
		(B.comp_id=A.comp_id)
		AND
		(B.f_year=A.f_year)
		AND
		(B.f_month=A.f_month)
		AND
		(B.dept=@dept)
	)
  LEFT JOIN tr_inv_out_detail AS C ON
	(
		(C.mat_code=A.mat_code)
		AND
		(C.out_dest=900)
		AND
		(C.cost_sheet_id=A.cost_sheet_id)
		AND
		(C.comp_id=A.comp_id)
		AND
		(C.f_year=A.f_year)
		AND
		(C.f_month=A.f_month)
		AND
		(C.dept=@dept)
	)
  LEFT JOIN tr_inv_out_detail AS D ON
	(
		(D.mat_code=A.mat_code)
		AND
		(D.out_dest NOT IN(800, 900))
		AND
		(D.cost_sheet_id=A.cost_sheet_id)
		AND
		(D.comp_id=A.comp_id)
		AND
		(D.f_year=A.f_year)
		AND
		(D.f_month=A.f_month)
		AND
		(D.dept=@dept)
	)
  WHERE
	(A.cost_sheet_id IN ('A21'))
	AND
	(A.comp_id = @COMP_ID)
	AND
	(A.f_year = @F_YEAR)
	AND
	(A.f_month = @F_MONTH)
	AND
	(A.dept=@dept)
  GROUP BY A.mat_code
) AS cs ON
(cs.mat_code = MT.mat_code)
ORDER BY M.ItmsGrpCod, MT.mat_code



END
