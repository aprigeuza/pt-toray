
-- =============================================
-- Author:		Apri
-- Create date: 2020-08-28
-- Description:
--				Staple Monthly
--				Report Untuk Logistic
-- =============================================
ALTER PROCEDURE  [sp_RptLogisticStapleMonthly]
	@COMP_ID INT, @F_YEAR INT, @F_MONTH INT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @LP_f_year INT, @LP_f_month INT

	IF @F_Month = 1
	BEGIN
		SET @LP_f_month = 12
		SET @LP_f_year = @F_Year - 1
	END
	ELSE
	BEGIN
		SET @LP_f_month = @F_Month - 1
		SET @LP_f_year = @F_Year
	END

	DECLARE @dept INT

	SET @dept = (SELECT TOP 1 dept FROM ms_dept WHERE comp_id=@COMP_ID AND dept_seq=10)

	SELECT
		MT.cost_sheet_id
		,MT.mat_code  AS MATERIAL_CODE
		,ISNULL(M.FrgnName, '') AS MATERIAL_NAME

		,NULLIF(BF.BF_BALE, 0) AS BF_BALE -- A
		,NULLIF(BF.BF_NET_QTY, 0) AS BF_NET_QTY -- B
		,NULLIF(BF.BF_INV_QTY, 0) AS BF_INV_QTY -- C

		,NULLIF(RC.RECEIVE_BALE, 0) AS RECEIVE_BALE -- D
		,NULLIF(RC.RECEIVE_NET_QTY, 0) AS RECEIVE_NET_QTY -- E
		,NULLIF(RC.RECEIVE_INV_QTY, 0) AS RECEIVE_INV_QTY -- F

		,NULLIF(CS.CONSUME_SPINNING_BALE, 0) AS CONSUME_SPINNING_BALE -- G
		,NULLIF(CS.CONSUME_SPINNING_NET_QTY, 0) AS CONSUME_SPINNING_NET_QTY -- H
		,NULLIF(CS.CONSUME_SPINNING_INV_QTY, 0) AS CONSUME_SPINNING_INV_QTY -- I

		,NULLIF(CR.CONSUME_RETURN_BALE, 0) AS CONSUME_RETURN_BALE -- J
		,NULLIF(CR.CONSUME_RETURN_NET_QTY, 0) AS CONSUME_RETURN_NET_QTY -- K
		,NULLIF(CR.CONSUME_RETURN_INV_QTY, 0) AS CONSUME_RETURN_INV_QTY -- L


		-- (A + D) - ( G + J )
		,NULLIF((ISNULL(BF.BF_BALE, 0)+ISNULL(RC.RECEIVE_BALE, 0) ) - (ISNULL(CS.CONSUME_SPINNING_BALE, 0)+ISNULL(CR.CONSUME_RETURN_BALE, 0)), 0) AS CF_BALE
		-- (B + E) - ( H + K )
		,NULLIF((ISNULL(BF.BF_NET_QTY, 0)+ISNULL(RC.RECEIVE_NET_QTY, 0)) - (ISNULL(CS.CONSUME_SPINNING_NET_QTY, 0)+ISNULL(CR.CONSUME_RETURN_NET_QTY, 0)), 0) AS CF_NET_QTY
		-- (C + F) - ( I + L )
		,NULLIF((ISNULL(BF.BF_INV_QTY, 0)+ISNULL(RC.RECEIVE_INV_QTY, 0)) - (ISNULL(CS.CONSUME_SPINNING_INV_QTY, 0)+ISNULL(CR.CONSUME_RETURN_INV_QTY, 0)), 0) AS CF_INV_QTY

	FROM
	(
		SELECT * FROM (
		SELECT cost_sheet_id, mat_code FROM tr_bal_mat WHERE (comp_id = @COMP_ID) AND (f_year = @LP_f_year) AND (f_month = @LP_f_month) AND (cost_sheet_id IN ('A10', 'A11', 'A13')) AND (dept=@dept) GROUP BY cost_sheet_id, mat_code
		UNION ALL
		SELECT cost_sheet_id, mat_code FROM tr_inv_in WHERE (comp_id = @COMP_ID) AND (f_year = @F_YEAR) AND (f_month = @F_MONTH) AND (cost_sheet_id IN ('A10', 'A11', 'A13')) AND (dept=@dept) GROUP BY cost_sheet_id, mat_code
		UNION ALL
		SELECT cost_sheet_id, mat_code FROM tr_inv_out_detail WHERE (comp_id = @COMP_ID) AND (f_year = @F_YEAR) AND (f_month = @F_MONTH) AND (cost_sheet_id IN ('A10', 'A11', 'A13')) AND (dept=@dept) GROUP BY cost_sheet_id, mat_code
		) AS A GROUP BY cost_sheet_id, mat_code
	) AS MT
	INNER JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM AS M ON (M.ItemCode COLLATE SQL_Latin1_General_CP1_CI_AS = MT.mat_code COLLATE SQL_Latin1_General_CP1_CI_AS)

	-- BF
	LEFT JOIN
	(
		SELECT
			mat_code
			,SUM(ISNULL(cf_bale_qty, 0)) AS BF_BALE -- A
			,SUM(ISNULL(cf_qty, 0)) AS BF_NET_QTY -- B
			,SUM(ISNULL(cf_invoice_qty, 0)) AS BF_INV_QTY -- C

		FROM tr_bal_mat
		WHERE
			(comp_id = @COMP_ID)
			AND
			(f_year = @LP_f_year)
			AND
			(f_month = @LP_f_month)
			AND
			(dept=@dept)
		GROUP BY mat_code
	) AS BF ON
	(BF.mat_code= MT.mat_code)


	-- Receive
	LEFT JOIN
	(
		SELECT
			mat_code
			,SUM(ISNULL(in_bale_qty, 0)) AS RECEIVE_BALE -- D
			,SUM(ISNULL(in_qty, 0)) AS RECEIVE_NET_QTY -- E
			,SUM(ISNULL(in_invoice_qty, 0)) AS RECEIVE_INV_QTY -- F
		FROM tr_inv_in
		WHERE
			(comp_id = @COMP_ID)
			AND
			(f_year = @F_YEAR)
			AND
			(f_month = @F_MONTH)
			AND
			(dept=@dept)
		GROUP BY mat_code
	) AS RC ON
	(RC.mat_code=MT.mat_code)

	-- Consume Spinning
	LEFT JOIN
	(
	  SELECT
		mat_code
		,SUM(ISNULL(out_bale_qty, 0)) AS CONSUME_SPINNING_BALE -- G
		,SUM(ISNULL(out_qty, 0)) AS CONSUME_SPINNING_NET_QTY -- H
		,SUM(ISNULL(out_invoice_qty, 0)) AS CONSUME_SPINNING_INV_QTY -- I
	  FROM tr_inv_out_detail
	  WHERE
		(comp_id = @COMP_ID)
		AND
		(f_year = @F_YEAR)
		AND
		(f_month = @F_MONTH)
		AND
		(dept=@dept)
		AND
		(out_dest=700)
	  GROUP BY mat_code
	) AS CS ON
	(CS.mat_code=MT.mat_code)


	-- Consume Return
	LEFT JOIN
	(
	  SELECT
		mat_code
		,SUM(ISNULL(out_bale_qty, 0)) AS CONSUME_RETURN_BALE -- J
		,SUM(ISNULL(out_qty, 0)) AS CONSUME_RETURN_NET_QTY -- K
		,SUM(ISNULL(out_invoice_qty, 0)) AS CONSUME_RETURN_INV_QTY -- L
	  FROM tr_inv_out_detail
	  WHERE
		(comp_id = @COMP_ID)
		AND
		(f_year = @F_YEAR)
		AND
		(f_month = @F_MONTH)
		AND
		(dept=@dept)
		AND
		(out_dest<>700)
	  GROUP BY mat_code
	) AS CR ON
	(CR.mat_code = MT.mat_code)
  ORDER BY
    MT.cost_sheet_id, MT.mat_code
END
