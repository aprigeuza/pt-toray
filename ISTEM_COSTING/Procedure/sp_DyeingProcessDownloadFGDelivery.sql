ALTER PROCEDURE  sp_DyeingProcessDownloadFGDelivery
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;
    -- FG Delivery
    -- sp_DyeingProcessDownloadFGDelivery
    -- FG Delivery

    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=7
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept40 INT
    DECLARE @proc_time AS DATETIME
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_no INT

    -- PERIOD
    DECLARE @from_date DATE, @to_date DATE
    SELECT TOP 1 @from_date = F_RefDate, @to_date = T_RefDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)

    --Konversi period SAP ke CIM
    DECLARE @target_cim_f_year INT
    DECLARE @target_cim_f_month INT

    IF (@f_month + 3) > 12
    BEGIN
        SET @target_cim_f_month = (@f_month - 9)
        SET @target_cim_f_year = @f_year + 1
    END
    ELSE
    BEGIN
        SET @target_cim_f_month = @f_month + 3
        SET @target_cim_f_year = @f_year
    END

    SET @dept40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40))
    SET @proc_time=GETDATE()
    SET @rec_sts='A'
    SET @proc_no=(SELECT ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_prod_fg_deliv WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40)

    IF @proc_no >= 2
    BEGIN
        SET @rec_sts='T'
    END

    DECLARE @_TR_PROD_FG_DELIV TABLE (
    	comp_id numeric(1,0) NOT NULL,
    	f_year numeric(4,0) NOT NULL,
    	f_month numeric(2,0) NOT NULL,
    	dept numeric(3,0) NOT NULL,
    	grey_no varchar(6) NOT NULL,
    	ci_no varchar(20) NOT NULL,
    	item_category varchar(2) NULL,
        deliv_class char(1) NOT NULL,
    	dom_exp char(1) NOT NULL,
    	undelivery bit NOT NULL,
        qty_unit varchar(5) NOT NULL,
    	dy_proc_type varchar(5) NOT NULL,
    	dyes_type_code varchar(4) NOT NULL,
    	color_shade_code varchar(2) NOT NULL,
    	deliv_qty numeric(12,2) DEFAULT 0 NULL,
        deliv_qty_mtr numeric(12,2) DEFAULT 0 NULL,
    	proc_time datetime NULL,
    	user_id varchar(6) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    	client_ip varchar(15) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    	rec_sts char(1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
    	proc_no tinyint NULL
    )

    INSERT INTO @_TR_PROD_FG_DELIV
    (
        comp_id
        ,f_year
        ,f_month
        ,dept
        ,grey_no
        ,ci_no
        ,item_category
        ,deliv_class
        ,dom_exp
        ,undelivery
        ,qty_unit
        ,dy_proc_type
        ,dyes_type_code
        ,color_shade_code
        ,deliv_qty
        ,deliv_qty_mtr
        ,proc_time
        ,user_id
        ,client_ip
        ,rec_sts
        ,proc_no
    )
    SELECT
         @comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept40 AS dept
        ,A.grey_no AS grey_no
        ,A.ci_no AS ci_no
        ,A.item_category AS item_category
        ,A.deli_class AS deliv_class
        ,A.dom_exp AS dom_exp
        ,A.undelivery AS undelivery
        ,A.u_ms AS qty_unit
        ,A.proc_type AS dy_proc_type
        ,A.dyes_type_code AS dyes_type_code
        ,A.color_shade_code AS color_shade_code
        ,SUM(A.deli_qty) AS deliv_qty
        ,SUM(A.deli_qty_mtr) AS deliv_qty_mtr
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
    FROM (
        -- exp pass
        SELECT
            A.comp_id
            ,A.f_year
            ,A.f_month
            ,A.dept
            ,A.grey_no
            ,A.ci_no
            ,B.item_category
            ,A.deli_class
            ,A.dom_exp
            ,A.undelivery
            ,A.u_ms
            ,ISNULL(C.proc_type,'') AS proc_type
            ,ISNULL(D.dyes_type_code,'') AS dyes_type_code
            ,ISNULL(D.color_shade_code,'') AS color_shade_code,
            SUM(A.deli_qty) AS deli_qty
            ,SUM(A.deli_qty_mtr) AS deli_qty_mtr
        FROM (
            SELECT
                1 AS comp_id
                ,YEAR(A.ship_date) AS f_year
                ,MONTH(A.ship_date) AS f_month
                ,900 AS dept
                ,C.iw_no
                ,C.ist_col
                ,C.grey_no
                ,C.ci_no
                ,'1' deli_class
                ,'E' dom_exp
                ,'0' undelivery
                ,C.u_ms
                ,SUM(C.f_length) AS deli_qty
                ,deli_qty_mtr = CASE WHEN C.u_ms='YDS' THEN SUM(C.f_length/1.0936) ELSE SUM(C.f_length) END
            FROM istem_sms.dbo.do_header a
            LEFT JOIN istem_sms.dbo.do_detail b ON (A.doc_no=B.doc_no)
            LEFT JOIN istem_sms.dbo.fg_warehouse_detail c ON (B.iw_no=C.iw_no AND B.list_no=C.list_no)
            WHERE
                YEAR(A.ship_date)= @target_cim_f_year
                AND MONTH(A.ship_date)=@target_cim_f_month
            GROUP BY
                YEAR(A.ship_date)
                ,MONTH(A.ship_date)
                ,C.iw_no
                ,C.ist_col
                ,C.grey_no
                ,C.ci_no
                ,C.u_ms
        ) AS A
        LEFT JOIN istem_sms.dbo.wv_fabric_analysis_master AS B ON A.grey_no=B.grey_no
        LEFT JOIN istem_sms.dbo.color_instruction_header AS C ON A.iw_no=C.iw_no
        LEFT JOIN istem_sms.dbo.color_instruction_detail_process AS D ON A.iw_no=D.iw_no AND A.ist_col=D.ist_color
        GROUP BY
            A.comp_id
            ,A.f_year
            ,A.f_month
            ,A.dept
            ,A.grey_no
            ,A.ci_no
            ,B.item_category
            ,A.deli_class
            ,A.dom_exp
            ,A.undelivery
            ,A.u_ms
            ,C.proc_type
            ,D.dyes_type_code
            ,D.color_shade_code

        UNION ALL

        -- dom pass (1),fact used (4)
        SELECT
            A.comp_id
            ,A.f_year
            ,A.f_month
            ,A.dept
            ,A.grey_no
            ,A.ci_no
            ,C.item_category
            ,A.deli_class,
            A.dom_exp
            ,A.undelivery
            ,A.u_ms
            ,ISNULL(D.proc_type,'') proc_type
            ,ISNULL(B.dyes_type_code,'') dyes_type_code
            ,ISNULL(B.color_shade_code,'') color_shade_code
            ,SUM(A.deli_qty) AS deli_qty
            ,SUM(A.deli_qty_mtr) AS deli_qty_mtr
        FROM (
            SELECT
                1 AS comp_id
                ,YEAR(A.deli_date) AS f_year
                ,MONTH(A.deli_date) AS f_month
                ,900 AS dept
                ,C.iw_no
                ,C.ist_col
                ,C.grey_no
                ,C.ci_no
                ,A.deli_class
                ,A.d_e AS dom_exp
                ,0 AS undelivery
                ,A.u_ms
                ,ISNULL(D.proc_type,'') proc_type
                ,SUM(C.f_length) deli_qty
                ,deli_qty_mtr = CASE WHEN A.u_ms='YDS' THEN SUM(C.f_length/1.0936) ELSE SUM(C.f_length) END
            FROM istem_sms.dbo.fg_delivery AS A WITH (NOLOCK)
            LEFT JOIN istem_sms.dbo.fg_delivery_detail AS B WITH (NOLOCK) ON A.deli_slip_no = B.deli_slip_no
            LEFT JOIN istem_sms.dbo.fg_warehouse_detail AS C WITH (NOLOCK) ON B.iw_no = C.iw_no AND B.pack_no = C.pack_no
            LEFT JOIN istem_sms.dbo.color_instruction_header AS D with (NOLOCK) ON B.iw_no = D.iw_no
            WHERE
                YEAR(A.deli_date) = @target_cim_f_year
                AND MONTH(A.deli_date) = @target_cim_f_month
                AND A.pass = 'P'
                AND A.d_e = 'D'
            GROUP BY
                YEAR(A.deli_date)
                ,MONTH(A.deli_date)
                ,C.iw_no
                ,C.ist_col
                ,C.grey_no
                ,C.ci_no
                ,A.deli_class
                ,A.d_e
                ,A.pass
                ,A.u_ms
                ,D.proc_type
        ) AS A
        LEFT JOIN istem_sms.dbo.color_instruction_detail_process AS B ON A.iw_no=B.iw_no AND A.ist_col=B.ist_color
        LEFT JOIN istem_sms.dbo.wv_fabric_analysis_master AS C ON A.grey_no=C.grey_no
        LEFT JOIN istem_sms.dbo.color_instruction_header AS D ON A.iw_no=D.iw_no
        GROUP BY
            A.comp_id
            ,A.f_year
            ,A.f_month
            ,A.dept
            ,A.grey_no
            ,A.ci_no
            ,C.item_category
            ,A.deli_class
            ,A.dom_exp
            ,A.undelivery
            ,A.u_ms
            ,D.proc_type
            ,B.dyes_type_code
            ,B.color_shade_code

        UNION ALL

        -- non pass (2)
        SELECT
            1 AS comp_id
            ,YEAR(A.deli_date) AS f_year
            ,MONTH(A.deli_date) AS f_month
            ,900 AS dept
            ,C.grey_no
            ,C.ci_no
            ,E.item_category
            ,A.deli_class
            , A.d_e dom_exp
            ,'1' undelivery
            ,A.u_ms
            ,ISNULL(D.proc_type,'') proc_type
            ,ISNULL(F.dyes_type_code,'') dyes_type_code
            ,ISNULL(F.color_shade_code,'') color_shade_code
            ,SUM(C.f_length) deli_qty
            ,deli_qty_mtr = CASE WHEN A.u_ms='YDS' THEN SUM(C.f_length/1.0936) ELSE SUM(C.f_length) END
        FROM istem_sms.dbo.fg_delivery AS A WITH (NOLOCK)
        INNER JOIN istem_sms.dbo.fg_delivery_detail AS B WITH (NOLOCK) ON (A.deli_slip_no = B.deli_slip_no)
        INNER JOIN istem_sms.dbo.fg_warehouse_detail AS C WITH (NOLOCK) ON (B.ci_no = C.ci_no AND B.chop_cart = C.chop_cart AND B.year = C.year)
        INNER JOIN istem_sms.dbo.color_instruction_header AS D with (NOLOCK) ON (C.iw_no=D.iw_no)
        LEFT JOIN istem_sms.dbo.wv_fabric_analysis_master AS E with (NOLOCK) ON (C.grey_no=E.grey_no)
        LEFT JOIN istem_sms.dbo.color_instruction_detail_process AS F with (NOLOCK) ON (C.iw_no=F.iw_no AND C.ist_col=F.ist_color)
        WHERE
            YEAR(A.deli_date) = @target_cim_f_year
            AND MONTH(A.deli_date) = @target_cim_f_month
            AND A.pass = 'N'
        GROUP BY
            YEAR(A.deli_date)
            ,MONTH(A.deli_date)
            ,C.grey_no
            ,C.ci_no
            ,E.item_category
            ,A.deli_class
            ,A.d_e
            ,A.u_ms
            ,D.proc_type
            ,F.dyes_type_code
            ,F.color_shade_code

    ) AS A
    WHERE
        A.grey_no IS NOT NULL
        AND A.ci_no IS NOT NULL
    GROUP BY
        A.comp_id
        ,A.f_year
        ,A.f_month
        ,A.dept
        ,A.grey_no
        ,A.ci_no
        ,A.item_category
        ,A.deli_class
        ,A.dom_exp
        ,A.undelivery
        ,A.u_ms
        ,A.proc_type
        ,A.dyes_type_code
        ,A.color_shade_code

    INSERT INTO istem_costing.dbo.tr_prod_fg_deliv
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,grey_no
            ,ci_no
            ,item_category
            ,deliv_class
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
            ,deliv_qty
            ,deliv_qty_mtr
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,grey_no
            ,ci_no
            ,item_category
            ,deliv_class
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
            ,deliv_qty
            ,deliv_qty_mtr
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        FROM @_TR_PROD_FG_DELIV;
END
