

ALTER PROCEDURE  sp_WeavingProcessWIP1
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @dept INT
			,@rec_sts VARCHAR(MAX)
			,@proc_time AS DATETIME
			,@proc_no INT
			,@max_date AS DATE
			,@tr_code AS VARCHAR(MAX)

	SET @tr_code='WIP1'
	SET @dept=(SELECT dept FROM ms_dept WHERE (comp_id=@comp_id AND dept_seq=30))
	SET @proc_time = GETDATE()
	SET @rec_sts='A'
	SET @proc_no=1
	SET @max_date = (SELECT MAX(stock_take_date) FROM istem_costing.dbo.tr_dept_detail1 WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept) AND (tr_code=@tr_code))

	-- TEMPORARY TABLE
	DECLARE @_TEMP_TR_DEPT_CALC TABLE (
		comp_id numeric(1, 0) NOT NULL,
		f_year numeric(4, 0) NOT NULL,
		f_month numeric(2, 0) NOT NULL,
		dept numeric(3, 0) NOT NULL,
		tr_code varchar(5) NOT NULL,
		mc_loc varchar(15) NOT NULL,
		proc_code varchar(7) NOT NULL,
		item_code varchar(45) NOT NULL,
		mat_code varchar(45) NOT NULL,
		mat_qty numeric(12, 2) NULL,
		mat_qty_unit varchar(5) NULL,
		proc_type varchar(20) NULL,
		proc_time datetime NULL,
		user_id varchar(6) NULL,
		client_ip varchar(15) NULL,
		rec_sts char(1) NULL,
		proc_no tinyint NULL
	)

	-- Cek tr_dept_calc
	-- Find Data
	DECLARE @c INT
	SET @c = (SELECT COUNT(*) FROM istem_costing.dbo.tr_dept_calc WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept) AND (tr_code=@tr_code))

	IF @c >= 1
	BEGIN
		-- Save History
		INSERT INTO istem_costing.dbo.tr_dept_calc_hist
				(comp_id, f_year, f_month, dept, tr_code, mc_loc, proc_code, item_code, mat_code, mat_qty, mat_qty_unit, proc_type, proc_time, user_id, client_ip, rec_sts, proc_no)
				SELECT comp_id, f_year, f_month, dept, tr_code, mc_loc, proc_code, item_code, mat_code, mat_qty, mat_qty_unit, proc_type, proc_time, user_id, client_ip, rec_sts, proc_no
				FROM istem_costing.dbo.tr_dept_calc
				WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept) AND (tr_code=@tr_code)

		-- Jika sudah ada
		-- Jadikan rec_sts = T
		SET @rec_sts = 'T'

		SET @proc_no = (SELECT MAX(proc_no) + 1 FROM istem_costing.dbo.tr_dept_calc WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept) AND (tr_code=@tr_code))
	END

	DELETE FROM istem_costing.dbo.tr_dept_calc WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept) AND (tr_code=@tr_code)

	INSERT INTO @_TEMP_TR_DEPT_CALC
		SELECT
			comp_id
			,f_year
			,f_month
			,dept
			,tr_code
			,mc_loc
			,proc_code
			,item_code
			,mat_code
			,mat_qty
			,mat_qty_unit
			,proc_type
			,@proc_time
			,@user_id
			,@client_ip
			,@rec_sts
			,@proc_no
		FROM
		(
			SELECT
				A.comp_id AS comp_id
				,A.f_year AS f_year
				,A.f_month AS f_month
				,A.dept AS dept
				,A.tr_code AS tr_code
				,A.mc_loc AS mc_loc
				,ISNULL(B.proc_code, '') AS proc_code
				,A.item_code AS item_code
				,CASE
					WHEN A.mc_proc_code='08. DW' THEN A.mat_code
					ELSE D.mat_code
				END AS mat_code
				,CASE
					WHEN A.mc_proc_code='08. DW' THEN A.input_qty
					ELSE CASE
						WHEN A.input_qty_unit='PCS' THEN (A.input_qty*D.warp_wg)
                        WHEN A.input_qty_unit='MTR' THEN (A.input_qty/D.length_gr) *  D.warp_wg
						ELSE A.input_qty
					END
				END AS mat_qty
				,B.mat_qty_unit AS mat_qty_unit
				,C.proc_type AS proc_type
			FROM istem_costing.dbo.tr_dept_detail1 AS A
			LEFT JOIN istem_costing.dbo.ms_mc_proc AS B ON (
				(B.comp_id=A.comp_id)
				AND (B.dept=A.dept)
				AND (B.mc_proc_code=A.mc_proc_code)
			)
			LEFT JOIN istem_costing.dbo.ms_process AS C ON (
				(C.comp_id=A.comp_id)
				AND (C.dept=A.dept)
				AND (C.tr_code=A.tr_code)
				AND (C.proc_code=B.proc_code)
			)
			LEFT JOIN (
				-- WARP 1
				SELECT * FROM (
					SELECT
						A.grey_no AS grey_no
						,ISNULL(A.warp1_yarncode, '') AS mat_code
						,ISNULL(A.warp1_kind, '') AS warp_kind
						,A.length_gr AS length_gr
						,A.warp_ln AS warp_ln
						,A.warp1_wg AS warp_wg
						,'WARP1' AS cat
					FROM istem_sms.dbo.wv_fabric_analysis_master AS A
					UNION ALL
					SELECT
						A.grey_no AS grey_no
						,ISNULL(A.warp2_yarncode, '') AS mat_code
						,ISNULL(A.warp2_kind, '') AS warp_kind
						,A.length_gr AS length_gr
						,A.warp_ln AS warp_ln
						,A.warp1_wg AS warp_wg
						,'WARP2' AS cat
					FROM istem_sms.dbo.wv_fabric_analysis_master AS A
					UNION ALL
					SELECT
						A.grey_no AS grey_no
						,ISNULL(A.warp3_yarncode, '') AS mat_code
						,ISNULL(A.warp3_kind, '') AS warp_kind
						,A.length_gr AS length_gr
						,A.warp_ln AS warp_ln
						,A.warp1_wg AS warp_wg
						,'WARP3' AS cat
					FROM istem_sms.dbo.wv_fabric_analysis_master AS A
					UNION ALL
					SELECT
						A.grey_no AS grey_no
						,ISNULL(A.warp4_yarncode, '') AS mat_code
						,ISNULL(A.warp4_kind, '') AS warp_kind
						,A.length_gr AS length_gr
						,A.warp_ln AS warp_ln
						,A.warp1_wg AS warp_wg
						,'WARP4' AS cat
					FROM istem_sms.dbo.wv_fabric_analysis_master AS A
				) AS TEMP
				WHERE
				(ISNULL(TEMP.mat_code, '')<>'')
			)
			AS D ON (D.grey_no=A.item_code)
			WHERE
				(A.f_year = @f_year)
			AND (A.f_month = @f_month)
			AND (A.dept = @dept)
			AND (A.stock_take_date = @max_date)
			AND (A.tr_code=@tr_code)
		) AS SOURCE_DATA
		WHERE
		(ISNULL(SOURCE_DATA.mat_code, '') <> '')

	INSERT INTO istem_costing.dbo.tr_dept_calc
			(comp_id
				,f_year
				,f_month
				,dept
				,tr_code
				,mc_loc
				,proc_code
				,item_code
				,mat_code
				,mat_qty
				,mat_qty_unit
				,proc_type
				,proc_time
				,user_id
				,client_ip
				,rec_sts
				,proc_no)
			SELECT
				 comp_id
				,f_year
				,f_month
				,dept
				,tr_code
				,mc_loc
				,proc_code
				,item_code
				,mat_code
				,SUM(mat_qty)
				,mat_qty_unit
				,proc_type
				,proc_time
				,user_id
				,client_ip
				,rec_sts
				,proc_no
			FROM @_TEMP_TR_DEPT_CALC
			GROUP BY
				 comp_id
				,f_year
				,f_month
				,dept
				,tr_code
				,mc_loc
				,proc_code
				,item_code
				,mat_code
				,mat_qty_unit
				,proc_type
				,proc_time
				,user_id
				,client_ip
				,rec_sts
				,proc_no


END
