
-- =============================================
-- Author:		Apri
-- Create date: 2020-10-01
-- Description:	Proses Data untuk Spinning
-- =============================================
ALTER PROCEDURE  [sp_SpinningProcess]
	@comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
AS
BEGIN
	EXEC [sp_SpinningProcessWIP] @comp_id, @f_year, @f_month, @user_id, @client_ip
	EXEC [sp_SpinningProcessRCVRM] @comp_id, @f_year, @f_month, @user_id, @client_ip
	EXEC [sp_SpinningProcessStapleCost] @comp_id, @f_year, @f_month, @user_id, @client_ip
	EXEC [sp_SpinningProcessWH] @comp_id, @f_year, @f_month, @user_id, @client_ip
	EXEC [sp_SpinningProcessWIPCALC] @comp_id, @f_year, @f_month, @user_id, @client_ip
	EXEC [sp_SpinningProcessConsMat] @comp_id, @f_year, @f_month, @user_id, @client_ip
END

