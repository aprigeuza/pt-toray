
-- =============================================
-- SUB 3
-- GR - GI Yarn Product : Receive Yarn Production Spinning
-- =============================================
ALTER PROCEDURE  [sp_LogDownMatGRYarnProd]
	@CompID INT, @F_YEAR INT, @F_MONTH INT, @LoginID VARCHAR(20), @ClientIP VARCHAR(24), @ProcNo INT
AS
BEGIN
	SET NOCOUNT ON;
	DECLARE @FromDate DATE, @ToDate DATE, @RecSts CHAR(1)

	SELECT TOP 1 @FromDate = F_RefDate, @ToDate = T_RefDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)


	IF @ProcNo >= 1
	BEGIN
	  SET @ProcNo = @ProcNo + 1
	  SET @RecSts = 'T'
	END
	ELSE
	BEGIN
	  SET @ProcNo = 1
	  SET @RecSts = 'A'
	END

	DECLARE @dept AS INT
	SELECT @dept = dept FROM ms_dept WHERE (comp_id = @CompID) AND (dept_seq = 10)

	DECLARE @_TEMP TABLE (
		[comp_id] [numeric](1, 0) NOT NULL,
		[f_year] [numeric](4, 0) NOT NULL,
		[f_month] [numeric](2, 0) NOT NULL,
		[dept] [numeric](3, 0) NOT NULL,
		[cost_sheet_id] [varchar](4) NOT NULL,
		[mat_code] [varchar](45) NOT NULL,
		[mat_usage] [char](1) NULL,
		[in_bale_qty] [numeric](4, 0) NULL,
		[in_qty] [numeric](14, 2) NULL,
		[in_amount] [numeric](14, 2) NULL,
		[in_invoice_qty] [numeric](14, 2) NULL,
		[qty_unit] [varchar](5) NULL,
		[proc_time] [datetime] NULL,
		[user_id] [varchar](6) NULL,
		[client_ip] [varchar](15) NULL,
		[rec_sts] [char](1) NULL,
		[proc_no] [tinyint] NULL
	)

	INSERT INTO @_TEMP
		SELECT
			@CompID AS comp_id
			,@F_YEAR AS f_year
			,@F_MONTH AS f_month
			,@dept AS dept
			,A.U_Cost_ID AS cost_sheet_id
			,A.ItemCode AS mat_code
			,ISNULL(A.U_Yarn_Usage, 'L') AS mat_usage
			,0 AS in_bale_qty
			,SUM(A.in_qty) AS in_qty
			,0 AS in_amount
			,0 AS in_invoice_qty
			,'LBS' AS qty_unit
			,GETDATE() AS proc_time
			,@LoginID AS user_id
			,@ClientIP AS client_ip
			,@RecSts AS rec_sts
			,@ProcNo AS proc_no
		FROM (
			SELECT
				T0.docdate,
				T1.ItemCode,
				T2.itmsgrpcod,
				T1.Dscription,
				T2.FrgnName,
				T1.Quantity AS in_qty,
				T1.OcrCode AS in_dest,
				T2.BuyUnitMsr AS Unit,
				T1.U_Yarn_Usage,
				T3.U_Cost_ID
			FROM SAP_ISM.SBO_ISM_LIVE.DBO.OIGN T0
			LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.IGN1 T1 on T0.docentry=T1.DocEntry
			LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM T2 on T1.itemcode=T2.itemcode
			LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITB T3 ON T3.ItmsGrpCod=T2.ItmsGrpCod
			WHERE 
				(T2.ItmsGrpCod IN (110, 240)) 
				AND (ISNULL(T0.U_GR_Rea, '')<>'ADJ')

			UNION ALL

			SELECT
				T0.docdate,
				T1.ItemCode,
				T2.itmsgrpcod,
				T1.Dscription,
				T2.FrgnName,
				(T1.Quantity*-1) AS in_qty,
				T1.OcrCode AS in_dest,
				T2.BuyUnitMsr AS Unit,
				T1.U_Yarn_Usage,
				T3.U_Cost_ID
			FROM SAP_ISM.SBO_ISM_LIVE.DBO.OIGE T0
			LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.IGE1 T1 on T0.docentry=T1.DocEntry
			LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM T2 on T1.itemcode=T2.itemcode
			LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITB T3 ON T3.ItmsGrpCod=T2.ItmsGrpCod
			WHERE
				(T2.ItmsGrpCod IN (110, 240)) 
				AND (T1.OcrCode<>800) 
				AND (ISNULL(T0.U_GI_Rea, '')='ADJ')
		) A

		WHERE
		  (A.DocDate >= @FromDate) AND (A.DocDate <= @ToDate)
		GROUP BY
			A.U_Cost_ID, A.ItemCode, A.U_Yarn_Usage, A.Unit


    -- Update tanggal 2 oktober 2020
    -- karena ada masalah di tr_inv_in yang juga di pakai di weaving wip process
    UPDATE @_TEMP SET mat_usage='S' WHERE cost_sheet_id='A31'


	INSERT INTO [istem_costing].[dbo].[tr_inv_in]
       (comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, mat_usage, in_bale_qty, in_qty, in_amount, in_invoice_qty, qty_unit, proc_time, user_id, client_ip, rec_sts, proc_no)
       SELECT comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, mat_usage,
        SUM(in_bale_qty) AS in_bale_qty,
        SUM(in_qty) AS in_qty,
        SUM(in_amount) AS in_amount,
        SUM(in_invoice_qty) AS in_invoice_qty,
        qty_unit,
        GETDATE() AS proc_time,
		@LoginID AS user_id,
		@ClientIP AS client_ip,
		@RecSts AS rec_sts,
		@ProcNo AS proc_no
       FROM @_TEMP GROUP BY comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, mat_usage, qty_unit


	-- SELECT COUNT(*) AS NumRows FROM @_TEMP
	DECLARE @res INT
	SELECT @res = COUNT(*) FROM @_TEMP GROUP BY comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, mat_usage, qty_unit;
	RETURN @res;
	
END


