ALTER PROCEDURE  sp_AcctDyeingFG_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- Acct - Dyeing
    -- Dyeing
    -- Acct - Dyeing FG
    -- sp_AcctDyeingFG_Data

    -- DECLARE
    -- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept INT
    SET @dept = (SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40))

    DECLARE @_TEMP TABLE (
        -- ROW HEADER
        cat varchar(25) NOT NULL,
        cat_name varchar(50) NOT NULL,
    	comp_id numeric(1,0) NOT NULL,
    	f_year numeric(4,0) NOT NULL,
    	f_month numeric(2,0) NOT NULL,
    	dept numeric(3,0) NOT NULL,
    	grey_no varchar(6) NOT NULL,
    	ci_no varchar(20) NOT NULL,
        item_category varchar(2) NULL,
    	dom_exp char(1) NOT NULL,
    	undelivery bit NOT NULL,
        qty_unit varchar(5) NOT NULL,

        -- COLUMN HEADER
        -- Opening Balance
        bf_qty numeric(16,4),
        bf_price numeric(16,4),
        bf_amount numeric(16,4),
        -- Transfer
        tf_qty numeric(16,4),
        tf_price numeric(16,4),
        tf_amount numeric(16,4),
        -- Production
        prod_qty numeric(16,4),
        prod_price numeric(16,4),
        prod_amount numeric(16,4),
        -- Average Price
        avg_price numeric(16,4),
        -- Sales A
        sales_a_qty numeric(16,4),
        sales_a_price numeric(16,4),
        sales_a_amount numeric(16,4),
        -- Sales C
        sales_c_qty numeric(16,4),
        sales_c_price numeric(16,4),
        sales_c_amount numeric(16,4),
        -- Sample
        samp_qty numeric(16,4),
        samp_price numeric(16,4),
        samp_amount numeric(16,4),
        -- Factory Use
        fac_qty numeric(16,4),
        fac_price numeric(16,4),
        fac_amount numeric(16,4),
        -- Return
        ret_qty numeric(16,4),
        ret_price numeric(16,4),
        ret_amount numeric(16,4),
        -- To Process
        to_proc_qty numeric(16,4),
        to_proc_price numeric(16,4),
        to_proc_amount numeric(16,4),
        -- Goods In transit
        git_qty numeric(16,4),
        git_price numeric(16,4),
        git_amount numeric(16,4),
        -- Total Issued
        tot_issue_qty numeric(16,4),
        tot_issue_price numeric(16,4),
        tot_issue_amount numeric(16,4),
        -- Ending Balance
        cf_qty numeric(16,4),
        cf_price numeric(16,4),
        cf_amount numeric(16,4),
        -- Impairment Price
        impairment_price numeric(16,4),

        sales_amount numeric(16,4),
        impairment_amount numeric(16,4),

        -- Ending Balance After impairment
        cf_final_qty numeric(16,4),
        cf_final_price numeric(16,4),
        cf_final_amount numeric(16,4),

    	fg_deliv_qty numeric(12,2) NULL,
    	sl_deliv_qty numeric(12,2) NULL,
    	var_fg_sl_qty numeric(12,2) NULL,
    	lm_mit_qty numeric(12,2) NULL,
    	lm_mit_amount numeric(19,2) NULL,
    	tm_mit_qty numeric(12,2) NULL,
    	tm_mit_amount numeric(19,2) NULL,
    	proc_time datetime NULL,
    	user_id varchar(6) NULL,
    	client_ip varchar(15) NULL,
    	rec_sts char(1) NULL,
    	proc_no tinyint NULL
    )

    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT

    IF @f_month = 1
        BEGIN
            SET @lp_f_month = 12
            SET @lp_f_year = @f_year - 1
        END
        ELSE
        BEGIN
            SET @lp_f_month = @f_month - 1
            SET @lp_f_year = @f_year
        END


    -- (1)	DOMESTIC TR (Mtr) (A)
    -- tr_prod_fg --> dom_exp = "D" and undelivery = 0 and qty_unit = "MTR" and item_category = "TR"
    -- Insert Data
    INSERT INTO @_TEMP
        (
            cat
            ,cat_name
            ,comp_id
            ,f_year
            ,f_month
            ,dept
            ,grey_no
            ,ci_no
            ,item_category
            ,dom_exp
            ,undelivery
            ,qty_unit
        )
        SELECT
            A.cat AS cat
            ,A.cat_name AS cat_name
            ,@comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept AS dept
            ,A.grey_no AS grey_no
            ,A.ci_no AS ci_no
            ,A.item_category AS item_category
            ,A.dom_exp AS dom_exp
            ,A.undelivery AS undelivery
            ,A.qty_unit AS qty_unit
        FROM (
            -- (1)	DOMESTIC TR (Mtr) (A) 			tr_prod_fg --> dom_exp = "D" and undelivery = 0 and qty_unit = "MTR" and item_category = "TR"
            SELECT 100 AS so, '1' AS cat, 'DOMESTIC TR (Mtr) (A)' AS cat_name, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('D') AND undelivery=0 AND qty_unit IN ('MTR') AND item_category IN ('TR')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (1).a	DOMESTIC SP (Mtr) (A) 			tr_prod_fg --> dom_exp = "D" and undelivery = 0 and qty_unit = "MTR" and item_category = "SP"
            SELECT 110 AS so, '1.A' AS c, 'DOMESTIC SP (Mtr) (A)' AS cat_nameat, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('D') AND undelivery=0 AND qty_unit IN ('MTR') AND item_category IN ('SP')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (2)	DOMESTIC TR (Mtr) (C) 			tr_prod_fg --> dom_exp = "D" and undelivery = 1 and qty_unit = "MTR" and item_category = "TR"
            SELECT 200 AS so, '2' AS cat, 'DOMESTIC TR (Mtr) (C)' AS cat_name, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('D') AND undelivery=1 AND qty_unit IN ('MTR') AND item_category IN ('TR')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (2).a	DOMESTIC SP (Mtr) (C) 			tr_prod_fg --> dom_exp = "D" and undelivery = 1 and qty_unit = "MTR" and item_category = "SP"
            SELECT 210 AS so, '2.A' AS c, 'DOMESTIC SP (Mtr) (C)' AS cat_nameat, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('D') AND undelivery=1 AND qty_unit IN ('MTR') AND item_category IN ('SP')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (3)	DOMESTIC TR (Yds) (A) 			tr_prod_fg --> dom_exp = "D" and undelivery = 0 and qty_unit = "YDS" and item_category = "TR"
            SELECT 300 AS so, '3' AS cat, 'DOMESTIC TR (Yds) (A)' AS cat_name, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('D') AND undelivery=0 AND qty_unit IN ('YDS') AND item_category IN ('TR')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (3).a	DOMESTIC SP (Yds) (A) 			tr_prod_fg --> dom_exp = "D" and undelivery = 0 and qty_unit = "YDS" and item_category = "SP"
            SELECT 310 AS so, '3.A' AS c, 'DOMESTIC SP (Yds) (A)' AS cat_nameat, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('D') AND undelivery=1 AND qty_unit IN ('MTR') AND item_category IN ('TR')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (4)	DOMESTIC TR (Yds) (C) 			tr_prod_fg --> dom_exp = "D" and undelivery = 1 and qty_unit = "YDS" and item_category = "TR"
            SELECT 400 AS so, '4' AS cat, 'DOMESTIC TR (Yds) (C)' AS cat_name, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('D') AND undelivery=1 AND qty_unit IN ('YDS') AND item_category IN ('TR')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (4).a	DOMESTIC SP (Yds) (C) 			tr_prod_fg --> dom_exp = "D" and undelivery = 1 and qty_unit = "YDS" and item_category = "SP"
            SELECT 410 AS so, '4' AS cat, 'DOMESTIC SP (Yds) (C)' AS cat_name, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('D') AND undelivery=1 AND qty_unit IN ('YDS') AND item_category IN ('SP')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (8)	EXPORT TR (Mtr)(A)			tr_prod_fg --> dom_exp = "E" and undelivery = 0 and qty_unit = "MTR" and item_category = "TR"
            SELECT 800 AS so, '8' AS cat, 'EXPORT TR (Mtr)(A)' AS cat_name, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('E') AND undelivery=0 AND qty_unit IN ('MTR') AND item_category IN ('TR')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (9)	EXPORT SP (Mtr)(A)			tr_prod_fg --> dom_exp = "E" and undelivery = 0 and qty_unit = "MTR" and item_category = "SP"
            SELECT 900 AS so, '9' AS cat, 'EXPORT SP (Mtr)(A)' AS cat_name, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('E') AND undelivery=0 AND qty_unit IN ('MTR') AND item_category IN ('SP')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (10)	EXPORT TR (Mtr)(C)			tr_prod_fg --> dom_exp = "E" and undelivery = 1 and qty_unit = "MTR" and item_category = "TR"
            SELECT 1000 AS so, '10' AS cat, 'EXPORT TR (Mtr)(C)' AS cat_name, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('E') AND undelivery=1 AND qty_unit IN ('MTR') AND item_category IN ('TR')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (11)	EXPORT SP (Mtr)(C)			tr_prod_fg --> dom_exp = "E" and undelivery = 1 and qty_unit = "MTR" and item_category = "SP"
            SELECT 1100 AS so, '11' AS cat, 'EXPORT SP (Mtr)(C)' AS cat_name, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('E') AND undelivery=1 AND qty_unit IN ('MTR') AND item_category IN ('SP')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (12)	EXPORT TR (Yds)(A)			tr_prod_fg --> dom_exp = "E" and undelivery = 0 and qty_unit = "YDS" and item_category = "TR"
            SELECT 1200 AS so, '12' AS cat, 'EXPORT TR (Yds)(A)' AS cat_name, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('E') AND undelivery=0 AND qty_unit IN ('YDS') AND item_category IN ('TR')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (13)	EXPORT SP (Yds)(A)			tr_prod_fg --> dom_exp = "E" and undelivery = 0 and qty_unit = "YDS" and item_category = "SP"
            SELECT 1300 AS so, '13' AS cat, 'EXPORT SP (Yds)(A)' AS cat_name, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('E') AND undelivery=0 AND qty_unit IN ('YDS') AND item_category IN ('SP')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (14)	EXPORT TR (Yds)(C)			tr_prod_fg --> dom_exp = "E" and undelivery = 1 and qty_unit = "YDS" and item_category = "TR"
            SELECT 1400 AS so, '13' AS cat, 'EXPORT TR (Yds)(C)' AS cat_name, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('E') AND undelivery=1 AND qty_unit IN ('YDS') AND item_category IN ('TR')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit

            UNION ALL

            -- (15)	EXPORT SP (Yds)(C)			tr_prod_fg --> dom_exp = "E" and undelivery = 1 and qty_unit = "YDS" and item_category = "SP"
            SELECT 1500 AS so, '15' AS cat, 'EXPORT SP (Yds)(C)' AS cat_name, grey_no, ci_no, item_category, dom_exp, undelivery, qty_unit
            FROM istem_costing.dbo.tr_prod_fg
            WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept AND dom_exp IN ('E') AND undelivery=1 AND qty_unit IN ('YDS') AND item_category IN ('SP')
            GROUP BY grey_no ,ci_no ,item_category ,dom_exp ,undelivery ,qty_unit
        ) AS A
        ORDER BY
            A.so
            ,A.cat
            ,A.grey_no
            ,A.ci_no
            ,A.item_category
            ,A.dom_exp
            ,A.undelivery
            ,A.qty_unit


    -- Beginning Balance
    UPDATE T SET
        T.bf_qty=S.bf_qty -- A1
        ,T.bf_price=S.bf_price -- A2
        ,T.bf_amount=S.bf_amount -- A3
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,item_category
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(cf_fg_qty) AS bf_qty
            ,NULLIF(SUM(cf_fg_amount), 0)/NULLIF(SUM(cf_fg_qty), 0) AS bf_price
            ,SUM(cf_fg_amount) AS bf_amount
        FROM istem_costing.dbo.tr_bal_fg
        WHERE
                comp_id=@comp_id
            AND f_year=@lp_f_year
            AND f_month=@lp_f_month
            AND dept=@dept
            AND undelivery=0
            AND ISNULL(ci_no, '') <> ''
            AND dom_exp IN ('D')
            AND qty_unit IN ('MTR')
        GROUP BY
            grey_no
            ,ci_no
            ,item_category
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.item_category=T.item_category AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)

    -- tf_qty       B1	sum(tr_prod_fg.prod_qty_mtr)
    -- tf_price     B2 	B3 / B1
    -- tf_amount    B3	sum(tr_prod_fg.material_cost+process_cost+pack_mat_cost)+ sum(tr_prod_detail2.prod_cost_distr) group by ci_no+grey_no
    UPDATE T SET
         T.tf_qty=S.tf_qty -- B1
        ,T.tf_price=S.tf_price -- B2
        ,T.tf_amount=S.tf_amount -- B3
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,item_category
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(prod_qty_mtr) AS tf_qty
            ,NULL AS tf_price
            ,NULL AS tf_amount
        FROM istem_costing.dbo.tr_prod_fg
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND wh_class IN ('7')
        GROUP BY
            grey_no
            ,ci_no
            ,item_category
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.item_category=T.item_category AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)

    -- prod_qty     C1	sum(tr_prod_fg.prod_qty_mtr)				sesuai kondisi "Category"  and (tr_prod_fg.wh_class=1 or wh_class=6)
    -- prod_price   C2 	C3 / C1
    -- prod_amount  C3	sum(tr_prod_fg.material_cost+process_cost+pack_mat_cost) 						 sesuai kondisi "Category"  and (tr_prod_fg.wh_class=1 or wh_class=6)
    UPDATE T SET
         T.prod_qty=S.prod_qty -- B1
        ,T.prod_price=S.prod_price -- B2
        ,T.prod_amount=S.prod_amount -- B3
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,item_category
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(prod_qty_mtr) AS prod_qty
            ,NULLIF(SUM(material_cost+process_cost+pack_mat_cost),0)/NULLIF(SUM(prod_qty_mtr), 0) AS prod_price
            ,SUM(material_cost+process_cost+pack_mat_cost) AS prod_amount
        FROM istem_costing.dbo.tr_prod_fg
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND wh_class IN ('1', '6')
        GROUP BY
            grey_no
            ,ci_no
            ,item_category
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.item_category=T.item_category AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)

    -- ############################################################
    UPDATE T SET
        T.sales_a_price=(NULLIF(ISNULL(T.bf_amount, 0)+ISNULL(T.tf_amount, 0)+ISNULL(T.prod_amount, 0), 0) / NULLIF(ISNULL(T.bf_qty, 0)+ISNULL(T.tf_qty, 0)+ISNULL(T.prod_qty, 0), 0))
    FROM @_TEMP AS T
    UPDATE T SET
        T.sales_c_price=T.sales_a_price
        ,T.fac_price=T.sales_a_price
        ,T.ret_price=T.sales_a_price
        ,T.to_proc_price=T.sales_a_price
        ,T.git_price=T.sales_a_price
        ,T.tot_issue_price=T.sales_a_price
        ,T.cf_price=T.cf_price
    FROM @_TEMP AS T
    -- ############################################################


    -- fg_deliv_qty     T1  sum(tr_prod_fg_deliv.deliv_qty_mtr)
    UPDATE T SET
        T.fg_deliv_qty=S.sum_deliv_qty_mtr
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            A.grey_no,
            A.ci_no,
            A.item_category,
            A.dom_exp,
            A.undelivery,
            A.qty_unit,
            SUM(A.deliv_qty_mtr) AS sum_deliv_qty_mtr
        FROM tr_prod_fg_deliv AS A
        WHERE
            A.comp_id=1
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.deliv_class='1'
        GROUP BY
            A.grey_no,
            A.ci_no,
            A.item_category,
            A.dom_exp,
            A.undelivery,
            A.qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.item_category=T.item_category AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)

    -- sl_deliv_qty	    T2	sum(dbo.istem_sms.sl_calculation.qty_mtr)
    -- var_fg_sl_qty	T3	T1 - T2
    -- ############################################################
    -- UPDATE T SET
    --     T.sl_deliv_qty=S.sl_deliv_qty
    --     ,T.var_fg_sl_qty=ISNULL(T.fg_deliv_qty, 0) - ISNULL(S.sl_deliv_qty, 0)
    -- FROM @_TEMP AS T
    -- LEFT JOIN (
    --     SELECT * AS sl_deliv_qty
    --     FROM
    -- ) AS S
    -- ############################################################
    -- ############################################################


    -- lm_mit_qty       U1	tr_fg_balance.tm_mit_qty  yang bulan lalu
    -- lm_mit_amount    U2	tr_fg_balance.tm_mit_amount yang bulan lalu
    UPDATE T SET
        T.lm_mit_qty=S.lm_mit_qty
        ,T.lm_mit_amount=S.lm_mit_amount
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT *
        FROM istem_costing.dbo.tr_fg_balance AS A
        WHERE
            A.comp_id=@comp_id
            AND A.f_year=@lp_f_year
            AND A.f_month=@lp_f_month
            AND A.dept=@dept
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.item_category=T.item_category AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit )

    -- tm_mit_qty       V1 	T3 + U1
    -- tm_mit_amount    V2	( A3 + B3 + C3) / (A1 + B1 + C1)  * V1
    -- ############################################################
    UPDATE T SET
        T.tm_mit_qty=ISNULL(T.var_fg_sl_qty, 0) + ISNULL(T.lm_mit_qty, 0)
        ,T.tm_mit_amount=(ISNULL(T.var_fg_sl_qty, 0) + ISNULL(T.lm_mit_qty, 0)) * T.sales_a_price
    FROM @_TEMP AS T
    -- ############################################################
    -- ############################################################


    -- sales_a_qty      E1	sum(tr_prod_fg_deliv.deliv_qty_mtr) + U1 - V2  *) sesuai kondisi "Category"   and tr_prod_fg_deliv.deliv_class=1
    -- sales_a_amount   E2	( A3 + B3 + C3 ) / (A1 + B1 + C1) * E1
    UPDATE T SET
         T.sales_a_qty=ISNULL(S.sales_a_qty, 0) + ISNULL(T.lm_mit_qty, 0) - ISNULL(T.tm_mit_qty, 0) -- B1
        ,T.sales_a_amount=(ISNULL(S.sales_a_qty, 0) + ISNULL(T.lm_mit_qty, 0) - ISNULL(T.tm_mit_qty, 0)) * T.sales_a_price -- B2
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,item_category
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(deliv_qty_mtr) AS sales_a_qty
        FROM istem_costing.dbo.tr_prod_fg_deliv
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND deliv_class IN ('1')
        GROUP BY
            grey_no
            ,ci_no
            ,item_category
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.item_category=T.item_category AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)

    -- sales_c_qty      F1	sum(tr_prod_fg_deliv.deliv_qty_mtr) + U1 - V2  *) sesuai kondisi "Category"   and tr_prod_fg_deliv.deliv_class=2
    -- sales_c_amount   F2	( A3 + B3 + C3 ) / (A1 + B1 + C1) * F1
    UPDATE T SET
         T.sales_c_qty=ISNULL(S.sales_c_qty, 0) + ISNULL(T.lm_mit_qty, 0) - ISNULL(T.tm_mit_qty, 0) -- B1
        ,T.sales_c_amount=(ISNULL(S.sales_c_qty, 0) + ISNULL(T.lm_mit_qty, 0) - ISNULL(T.tm_mit_qty, 0)) * T.sales_c_price -- B2
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,item_category
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(deliv_qty_mtr) AS sales_c_qty
        FROM istem_costing.dbo.tr_prod_fg_deliv
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND deliv_class IN ('2')
        GROUP BY
            grey_no
            ,ci_no
            ,item_category
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.item_category=T.item_category AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)

    -- fac_qty      H1	sum(tr_prod_fg_deliv.deliv_qty_mtr)				sesuai kondisi "Category"  and tr_prod_fg_deliv.deliv_class=4
    -- fac_amount   H2	( A3 + B3 + C3 ) / (A1 + B1 + C1) * G1
    UPDATE T SET
        T.fac_qty=S.fac_qty
        ,T.fac_amount=S.fac_qty*T.fac_price
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(deliv_qty_mtr) AS fac_qty
        FROM istem_costing.dbo.tr_prod_fg_deliv
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND deliv_class IN ('4')
        GROUP BY
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)

    -- ret_qty      J1	sum(tr_prod_fg.prod_qty_mtr) * -1				sesuai kondisi "Category"  and tr_prod_fg.wh_class=2
    -- ret_amount   J2	( A3 + B3 + C3 ) / (A1 + B1 + C1) * J1
    UPDATE T SET
        T.ret_qty=S.ret_qty
        ,T.ret_amount=S.ret_qty*T.ret_price
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(prod_qty_mtr) * -1 AS ret_qty
        FROM istem_costing.dbo.tr_prod_fg
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND wh_class IN ('2')
        GROUP BY
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)

    -- to_proc_qty      K1	sum(tr_prod_fg_deliv.deliv_qty_mtr)				sesuai kondisi "Category"   and tr_prod_fg_deliv.deliv_class=5
    -- to_proc_amount   K2	( A3 + B3 + C3 ) / (A1 + B1 + C1) * K1
    UPDATE T SET
        T.to_proc_qty=S.to_proc_qty
        ,T.to_proc_amount=S.to_proc_qty*T.to_proc_price
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,SUM(deliv_qty_mtr) AS to_proc_qty
        FROM istem_costing.dbo.tr_prod_fg_deliv
        WHERE
                comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND deliv_class IN ('5')
        GROUP BY
            grey_no
            ,ci_no
            ,dom_exp
            ,undelivery
            ,qty_unit
    ) AS S ON (S.grey_no=T.grey_no AND S.ci_no=T.ci_no AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery AND S.qty_unit=T.qty_unit)

    -- git_qty      L1	V1 - U1
    -- git_amount   L2	( A3 + B3 + C3 ) / (A1 + B1 + C1) * L1
    UPDATE T SET
        T.git_qty=ISNULL(T.tm_mit_qty, 0)-ISNULL(T.lm_mit_qty, 0)
        ,T.git_amount=ISNULL(T.tm_mit_qty, 0)-ISNULL(T.lm_mit_qty, 0) * T.git_price
    FROM @_TEMP AS T

    --
    -- tot_issue_qty	   M1	E1 + F1 + G1 + H1 + J1 + K1 + L1
	-- tot_issue_price     M2 	( A3 + B3 + C3) / (A1 + B1 + C1)
	-- tot_issue_amount	   M3	M1 * M2
    UPDATE T SET
        T.tot_issue_qty=ISNULL(T.sales_a_qty, 0)+ISNULL(T.sales_c_qty, 0)+ISNULL(T.samp_qty, 0)+ISNULL(T.fac_qty, 0)+ISNULL(T.ret_qty, 0)+ISNULL(T.ret_qty, 0)+ISNULL(T.to_proc_qty, 0)+ISNULL(T.git_qty, 0)
        ,T.tot_issue_amount=ISNULL(T.sales_a_qty, 0)+ISNULL(T.sales_c_qty, 0)+ISNULL(T.samp_qty, 0)+ISNULL(T.fac_qty, 0)+ISNULL(T.ret_qty, 0)+ISNULL(T.ret_qty, 0)+ISNULL(T.to_proc_qty, 0)+ISNULL(T.git_qty, 0) * T.tot_issue_price
    FROM @_TEMP AS T

    -- cf_qty       N1  A1 + B1 + C1 - M1
    -- cf_price	    N2  ( A3 + B3 + C3) / (A1 + B1 + C1)
    -- cf_amount    N3  N1 * N2
    -- CF
    UPDATE T SET
        T.cf_qty=ISNULL(T.bf_qty, 0)+ISNULL(T.tf_qty, 0)+ISNULL(T.prod_qty, 0)+ISNULL(T.tot_issue_qty, 0)
        ,T.cf_amount=ISNULL(T.bf_qty, 0)+ISNULL(T.tf_qty, 0)+ISNULL(T.prod_qty, 0)+ISNULL(T.tot_issue_qty, 0)*T.cf_price
    FROM @_TEMP AS T


    -- impairment_price	P1	tr_fg_price.impair_fg_price			bila tr_fg_price.grey_no = blank --> berlaku untuk semua grey_no, bila <> blank --> sesuai grey_no
	-- sales_amount	Q2	N1 * P1
	-- impairment_amount	R1	N3 - Q2
    DECLARE @im INTEGER
    SET @im = (
        SELECT
            COUNT(grey_no)
        FROM istem_costing.dbo.tr_fg_price
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND ISNULL(grey_no, '')='')

    IF @im >= 1
    BEGIN
        DECLARE @im_price INTEGER
        SET @im_price = (
            SELECT
                impair_fg_price
            FROM istem_costing.dbo.tr_fg_price
            WHERE
                comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept=@dept
                AND ISNULL(grey_no, '')='')

        UPDATE T SET
            T.impairment_price=@im_price
        FROM @_TEMP AS T
    END
    ELSE
    BEGIN
        UPDATE T SET
            T.impairment_price=CASE WHEN S.impairment_price IS NULL THEN T.cf_price ELSE S.impairment_price END
        FROM @_TEMP AS T
        LEFT JOIN (
            SELECT
                grey_no
                ,dom_exp
                ,undelivery
                ,impair_fg_price AS impairment_price
            FROM istem_costing.dbo.tr_fg_price
            WHERE
                comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept=@dept
                AND ISNULL(grey_no, '')=''
        ) AS S ON (S.grey_no=T.grey_no AND S.dom_exp=T.dom_exp AND S.undelivery=T.undelivery)
    END

    -- sales_amount	Q2	N1 * P1
    -- impairment_amount	R1	N3 - Q2
    UPDATE T SET
        T.sales_amount=NULLIF(T.cf_qty, 0)/NULLIF(T.impairment_price, 0)
        ,T.impairment_amount=ISNULL(T.cf_amount, 0)-ISNULL(NULLIF(T.cf_qty, 0)/NULLIF(T.impairment_price, 0), 0)
    FROM @_TEMP AS T

    -- cf_final_qty	S1	N1
    -- cf_final_price	 S2 	S3 / S1
    -- cf_final_amount	S3	N3 - R1

    UPDATE T SET
        T.cf_final_qty=NULLIF(T.cf_qty, 0)
        ,T.cf_final_price=NULLIF(ISNULL(T.cf_amount, 0)-ISNULL(T.impairment_amount, 0), 0)/NULLIF(T.cf_qty, 0)
        ,T.cf_final_amount=ISNULL(T.cf_amount, 0)-ISNULL(T.impairment_amount, 0)
    FROM @_TEMP AS T


    SELECT * FROM @_TEMP;

END
