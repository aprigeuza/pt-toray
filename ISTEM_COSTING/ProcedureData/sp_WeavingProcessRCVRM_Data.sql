CREATE PROCEDURE  sp_WeavingProcessRCVRM_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- DECLARE
    -- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept INT
    		,@rec_sts VARCHAR(MAX)
    		,@proc_time AS DATETIME
    		,@proc_no INT
    		,@tr_code VARCHAR(MAX)
    		,@proc_code VARCHAR(MAX)
    		,@proc_type VARCHAR(MAX)

    SET @tr_code='RCVRM'
    SET @proc_code='RCVRM'
    SET @dept=(SELECT dept FROM ms_dept WHERE (comp_id=@comp_id AND dept_seq=30))
    SET @proc_time = GETDATE()
    SET @rec_sts='A'
    SET @proc_no=1
    SET @proc_type = (SELECT TOP 1 proc_type FROM istem_costing.dbo.ms_process WHERE (comp_id=@comp_id AND dept=@dept AND proc_code=@proc_code))

    -- TEMPORARY TABLE
    DECLARE @_TEMP_TR_DEPT_CALC TABLE (
    	[comp_id] [numeric](1, 0) NOT NULL,
    	[f_year] [numeric](4, 0) NOT NULL,
    	[f_month] [numeric](2, 0) NOT NULL,
    	[dept] [numeric](3, 0) NOT NULL,
    	[tr_code] [varchar](5) NOT NULL,
    	[mc_loc] [varchar](15) NOT NULL,
    	[proc_code] [varchar](7) NOT NULL,
    	[item_code] [varchar](45) NOT NULL,
    	[mat_code] [varchar](45) NOT NULL,
    	[mat_qty] [numeric](12, 2) NULL,
    	[mat_qty_unit] [varchar](5) NULL,
    	[proc_type] [varchar](20) NULL,
    	[proc_time] [datetime] NULL,
    	[user_id] [varchar](6) NULL,
    	[client_ip] [varchar](15) NULL,
    	[rec_sts] [char](1) NULL,
    	[proc_no] [tinyint] NULL
    )

    INSERT INTO @_TEMP_TR_DEPT_CALC
    	(
    		 comp_id
    		,f_year
    		,f_month
    		,dept
    		,tr_code
    		,mc_loc
    		,proc_code
    		,item_code
    		,mat_code
    		,mat_qty
    		,mat_qty_unit
    		,proc_type
    		,proc_time
    		,user_id
    		,client_ip
    		,rec_sts
    		,proc_no
    	)
    	SELECT
    		 A.comp_id AS comp_id
    		,A.f_year AS f_year
    		,A.f_month AS f_month
    		,@dept AS dept
    		,@tr_code AS tr_code
    		,'' AS mc_loc
    		,@proc_code AS proc_code
    		,'' AS item_code
    		,A.mat_code AS mat_code
    		,A.out_qty AS mat_qty
    		,A.qty_unit AS mat_qty_unit
    		,@proc_type AS proc_type
    		,@proc_time AS proc_time
    		,@user_id AS user_id
    		,@client_ip AS client_ip
    		,@rec_sts AS rec_sts
    		,@proc_no AS proc_no
    	FROM tr_inv_out_detail AS A
    	WHERE
    			(A.f_year=@f_year)
    		AND (A.f_month=@f_month)
    		AND (A.out_dest=@dept)
    		AND (A.cost_sheet_id IN ('A20', 'A21'))

    SELECT
    	 comp_id
    	,f_year
    	,f_month
    	,dept
    	,tr_code
    	,mc_loc
    	,proc_code
    	,item_code
    	,mat_code
    	,SUM(mat_qty)
    	,mat_qty_unit
    	,proc_type
    	,proc_time
    	,user_id
    	,client_ip
    	,rec_sts
    	,proc_no
    FROM @_TEMP_TR_DEPT_CALC
    GROUP BY
    	 comp_id
    	,f_year
    	,f_month
    	,dept
    	,tr_code
    	,mc_loc
    	,proc_code
    	,item_code
    	,mat_code
    	,mat_qty_unit
    	,proc_type
    	,proc_time
    	,user_id
    	,client_ip
    	,rec_sts
    	,proc_no
END 
