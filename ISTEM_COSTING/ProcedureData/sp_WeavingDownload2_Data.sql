CREATE PROCEDURE  sp_WeavingDownload2_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    DECLARE @dept INT, @dept_seq INT

    SET @dept_seq = 30
    SELECT @dept = dept FROM istem_costing.dbo.ms_dept WHERE (dept_seq=@dept_seq)

    SELECT
    	 A.proc_name
    	,A.proc_type
    	,NULLIF(SUM(B.mat_qty), 0) AS qty
    FROM istem_costing.dbo.ms_process AS A
    LEFT JOIN istem_costing.dbo.tr_dept_calc AS B
    	ON (
    			B.comp_id=A.comp_id
    		AND B.f_year=@f_year
    		AND B.f_month=@f_month
    		AND B.dept=A.dept
    		AND B.tr_code=A.tr_code
    		AND B.proc_code=A.proc_code
    	)
    WHERE
    		A.comp_id=1
    	AND A.dept=@dept
    	AND A.proc_code NOT IN ('RCVRM', 'WH')

    GROUP BY
    	 A.proc_name
    	,A.proc_type
END
