CREATE PROCEDURE  sp_LogDownMatGRPO_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- DECLARE
    -- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1;
    -- SET @f_year=2020;
    -- SET @f_month=10;
    -- SET @user_id='SYS';
    -- SET @client_ip='192.168.1.1';

    DECLARE @proc_no INT


    DECLARE @FromDate DATE, @ToDate DATE, @rec_sts CHAR(1), @proc_time AS DATETIME
    SET @proc_time=GETDATE()
    SELECT TOP 1 @FromDate = F_RefDate, @ToDate = T_RefDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)

    IF @proc_no >= 1
    BEGIN
      SET @proc_no = @proc_no + 1
      SET @rec_sts = 'T'
    END
    ELSE
    BEGIN
      SET @proc_no = 1
      SET @rec_sts = 'A'
    END

    DECLARE @_TEMP TABLE (
    	comp_id numeric(1, 0) NOT NULL,
    	f_year numeric(4, 0) NOT NULL,
    	f_month numeric(2, 0) NOT NULL,
    	cost_sheet_id varchar(4) NOT NULL,
    	doc_type varchar(4) NOT NULL,
    	doc_date date NOT NULL,
    	gr_rtv_no varchar(30) NOT NULL,
    	pi_gr_no varchar(30) NOT NULL,
    	supplier_name varchar(50) NOT NULL,
    	item_code varchar(12) NOT NULL,
    	item_name varchar(50) NOT NULL,
    	item_grp_cod numeric(4, 0) NULL,
    	frgn_name varchar(50) NULL,
    	unit varchar(5) NULL,
    	curr varchar(5) NULL,
    	qty_rec numeric(14, 2) NULL,
    	price numeric(10, 5) NULL,
    	trans_amount numeric(14, 2) NULL,
    	landed_cost numeric(11, 2) NULL,
    	bale_qty numeric(14, 2) NULL,
    	nett_qty numeric(14, 2) NULL,
    	proc_time datetime NULL,
    	user_id varchar(6) NULL,
    	client_ip varchar(15) NULL,
    	rec_sts char(1) NULL,
    	proc_no tinyint NULL,
        supplier_code varchar(10)
    )

    INSERT INTO @_TEMP
        SELECT
            @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,T0.Cost_ID AS cost_sheet_id
            ,T0.DocType AS doc_type
            ,T0.DocDate AS doc_date
            ,T0.GR_RTV_No AS gr_rtv_no
            ,ISNULL(T0.PI_GR_No, '') AS pi_gr_no
            ,T0.Supplier_Name AS supplier_name
            ,T0.ItemCode AS item_code
            ,LEFT(T0.ItemName, 50) AS item_name
            ,T0.ItmsGrpCod AS item_grp_cod
            ,LEFT(T0.FrgnName, 50) AS frgn_name
            ,T0.Unit AS unit
            ,T0.Curr AS curr
            ,T0.Qty_Rec AS qty_rec
            ,T0.Price AS price
            ,T0.trans_Amount AS trans_amount
            ,T0.Landed_Cost AS landed_cost
            ,T0.Bale_Qty AS bale_qty
            ,T0.Nett_Qty AS nett_qty
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_n
            ,T0.Supplier_Code AS supplier_code
        FROM
        (
            SELECT
                T6.U_Cost_ID AS Cost_ID,
                'GRPO' AS DocType,
                T0.DocDate,
                T0.U_GR_No As GR_RTV_No,
                CASE WHEN T0.U_Loc_Imp='LOC' THEN T5.U_Inv_No ELSE T0.u_inv_no END AS PI_GR_No,
                T0.CardCode AS Supplier_Code,
                T0.CardName AS Supplier_Name,
                T1.ItemCode,
                T2.ItmsGrpCod,
                T1.Dscription AS ItemName,
                T2.FrgnName,
                T2.BuyUnitMsr AS Unit ,
                T0.DocCur AS Curr,
                T1.Quantity AS Qty_Rec,
                T1.Price,
                T1.LineTotal AS trans_Amount ,
                ISNULL(T4.TtlExpndLC,0) AS Landed_Cost,
                ISNULL(T1.U_Bale_Qty,0) AS Bale_Qty,
                CASE WHEN T2.ItmsGrpCod=107 THEN ISNULL(T1.U_Nett_Qty,0) ELSE T1.Quantity END AS Nett_Qty
            FROM SAP_ISM.SBO_ISM_LIVE.DBO.OPDN T0
            INNER JOIN SAP_ISM.SBO_ISM_LIVE.DBO.PDN1 T1 ON T0.DocEntry = T1.DocEntry
            LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM T2 ON T1.ItemCode = T2.ItemCode
            LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.PCH1 T3 ON (T0.DocNum = T3.BaseEntry AND T1.ItemCode=T3.ItemCode)
            LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.IPF1 T4 ON T4.GRPOAbsEnt = T1.DocEntry AND t4.OrigLine=T1.VisOrder
            LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OPCH T5 ON T3.DocEntry=T5.DocEntry
            LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITB T6 ON T2.ItmsGrpCod=T6.ItmsGrpCod

            UNION ALL

            SELECT
                T6.U_Cost_ID AS Cost_ID,
                'RTV' AS DocType,
                T0.DocDate,
                T0.U_RT_No As GR_RTV_No,
                T3.U_GR_No AS PI_GR_No,
                T0.CardCode AS Supplier_Code,
                T0.CardName AS Supplier_Name,
                T1.ItemCode,
                T4.ItmsGrpCod,
                T1.Dscription AS ItemName,
                T4.FrgnName,
                T4.BuyUnitMsr AS Unit,
                T0.DocCur AS Curr,
                T1.Quantity*-1 AS Qty_Rec, T1.Price, T1.LineTotal*-1 AS trans_Amount , 0 AS Landed_Cost,
                ISNULL(T1.U_Bale_Qty,0) AS Bale_Qty,
                CASE WHEN T4.ItmsGrpCod=107 THEN ISNULL(T1.U_Nett_Qty,0) ELSE T1.Quantity END AS Nett_Qty
            FROM SAP_ISM.SBO_ISM_LIVE.DBO.ORPD T0
            LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.rpd1 T1 ON T0.DocEntry=T1.DocEntry
            LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.pdn1 T2 ON (T1.BaseEntry=T2.DocEntry AND T1.ItemCode=T2.ItemCode)
            LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.opdn T3 ON T2.DocEntry=T3.DocEntry
            LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM T4 ON T1.ItemCode = T4.ItemCode
            LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITB T6 ON T4.ItmsGrpCod=T6.ItmsGrpCod
        ) T0
        WHERE
            (T0.DocDate  >= @FromDate AND  T0.DocDate <= @ToDate)
            AND (T0.ItmsGrpCod >= 106 AND T0.ItmsGrpCod <=111)
        ORDER BY T0.ItemCode, T0.DocDate
    SELECT comp_id, f_year, f_month, cost_sheet_id, doc_type, doc_date, gr_rtv_no, pi_gr_no, supplier_name, item_code, item_name, item_grp_cod, frgn_name, unit, curr, qty_rec, price, trans_amount, landed_cost, bale_qty, nett_qty, proc_time, user_id, client_ip, rec_sts, proc_no, supplier_code FROM @_TEMP
END
