CREATE PROCEDURE  sp_AcctTotalCostWeaving_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- Acct - Weaving Total Cost
    -- sp_AcctTotalCostWeaving_Data
    -- Acct - Weaving Total Cost


    -- DECLARE
    -- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept_seq10 INT
    DECLARE @dept10 INT
    DECLARE @dept11 INT
    DECLARE @dept_seq30 INT
    DECLARE @dept30 INT
    DECLARE @tr_code VARCHAR(MAX)
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_time AS DATETIME
    DECLARE @proc_no INT
    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT

    SET @dept_seq10 = 10
    SET @dept10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=@dept_seq10))
    SET @dept11=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=11))
    SET @dept_seq30 = 30
    SET @dept30=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=@dept_seq30))
    SET @tr_code=''
    SET @proc_time = GETDATE()
    SET @rec_sts='A'
    SET @proc_no=1

    IF @f_month = 1
    BEGIN
    	SET @lp_f_month = 12
    	SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
    	SET @lp_f_month = @f_month - 1
    	SET @lp_f_year = @f_year
    END

    DECLARE @_TEMP TABLE(
         item_code     VARCHAR(MAX)
        ,item_desc     VARCHAR(MAX)
        ,product_qty   NUMERIC(16,5)
        ,cr_electric   NUMERIC(16,5)
        ,cr_water      NUMERIC(16,5)
        ,cr_steam      NUMERIC(16,5)
        ,cr_lng        NUMERIC(16,5)
        ,cr_production NUMERIC(16,5)
        ,cr_diestuff   NUMERIC(16,5)
        ,cr_chemical   NUMERIC(16,5)
        ,cr_resin      NUMERIC(16,5)
        ,cr_labor      NUMERIC(16,5)
        ,cr_repair     NUMERIC(16,5)
        ,eq_electric   NUMERIC(16,5)
        ,eq_water      NUMERIC(16,5)
        ,eq_steam      NUMERIC(16,5)
        ,eq_lng        NUMERIC(16,5)
        ,eq_production NUMERIC(16,5)
        ,eq_diestuff   NUMERIC(16,5)
        ,eq_chemical   NUMERIC(16,5)
        ,eq_resin      NUMERIC(16,5)
        ,eq_labor      NUMERIC(16,5)
        ,eq_repair     NUMERIC(16,5)
        ,cd_electric   NUMERIC(16,5)
        ,cd_water      NUMERIC(16,5)
        ,cd_steam      NUMERIC(16,5)
        ,cd_lng        NUMERIC(16,5)
        ,cd_production NUMERIC(16,5)
        ,cd_diestuff   NUMERIC(16,5)
        ,cd_chemical   NUMERIC(16,5)
        ,cd_resin      NUMERIC(16,5)
        ,leno          NUMERIC(16,5)
        ,material_cost NUMERIC(16,5)
        ,pc_fixed      NUMERIC(16,5)
        ,pc_labour     NUMERIC(16,5)
        ,pc_repair     NUMERIC(16,5)
        ,total_cost    NUMERIC(16,5)
        ,unit_cost     NUMERIC(16,5)
    )


    DECLARE @tot_product_qty   NUMERIC(16,5)
    DECLARE @tot_cr_electric   NUMERIC(16,5)
    DECLARE @tot_cr_water      NUMERIC(16,5)
    DECLARE @tot_cr_steam      NUMERIC(16,5)
    DECLARE @tot_cr_lng        NUMERIC(16,5)
    DECLARE @tot_cr_production NUMERIC(16,5)
    DECLARE @tot_cr_diestuff   NUMERIC(16,5)
    DECLARE @tot_cr_chemical   NUMERIC(16,5)
    DECLARE @tot_cr_resin      NUMERIC(16,5)
    DECLARE @tot_cr_labor      NUMERIC(16,5)
    DECLARE @tot_cr_repair     NUMERIC(16,5)
    DECLARE @tot_eq_electric   NUMERIC(16,5)
    DECLARE @tot_eq_water      NUMERIC(16,5)
    DECLARE @tot_eq_steam      NUMERIC(16,5)
    DECLARE @tot_eq_lng        NUMERIC(16,5)
    DECLARE @tot_eq_production NUMERIC(16,5)
    DECLARE @tot_eq_diestuff   NUMERIC(16,5)
    DECLARE @tot_eq_chemical   NUMERIC(16,5)
    DECLARE @tot_eq_resin      NUMERIC(16,5)
    DECLARE @tot_eq_labor      NUMERIC(16,5)
    DECLARE @tot_eq_repair     NUMERIC(16,5)
    DECLARE @tot_cd_electric   NUMERIC(16,5)
    DECLARE @tot_cd_water      NUMERIC(16,5)
    DECLARE @tot_cd_steam      NUMERIC(16,5)
    DECLARE @tot_cd_lng        NUMERIC(16,5)
    DECLARE @tot_cd_production NUMERIC(16,5)
    DECLARE @tot_cd_diestuff   NUMERIC(16,5)
    DECLARE @tot_cd_chemical   NUMERIC(16,5)
    DECLARE @tot_cd_resin      NUMERIC(16,5)
    DECLARE @tot_leno          NUMERIC(16,5)
    DECLARE @tot_material_cost NUMERIC(16,5)
    DECLARE @tot_pc_fixed      NUMERIC(16,5)
    DECLARE @tot_pc_labour     NUMERIC(16,5)
    DECLARE @tot_pc_repair     NUMERIC(16,5)
    DECLARE @tot_total_cost    NUMERIC(16,5)
    DECLARE @tot_unit_cost     NUMERIC(16,5)


    -- Insert Item
    PRINT 'Proc : 1';

    INSERT INTO @_TEMP
    (
        item_code
        ,item_desc
        ,product_qty
    )
    SELECT
         A.item_code
        ,A.item_code
        ,A.prod_qty
    FROM istem_costing.dbo.tr_prod AS A
    WHERE
        A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept=@dept30



    DECLARE @_TEMP_CONVERT_ITEM TABLE(
        item_code VARCHAR(50)
        ,convert_type_code VARCHAR(12)
        ,convert_ratio DECIMAL(16,5)
    )

    PRINT 'Proc : 2';
    INSERT INTO @_TEMP_CONVERT_ITEM
        SELECT
            item_code
            ,convert_type_code
            ,convert_ratio
        FROM istem_costing.dbo.tr_convert_item
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept30


    PRINT 'Proc : 3';
    UPDATE T SET
        T.cr_electric       = electric.convert_ratio
        ,T.cr_water         = water.convert_ratio
        ,T.cr_steam         = steam.convert_ratio
        ,T.cr_production    = production.convert_ratio
        ,T.cr_chemical      = chem.convert_ratio
        ,T.cr_labor         = labor.convert_ratio
        ,T.cr_repair        = repair.convert_ratio
        ,T.eq_electric      = NULLIF(T.product_qty, 0) * NULLIF(electric.convert_ratio, 0)
        ,T.eq_water         = NULLIF(T.product_qty, 0) * NULLIF(water.convert_ratio, 0)
        ,T.eq_steam         = NULLIF(T.product_qty, 0) * NULLIF(steam.convert_ratio, 0)
        ,T.eq_production    = NULLIF(T.product_qty, 0) * NULLIF(production.convert_ratio, 0)
        ,T.eq_chemical      = NULLIF(T.product_qty, 0) * NULLIF(chem.convert_ratio, 0)
        ,T.eq_labor         = NULLIF(T.product_qty, 0) * NULLIF(labor.convert_ratio, 0)
        ,T.eq_repair        = NULLIF(T.product_qty, 0) * NULLIF(repair.convert_ratio, 0)
    FROM @_TEMP AS T
    LEFT JOIN @_TEMP_CONVERT_ITEM AS electric    ON (electric.item_code=T.item_code AND electric.convert_type_code='R01')
    LEFT JOIN @_TEMP_CONVERT_ITEM AS water       ON (water.item_code=T.item_code AND water.convert_type_code='R02')
    LEFT JOIN @_TEMP_CONVERT_ITEM AS steam       ON (steam.item_code=T.item_code AND steam.convert_type_code='R03')
    LEFT JOIN @_TEMP_CONVERT_ITEM AS production  ON (production.item_code=T.item_code AND production.convert_type_code='R07')
    LEFT JOIN @_TEMP_CONVERT_ITEM AS chem        ON (chem.item_code=T.item_code AND chem.convert_type_code='R09')
    LEFT JOIN @_TEMP_CONVERT_ITEM AS labor       ON (labor.item_code=T.item_code AND labor.convert_type_code='R10')
    LEFT JOIN @_TEMP_CONVERT_ITEM AS repair      ON (repair.item_code=T.item_code AND repair.convert_type_code='R11')

    PRINT 'Proc : 4';
    SELECT
        @tot_product_qty = ISNULL(SUM(ISNULL(product_qty, 0)), 0)
        ,@tot_eq_electric = ISNULL(SUM(ISNULL(eq_electric, 0)), 0)
        ,@tot_eq_water = ISNULL(SUM(ISNULL(eq_water, 0)), 0)
        ,@tot_eq_production = ISNULL(SUM(ISNULL(eq_production, 0)), 0)
        ,@tot_eq_steam = ISNULL(SUM(ISNULL(eq_steam, 0)), 0)
        ,@tot_eq_chemical = ISNULL(SUM(ISNULL(eq_chemical, 0)), 0)
        ,@tot_eq_labor = ISNULL(SUM(ISNULL(eq_labor, 0)), 0)
        ,@tot_eq_repair = ISNULL(SUM(ISNULL(eq_repair, 0)), 0)
    FROM @_TEMP


    PRINT 'Proc : 5';
    SELECT @tot_cd_electric = ISNULL(SUM(cons_wip_cost_amount), 0) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=1 AND f_year=@f_year AND f_month=@f_month AND dept=@dept30 AND cost_sheet_id='C02'
    SELECT @tot_cd_water = ISNULL(SUM(cons_wip_cost_amount), 0) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=1 AND f_year=@f_year AND f_month=@f_month AND dept=@dept30 AND cost_sheet_id='C03'
    SELECT @tot_cd_steam = ISNULL(SUM(cons_wip_cost_amount), 0) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=1 AND f_year=@f_year AND f_month=@f_month AND dept=@dept30 AND cost_sheet_id='C04'
    SELECT @tot_cd_chemical = ISNULL(SUM(cons_wip_cost_amount), 0) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=1 AND f_year=@f_year AND f_month=@f_month AND dept=@dept30 AND cost_sheet_id='B31'


    PRINT 'Proc : 6';
    UPDATE T SET
        T.cd_electric = NULLIF(@tot_cd_electric, 0) / NULLIF(@tot_eq_electric, 0) * NULLIF(T.eq_electric, 0)
        ,T.cd_water = NULLIF(@tot_cd_water, 0) / NULLIF(@tot_eq_water, 0) * NULLIF(T.eq_water, 0)
        ,T.cd_steam = NULLIF(@tot_cd_steam, 0) / NULLIF(@tot_eq_steam, 0) * NULLIF(T.eq_steam, 0)
        ,T.cd_chemical = NULLIF(@tot_cd_chemical, 0) / NULLIF(@tot_eq_chemical, 0) * NULLIF(T.eq_chemical, 0)
    FROM @_TEMP AS T


    PRINT 'Proc : 7';
    UPDATE T SET
        T.material_cost = S1.material_cost
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            item_code,
            material_cost
        FROM istem_costing.dbo.tr_prod
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept30
    ) AS S1 ON (S1.item_code=T.item_code)

    SELECT @tot_material_cost=SUM(ISNULL(material_cost, 0)) FROM @_TEMP


    PRINT 'Proc : 8';
    SET @tot_leno = (SELECT SUM(A.cons_mat_amount)
        FROM istem_costing.dbo.tr_wip AS A
        INNER JOIN istem_sms.dbo.sp_ms_yarn AS B ON (B.yarn_code=A.mat_code)
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept=@dept30
            AND ISNULL(A.item_category, '') = ''
            AND ISNULL(A.item_code, '') = ''
            AND B.yarn_kind = 'L')

    UPDATE T SET
        T.leno = NULLIF(@tot_leno, 0) / NULLIF(@tot_product_qty, 0) * NULLIF(T.product_qty, 0)
    FROM @_TEMP AS T

    -- Total Fixed Cost per Dept
    PRINT 'Proc : 9';
    SELECT
        @tot_pc_labour=SUM(A.manex_amount)
    FROM istem_costing.dbo.tr_manex AS A
    LEFT JOIN ms_cost_group AS B ON (B.comp_id=A.comp_id AND B.cost_sheet_id=A.cost_sheet_id)
    WHERE
        A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept=@dept30
        AND A.tr_code='FCD'
        AND B.fix_cost_group='R10'

    -- Total Fixed Cost per Dept
    PRINT 'Proc : 10';
    SELECT
        @tot_pc_repair=SUM(A.manex_amount)
    FROM istem_costing.dbo.tr_manex AS A
    LEFT JOIN ms_cost_group AS B ON (B.comp_id=A.comp_id AND B.cost_sheet_id=A.cost_sheet_id)
    WHERE
        A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept=@dept30
        AND A.tr_code IN ('FCD', 'AUFC', 'ACGA', 'ACEN')
        AND B.fix_cost_group='R11'

    -- SELECT
    --     @tot_pc_labour=SUM(A.manex_amount)
    -- FROM istem_costing.dbo.tr_manex AS A
    -- LEFT JOIN ms_cost_group AS B ON (B.comp_id=A.comp_id AND B.cost_sheet_id=A.cost_sheet_id)
    -- WHERE
    --     A.comp_id=@comp_id
    --     AND A.f_year=@f_year
    --     AND A.f_month=@f_month
    --     AND A.dept=@dept30
    --     AND A.tr_code IN ('FCD', 'AUFC', 'ACGA', 'ACEN')
    --     AND B.fix_cost_group='R10'



    -- Total Fixed Cost per Dept
    PRINT 'Proc : 11';
    SELECT
        @tot_pc_fixed=ISNULL(SUM(A.manex_amount) , 0) - (ISNULL(@tot_pc_repair, 0) - ISNULL(@tot_pc_labour, 0))
    FROM istem_costing.dbo.tr_manex AS A
    WHERE
        A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept=@dept30
        AND A.tr_code IN ('FCD', 'AUFC', 'ACGA', 'ACEN')

    PRINT 'Proc : 12';
    UPDATE T SET
        T.pc_fixed      = NULLIF(@tot_pc_fixed, 0)/NULLIF(@tot_eq_production, 0) * T.eq_production
        ,T.pc_labour    = NULLIF(@tot_pc_labour, 0)/NULLIF(@tot_eq_labor, 0) * T.eq_labor
        ,T.pc_repair    = NULLIF(@tot_pc_repair, 0)/NULLIF(@tot_eq_repair, 0) * T.eq_repair
    FROM @_TEMP AS T

    PRINT 'Proc : 13';
    UPDATE T SET
        T.total_cost = NULLIF(
                ISNULL(T.cd_electric, 0) +
                ISNULL(T.cd_water, 0) +
                ISNULL(T.cd_steam, 0) +
                ISNULL(T.cd_lng, 0) +
                ISNULL(T.cd_chemical, 0) +
                ISNULL(T.cd_diestuff, 0) +
                ISNULL(T.cd_resin, 0) +
                ISNULL(T.leno, 0) +
                ISNULL(T.material_cost, 0) +
                ISNULL(T.pc_fixed, 0) +
                ISNULL(T.pc_labour, 0) +
                ISNULL(T.pc_repair, 0)
            ,0)
    FROM @_TEMP AS T


    PRINT 'Proc : 14';
    UPDATE T SET
        T.unit_cost = NULLIF(T.total_cost/T.product_qty, 0)
    FROM @_TEMP AS T

    SELECT * FROM @_TEMP
END
