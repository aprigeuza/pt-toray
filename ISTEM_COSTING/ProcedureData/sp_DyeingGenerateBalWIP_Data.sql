CREATE PROCEDURE  sp_DyeingGenerateBalWIP_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- DECLARE
    -- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    -- sp_DyeingGenerateBalWIP_Data
    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT
    DECLARE @rec_sts VARCHAR(1)
    DECLARE @proc_no INT
    DECLARE @dept INT

    SET @dept = (SELECT dept FROM istem_costing.dbo.ms_dept WHERE comp_id=@comp_id AND dept_seq=40)

    SELECT
         @comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,A.dept AS dept
        ,A.cost_sheet_id AS cost_sheet_id
        ,A.mat_code AS mat_code
        ,A.item_category AS item_category
        ,A.item_code AS item_code
        ,A.qty_unit AS qty_unit
        ,A.cf_wip_qty AS cf_wip_qty
        ,A.cf_wip_amount AS cf_wip_amount
        ,GETDATE() AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
        ,A.r_flag AS r_flag
    FROM istem_costing.dbo.tr_wip AS A
    WHERE
            A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept=@dept
        AND (
            ISNULL(A.cf_wip_qty, 0) <> 0
        )
END
