CREATE PROCEDURE  sp_AcctBalanceCostWeaving_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN

    -- sp_AcctBalanceCostWeaving
    -- Acct - Weaving Balance Cost

    -- DECLARE
    -- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept INT

    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT

    SET @dept=800


    IF @f_month = 1
    BEGIN
        SET @lp_f_month = 12
        SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
        SET @lp_f_month = @f_month - 1
        SET @lp_f_year = @f_year
    END


    DECLARE @_TEMP TABLE(
         res VARCHAR(MAX)
        ,mat_code VARCHAR(MAX)
        ,mat_name VARCHAR(MAX)
        ,product_item VARCHAR(MAX)
        ,product_item2 VARCHAR(MAX)
        ,cost_sheet_id VARCHAR(MAX)
        ,bf_qty NUMERIC(14,5)
        ,bf_price NUMERIC(14,5)
        ,bf_amount NUMERIC(14,5)
        ,rc_qty NUMERIC(14,5)
        ,rc_price NUMERIC(14,5)
        ,rc_amount NUMERIC(14,5)
        ,co_qty NUMERIC(14,5)
        ,co_price NUMERIC(14,5)
        ,co_amount NUMERIC(14,5)
        ,cf_qty NUMERIC(14,5)
        ,cf_price NUMERIC(14,5)
        ,cf_amount NUMERIC(14,5)
        ,so VARCHAR(MAX) DEFAULT NULL
    )

    -- Create Material/Item
    INSERT INTO @_TEMP (res, mat_code, mat_name, product_item, product_item2, cost_sheet_id, so)
    SELECT
        'A1' AS res
        ,CASE
            WHEN cost_sheet_id = 'A99' THEN 'Material Total'
            WHEN cost_sheet_id = 'C02' THEN 'Utility Prop Cost'
            WHEN cost_sheet_id = 'C03' THEN 'Utility Prop Cost'
            WHEN cost_sheet_id = 'C04' THEN 'Utility Prop Cost'
            WHEN cost_sheet_id = 'B31' THEN 'Sub Material'
            WHEN cost_sheet_id = 'H99' THEN 'Processing Cost'
            ELSE ''
        END AS mat_code
        ,CASE
            WHEN cost_sheet_id = 'A99' THEN 'Material Total'
            WHEN cost_sheet_id = 'C02' THEN 'Utility Prop Cost'
            WHEN cost_sheet_id = 'C03' THEN 'Utility Prop Cost'
            WHEN cost_sheet_id = 'C04' THEN 'Utility Prop Cost'
            WHEN cost_sheet_id = 'B31' THEN 'Sub Material'
            WHEN cost_sheet_id = 'H99' THEN 'Processing Cost'
            ELSE ''
        END AS mat_name
        ,NULL AS product_item
        ,NULL AS product_item2
        ,cost_sheet_id AS cost_sheet_id
        ,CASE
            WHEN cost_sheet_id = 'A99' THEN 1
            WHEN cost_sheet_id = 'C02' THEN 2
            WHEN cost_sheet_id = 'C03' THEN 3
            WHEN cost_sheet_id = 'C04' THEN 4
            WHEN cost_sheet_id = 'B31' THEN 5
            WHEN cost_sheet_id = 'H99' THEN 6
            ELSE ''
        END AS so
    FROM istem_costing.dbo.tr_bal_wip_cost
    WHERE
        comp_id=@comp_id
        AND f_year=@lp_f_year
        AND f_month=@lp_f_month
        AND dept=@dept
    INSERT INTO @_TEMP (res, mat_code, mat_name, product_item, product_item2, cost_sheet_id, so)
    VALUES ('A1', 'Sub Material', 'Sub Material', NULL, NULL, 'B31', 5)


    -- BF
    -- Total Material, Utility Prop Cost, Processing Cost
    UPDATE T SET
        T.bf_qty = S.cf_wip_cost_qty
        ,T.bf_price = NULLIF(S.cf_wip_cost_amount, 0)/NULLIF(S.cf_wip_cost_qty, 0)
        ,T.bf_amount = S.cf_wip_cost_amount
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            cf_wip_cost_qty
            ,cf_wip_cost_amount
            ,cost_sheet_id AS cost_sheet_id
        FROM istem_costing.dbo.tr_bal_wip_cost
        WHERE
            comp_id=@comp_id
            AND f_year=@lp_f_year
            AND f_month=@lp_f_month
            AND dept=@dept
    ) AS S ON (S.cost_sheet_id=T.cost_sheet_id)
    WHERE
        T.res='A1'


    -- Total Material ( A99 )
    -- RC, CO, CF
    UPDATE T SET
        T.rc_qty = S.sum_rcv_mat_qty
        ,T.rc_price = NULLIF(S.sum_rcv_mat_amount, 0)/NULLIF(S.sum_rcv_mat_qty, 0)
        ,T.rc_amount = S.sum_rcv_mat_amount
        ,T.co_qty = S.sum_cons_mat_qty
        ,T.co_price = NULLIF(S.sum_cons_mat_amount, 0)/NULLIF(S.sum_cons_mat_qty, 0)
        ,T.co_amount = S.sum_cons_mat_amount
        ,T.cf_qty = S.sum_cf_wip_qty
        ,T.cf_price = NULLIF(S.sum_cf_wip_amount, 0)/NULLIF(S.sum_cf_wip_qty, 0)
        ,T.cf_amount = S.sum_cf_wip_amount
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            'A99' AS cost_sheet_id
            ,SUM(A.rcv_mat_qty) AS sum_rcv_mat_qty
            ,SUM(A.rcv_mat_amount) AS sum_rcv_mat_amount
            ,SUM(A.cons_mat_qty) AS sum_cons_mat_qty
            ,SUM(A.cons_mat_amount) AS sum_cons_mat_amount
            ,SUM(A.cf_wip_qty) AS sum_cf_wip_qty
            ,SUM(A.cf_wip_amount) AS sum_cf_wip_amount
        FROM istem_costing.dbo.tr_wip AS A
        LEFT OUTER JOIN istem_sms.dbo.sp_ms_yarn AS B ON (B.yarn_code=A.mat_code)
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept=@dept
            AND ISNULL(A.item_category, '') = ''
            AND ISNULL(A.item_code, '') = ''
            AND B.yarn_kind <> 'L'
    ) AS S ON (S.cost_sheet_id=T.cost_sheet_id)
    WHERE
        T.res='A1'
        AND T.mat_code='Material Total'
        AND T.mat_name='Material Total'
        AND T.cost_sheet_id='A99'


    -- Utility Prop Cost ( C02, C03, C04 )
    -- RC, CO, CF
    -- rc_amount
    UPDATE T SET
        T.rc_amount = S.rc_amount
    FROM @_TEMP AS T
    INNER JOIN (
        SELECT
            cost_sheet_id
            ,SUM(manex_amount) AS rc_amount
        FROM istem_costing.dbo.tr_manex
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND cost_sheet_id LIKE 'C%'
            AND cost_sheet_id <> 'C01'
            AND tr_code='AUPC'
      GROUP BY cost_sheet_id
    ) AS S ON (S.cost_sheet_id=T.cost_sheet_id)

    -- Utility Prop Cost ( C02, C03, C04 )
    -- co_qty
    UPDATE T SET
        T.co_qty = S.co_qty
    FROM @_TEMP AS T
    INNER JOIN (
        SELECT
            cost_sheet_id
            ,SUM(tot_prod_equiv_qty) AS co_qty
        FROM istem_costing.dbo.tr_prod_detail1
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND cost_sheet_id LIKE 'C%'
            AND cost_sheet_id <> 'C01'
        GROUP BY cost_sheet_id
    ) AS S ON (S.cost_sheet_id=T.cost_sheet_id)

    -- Utility Prop Cost ( C02, C03, C04 )
    -- cf_qty
    UPDATE T SET
        T.cf_qty = S.cf_qty
    FROM @_TEMP AS T
    INNER JOIN (
        SELECT
            cost_sheet_id
            ,SUM(tot_wip_equiv_qty) AS cf_qty
        FROM istem_costing.dbo.tr_wip_eqv_header AS A
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND cost_sheet_id LIKE 'C%'
            AND cost_sheet_id <> 'C01'
        GROUP BY cost_sheet_id
    ) AS S ON (S.cost_sheet_id=T.cost_sheet_id)

    -- Utility Prop Cost ( C02, C03, C04 )
    UPDATE T SET
        T.rc_qty = ISNULL(T.cf_qty, 0)+ISNULL(T.co_qty, 0)-ISNULL(T.bf_qty, 0)
    FROM @_TEMP AS T
    WHERE
        cost_sheet_id LIKE 'C%'

    -- Utility Prop Cost ( C02, C03, C04 )
    UPDATE T SET
        T.rc_price = NULLIF(T.rc_amount, 0)/NULLIF(T.rc_qty, 0)
        ,T.co_price = NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0)
        ,T.co_amount = NULLIF(T.co_qty, 0) * NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0)
        ,T.cf_price = NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0)
        ,T.cf_amount = NULLIF(T.cf_qty, 0) * NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0)
    FROM @_TEMP AS T
    WHERE
        cost_sheet_id LIKE 'C%'

    -- Processing Cost ( H99 )
    -- RC, CO, CF
    DECLARE @proc_co_rc_qty AS DECIMAL (16,5)
    DECLARE @proc_co_rc_price AS DECIMAL (16,5)
    DECLARE @proc_co_rc_amount AS DECIMAL (16,5)
    DECLARE @proc_co_co_qty AS DECIMAL (16,5)
    DECLARE @proc_co_co_price AS DECIMAL (16,5)
    DECLARE @proc_co_co_amount AS DECIMAL (16,5)
    DECLARE @proc_co_cf_qty AS DECIMAL (16,5)
    DECLARE @proc_co_cf_price AS DECIMAL (16,5)
    DECLARE @proc_co_cf_amount AS DECIMAL (16,5)

    -- rc_amount
    SELECT
        @proc_co_rc_amount = SUM(manex_amount)
    FROM istem_costing.dbo.tr_manex
    WHERE
        comp_id=@comp_id
        AND f_year=@f_year
        AND f_month=@f_month
        AND dept=@dept
        AND tr_code IN ('AUFC', 'FCD', 'ACGA', 'ACEN')

    -- co_qty
    SELECT
        @proc_co_co_qty = SUM(tot_prod_equiv_qty)
    FROM istem_costing.dbo.tr_prod_detail1
    WHERE
        comp_id=@comp_id
        AND f_year=@f_year
        AND f_month=@f_month
        AND dept=@dept
        AND cost_sheet_id LIKE 'N10'

    -- cf_qty
    SELECT
        @proc_co_cf_qty = SUM(tot_wip_equiv_qty)
    FROM istem_costing.dbo.tr_wip_eqv_header AS A
    WHERE
        comp_id=@comp_id
        AND f_year=@f_year
        AND f_month=@f_month
        AND dept=@dept
        AND (cost_sheet_id LIKE 'F%' or cost_sheet_id LIKE 'G%' or cost_sheet_id LIKE 'H%')

    UPDATE T SET
        T.rc_qty = (ISNULL(@proc_co_cf_qty, 0)+ISNULL(@proc_co_co_qty, 0))-ISNULL(T.bf_qty, 0)
        ,T.rc_amount = @proc_co_rc_amount
        ,T.co_qty = @proc_co_co_qty
        ,T.co_amount = @proc_co_co_amount
        ,T.cf_qty =  @proc_co_cf_qty
        ,T.cf_amount = @proc_co_cf_amount
    FROM @_TEMP AS T
    WHERE
        T.res='A1'
        AND T.mat_code='Processing Cost'
        AND T.mat_name='Processing Cost'
        AND T.cost_sheet_id='H99'

    UPDATE T SET
        T.rc_price = NULLIF(T.rc_amount, 0)/NULLIF(T.rc_qty, 0)
        ,T.co_price = NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0)
        ,T.co_amount = NULLIF(T.co_qty, 0) * NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0)
        ,T.cf_price = NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0)
        ,T.cf_amount = NULLIF(T.cf_qty, 0) * NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0)
    FROM @_TEMP AS T
    WHERE
        T.res='A1'
        AND T.mat_code='Processing Cost'
        AND T.mat_name='Processing Cost'
        AND T.cost_sheet_id='H99'

    -- Sub Material ( B31 )
    -- bf_qty
    UPDATE T SET
        T.bf_qty=S.bf_qty
        ,T.bf_amount=S.bf_amount
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            'B31' AS  cost_sheet_id,
            SUM(cf_wip_cost_qty) AS bf_qty,
            SUM(cf_wip_cost_amount) AS bf_amount
        FROM istem_costing.dbo.tr_bal_wip_cost
        WHERE
            comp_id=@comp_id
            AND f_year=@lp_f_year
            AND f_month=@lp_f_month
            AND dept=@dept
            AND cost_sheet_id LIKE 'B%' OR cost_sheet_id LIKE 'C01' OR cost_sheet_id LIKE 'D%'
    ) AS S ON (S.cost_sheet_id=T.cost_sheet_id)
    WHERE
        T.res='A1'
        AND T.mat_code='Sub Material'
        AND T.mat_name='Sub Material'
        AND T.cost_sheet_id='B31'

    -- Sub Material ( B31 )
    -- rc_amount
    UPDATE T SET
        T.rc_amount=S.rc_amount
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            'B31' AS cost_sheet_id,
            SUM(manex_amount) AS rc_amount
        FROM istem_costing.dbo.tr_manex
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND tr_code IN ('PCD')
    ) AS S ON (S.cost_sheet_id=T.cost_sheet_id)
    WHERE
        T.res='A1'
        AND T.mat_code='Sub Material'
        AND T.mat_name='Sub Material'
        AND T.cost_sheet_id='B31'

    -- Sub Material ( B31 )
    -- co_qty
    UPDATE T SET
        T.co_qty=S.co_qty
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            'B31' AS cost_sheet_id,
            SUM(tot_prod_equiv_qty) AS co_qty
        FROM istem_costing.dbo.tr_prod_detail1
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND cost_sheet_id LIKE 'B%' OR cost_sheet_id LIKE 'C01' OR cost_sheet_id LIKE 'D%'
    ) AS S ON (S.cost_sheet_id=T.cost_sheet_id)
    WHERE
        T.res='A1'
        AND T.mat_code='Sub Material'
        AND T.mat_name='Sub Material'
        AND T.cost_sheet_id='B31'

    -- Sub Material ( B31 )
    -- cf_qty
    UPDATE T SET
        T.cf_qty=S.cf_qty
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            'B31' AS cost_sheet_id,
            SUM(tot_wip_equiv_qty) AS cf_qty
        FROM istem_costing.dbo.tr_wip_eqv_header
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept
            AND cost_sheet_id='B31'
    ) AS S ON (S.cost_sheet_id=T.cost_sheet_id)
    WHERE
        T.res='A1'
        AND T.mat_code='Sub Material'
        AND T.mat_name='Sub Material'
        AND T.cost_sheet_id='B31'

    UPDATE T SET
        T.rc_qty = ISNULL(T.cf_qty, 0)+ISNULL(T.co_qty, 0)-ISNULL(T.bf_qty, 0)
    FROM @_TEMP AS T
    WHERE
        T.res='A1'
        AND T.mat_code='Sub Material'
        AND T.mat_name='Sub Material'
        AND T.cost_sheet_id='B31'

    UPDATE T SET
        T.rc_price = NULLIF(T.rc_amount, 0)/NULLIF(T.rc_qty, 0)
        ,T.co_price = NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0)
        ,T.co_amount = NULLIF(T.co_qty, 0) * NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0)
        ,T.cf_price = NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0)
        ,T.cf_amount = NULLIF(T.cf_qty, 0) * NULLIF((ISNULL(T.bf_amount, 0)+ISNULL(T.rc_amount, 0)), 0)/NULLIF((ISNULL(T.bf_qty, 0)+ISNULL(T.rc_qty, 0)), 0)
    FROM @_TEMP AS T
    WHERE
        T.res='A1'
        AND T.mat_code='Sub Material'
        AND T.mat_name='Sub Material'
        AND T.cost_sheet_id='B31'
    -- =========================================================================
    SELECT
         @comp_id AS comp_id
        ,@f_year AS f_year
        ,@f_month AS f_month
        ,@dept AS dept
        ,*
    FROM @_TEMP ORDER BY so;

END
