CREATE PROCEDURE  sp_AcctDownDCA_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- ACCOUNTING DCA

    -- sp_AcctDownDCA_Data
    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @user_id='SYS'
    -- SET @client_ip='0.0.0.0'

    DECLARE @dept_seq INT
    DECLARE @dept INT
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_time AS DATETIME
    DECLARE @proc_no INT

    SET @dept_seq = 20
    SET @dept=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=@dept_seq))
    SET @proc_time = GETDATE()
    SET @rec_sts='A'
    SET @proc_no=1

    DECLARE @c INT

    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT
    IF @f_month = 1
    BEGIN
    	SET @lp_f_month = 12
    	SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
    	SET @lp_f_month = @f_month - 1
    	SET @lp_f_year = @f_year
    END


    -- PERIOD
    DECLARE @from_date DATE, @to_date DATE
    SELECT TOP 1 @from_date = F_RefDate, @to_date = T_RefDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)

    DECLARE @_GRPO TABLE (
    	 dept INT
    	,cost_sheet_id VARCHAR(25)
    	,unit VARCHAR(25)
    	,qty_rec DECIMAL(14, 5)
    	,tr_amount DECIMAL(14, 5)
    )

    INSERT INTO @_GRPO
        (
    		 dept
    		,cost_sheet_id
    		,unit
    		,qty_rec
    		,tr_amount
    	)
        SELECT
            A.Dept AS dept
           ,A.Cost_ID AS cost_sheet_id
           ,A.Unit AS unit
           ,A.qtry_rec AS qty_rec
           ,A.tr_amount AS tr_amount
        FROM (
            SELECT
                A.Dept
                ,A.Cost_ID
                ,A.Unit
                ,SUM(A.qty_rec) AS qtry_rec
                ,SUM(A.trans_amount)+SUM(A.landed_cost) AS tr_amount
            FROM
            (
                SELECT
                    T0.Dept
                    ,T0.Cost_ID
                    ,T0.DocType
                    ,T0.DocDate
                    ,T0.GR_RTV_No
                    ,T0.PI_GR_No
                    ,T0.Supplier_Name
                    ,T0.ItemCode
                    ,T0.ItmsGrpCod
                    ,T0.ItemName
                    ,T0.FrgnName
                    ,T0.Unit
                    ,T0.Curr
                    ,T0.Qty_Rec
                    ,T0.Price
                    ,T0.trans_Amount
                    ,T0.Landed_Cost
                    ,T0.Bale_Qty
                    ,T0.Nett_Qty
                FROM (
                    SELECT
                        T0.u_department AS Dept
                        ,T6.U_Cost_ID AS Cost_ID
                        ,'GRPO' AS DocType
                        ,T0.DocDate
                        ,T0.U_GR_No As GR_RTV_No
                        ,CASE
                            WHEN T0.U_Loc_Imp='LOC' THEN T5.U_Inv_No
                            ELSE T0.u_inv_no
                        END AS PI_GR_No
                        ,T0.CardName AS Supplier_Name
                        ,T1.ItemCode
                        ,T2.ItmsGrpCod
                        ,T1.Dscription AS ItemName
                        ,T2.FrgnName
                        ,T2.BuyUnitMsr AS Unit
                        ,T0.DocCur AS Curr
                        ,T1.Quantity AS Qty_Rec
                        ,T1.Price
                        ,CASE WHEN T5.Comments like '%FREE%' THEN 0 ELSE T1.LineTotal END AS trans_Amount
                        ,CASE WHEN T5.Comments like '%FREE%' THEN 0 ELSE ISNULL(T4.TtlExpndLC, 0) END AS Landed_Cost
                        ,ISNULL(T1.U_Bale_Qty, 0) AS Bale_Qty
                        ,CASE WHEN T2.ItmsGrpCod=107 THEN ISNULL(T1.U_Nett_Qty, 0) ELSE T1.Quantity END AS Nett_Qty
                    FROM SAP_ISM.SBO_ISM_LIVE.DBO.OPDN T0
                    INNER JOIN SAP_ISM.SBO_ISM_LIVE.DBO.PDN1 T1 ON T0.DocEntry=T1.DocEntry
                    LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM T2 ON T1.ItemCode=T2.ItemCode
                    LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.PCH1 T3 ON T0.DocNum=T3.BaseEntry AND T3.ItemCode=T1.ItemCode
                    LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.IPF1 T4 ON  T4.GRPOAbsEnt=T1.DocEntry AND t4.OrigLine=T1.VisOrder AND T4.ItemCode=T1.ItemCode
                    LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OPCH T5 ON T3.DocEntry=T5.DocEntry
                    LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITB T6 ON T2.ItmsGrpCod=T6.ItmsGrpCod
                     --LEFT JOIN OIPF S5 ON T4.DocEntry=T5.DocEntry

                    UNION ALL

                    SELECT
                        T0.u_department AS Dept
                        ,T6.U_Cost_ID AS Cost_ID
                        ,'RTV' AS DocType
                        ,T0.DocDate
                        ,T0.U_RT_No As GR_RTV_No
                        ,T3.U_GR_No AS PI_GR_No
                        ,T0.CardName AS Supplier_Name
                        ,T1.ItemCode
                        ,T4.ItmsGrpCod
                        ,T1.Dscription AS ItemName
                        ,T4.FrgnName
                        ,T4.BuyUnitMsr AS Unit
                        ,T0.DocCur AS Curr
                        ,T1.Quantity*-1 AS Qty_Rec
                        ,T1.Price
                        ,T1.LineTotal*-1 AS trans_Amount
                        ,0 AS Landed_Cost
                        ,ISNULL(T1.U_Bale_Qty, 0) AS Bale_Qty
                        ,CASE WHEN T4.ItmsGrpCod=107 THEN ISNULL(T1.U_Nett_Qty, 0) ELSE T1.Quantity END AS Nett_Qty
                    FROM SAP_ISM.SBO_ISM_LIVE.DBO.ORPD T0
                    LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.RPD1 T1 on T0.DocEntry=T1.DocEntry
                    LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.PDN1 T2 on T1.BaseEntry=T2.DocEntry
                    LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OPDN T3 on T2.DocEntry=T3.DocEntry
                    LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM T4 ON T1.ItemCode=T4.ItemCode
                     --LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OIPF S5 ON T4.DocEntry=T5.DocEntry
                    LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITB T6 ON T4.ItmsGrpCod=T6.ItmsGrpCod

                    UNION ALL

                    SELECT
                        T0.u_department AS Dept
                        ,T6.U_Cost_ID AS Cost_ID
                        ,'GR' AS DocType
                        ,T0.DocDate
                        ,T0.U_RT_No As GR_RTV_No
                        ,T0.U_GD_No AS PI_GR_No
                        ,T0.CardName AS Supplier_Name
                        ,T1.ItemCode
                        ,T4.ItmsGrpCod
                        ,T1.Dscription AS ItemName
                        ,T4.FrgnName
                        ,T4.BuyUnitMsr AS Unit
                        ,T0.DocCur AS Curr
                        ,T1.Quantity AS Qty_Rec
                        ,T1.Price
                        ,T1.LineTotal AS trans_Amount
                        ,0 AS Landed_Cost
                        ,ISNULL(T1.U_Bale_Qty, 0) AS Bale_Qty
                        ,CASE WHEN T4.ItmsGrpCod=107 THEN ISNULL(T1.U_Nett_Qty, 0) ELSE T1.Quantity END AS Nett_Qty
                    FROM SAP_ISM.SBO_ISM_LIVE.DBO.OIGN T0
                    LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.IGN1 T1 on T0.DocEntry=T1.DocEntry
                    LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM T4 ON T1.ItemCode=T4.ItemCode
                    LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITB T6 ON T4.ItmsGrpCod=t6.ItmsGrpCod
                    --WHERE T0.DOCDATE BETWEEN '2020-10-01' AND '2020-10-31'
                    --AND T4.ITMSGRPCOD=120
                ) AS T0
    			WHERE
    			   (T0.DocDate  >= @from_date AND  T0.DocDate <= @to_date)
                    --T0.DocDate  >= %0 AND  T0.DocDate <= %1
                    AND
                    --(T0.ItmsGrpCod >= %3 AND T0.ItmsGrpCod <=%4)
                    (T0.ItmsGrpCod >= 113 AND T0.ItmsGrpCod <=125)
                    --ORDER BY T0.ItemCode, T0.DocDate
            ) A
            GROUP BY
                A.Dept
                ,A.cost_id
                ,A.Unit
        ) AS A
    	WHERE
    		(A.Unit='KG')

    -- TR_DCA
    DECLARE @_TR_DCA TABLE (
    	[comp_id] [numeric](1, 0) NOT NULL,
    	[f_year] [numeric](4, 0) NOT NULL,
    	[f_month] [numeric](2, 0) NOT NULL,
    	[dept] [numeric](3, 0) NOT NULL,
    	[cost_sheet_id] [varchar](4) NOT NULL,
    	[rec_dca_qty] [numeric](12, 2) NULL,
    	[rec_dca_amount] [numeric](15, 2) NULL,
    	[cons_dca_qty] [numeric](12, 2) NULL,
    	[cons_dca_amount] [numeric](15, 2) NULL,
    	[cf_dca_qty] [numeric](12, 2) NULL,
    	[cf_dca_amount] [numeric](15, 2) NULL,
    	[qty_unit] [varchar](5) NULL,
    	[proc_time] [datetime] NULL,
    	[user_id] [varchar](6) NULL,
    	[client_ip] [varchar](15) NULL,
    	[rec_sts] [char](1) NULL,
    	[proc_no] [tinyint] NULL
    )

    -- 1	Receive (Purchase)
    INSERT INTO @_TR_DCA
    	(
    		comp_id
    		,f_year
    		,f_month
    		,dept
    		,cost_sheet_id
    		,rec_dca_qty
    		,rec_dca_amount
    		,cons_dca_qty
    		,cons_dca_amount
    		,cf_dca_qty
    		,cf_dca_amount
    		,qty_unit
    		,proc_time
    		,user_id
    		,client_ip
    		,rec_sts
    		,proc_no
    	)
    	SELECT
    		 @comp_id AS comp_id
    		,@f_year AS f_year
    		,@f_month AS f_month
    		,A.dept AS dept
    		,A.cost_sheet_id AS cost_sheet_id
    		,A.qty_rec AS rec_dca_qty
    		,A.tr_amount AS rec_dca_amount
    		,0 AS cons_dca_qty
    		,0 AS cons_dca_amount
    		,0 AS cf_dca_qty
    		,0 AS cf_dca_amount
    		,A.unit AS qty_unit
    		,@proc_time AS proc_time
    		,@user_id AS user_id
    		,@client_ip AS client_ip
    		,@rec_sts AS rec_sts
    		,@proc_no AS proc_no
    	FROM @_GRPO AS A

    -- 2 Consume DCA
    UPDATE T
    SET
    	T.cons_dca_qty=S.cons_dca_qty
    FROM @_TR_DCA AS T
    LEFT JOIN (
    	SELECT
    		A.Dept AS dept
    		,A.Cost_ID AS cost_sheet_id
    		,A.out_qty AS cons_dca_qty
    		,'KG' AS qty_unit
    	FROM
    	(
    		SELECT
    			 B.Dept
    			,B.Cost_ID
    			,B.unitMsr
    			,SUM(B.out_qty) AS out_qty
    		FROM
    		(
    			SELECT
    				A.Dept,
    				A.docdate,
    				A.ItemCode,
    				A.itmsgrpcod,
    				A.Dscription,
    				A.FrgnName,
    				A.out_qty,
    				A.out_dest,
    				A.out_bale_Qty,
    				A.out_nett_Qty,
    				A.cost_id,
    				A.unitMsr
    			FROM (
    				SELECT
    					T0.u_department AS Dept,
    					T0.docdate,
    					T1.ItemCode,
    					T2.itmsgrpcod,
    					T1.Dscription,
    					T2.FrgnName,
    					T1.Quantity AS out_qty,
    					T1.OcrCode AS out_dest,
    					ISNULL(T1.U_Bale_Qty,0) AS out_bale_Qty,
    					ISNULL(T1.U_Nett_Qty,0) AS out_nett_Qty,
    					T3.U_Cost_ID AS Cost_ID,
    					T1.unitMsr
    				FROM SAP_ISM.SBO_ISM_LIVE.DBO.OIGE T0
    				LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.IGE1 T1 ON T0.docentry=T1.DocEntry
    				LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM T2 ON T1.itemcode=T2.itemcode
    				LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITB T3 ON T2.ItmsGrpCod=T3.ItmsGrpCod
    				WHERE
    					-- ISM: 106-108  ; ACM: 101-103
    					(T2.ItmsGrpCod BETWEEN 113 AND 125)
    					AND U_GI_Rea<> 'ADJ'

    				UNION ALL

    				SELECT
    					T0.U_department AS Dept,
    					T0.docdate,
    					T1.ItemCode,
    					T2.itmsgrpcod,
    					T1.Dscription,
    					T2.FrgnName,
    					T1.Quantity*-1 AS out_qty,
    					T1.OcrCode AS out_dest,
    					ISNULL(T1.U_Bale_Qty,0)*-1 AS out_bale_Qty,
    					ISNULL(T1.U_Nett_Qty,0)*-1 AS out_nett_Qty,
    					T3.U_Cost_ID AS Cost_ID,
    					T1.unitMsr
    				FROM SAP_ISM.SBO_ISM_LIVE.DBO.OIGN T0
    				LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.IGN1 T1 ON T0.docentry=T1.DocEntry
    				LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM T2 ON T1.itemcode=T2.itemcode
    				LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITB T3 ON T2.ItmsGrpCod=T3.ItmsGrpCod
    				WHERE
    					-- ISM: 106-108  ; ACM: 101-103
    					(T2.ItmsGrpCod BETWEEN 113 AND 125) AND T1.OcrCode=400 AND U_GR_Rea='ADJ'
    			) A
    			WHERE
    				(A.DocDate>=@from_date AND A.DocDate<=@to_date)
    				--order by A.ItmsGrpCod
    		) B
    		WHERE
    			(B.unitMsr='KG')
    		GROUP BY B.Dept, B.Cost_ID, B.unitMsr
    	) AS A
    ) AS S ON (S.dept=T.dept AND S.cost_sheet_id COLLATE SQL_Latin1_General_CP1_CI_AS=T.cost_sheet_id COLLATE SQL_Latin1_General_CP1_CI_AS)

    UPDATE T
    SET
    	T.cons_dca_amount = ISNULL(T.cons_dca_qty, 0) * ISNULL(NULLIF((ISNULL(S.cf_dca_amount, 0) + ISNULL(T.rec_dca_amount, 0)), 0) / NULLIF((ISNULL(S.cf_dca_qty, 0) + ISNULL(T.rec_dca_qty, 0)), 0), 0)
    	,T.cf_dca_qty = (ISNULL(S.cf_dca_qty, 0) + ISNULL(T.rec_dca_qty, 0)) - ISNULL(T.cons_dca_qty, 0)
    	,T.cf_dca_amount = (ISNULL(S.cf_dca_amount, 0) + ISNULL(T.rec_dca_amount, 0)) - ISNULL(T.cons_dca_qty, 0) * ISNULL(NULLIF((ISNULL(S.cf_dca_amount, 0) + ISNULL(T.rec_dca_amount, 0)), 0) / NULLIF((ISNULL(S.cf_dca_qty, 0) + ISNULL(T.rec_dca_qty, 0)), 0), 0)
    FROM @_TR_DCA AS T
    LEFT JOIN istem_costing.dbo.tr_bal_dca AS S
    	ON (S.comp_id=T.comp_id AND S.f_year=@lp_f_year AND S.f_month=@lp_f_month AND S.dept=T.dept AND S.cost_sheet_id=T.cost_sheet_id)


    SELECT
        comp_id
        ,f_year
        ,f_month
        ,dept
        ,cost_sheet_id
        ,rec_dca_qty
        ,rec_dca_amount
        ,cons_dca_qty
        ,cons_dca_amount
        ,cf_dca_qty
        ,cf_dca_amount
        ,qty_unit
        ,proc_time
        ,user_id
        ,client_ip
        ,rec_sts
        ,proc_no
    FROM @_TR_DCA
END
