CREATE PROCEDURE  sp_DyeingProcessGreyCostLogtGreyPurc_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- sp_DyeingProcessGreyCostLogtGreyPurc_Data
    -- Grey Cost
    -- SUB1 : Logistic- Grey Purchase
    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept_10 INT
    DECLARE @dept_11 INT
    DECLARE @proc_time AS DATETIME
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_no INT

    SET @dept_10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=10))
    SET @dept_11=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=11))
    SET @proc_time=GETDATE()
    SET @rec_sts='A'


    DECLARE @_TEMP TABLE(
         mat_code VARCHAR(MAX)
        ,item_code VARCHAR(MAX)
        ,mat_name VARCHAR(MAX)
        ,bf_qty NUMERIC(14,4)
        ,bf_price NUMERIC(14,4)
        ,bf_amount NUMERIC(14,4)
        ,rc_qty NUMERIC(14,4)
        ,rc_price NUMERIC(14,4)
        ,rc_amount NUMERIC(14,4)
        ,co_dy_qty NUMERIC(14,4)
        ,co_dy_price NUMERIC(14,4)
        ,co_dy_amount NUMERIC(14,4)
        ,co_ot_qty NUMERIC(14,4)
        ,co_ot_price NUMERIC(14,4)
        ,co_ot_amount NUMERIC(14,4)
        ,cf_qty NUMERIC(14,4)
        ,cf_price NUMERIC(14,4)
        ,cf_amount NUMERIC(14,4)
        ,so VARCHAR(MAX) DEFAULT NULL
    )

    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT

    IF @f_month = 1
    BEGIN
        SET @lp_f_month = 12
        SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
        SET @lp_f_month = @f_month - 1
        SET @lp_f_year = @f_year
    END

    INSERT INTO @_TEMP (mat_code, item_code, mat_name)
    SELECT
        A.mat_code AS mat_code
        ,ISNULL(M.U_ypur_wvname, '') AS item_code
        ,ISNULL(M.FrgnName, '') AS mat_name
    FROM (
        SELECT mat_code
        FROM (
            SELECT A.mat_code
            FROM istem_costing.dbo.tr_bal_mat AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@lp_f_year
                AND A.f_month=@lp_f_month
                AND A.dept=@dept_10
                AND A.cost_sheet_id='A31'
            UNION ALL
            SELECT A.mat_code
            FROM istem_costing.dbo.tr_inv_in AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
                AND A.dept IN (@dept_10)
                AND A.cost_sheet_id='A31'
        ) AS A
        GROUP BY mat_code
    ) AS A
    INNER JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OITM AS M ON (M.ItemCode COLLATE DATABASE_DEFAULT = A.mat_code COLLATE DATABASE_DEFAULT)
    GROUP BY A.mat_code, ISNULL(M.FrgnName, ''), ISNULL(M.U_ypur_wvname, '')


    UPDATE T SET
         bf_qty = S.bf_qty
        ,bf_price = NULLIF(S.bf_amount, 0)/NULLIF(S.bf_qty, 0)
        ,bf_amount = S.bf_amount
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT A.mat_code, A.cf_qty AS bf_qty, A.cf_amount AS bf_amount
        FROM istem_costing.dbo.tr_bal_mat AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@lp_f_year
            AND A.f_month=@lp_f_month
            AND A.dept=@dept_10
            AND A.cost_sheet_id='A31'
    ) AS S ON (S.mat_code=T.mat_code)

    UPDATE T SET
         rc_qty = S.rc_qty
        ,rc_price = NULLIF(S.rc_amount, 0)/NULLIF(S.rc_qty, 0)
        ,rc_amount = S.rc_amount
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT A.mat_code AS mat_code, A.in_qty AS rc_qty, A.in_amount AS rc_amount
        FROM istem_costing.dbo.tr_inv_in AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_10)
            AND A.cost_sheet_id='A31'
    ) AS S ON (S.mat_code=T.mat_code)


    UPDATE T SET
         co_dy_qty = S.rc_qty
        ,co_dy_price = NULLIF(ISNULL(T.bf_amount, 0) + ISNULL(T.rc_amount, 0), 0)/NULLIF(ISNULL(T.bf_qty, 0) + ISNULL(T.rc_qty, 0), 0)
        ,co_dy_amount = S.rc_qty *  NULLIF(ISNULL(T.bf_amount, 0) + ISNULL(T.rc_amount, 0), 0)/NULLIF(ISNULL(T.bf_qty, 0) + ISNULL(T.rc_qty, 0), 0)
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT A.mat_code AS item_code, A.in_qty AS rc_qty, A.in_amount AS rc_amount
        FROM istem_costing.dbo.tr_inv_in AS A
        WHERE
                A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_11)
            AND A.cost_sheet_id='A31'
    ) AS S ON (S.item_code=T.item_code)


    UPDATE T SET
         cf_qty = NULLIF(ISNULL(T.bf_qty, 0) + ISNULL(T.cf_qty, 0) - (ISNULL(T.co_dy_qty, 0) + ISNULL(T.co_ot_qty, 0)), 0)
        ,cf_price = T.co_dy_price
        ,cf_amount = NULLIF(ISNULL(T.bf_qty, 0) + ISNULL(T.cf_qty, 0) - (ISNULL(T.co_dy_qty, 0) + ISNULL(T.co_ot_qty, 0)), 0) * T.co_dy_price
    FROM @_TEMP AS T

    SELECT * FROM @_TEMP ORDER BY so
END
