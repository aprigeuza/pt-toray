CREATE PROCEDURE  sp_DyeingProcessGreyDelivery_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- sp_DyeingProcessGreyDelivery_Data
    -- Grey Delivery
    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept_11 INT
    DECLARE @proc_time AS DATETIME
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_no INT

    -- PERIOD
    DECLARE @from_date DATE, @to_date DATE
    SELECT TOP 1 @from_date = F_RefDate, @to_date = T_RefDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)


    --Konversi period SAP ke CIM
    DECLARE @target_cim_f_year INT
    DECLARE @target_cim_f_month INT

    IF (@f_month + 3) > 12
    BEGIN
        SET @target_cim_f_month = (@f_month - 9)
        SET @target_cim_f_year = @f_year + 1
    END
    ELSE
    BEGIN
        SET @target_cim_f_month = @f_month + 3
        SET @target_cim_f_year = @f_year
    END

    SET @dept_11=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=11))
    SET @proc_time=GETDATE()
    SET @rec_sts='A'
    SET @proc_no=(SELECT ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_inv_out_header_hist WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept_11)

    IF @proc_no >= 2
    BEGIN
        SET @rec_sts='T'
    END

    DECLARE @_TR_INV_OUT_DETAIL TABLE (
    	comp_id NUMERIC(1, 0) NOT NULL,
    	f_year NUMERIC(4, 0) NOT NULL,
    	f_month NUMERIC(2, 0) NOT NULL,
    	dept NUMERIC(3, 0) NOT NULL,
    	cost_sheet_id VARCHAR(4) NOT NULL,
    	mat_code VARCHAR(45) NOT NULL,
    	mat_usage CHAR(1) NOT NULL,
    	out_dest NUMERIC(3, 0) NOT NULL,
    	out_bale_qty NUMERIC(7, 2) NULL,
    	out_qty NUMERIC(14, 2) NULL,
    	out_invoice_qty NUMERIC(14, 2) NULL,
    	out_amount NUMERIC(14, 2) NULL,
    	qty_unit VARCHAR(5) NULL,
    	rec_sts CHAR(1) NULL,
    	proc_no TINYINT NULL,
    	out_qty_pcs NUMERIC(9, 2) NULL
    )

    INSERT INTO @_TR_INV_OUT_DETAIL
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,mat_code
            ,mat_usage
            ,out_dest
            ,out_bale_qty
            ,out_qty
            ,out_invoice_qty
            ,out_amount
            ,qty_unit
            ,rec_sts
            ,proc_no
            ,out_qty_pcs
        )
        SELECT
            @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept_11 AS dept
            ,CASE WHEN B.proc_by = 'COM' THEN 'A31' ELSE 'A30' END AS cost_sheet_id
            ,A.mat_code AS mat_code
            ,A.out_purpose AS mat_usage
            ,A.out_dest AS out_dest
            ,NULL AS out_bale_qty
            ,A.qty_meter AS out_qty
            ,NULL AS out_invoice_qty
            ,NULL AS out_amount
            ,'MTR' AS qty_unit
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,A.qty_pcs AS out_qty_pcs
        FROM (
            --Query Grey Delivery
            SELECT
                R.grey_no AS mat_code,
                R.out_dest,
                R.out_purpose,
                SUM(R.piece) AS qty_pcs,
                SUM(R.length_MTR) AS qty_meter
            FROM (
                SELECT
                    H.grey_no
                    ,SUM(CASE WHEN H.status_pcs='H' THEN 0.5*H.tot_pcs ELSE CASE WHEN H.status_pcs='Q' THEN 0.25*H.tot_pcs ELSE 1*H.tot_pcs END END) AS piece
                    ,SUM(CASE WHEN H.u_ms='YDS' THEN H.length/1.0936 ELSE H.length END) AS length_MTR
                    ,H.rec_type_sts
                    ,(CASE WHEN H.rec_type_sts='1' THEN 900 ELSE
                        (CASE WHEN H.rec_type_sts='2' THEN 300 ELSE
                            (CASE WHEN H.rec_type_sts='3' THEN 300 ELSE
                                (CASE WHEN H.rec_type_sts='4' THEN 999 ELSE
                                    (CASE WHEN H.rec_type_sts='7' THEN 999 ELSE 0 END) END) END) END) END) AS out_dest
                    ,(CASE WHEN H.rec_type_sts='1' THEN '' ELSE
                        (CASE WHEN H.rec_type_sts='2' THEN 'E' ELSE
                            (CASE WHEN H.rec_type_sts='3' THEN 'D' ELSE
                                (CASE WHEN H.rec_type_sts='4' THEN 'T' ELSE
                                (CASE WHEN H.rec_type_sts='7' THEN 'P' ELSE '' END) END) END) END) END) AS out_purpose
                FROM istem_sms.dbo.wv_wh_header AS H
                WHERE
                    H.rec_type='D'
                    AND YEAR(H.proc_date)=@target_cim_f_year
                    AND MONTH(H.proc_date)=@target_cim_f_month
                    -- AND (H.proc_date>=@from_date AND H.proc_date<=@to_date)
                GROUP BY
                    YEAR(H.proc_date)
                    ,MONTH(H.proc_date)
                    ,H.grey_no
                    ,H.rec_type_sts

                UNION ALL

                SELECT
                    H.grey_no
                    ,SUM(CASE WHEN H.status_pcs='H' THEN 0.5*H.tot_pcs ELSE CASE WHEN H.status_pcs='Q' THEN 0.25*H.tot_pcs ELSE 1*H.tot_pcs END END) AS piece, SUM(CASE WHEN H.u_ms='YDS' THEN H.length/1.0936 ELSE H.length END) AS length_MTR
                    ,H.rec_type_sts
                    ,(CASE WHEN H.rec_type_sts='1' THEN 410 ELSE 0 END) AS out_dest
                    ,(CASE WHEN H.rec_type_sts='1' THEN '' ELSE
                        (CASE WHEN H.rec_type_sts='2' THEN 'E' ELSE
                            (CASE WHEN H.rec_type_sts='3' THEN 'D' ELSE
                                (CASE WHEN H.rec_type_sts='4' THEN 'T' ELSE
                                    (CASE WHEN H.rec_type_sts='7' THEN 'P' ELSE '' END) END) END) END) END) AS out_purpose
                FROM istem_sms.dbo.wv_wh_header AS H
                WHERE
                    H.rec_type='R'
                    AND H.rec_sts='A'
                    AND returnF=1
                    AND YEAR(H.proc_date)=@target_cim_f_year
                    AND MONTH(H.proc_date)=@target_cim_f_month
                    -- AND (H.proc_date>=@from_date AND H.proc_date<=@to_date)
                GROUP BY
                    YEAR(H.whs_date)
                    ,MONTH(H.whs_date)
                    ,H.grey_no
                    ,H.rec_type_sts
            ) AS R
            GROUP BY
                R.grey_no
                ,R.out_dest
                ,R.out_purpose
        ) AS A
        LEFT JOIN (
            SELECT
                grey_no,
                proc_by
            FROM istem_sms.dbo.wv_fabric_analysis_master
            GROUP BY
                grey_no,
                proc_by
        ) AS B ON (B.grey_no=A.mat_code)


    SELECT * FROM @_TR_INV_OUT_DETAIL;
END
