CREATE PROCEDURE  sp_EngineringDownloadEntry_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1;
    -- SET @f_year=2020;
    -- SET @f_month=10;
    -- SET @user_id='SYS';
    -- SET @client_ip='192.168.1.1';

    -- sp_EngineringDownloadEntry
    SELECT

        A.comp_id,
        A.f_year,
        A.f_month,
        A.convert_type_code,
        C.convert_type_name,
        A.dept,
        B.dept_name,
        A.utility_cons_qty,
        C.qty_unit
      FROM [istem_costing].[dbo].[tr_utility_cons] AS A
      LEFT JOIN istem_costing.dbo.ms_dept AS B ON (B.comp_id=A.comp_id AND B.dept=A.dept)
      LEFT JOIN istem_costing.dbo.ms_convert_type AS C ON (C.comp_id=A.comp_id AND C.convert_type_code=A.convert_type_code)
      WHERE
        A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
      ORDER BY
        A.convert_type_code,
        C.convert_type_name,
        A.dept,
        B.dept_name,
        C.qty_unit
END
