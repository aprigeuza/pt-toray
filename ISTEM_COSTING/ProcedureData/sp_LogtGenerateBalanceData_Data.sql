CREATE PROCEDURE  sp_LogtGenerateBalanceData_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24);
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @proc_time DATETIME, @proc_no INT, @rec_sts VARCHAR(3);
    SET @proc_time=GETDATE();
    SET @proc_no=(SELECT ISNULL(MAX(proc_no), 0)+1 FROM istem_costing.dbo.tr_bal_mat WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month);
    SET @rec_sts=(SELECT CASE WHEN ISNULL(COUNT(*), 0) = 0 THEN 'A' ELSE 'T' END FROM istem_costing.dbo.tr_bal_mat WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month);

    DECLARE @lp_f_year INT, @lp_f_month INT;

    IF @f_month = 1
    BEGIN
    	SET @lp_f_month = 12;
    	SET @lp_f_year = @f_year - 1;
    END
    ELSE
    BEGIN
    	SET @lp_f_month = @f_month - 1;
    	SET @lp_f_year = @f_year;
    END

    DECLARE @dept10 INT;
    SET @dept10 = (SELECT TOP 1 dept FROM istem_costing.dbo.ms_dept WHERE comp_id=@comp_id AND dept_seq=10);

    DECLARE @_TEMP_TR_BAL_MAT TABLE (
    	comp_id NUMERIC(1, 0) NOT NULL,
    	f_year NUMERIC(4, 0) NOT NULL,
    	f_month NUMERIC(2, 0) NOT NULL,
    	dept NUMERIC(3, 0) NOT NULL,
    	mat_code VARCHAR(45) NOT NULL,
    	mat_usage CHAR(1) NULL,
    	cost_sheet_id VARCHAR(4) NOT NULL,
    	qty_unit VARCHAR(5) NULL,
    	cf_bale_qty NUMERIC(4, 0) NULL,
    	cf_qty NUMERIC(14, 2) NULL,
    	cf_amount NUMERIC(14, 2) NULL,
    	cf_invoice_qty NUMERIC(14, 2) NULL,
    	proc_time DATETIME NULL,
    	user_id VARCHAR(6) NULL,
    	client_ip VARCHAR(15) NULL,
    	rec_sts CHAR(1) NULL,
    	proc_no TINYINT NULL
    );

    DECLARE @_MATERIAL TABLE (
    	mat_code VARCHAR(50),
    	cost_sheet_id VARCHAR(50),
    	qty_unit VARCHAR(50)
    );

    INSERT INTO @_MATERIAL
    	SELECT
    		A.mat_code,
    		A.cost_sheet_id,
    		A.qty_unit
    	FROM (
    		SELECT
    			mat_code,
    			cost_sheet_id,
    			qty_unit
    		FROM istem_costing.dbo.tr_bal_mat WHERE (comp_id = @comp_id) AND (f_year = @lp_f_year) AND (f_month = @lp_f_month) GROUP BY mat_code,cost_sheet_id,qty_unit
    		UNION ALL
    		SELECT
    			mat_code,
    			cost_sheet_id,
    			qty_unit
    		FROM istem_costing.dbo.tr_inv_in WHERE (comp_id = @comp_id) AND (f_year = @f_year) AND (f_month = @f_month) GROUP BY mat_code,cost_sheet_id,qty_unit
    		UNION ALL
    		SELECT
    			mat_code,
    			cost_sheet_id,
    			qty_unit
    		FROM istem_costing.dbo.tr_inv_out_detail WHERE (comp_id = @comp_id) AND (f_year = @f_year) AND (f_month = @f_month) GROUP BY mat_code,cost_sheet_id,qty_unit
    	) AS A
    	GROUP BY
    		mat_code
    		,cost_sheet_id
    		,qty_unit

    -- Report Monthly Staple
    INSERT INTO @_TEMP_TR_BAL_MAT (
    		comp_id
    		,f_year
    		,f_month
    		,dept
    		,mat_code
    		,mat_usage
    		,cost_sheet_id
    		,qty_unit
    		,cf_bale_qty
    		,cf_qty
    		,cf_amount
    		,cf_invoice_qty
    		,proc_time
    		,user_id
    		,client_ip
    		,rec_sts
    		,proc_no
    	)
    	SELECT
    		 @comp_id AS comp_id
    		,@f_year AS f_year
    		,@f_month AS f_month
    		,@dept10 AS dept
    		,A.mat_code AS mat_code
    		,'' AS mat_usage
    		,A.cost_sheet_id AS cost_sheet_id
    		,'LBS' AS qty_unit
    		,ISNULL(A.cf_bale, 0) AS cf_bale_qty
    		,ISNULL(A.cf_net_qty, 0) AS cf_qty
    		,0 AS cf_amount
    		,ISNULL(A.cf_inv_qty, 0) AS cf_invoice_qty
    		,@proc_time AS proc_time
    		,@user_id AS user_id
    		,@client_ip AS client_ip
    		,@rec_sts AS rec_sts
    		,@proc_no AS proc_no
    	FROM (
    		SELECT
    			MT.cost_sheet_id
    			,MT.mat_code  AS mat_code
    			,NULLIF((ISNULL(BF.BF_BALE, 0)+ISNULL(RC.RECEIVE_BALE, 0) ) - (ISNULL(CS.CONSUME_SPINNING_BALE, 0)+ISNULL(CR.CONSUME_RETURN_BALE, 0)), 0) AS cf_bale
    			,NULLIF((ISNULL(BF.BF_NET_QTY, 0)+ISNULL(RC.RECEIVE_NET_QTY, 0)) - (ISNULL(CS.CONSUME_SPINNING_NET_QTY, 0)+ISNULL(CR.CONSUME_RETURN_NET_QTY, 0)), 0) AS cf_net_qty
    			,NULLIF((ISNULL(BF.BF_INV_QTY, 0)+ISNULL(RC.RECEIVE_INV_QTY, 0)) - (ISNULL(CS.CONSUME_SPINNING_INV_QTY, 0)+ISNULL(CR.CONSUME_RETURN_INV_QTY, 0)), 0) AS cf_inv_qty
    		FROM (
    			SELECT *
    			FROM (
    				SELECT cost_sheet_id, mat_code FROM istem_costing.dbo.tr_bal_mat WHERE (comp_id = @comp_id) AND (f_year = @lp_f_year) AND (f_month = @lp_f_month) AND (cost_sheet_id IN ('A10', 'A11', 'A13')) AND (dept=@dept10) GROUP BY cost_sheet_id, mat_code
    				UNION ALL
    				SELECT cost_sheet_id, mat_code FROM istem_costing.dbo.tr_inv_in WHERE (comp_id = @comp_id) AND (f_year = @f_year) AND (f_month = @f_month) AND (cost_sheet_id IN ('A10', 'A11', 'A13')) AND (dept=@dept10) GROUP BY cost_sheet_id, mat_code
    				UNION ALL
    				SELECT cost_sheet_id, mat_code FROM istem_costing.dbo.tr_inv_out_detail WHERE (comp_id = @comp_id) AND (f_year = @f_year) AND (f_month = @f_month) AND (cost_sheet_id IN ('A10', 'A11', 'A13')) AND (dept=@dept10) GROUP BY cost_sheet_id, mat_code
    			) AS A GROUP BY cost_sheet_id, mat_code
    		) AS MT
    		-- BF
    		LEFT JOIN (
    			SELECT
    				mat_code
    				,SUM(ISNULL(cf_bale_qty, 0)) AS BF_BALE -- A
    				,SUM(ISNULL(cf_qty, 0)) AS BF_NET_QTY -- B
    				,SUM(ISNULL(cf_invoice_qty, 0)) AS BF_INV_QTY -- C
    			FROM istem_costing.dbo.tr_bal_mat
    			WHERE
    				(comp_id = @comp_id)
    				AND (f_year = @lp_f_year)
    				AND (f_month = @lp_f_month)
    				AND (dept=@dept10)
    			GROUP BY mat_code
    		) AS BF ON (BF.mat_code= MT.mat_code)
    		-- Receive
    		LEFT JOIN (
    			SELECT
    				mat_code
    				,SUM(ISNULL(in_bale_qty, 0)) AS RECEIVE_BALE -- D
    				,SUM(ISNULL(in_qty, 0)) AS RECEIVE_NET_QTY -- E
    				,SUM(ISNULL(in_invoice_qty, 0)) AS RECEIVE_INV_QTY -- F
    			FROM istem_costing.dbo.tr_inv_in
    			WHERE
    				(comp_id = @comp_id)
    				AND (f_year = @f_year)
    				AND (f_month = @f_month)
    				AND (dept=@dept10)
    			GROUP BY mat_code
    		) AS RC ON (RC.mat_code=MT.mat_code)
    		-- Consume Spinning
    		LEFT JOIN (
    			SELECT
    				mat_code
    				,SUM(ISNULL(out_bale_qty, 0)) AS CONSUME_SPINNING_BALE -- G
    				,SUM(ISNULL(out_qty, 0)) AS CONSUME_SPINNING_NET_QTY -- H
    				,SUM(ISNULL(out_invoice_qty, 0)) AS CONSUME_SPINNING_INV_QTY -- I
    			FROM istem_costing.dbo.tr_inv_out_detail
    			WHERE
    				(comp_id = @comp_id)
    				AND (f_year = @f_year)
    				AND (f_month = @f_month)
    				AND (dept=@dept10)
    				AND (out_dest=700)
    			GROUP BY mat_code
    		) AS CS ON (CS.mat_code=MT.mat_code)
    		-- Consume Return
    		LEFT JOIN (
    			SELECT
    				mat_code
    				,SUM(ISNULL(out_bale_qty, 0)) AS CONSUME_RETURN_BALE -- J
    				,SUM(ISNULL(out_qty, 0)) AS CONSUME_RETURN_NET_QTY -- K
    				,SUM(ISNULL(out_invoice_qty, 0)) AS CONSUME_RETURN_INV_QTY -- L
    			FROM istem_costing.dbo.tr_inv_out_detail
    			WHERE
    				(comp_id = @comp_id)
    				AND (f_year = @f_year)
    				AND (f_month = @f_month)
    				AND (dept=@dept10)
    				AND (out_dest<>700)
    			GROUP BY mat_code
    		) AS CR ON (CR.mat_code = MT.mat_code)
    	) AS A
    	WHERE
    		ISNULL(A.cf_bale, 0) <> 0
    		OR ISNULL(A.cf_net_qty, 0) <> 0
    		OR ISNULL(A.cf_inv_qty, 0) <> 0
    	ORDER BY
    		A.cost_sheet_id, A.mat_code;

    -- Yarn Product Monthly
    DECLARE @_RptLogisticYarnProductMonthly TABLE (
    	mat_code VARCHAR(50),
    	cf_l NUMERIC(14, 2) NULL,
    	cf_p NUMERIC(14, 2) NULL,
    	cf_total NUMERIC(14, 2) NULL
    );

    INSERT INTO @_RptLogisticYarnProductMonthly (
    		mat_code
    		,cf_l
    		,cf_p
    		,cf_total
    	)
    	SELECT
    		MT.mat_code  AS mat_code
    		,NULLIF((ISNULL(BF_L.BF_L, 0) + ISNULL(RC_L.RC_L, 0)) - (ISNULL(CWV_L.CWV_L, 0)+ISNULL(CSALES.CSALES, 0)+ISNULL(CDYING.CDYING, 0)+ISNULL(COTHER.COTHER, 0)), 0) AS cf_l
    		,NULLIF((ISNULL(BF_P.BF_P, 0) + ISNULL(RC_P.RC_P, 0)) - (ISNULL(CWV_P.CWV_P, 0)), 0) AS cf_p
    		,NULLIF((ISNULL(BF_L.BF_L, 0) + ISNULL(RC_L.RC_L, 0)) - (ISNULL(CWV_L.CWV_L, 0)+ISNULL(CSALES.CSALES, 0)+ISNULL(CDYING.CDYING, 0)+ISNULL(COTHER.COTHER, 0))
    		+(ISNULL(BF_P.BF_P, 0) + ISNULL(RC_P.RC_P, 0)) - (ISNULL(CWV_P.CWV_P, 0)), 0) AS cf_total
    	FROM (
    		SELECT *
    		FROM (
    			SELECT mat_code FROM istem_costing.dbo.tr_bal_mat WHERE (comp_id = @comp_id) AND (f_year = @lp_f_year) AND (f_month = @lp_f_month) AND (dept = @dept10) AND (cost_sheet_id IN ('A20')) GROUP BY mat_code, mat_usage
    			UNION ALL
    			SELECT mat_code FROM istem_costing.dbo.tr_inv_in WHERE (comp_id = @comp_id) AND (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept10) AND (cost_sheet_id IN ('A20')) GROUP BY mat_code, mat_usage
    			UNION ALL
    			SELECT mat_code FROM istem_costing.dbo.tr_inv_out_detail WHERE (comp_id = @comp_id) AND (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept10) AND (cost_sheet_id IN ('A20')) GROUP BY mat_code, mat_usage
    		) AS A GROUP BY mat_code
    	) AS MT
    	-- BF L & P
    	LEFT JOIN (
    		SELECT mat_code, SUM(ISNULL(cf_qty, 0)) AS BF_L FROM istem_costing.dbo.tr_bal_mat WHERE (dept=@dept10)AND(comp_id=@comp_id)AND(f_year=@lp_f_year)AND(f_month=@lp_f_month)AND(cost_sheet_id IN ('A20'))AND(mat_usage='L') GROUP BY mat_code
    	) AS BF_L ON (BF_L.mat_code=MT.mat_code)
    	LEFT JOIN (
    		SELECT mat_code, SUM(ISNULL(cf_qty, 0)) AS BF_P FROM istem_costing.dbo.tr_bal_mat WHERE (dept=@dept10)AND(comp_id=@comp_id)AND(f_year=@lp_f_year)AND(f_month=@lp_f_month)AND(cost_sheet_id IN ('A20'))AND(mat_usage='P') GROUP BY mat_code
    	) AS BF_P ON (BF_P.mat_code=MT.mat_code)
    	-- Receive L & P
    	LEFT JOIN (
    		SELECT mat_code, SUM(ISNULL(in_qty, 0)) AS RC_L FROM istem_costing.dbo.tr_inv_in WHERE (dept=@dept10)AND(comp_id=@comp_id)AND(f_year=@f_year)AND(f_month=@f_month)AND(cost_sheet_id IN ('A20'))AND(mat_usage='L') GROUP BY mat_code
    	) AS RC_L ON (RC_L.mat_code=MT.mat_code)
    	LEFT JOIN (
    		SELECT mat_code, SUM(ISNULL(in_qty, 0)) AS RC_P FROM istem_costing.dbo.tr_inv_in WHERE (dept=@dept10)AND(comp_id=@comp_id)AND(f_year=@f_year)AND(f_month=@f_month)AND(cost_sheet_id IN ('A20'))AND(mat_usage='P') GROUP BY mat_code
    	) AS RC_P ON (RC_P.mat_code=MT.mat_code)
    	-- Consume
    	LEFT JOIN (
    		SELECT mat_code, SUM(ISNULL(out_qty, 0)) AS CWV_L FROM istem_costing.dbo.tr_inv_out_detail WHERE (dept=@dept10)AND(comp_id=@comp_id)AND(f_year=@f_year)AND(f_month=@f_month)AND(cost_sheet_id IN ('A20'))AND(mat_usage='L')AND(out_dest=800) GROUP BY mat_code
    	) AS CWV_L ON (CWV_L.mat_code=MT.mat_code)
    	LEFT JOIN (
    		SELECT mat_code, SUM(ISNULL(out_qty, 0)) AS CWV_P FROM istem_costing.dbo.tr_inv_out_detail WHERE (dept=@dept10)AND(comp_id=@comp_id)AND(f_year=@f_year)AND(f_month=@f_month)AND(cost_sheet_id IN ('A20'))AND(mat_usage='P')AND(out_dest=800) GROUP BY mat_code
    	) AS CWV_P ON (CWV_P.mat_code=MT.mat_code)
    	LEFT JOIN (
    		SELECT mat_code, SUM(ISNULL(out_qty, 0)) AS CSALES FROM istem_costing.dbo.tr_inv_out_detail WHERE (dept=@dept10)AND(comp_id=@comp_id)AND(f_year=@f_year)AND(f_month=@f_month)AND(cost_sheet_id IN ('A20'))AND(out_dest=300) GROUP BY mat_code
    	) AS CSALES ON (CSALES.mat_code=MT.mat_code)
    	LEFT JOIN (
    		SELECT mat_code, SUM(ISNULL(out_qty, 0)) AS CDYING FROM istem_costing.dbo.tr_inv_out_detail WHERE (dept=@dept10)AND(comp_id=@comp_id)AND(f_year=@f_year)AND(f_month=@f_month)AND(cost_sheet_id IN ('A20'))AND(out_dest=900) GROUP BY mat_code
    	) AS CDYING ON (CDYING.mat_code=MT.mat_code)
    	LEFT JOIN (
    		SELECT mat_code, SUM(ISNULL(out_qty, 0)) AS COTHER FROM istem_costing.dbo.tr_inv_out_detail WHERE (dept=@dept10)AND(comp_id=@comp_id)AND(f_year=@f_year)AND(f_month=@f_month)AND(cost_sheet_id IN ('A20'))AND(out_dest NOT IN (800, 300, 900)) GROUP BY mat_code
    	) AS COTHER ON (COTHER.mat_code=MT.mat_code)
    	ORDER BY MT.mat_code;

    INSERT INTO @_TEMP_TR_BAL_MAT (
    		comp_id
    		,f_year
    		,f_month
    		,dept
    		,mat_code
    		,mat_usage
    		,cost_sheet_id
    		,qty_unit
    		,cf_bale_qty
    		,cf_qty
    		,cf_amount
    		,cf_invoice_qty
    		,proc_time
    		,user_id
    		,client_ip
    		,rec_sts
    		,proc_no
    	)
    	SELECT
    		@comp_id AS comp_id
    		,@f_year AS f_year
    		,@f_month AS f_month
    		,@dept10 AS dept
    		,A.mat_code AS mat_code
    		,A.mat_usage AS mat_usage
    		,'A20' AS cost_sheet_id
    		,'LBS' AS qty_unit
    		,0 AS cf_bale_qty
    		,A.cf_qty AS cf_qty
    		,0 AS cf_amount
    		,0 AS cf_invoice_qty
    		,@proc_time AS proc_time
    		,@user_id AS user_id
    		,@client_ip AS client_ip
    		,@rec_sts AS rec_sts
    		,@proc_no AS proc_no
    	FROM (
    		SELECT
    		mat_code,
    		cf_l AS cf_qty,
    		'L' AS mat_usage
    		FROM @_RptLogisticYarnProductMonthly
    		UNION ALL
    		SELECT
    		mat_code,
    		cf_p AS cf_qty,
    		'P' AS mat_usage
    		FROM @_RptLogisticYarnProductMonthly
    	) AS A
    	WHERE
    		ISNULL(A.cf_qty, 0) <> 0
    	ORDER BY
    		A.mat_code;

    -- yarn purchase
    -- A21
    DECLARE @_RptLogisticYarnPurchaseMonthly TABLE (
    	mat_code VARCHAR(50),
    	cf NUMERIC(14, 2) NULL
    );

    INSERT INTO @_RptLogisticYarnPurchaseMonthly (
    		mat_code
    		,cf
    	)
    	SELECT
    		MT.mat_code AS mat_code
    		,NULLIF((ISNULL(BF.BF_INV_QTY, 0) + ISNULL(RC.RECEIVE_NET_QTY, 0)) - ((ISNULL(CS.WEAVING, 0) + ISNULL(CS.DYING, 0) + ISNULL(CS.OTHER, 0))), 0) AS cf
    	FROM (
    		SELECT *
    		FROM (
    			SELECT mat_code FROM istem_costing.dbo.tr_bal_mat WHERE (comp_id = @comp_id) AND (f_year = @lp_f_year) AND (f_month = @lp_f_month) AND (cost_sheet_id IN ('A21')) AND (dept=@dept10) GROUP BY mat_code
    			UNION ALL
    			SELECT mat_code FROM istem_costing.dbo.tr_inv_in WHERE (comp_id = @comp_id) AND (f_year = @f_year) AND (f_month = @f_month) AND (cost_sheet_id IN ('A21')) AND (dept=@dept10) GROUP BY mat_code
    			UNION ALL
    			SELECT mat_code FROM istem_costing.dbo.tr_inv_out_detail WHERE (comp_id = @comp_id) AND (f_year = @f_year) AND (f_month = @f_month) AND (cost_sheet_id IN ('A21')) AND (dept=@dept10) GROUP BY mat_code
    		) AS A GROUP BY mat_code
    	) AS MT
    	LEFT JOIN (
    		SELECT
    			mat_code
    			,SUM(ISNULL(cf_qty, 0)) AS BF_INV_QTY -- C
    		FROM istem_costing.dbo.tr_bal_mat
    		WHERE
    			(comp_id = @comp_id)
    			AND (f_year = @lp_f_year)
    			AND (f_month = @lp_f_month)
    			AND (dept=@dept10)
    		GROUP BY mat_code
    	) AS BF ON (BF.mat_code= MT.mat_code)
    	LEFT JOIN (
    		SELECT
    			mat_code
    			,SUM(ISNULL(in_qty, 0)) AS RECEIVE_NET_QTY -- E
    		FROM istem_costing.dbo.tr_inv_in
    		WHERE
    			(comp_id = @comp_id)
    			AND (f_year = @f_year)
    			AND (f_month = @f_month)
    			AND (dept=@dept10)
    		GROUP BY mat_code
    	) AS RC ON (RC.mat_code=MT.mat_code)
    	LEFT JOIN (
    		SELECT
        		A.mat_code
        		,SUM(B.out_qty) AS WEAVING
        		,SUM(C.out_qty) AS DYING
        		,SUM(D.out_qty) AS OTHER
    		FROM istem_costing.dbo.tr_inv_out_header AS A
    		LEFT JOIN istem_costing.dbo.tr_inv_out_detail AS B ON (
    			(B.mat_code=A.mat_code)
    			AND (B.out_dest=800)
    			AND (B.cost_sheet_id=A.cost_sheet_id)
    			AND (B.comp_id=A.comp_id)
    			AND (B.f_year=A.f_year)
    			AND (B.f_month=A.f_month)
    			AND (B.dept=@dept10)
    		)
    		LEFT JOIN istem_costing.dbo.tr_inv_out_detail AS C ON (
    			(C.mat_code=A.mat_code)
    			AND (C.out_dest=900)
    			AND (C.cost_sheet_id=A.cost_sheet_id)
    			AND (C.comp_id=A.comp_id)
    			AND (C.f_year=A.f_year)
    			AND (C.f_month=A.f_month)
    			AND (C.dept=@dept10)
    		)
    		LEFT JOIN istem_costing.dbo.tr_inv_out_detail AS D ON (
    			(D.mat_code=A.mat_code)
    			AND (D.out_dest NOT IN(800, 900))
    			AND (D.cost_sheet_id=A.cost_sheet_id)
    			AND (D.comp_id=A.comp_id)
    			AND (D.f_year=A.f_year)
    			AND (D.f_month=A.f_month)
    			AND (D.dept=@dept10)
    		)
    		WHERE
        		(A.cost_sheet_id IN ('A21'))
        		AND (A.comp_id=@comp_id)
        		AND (A.f_year=@f_year)
        		AND (A.f_month=@f_month)
        		AND (A.dept=@dept10)
    		GROUP BY A.mat_code
    	) AS CS ON (CS.mat_code=MT.mat_code)
    	ORDER BY MT.mat_code

    INSERT INTO @_TEMP_TR_BAL_MAT (
    		comp_id
    		,f_year
    		,f_month
    		,dept
    		,mat_code
    		,mat_usage
    		,cost_sheet_id
    		,qty_unit
    		,cf_bale_qty
    		,cf_qty
    		,cf_amount
    		,cf_invoice_qty
    		,proc_time
    		,user_id
    		,client_ip
    		,rec_sts
    		,proc_no
    	)
    	SELECT
    		@comp_id AS comp_id
    		,@f_year AS f_year
    		,@f_month AS f_month
    		,@dept10 AS dept
    		,A.mat_code AS mat_code
    		,'' AS mat_usage
    		,'A21' AS cost_sheet_id
    		,'LBS' AS qty_unit
    		,0 AS cf_bale_qty
    		,A.cf AS cf_qty
    		,0 AS cf_amount
    		,0 AS cf_invoice_qty
    		,@proc_time AS proc_time
    		,@user_id AS user_id
    		,@client_ip AS client_ip
    		,@rec_sts AS rec_sts
    		,@proc_no AS proc_no
    	FROM @_RptLogisticYarnPurchaseMonthly AS A
    	WHERE
    		ISNULL(A.cf, 0) <> 0;

    -- A31
    DECLARE @_RptLogisticYarnPurchaseMonthly2 TABLE (
    	mat_code VARCHAR(50),
    	cf NUMERIC(14, 2) NULL
    );
    INSERT INTO @_RptLogisticYarnPurchaseMonthly2 (
    		mat_code
    		,cf
    	)
    	SELECT
    		MT.mat_code AS mat_code
    		,NULLIF((ISNULL(BF.BF_INV_QTY, 0) + ISNULL(RC.RECEIVE_NET_QTY, 0)) - ((ISNULL(CS.WEAVING, 0) + ISNULL(CS.DYING, 0) + ISNULL(CS.OTHER, 0))), 0) AS cf
    	FROM (
    		SELECT *
    		FROM (
    			SELECT mat_code FROM istem_costing.dbo.tr_bal_mat WHERE (comp_id = @comp_id) AND (f_year = @lp_f_year) AND (f_month = @lp_f_month) AND (cost_sheet_id IN ('A31')) AND (dept=@dept10) GROUP BY mat_code
    			UNION ALL
    			SELECT mat_code FROM istem_costing.dbo.tr_inv_in WHERE (comp_id = @comp_id) AND (f_year = @f_year) AND (f_month = @f_month) AND (cost_sheet_id IN ('A31')) AND (dept=@dept10) GROUP BY mat_code
    			UNION ALL
    			SELECT mat_code FROM istem_costing.dbo.tr_inv_out_detail WHERE (comp_id = @comp_id) AND (f_year = @f_year) AND (f_month = @f_month) AND (cost_sheet_id IN ('A31')) AND (dept=@dept10) GROUP BY mat_code
    		) AS A GROUP BY mat_code
    	) AS MT
    	LEFT JOIN (
    		SELECT
    			mat_code
    			,SUM(ISNULL(cf_qty, 0)) AS BF_INV_QTY -- C
    		FROM istem_costing.dbo.tr_bal_mat
    		WHERE
        		(cost_sheet_id IN ('A31'))
        		AND (comp_id = @comp_id)
    			AND (f_year = @lp_f_year)
    			AND (f_month = @lp_f_month)
    			AND (dept=@dept10)
    		GROUP BY mat_code
    	) AS BF ON (BF.mat_code= MT.mat_code)
    	LEFT JOIN (
    		SELECT
    			mat_code
    			,SUM(ISNULL(in_qty, 0)) AS RECEIVE_NET_QTY -- E
    		FROM istem_costing.dbo.tr_inv_in
    		WHERE
    			(cost_sheet_id IN ('A31'))
    			AND (comp_id = @comp_id)
    			AND (f_year = @f_year)
    			AND (f_month = @f_month)
    			AND (dept=@dept10)
    		GROUP BY mat_code
    	) AS RC ON (RC.mat_code=MT.mat_code)
    	LEFT JOIN (
    		SELECT
    			A.mat_code
    			,SUM(B.out_qty) AS WEAVING
    			,SUM(C.out_qty) AS DYING
    			,SUM(D.out_qty) AS OTHER
    		FROM istem_costing.dbo.tr_inv_out_header AS A
    		LEFT JOIN istem_costing.dbo.tr_inv_out_detail AS B ON (
    			(B.mat_code=A.mat_code)
    			AND (B.out_dest=800)
    			AND (B.cost_sheet_id=A.cost_sheet_id)
    			AND (B.comp_id=A.comp_id)
    			AND (B.f_year=A.f_year)
    			AND (B.f_month=A.f_month)
    			AND (B.dept=@dept10)
    		)
    		LEFT JOIN istem_costing.dbo.tr_inv_out_detail AS C ON (
    			(C.mat_code=A.mat_code)
    			AND (C.out_dest=900)
    			AND (C.cost_sheet_id=A.cost_sheet_id)
    			AND (C.comp_id=A.comp_id)
    			AND (C.f_year=A.f_year)
    			AND (C.f_month=A.f_month)
    			AND (C.dept=@dept10)
    		)
    		LEFT JOIN istem_costing.dbo.tr_inv_out_detail AS D ON
    		(
    			(D.mat_code=A.mat_code)
    			AND (D.out_dest NOT IN(800, 900))
    			AND (D.cost_sheet_id=A.cost_sheet_id)
    			AND (D.comp_id=A.comp_id)
    			AND (D.f_year=A.f_year)
    			AND (D.f_month=A.f_month)
    			AND (D.dept=@dept10)
    		)
    		WHERE
        		(A.cost_sheet_id IN ('A31'))
        		AND (A.comp_id=@comp_id)
        		AND (A.f_year=@f_year)
        		AND (A.f_month=@f_month)
        		AND (A.dept=@dept10)
    		GROUP BY A.mat_code
    	) AS CS ON (CS.mat_code = MT.mat_code)
    	ORDER BY MT.mat_code

    INSERT INTO @_TEMP_TR_BAL_MAT (
    		comp_id
    		,f_year
    		,f_month
    		,dept
    		,mat_code
    		,mat_usage
    		,cost_sheet_id
    		,qty_unit
    		,cf_bale_qty
    		,cf_qty
    		,cf_amount
    		,cf_invoice_qty
    		,proc_time
    		,user_id
    		,client_ip
    		,rec_sts
    		,proc_no
    	)
    	SELECT
    		@comp_id AS comp_id
    		,@f_year AS f_year
    		,@f_month AS f_month
    		,@dept10 AS dept
    		,A.mat_code AS mat_code
    		,'' AS mat_usage
    		,'A31' AS cost_sheet_id
    		,'LBS' AS qty_unit
    		,0 AS cf_bale_qty
    		,A.cf AS cf_qty
    		,0 AS cf_amount
    		,0 AS cf_invoice_qty
    		,@proc_time AS proc_time
    		,@user_id AS user_id
    		,@client_ip AS client_ip
    		,@rec_sts AS rec_sts
    		,@proc_no AS proc_no
    	FROM @_RptLogisticYarnPurchaseMonthly2 AS A
    	WHERE
    		ISNULL(A.cf, 0) <> 0

    UPDATE T
    	SET T.qty_unit = S.qty_unit
    	FROM @_TEMP_TR_BAL_MAT AS T
    	LEFT JOIN @_MATERIAL AS S ON (S.mat_code=T.mat_code AND S.cost_sheet_id=T.cost_sheet_id)

    SELECT * FROM @_TEMP_TR_BAL_MAT;
END
