CREATE PROCEDURE  sp_LogDownMatGRPO2_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- DECLARE
    -- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1;
    -- SET @f_year=2020;
    -- SET @f_month=10;
    -- SET @user_id='SYS';
    -- SET @client_ip='192.168.1.1';


    DECLARE @proc_no INT

    DECLARE @FromDate DATE, @ToDate DATE, @RecSts CHAR(1)

	SELECT TOP 1 @FromDate = F_RefDate, @ToDate = T_RefDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)

	IF @proc_no >= 1
	BEGIN
	  SET @proc_no = @proc_no + 1
	  SET @RecSts = 'T'
	END
	ELSE
	BEGIN
	  SET @proc_no = 1
	  SET @RecSts = 'A'
	END

	DECLARE @dept AS INT
	SELECT @dept = dept FROM istem_costing.dbo.ms_dept WHERE (comp_id = @comp_id) AND (dept_seq = 10)

	DECLARE @_TEMP TABLE (
		[comp_id] [numeric](1, 0) NOT NULL,
		[f_year] [numeric](4, 0) NOT NULL,
		[f_month] [numeric](2, 0) NOT NULL,
		[dept] [numeric](3, 0) NOT NULL,
		[cost_sheet_id] [varchar](4) NOT NULL,
		[mat_code] [varchar](45) NOT NULL,
		[mat_usage] [char](1) NULL,
		[in_bale_qty] [numeric](4, 0) NULL,
		[in_qty] [numeric](14, 2) NULL,
		[in_amount] [numeric](14, 2) NULL,
		[in_invoice_qty] [numeric](14, 2) NULL,
		[qty_unit] [varchar](5) NULL,
		[proc_time] [datetime] NULL,
		[user_id] [varchar](6) NULL,
		[client_ip] [varchar](15) NULL,
		[rec_sts] [char](1) NULL,
		[proc_no] [tinyint] NULL,
	    [in_qty_pcs] numeric(9,2) NULL
	)

	INSERT INTO @_TEMP
		SELECT
		@comp_id AS comp_id
		,@f_year AS f_year
		,@f_month AS f_month
		,@dept AS dept
		,cost_sheet_id AS cost_sheet_id
		,item_code AS mat_code
		,'' AS mat_usage
		,SUM(bale_qty) AS in_bale_qty
		,CASE WHEN cost_sheet_id <> 'A31' THEN SUM(nett_Qty*2.2046) ELSE SUM(nett_Qty) END AS in_qty
		,ISNULL(SUM(trans_amount), 0) + ISNULL(SUM(landed_cost), 0) AS in_amount
		,CASE WHEN cost_sheet_id <> 'A31' THEN SUM(Qty_Rec*2.2046) ELSE SUM(Qty_Rec) END AS in_invoice_qty
		,CASE WHEN cost_sheet_id <> 'A31' THEN 'LBS' ELSE 'MTR' END AS qty_unit
		,GETDATE() AS proc_time
		,@user_id AS user_id
		,@client_ip AS client_ip
		,@RecSts AS rec_sts
		,@proc_no AS proc_no
        ,CASE WHEN cost_sheet_id = 'A31' THEN SUM(nett_Qty) ELSE NULL END AS in_qty_pcs
	FROM istem_costing.dbo.tr_sap_purchase
	WHERE
		(comp_id = @comp_id)
		AND
		(f_year = @f_year)
		AND
		(f_month = @f_month)
	GROUP BY
		comp_id
		,f_year
		,f_month
		,cost_sheet_id
		,item_code
		,unit


    -- Update tanggal 2 oktober 2020
    -- karena ada masalah di tr_inv_in yang juga di pakai di weaving wip process
    UPDATE @_TEMP SET mat_usage='S' WHERE cost_sheet_id='A31';



    -- Update tanggal 14 Desember 2020
    -- Untuk adjustment biaya truck pada in_amount
    DECLARE @_TEMP_ADJ TABLE(
        comp_id INT,
        f_year INT,
        f_month INT,
        cost_sheet_id VARCHAR(MAX),
        supplier_code VARCHAR(MAX),
        item_code VARCHAR(MAX),
        amount NUMERIC(16,4)
    );

    INSERT INTO @_TEMP_ADJ
    SELECT
        A.comp_id,
        A.f_year,
        A.f_month,
        A.cost_sheet_id,
        A.supplier_code,
        A.item_code,
        SUM(A.amount) AS amount
    FROM (
        SELECT
            A.comp_id,
            A.f_year,
            A.f_month,
            A.cost_sheet_id,
            A.supplier_code,
            A.item_code,
            (A.nett_qty/X.sum_nett_qty)*(B.amount) AS amount
        FROM istem_costing.dbo.tr_sap_purchase AS A
        INNER JOIN (
            SELECT supplier_code,
            SUM(nett_qty) AS sum_nett_qty
            FROM istem_costing.dbo.tr_sap_purchase
            GROUP BY supplier_code
        ) AS X ON (X.supplier_code=A.supplier_code)
        INNER JOIN (
            SELECT
                f_year AS f_year,
                f_month AS f_month,
                SUM(amount) AS amount
            FROM (
                SELECT * FROM istem_costing.dbo.v_SAPVerdorAdditionalCost
                WHERE f_year=@f_year AND f_month=@f_month
            ) AS A
            WHERE vendor_code = 'RM-ALL'
            GROUP BY f_year, f_month
        ) AS B ON (B.f_year = A.f_year AND B.f_month = A.f_month)
        WHERE
            A.comp_id = @comp_id
            AND A.f_year = @f_year
            AND A.f_month = @f_month
            AND A.cost_sheet_id IN ('A10', 'A11', 'A12', 'A13')

        UNION ALL

        SELECT
            A.comp_id,
            A.f_year,
            A.f_month,
            A.cost_sheet_id,
            A.supplier_code,
            A.item_code,
            (A.nett_qty/B.sum_nett_qty) * C.amount AS amount
        FROM istem_costing.dbo.tr_sap_purchase AS A
        INNER JOIN (
            SELECT
                A.cost_sheet_id,
                A.supplier_code,
                SUM(A.nett_qty) AS sum_nett_qty
            FROM istem_costing.dbo.tr_sap_purchase AS A
            INNER  JOIN (
                SELECT * FROM istem_costing.dbo.v_SAPVerdorAdditionalCost
                WHERE f_year=@f_year AND f_month=@f_month
            ) AS B
                ON (B.supplier_code COLLATE SQL_Latin1_General_CP1_CI_AS=A.supplier_code COLLATE SQL_Latin1_General_CP1_CI_AS)
            WHERE
                A.comp_id = @comp_id
                AND A.f_year = @f_year
                AND A.f_month = @f_month
                AND B.vendor_code <> 'RM-ALL'
            GROUP BY
                A.cost_sheet_id,
                A.supplier_code
        ) AS B ON (B.cost_sheet_id = A.cost_sheet_id AND B.supplier_code = A.supplier_code)
        INNER  JOIN (
            SELECT * FROM istem_costing.dbo.v_SAPVerdorAdditionalCost
            WHERE f_year=@f_year AND f_month=@f_month
        ) AS C
            ON (C.supplier_code COLLATE SQL_Latin1_General_CP1_CI_AS=A.supplier_code COLLATE SQL_Latin1_General_CP1_CI_AS)
        WHERE
            A.comp_id = @comp_id
            AND A.f_year = @f_year
            AND A.f_month = @f_month
            AND C.vendor_code <> 'RM-ALL'
    ) AS A
    GROUP BY
        comp_id
        ,f_year
        ,f_month
        ,cost_sheet_id
        ,supplier_code
        ,item_code

    UPDATE T
    SET
        T.in_amount=ISNULL(T.in_amount, 0)+ISNULL(S.amount, 0)
        --,T.mat_usage='OKE'
    FROM @_TEMP AS T
    INNER JOIN @_TEMP_ADJ AS S ON ( S.f_year=T.f_year
            AND S.f_month=T.f_month
            AND S.cost_sheet_id=T.cost_sheet_id
            AND S.item_code=T.mat_code )


   SELECT comp_id, f_year, f_month, dept, cost_sheet_id, mat_code, mat_usage, in_bale_qty, in_qty, in_amount, in_invoice_qty, qty_unit, proc_time, user_id, client_ip, rec_sts, proc_no, in_qty_pcs FROM @_TEMP
END
