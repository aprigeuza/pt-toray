CREATE PROCEDURE  sp_DyeingProdWIPEquivalent_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- sp_DyeingProdWIPEquivalent_Data

    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1;
    -- SET @f_year=2020;
    -- SET @f_month=10;
    -- SET @user_id='SYS';
    -- SET @client_ip='192.168.1.1';

    DECLARE @dept10 INT;
    DECLARE @dept40 INT;
    DECLARE @proc_time AS DATETIME;
    DECLARE @rec_sts VARCHAR(MAX);
    DECLARE @proc_no INT;

    SET @dept10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=10));
    SET @dept40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40));
    SET @proc_time = GETDATE();
    SET @rec_sts='A';
    SET @proc_no=1;


    DECLARE @_TEMP_TR_PROD_DETAIL1 TABLE (
        comp_id NUMERIC(1, 0) NOT NULL,
        f_year NUMERIC(4, 0) NOT NULL,
        f_month NUMERIC(2, 0) NOT NULL,
        dept NUMERIC(3, 0) NOT NULL,
        cost_sheet_id VARCHAR(4) NOT NULL,
        convert_type_code VARCHAR(3) NOT NULL,
        item_code VARCHAR(45) NOT NULL,
        fix_cost_group VARCHAR(3) NOT NULL,
        tot_prod_equiv_qty NUMERIC(12, 2) NULL,
        tot_prod_cost_distr NUMERIC(12, 2) NULL,
        proc_time datetime NULL,
        user_id VARCHAR(6) NULL,
        client_ip VARCHAR(15) NULL,
        rec_sts CHAR(1) NULL,
        proc_no TINYINT NULL
    )
    DECLARE @_TEMP_TR_PROD_DETAIL2 TABLE (
        comp_id numeric(1,0) NOT NULL,
    	f_year numeric(4,0) NOT NULL,
    	f_month numeric(2,0) NOT NULL,
    	dept numeric(3,0) NOT NULL,
    	cost_sheet_id varchar(4) NOT NULL,
    	item_code varchar(45) NOT NULL,
    	convert_type_code varchar(3) NOT NULL,
    	fix_cost_group varchar(4) NOT NULL,
    	prod_equiv_qty numeric(12,2) DEFAULT 0 NULL,
    	prod_cost_distr numeric(12,2) DEFAULT 0 NULL,
    	proc_time datetime NULL,
    	user_id varchar(6) NULL,
    	client_ip varchar(15) NULL,
    	rec_sts char(1) NULL,
    	proc_no tinyint NULL,
    	dy_proc_type varchar(5) DEFAULT '' NOT NULL,
    	dyes_type_code varchar(4) DEFAULT '' NOT NULL,
    	color_shade_code varchar(2) DEFAULT '' NOT NULL
    )
    -- Select : proc_no
    SELECT
            @proc_no=ISNULL(MAX(proc_no), 0) + 1
            FROM istem_costing.dbo.tr_prod_detail2
            WHERE
                comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept IN (@dept40);
        SET @rec_sts = 'A';
    IF @proc_no >= 2
        BEGIN
            SET @rec_sts = 'T'
        END
    -- Ambil data untuk tr_prod_detail2
    INSERT INTO @_TEMP_TR_PROD_DETAIL2
        (
              comp_id
             ,f_year
             ,f_month
             ,dept
             ,cost_sheet_id
             ,item_code
             ,convert_type_code
             ,fix_cost_group
             ,prod_equiv_qty
             ,prod_cost_distr
             ,proc_time
             ,user_id
             ,client_ip
             ,rec_sts
             ,proc_no
             ,dy_proc_type
             ,dyes_type_code
             ,color_shade_code
        )
        SELECT
            @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept40 AS dept
            ,D.cost_sheet_id AS cost_sheet_id
            ,A.grey_no AS item_code
            ,C.convert_type_code AS convert_type_code
            ,'' AS fix_cost_group
            ,A.v_tot_prod_qty * C.convert_ratio AS prod_equiv_qty
            ,NULL AS prod_cost_distr
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
            ,A.dy_proc_type AS dy_proc_type
            ,A.dyes_type_code AS dyes_type_code
            ,A.color_shade_code AS color_shade_code
        FROM (
            SELECT
                grey_no AS grey_no
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
                ,SUM(prod_qty_mtr) AS v_tot_prod_qty
            FROM istem_costing.dbo.tr_prod_fg
            WHERE
                comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept=@dept40
            GROUP BY
                grey_no
                ,dy_proc_type
                ,dyes_type_code
                ,color_shade_code
        ) AS A
        INNER JOIN (
                SELECT
                     convert_type_code
                    ,dept AS v_dept
                    ,dy_proc_type
                    ,dyes_type_code
                    ,color_shade_code
                    ,item_code
                    ,convert_ratio
                FROM istem_costing.dbo.tr_convert_item
                WHERE
                    comp_id=@comp_id
                    AND f_year=@f_year
                    AND f_month=@f_month
                    AND dept=@dept40
                    AND convert_type_code LIKE 'R%'
            ) AS C ON (
                    C.dy_proc_type=A.dy_proc_type
                AND C.dyes_type_code=A.dyes_type_code
                AND C.color_shade_code=A.color_shade_code
                AND C.item_code=A.grey_no
            )
        INNER JOIN (
            SELECT
                cost_sheet_id,
                convert_type_code
            FROM istem_costing.dbo.ms_convert_type
            WHERE
                comp_id=@comp_id
                AND convert_type_code LIKE 'R%'
        ) AS D ON (D.convert_type_code=C.convert_type_code)
    INSERT INTO @_TEMP_TR_PROD_DETAIL1
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,cost_sheet_id
            ,convert_type_code
            ,item_code
            ,fix_cost_group
            ,tot_prod_equiv_qty
            ,tot_prod_cost_distr
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
            A.comp_id AS comp_id
            ,A.f_year AS f_year
            ,A.f_month AS f_month
            ,A.dept AS dept
            ,A.cost_sheet_id AS cost_sheet_id
            ,A.convert_type_code AS convert_type_code
            ,'' AS item_code
            ,A.fix_cost_group AS fix_cost_group
            ,SUM(A.prod_equiv_qty) AS tot_prod_equiv_qty
            ,NULL AS tot_prod_cost_distr
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
        FROM @_TEMP_TR_PROD_DETAIL2 AS A
        GROUP BY
            A.comp_id
            ,A.f_year
            ,A.f_month
            ,A.dept
            ,A.cost_sheet_id
            ,A.convert_type_code
            ,A.fix_cost_group

    -- ###################################
    DECLARE @_TEMP_TR_WIP_EQV_DETAIL TABLE (
        comp_id numeric(1,0) NOT NULL,
        f_year numeric(4,0) NOT NULL,
        f_month numeric(2,0) NOT NULL,
        dept numeric(3,0) NOT NULL,
        proc_type varchar(20) NOT NULL,
        cost_sheet_id varchar(4) NOT NULL,
        item_code varchar(45) NOT NULL,
        convert_type_code varchar(3) NOT NULL,
        wip_equiv_qty numeric(12,2) DEFAULT 0 NULL,
        wip_cost_distr numeric(12,2) DEFAULT 0 NULL,
        proc_time datetime NULL,
        user_id varchar(6) NULL,
        client_ip varchar(15) NULL,
        rec_sts char(1) NULL,
        proc_no tinyint NULL
    )
    DECLARE @_TEMP_TR_WIP_EQV_HEADER TABLE (
        comp_id numeric(1,0) NOT NULL,
        f_year numeric(4,0) NOT NULL,
        f_month numeric(2,0) NOT NULL,
        dept numeric(3,0) NOT NULL,
        proc_type varchar(20) NOT NULL,
        cost_sheet_id varchar(4) NOT NULL,
        convert_type_code varchar(3) NOT NULL,
        tot_wip_equiv_qty numeric(12,2) DEFAULT 0 NULL,
        proc_time datetime NULL,
        user_id varchar(6) NULL,
        client_ip varchar(15) NULL,
        rec_sts char(1) NULL,
        proc_no tinyint NULL
    )

    INSERT INTO @_TEMP_TR_WIP_EQV_DETAIL
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,proc_type
            ,cost_sheet_id
            ,item_code
            ,convert_type_code
            ,wip_equiv_qty
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
             @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept40 AS dept
            ,A.proc_type AS proc_type
            ,D.cost_sheet_id AS cost_sheet_id
            ,A.item_code AS item_code
            ,C.convert_type_code AS convert_type_code
            ,NULLIF(A.sum_of_mat_qty, 0) * 0.5 * NULLIF(C.convert_ratio, 0) AS wip_equiv_qty
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
        FROM (
            SELECT
                item_code,
                proc_type,
                SUM(mat_qty) AS sum_of_mat_qty
            FROM istem_costing.dbo.tr_dept_calc
            WHERE
                comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept=@dept40
                AND tr_code LIKE 'WIP%'
            GROUP BY
                item_code,
                proc_type
        ) AS A
        INNER JOIN (
                SELECT
                     convert_type_code
                    ,item_code
                    ,convert_ratio
                FROM istem_costing.dbo.tr_convert_item
                WHERE
                    comp_id=@comp_id
                    AND f_year=@f_year
                    AND f_month=@f_month
                    AND dept=@dept40
                    AND convert_type_code LIKE 'R%'
                    AND ISNULL(dy_proc_type, '') = ''
                    AND ISNULL(dyes_type_code, '') = ''
                    AND ISNULL(color_shade_code, '') = ''
            ) AS C ON (C.item_code=A.item_code)
        INNER JOIN (
            SELECT
                cost_sheet_id,
                convert_type_code
            FROM istem_costing.dbo.ms_convert_type
            WHERE
                comp_id=@comp_id
                AND convert_type_code LIKE 'R%'
        ) AS D ON (D.convert_type_code=C.convert_type_code)
    INSERT INTO @_TEMP_TR_WIP_EQV_HEADER
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,proc_type
            ,cost_sheet_id
            ,convert_type_code
            ,tot_wip_equiv_qty
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
             @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept40 AS dept
            ,'' AS proc_type
            ,A.cost_sheet_id AS cost_sheet_id
            ,A.convert_type_code AS convert_type_code
            ,SUM(A.wip_equiv_qty) AS tot_wip_equiv_qty
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
        FROM @_TEMP_TR_WIP_EQV_DETAIL AS A
        GROUP BY
            A.cost_sheet_id
            ,A.convert_type_code




    SELECT * FROM @_TEMP_TR_WIP_EQV_DETAIL
END
