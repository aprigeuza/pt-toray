CREATE PROCEDURE  sp_AcctCalcUtPropCost_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN

    -- sp_AcctCalcUtPropCost_Data
    PRINT 'SUB1 : sp_AcctCalcUtPropCost_Data'

    -- SUB1
    -- Hitung Alokasi Utility Proportional Cost Amount untuk tiap2 Dept
    -- berdasarkan Utility Consume Qty per Dept yang diinput oleh Engineering


    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept_10 INT
    DECLARE @dept_20 INT
    DECLARE @dept_30 INT
    DECLARE @dept_40 INT
    -- DECLARE @proc_time AS DATETIME
    -- DECLARE @rec_sts VARCHAR(MAX)
    -- DECLARE @proc_no INT

    SET @dept_10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=10))
    SET @dept_20=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=20))
    SET @dept_30=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=30))
    SET @dept_40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40))
    -- SET @proc_time = GETDATE()
    -- SET @rec_sts='A'
    -- SET @proc_no=1

    DECLARE @_TR_MANEX TABLE (
    	comp_id numeric(1, 0) NOT NULL,
    	f_year numeric(4, 0) NOT NULL,
    	f_month numeric(2, 0) NOT NULL,
    	tr_code varchar(5) NOT NULL,
    	dept numeric(3, 0) NOT NULL,
    	ori_dept numeric(3, 0) NOT NULL,
    	proc_code varchar(7) NOT NULL,
    	cost_sheet_id varchar(4) NOT NULL,
    	manex_amount numeric(14, 2) NULL
    )



    DECLARE @_TEMP1 TABLE(
        comp_id INT
        ,f_year INT
        ,f_month INT
        ,dept INT
        ,convert_type_code VARCHAR(20)
        ,manex_amount NUMERIC(14,5)
        ,cost_flag TINYINT
    )

    INSERT INTO @_TEMP1
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,convert_type_code
            ,manex_amount
            ,cost_flag
        )
        SELECT
            A.comp_id
            ,A.f_year
            ,A.f_month
            ,A.dept
            ,A.convert_type_code
            ,SUM(A.utility_cons_qty)
            ,A.cost_flag
        FROM (
            SELECT
                 A.comp_id
                ,A.f_year
                ,A.f_month
                ,CASE WHEN A.dept=985 THEN 505
                ELSE A.dept END AS dept
                ,A.convert_type_code
                ,A.utility_cons_qty
                ,B.cost_flag
            FROM istem_costing.dbo.tr_utility_cons AS A
            LEFT JOIN istem_costing.dbo.ms_dept AS B ON (B.comp_id=A.comp_id AND B.dept=A.dept)
            WHERE
                A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
                AND B.cost_flag=1
        ) AS A
        GROUP BY
            A.comp_id
            ,A.f_year
            ,A.f_month
            ,A.dept
            ,A.convert_type_code
            ,A.cost_flag

    INSERT INTO @_TR_MANEX
        (
            comp_id
            ,f_year
            ,f_month
            ,tr_code
            ,dept
            ,ori_dept
            ,proc_code
            ,cost_sheet_id
            ,manex_amount
        )
        SELECT
             @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,'AUPC' AS tr_code
            ,A.dept AS dept
            ,A.ori_dept AS ori_dept
            ,A.proc_code AS proc_code
            ,A.cost_sheet_id AS cost_sheet_id
            ,B.expense_amount * (C.manex_amount/D.manex_amount) AS manex_amount
        FROM (
            SELECT
                A.dept AS dept
                ,A.convert_type_code AS convert_type_code
                ,B.ut_cost_center AS ori_dept
                ,B.ut_prop_account AS ut_prop_account
                ,B.proc_code AS proc_code
                ,B.cost_sheet_id AS cost_sheet_id
            FROM (
                SELECT dept, convert_type_code FROM @_TEMP1 WHERE dept<>985 GROUP BY dept, convert_type_code
            ) AS A
            INNER JOIN (
                SELECT *
                FROM istem_costing.dbo.ms_convert_type AS A
                WHERE A.comp_id=@comp_id
            ) AS B ON (B.convert_type_code=A.convert_type_code)
            GROUP BY
                A.dept
                ,A.convert_type_code
                ,B.ut_cost_center
                ,B.ut_prop_account
                ,B.proc_code
                ,B.cost_sheet_id
        ) AS A
        INNER JOIN (
            SELECT *
            FROM istem_costing.dbo.tr_sap_sme AS A
            WHERE
                    A.comp_id=@comp_id
                AND A.f_year=@f_year
                AND A.f_month=@f_month
        ) AS B ON (B.dept=A.ori_dept AND B.acct_code=A.ut_prop_account)
        INNER JOIN (
            SELECT dept, convert_type_code, SUM(manex_amount) AS manex_amount
            FROM @_TEMP1 GROUP BY dept, convert_type_code
        ) AS C ON (C.dept=A.dept AND C.convert_type_code=A.convert_type_code)
        INNER JOIN (
            SELECT convert_type_code, SUM(manex_amount) AS manex_amount
            FROM @_TEMP1 GROUP BY convert_type_code
        ) AS D ON (D.convert_type_code=A.convert_type_code)
        ORDER BY
            A.dept
            ,A.ori_dept
            ,A.proc_code
            ,A.cost_sheet_id

    SELECT comp_id, f_year, f_month, tr_code, dept, ori_dept, proc_code, cost_sheet_id, manex_amount FROM @_TR_MANEX;
END
