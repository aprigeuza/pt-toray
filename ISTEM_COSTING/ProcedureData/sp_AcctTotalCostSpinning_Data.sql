CREATE PROCEDURE  sp_AcctTotalCostSpinning_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'


    -- sp_AcctTotalCostSpinning_Data
    -- Acct - Spinning Total Cost

    --
    -- DECLARE
    -- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept_seq10 INT
    DECLARE @dept10 INT
    DECLARE @dept_seq20 INT
    DECLARE @dept20 INT
    DECLARE @tr_code VARCHAR(MAX)
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_time AS DATETIME
    DECLARE @proc_no INT
    DECLARE @lp_f_year INT
    DECLARE @lp_f_month INT

    SET @dept_seq10 = 10
    SET @dept10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=@dept_seq10))
    SET @dept_seq20 = 20
    SET @dept20=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=@dept_seq20))
    SET @tr_code=''
    SET @proc_time = GETDATE()
    SET @rec_sts='A'
    SET @proc_no=1

    IF @f_month = 1
    BEGIN
    	SET @lp_f_month = 12
    	SET @lp_f_year = @f_year - 1
    END
    ELSE
    BEGIN
    	SET @lp_f_month = @f_month - 1
    	SET @lp_f_year = @f_year
    END

    DECLARE @_TEMP TABLE(
         item_code     VARCHAR(MAX)
        ,item_desc     VARCHAR(MAX)
        ,product_qty   NUMERIC(16,4)
        ,cr_electric   NUMERIC(16,4)
        ,cr_water      NUMERIC(16,4)
        ,cr_steam      NUMERIC(16,4)
        ,cr_lng        NUMERIC(16,4)
        ,cr_production NUMERIC(16,4)
        ,cr_diestuff   NUMERIC(16,4)
        ,cr_chemical   NUMERIC(16,4)
        ,cr_resin      NUMERIC(16,4)
        ,cr_labor      NUMERIC(16,4)
        ,cr_repair     NUMERIC(16,4)
        ,eq_electric   NUMERIC(16,4)
        ,eq_water      NUMERIC(16,4)
        ,eq_steam      NUMERIC(16,4)
        ,eq_lng        NUMERIC(16,4)
        ,eq_production NUMERIC(16,4)
        ,eq_diestuff   NUMERIC(16,4)
        ,eq_chemical   NUMERIC(16,4)
        ,eq_resin      NUMERIC(16,4)
        ,eq_labor      NUMERIC(16,4)
        ,eq_repair     NUMERIC(16,4)
        ,cd_electric   NUMERIC(16,4)
        ,cd_water      NUMERIC(16,4)
        ,cd_steam      NUMERIC(16,4)
        ,cd_lng        NUMERIC(16,4)
        ,cd_production NUMERIC(16,4)
        ,cd_diestuff   NUMERIC(16,4)
        ,cd_chemical   NUMERIC(16,4)
        ,cd_resin      NUMERIC(16,4)
        ,leno          NUMERIC(16,4)
        ,material_cost NUMERIC(16,4)
        ,pc_fixed      NUMERIC(16,4)
        ,pc_labour     NUMERIC(16,4)
        ,pc_repair     NUMERIC(16,4)
        ,total_cost    NUMERIC(16,4)
        ,unit_cost     NUMERIC(16,4)
    )


    DECLARE @tot_product_qty   NUMERIC(16,4)
    DECLARE @tot_cr_electric   NUMERIC(16,4)
    DECLARE @tot_cr_water      NUMERIC(16,4)
    DECLARE @tot_cr_steam      NUMERIC(16,4)
    DECLARE @tot_cr_lng        NUMERIC(16,4)
    DECLARE @tot_cr_production NUMERIC(16,4)
    DECLARE @tot_cr_diestuff   NUMERIC(16,4)
    DECLARE @tot_cr_chemical   NUMERIC(16,4)
    DECLARE @tot_cr_resin      NUMERIC(16,4)
    DECLARE @tot_cr_labor      NUMERIC(16,4)
    DECLARE @tot_cr_repair     NUMERIC(16,4)
    DECLARE @tot_eq_electric   NUMERIC(16,4)
    DECLARE @tot_eq_water      NUMERIC(16,4)
    DECLARE @tot_eq_steam      NUMERIC(16,4)
    DECLARE @tot_eq_lng        NUMERIC(16,4)
    DECLARE @tot_eq_production NUMERIC(16,4)
    DECLARE @tot_eq_diestuff   NUMERIC(16,4)
    DECLARE @tot_eq_chemical   NUMERIC(16,4)
    DECLARE @tot_eq_resin      NUMERIC(16,4)
    DECLARE @tot_eq_labor      NUMERIC(16,4)
    DECLARE @tot_eq_repair     NUMERIC(16,4)
    DECLARE @tot_cd_electric   NUMERIC(16,4)
    DECLARE @tot_cd_water      NUMERIC(16,4)
    DECLARE @tot_cd_steam      NUMERIC(16,4)
    DECLARE @tot_cd_lng        NUMERIC(16,4)
    DECLARE @tot_cd_production NUMERIC(16,4)
    DECLARE @tot_cd_diestuff   NUMERIC(16,4)
    DECLARE @tot_cd_chemical   NUMERIC(16,4)
    DECLARE @tot_cd_resin      NUMERIC(16,4)
    DECLARE @tot_leno          NUMERIC(16,4)
    DECLARE @tot_material_cost NUMERIC(16,4)
    DECLARE @tot_pc_fixed      NUMERIC(16,4)
    DECLARE @tot_pc_labour     NUMERIC(16,4)
    DECLARE @tot_pc_repair     NUMERIC(16,4)
    DECLARE @tot_total_cost    NUMERIC(16,4)
    DECLARE @tot_unit_cost     NUMERIC(16,4)


    -- Insert Item
    PRINT 'Proc : 1';

    INSERT INTO @_TEMP
    (
        item_code
        ,item_desc
        ,product_qty
    )
    SELECT
         A.item_code
        ,B.yarn_prev_name
        ,A.prod_qty
    FROM istem_costing.dbo.tr_prod AS A
    INNER JOIN istem_sms.dbo.sp_ms_yarn AS B ON (B.yarn_code = A.item_code)
    WHERE
        A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept=@dept20



    DECLARE @_TEMP_CONVERT_ITEM TABLE(
        item_code VARCHAR(50)
        ,convert_type_code VARCHAR(12)
        ,convert_ratio DECIMAL(16,5)
    )

    PRINT 'Proc : 2';
    INSERT INTO @_TEMP_CONVERT_ITEM
        SELECT
            item_code
            ,convert_type_code
            ,convert_ratio
        FROM istem_costing.dbo.tr_convert_item
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept20


    PRINT 'Proc : 3';
    UPDATE T SET
        T.cr_electric       = electric.convert_ratio
        ,T.cr_water         = water.convert_ratio
        ,T.cr_production    = production.convert_ratio
        ,T.cr_labor         = labor.convert_ratio
        ,T.cr_repair        = repair.convert_ratio
        ,T.eq_electric      = T.product_qty * electric.convert_ratio
        ,T.eq_water         = T.product_qty * water.convert_ratio
        ,T.eq_production    = T.product_qty * production.convert_ratio
        ,T.eq_labor         = T.product_qty * labor.convert_ratio
        ,T.eq_repair        = T.product_qty * repair.convert_ratio
    FROM @_TEMP AS T
    LEFT JOIN @_TEMP_CONVERT_ITEM AS electric    ON (electric.item_code=T.item_code AND electric.convert_type_code='R01')
    LEFT JOIN @_TEMP_CONVERT_ITEM AS water       ON (water.item_code=T.item_code AND water.convert_type_code='R02')
    LEFT JOIN @_TEMP_CONVERT_ITEM AS production  ON (production.item_code=T.item_code AND production.convert_type_code='R07')
    LEFT JOIN @_TEMP_CONVERT_ITEM AS labor       ON (labor.item_code=T.item_code AND labor.convert_type_code='R10')
    LEFT JOIN @_TEMP_CONVERT_ITEM AS repair      ON (repair.item_code=T.item_code AND repair.convert_type_code='R11')

    PRINT 'Proc : 4';
    SELECT
        @tot_eq_electric = SUM(ISNULL(eq_electric, 0))
        ,@tot_eq_production = SUM(ISNULL(eq_production, 0))
        ,@tot_eq_labor = SUM(ISNULL(eq_labor, 0))
        ,@tot_eq_repair = SUM(ISNULL(eq_repair, 0))
    FROM @_TEMP


    PRINT 'Proc : 5';
    SELECT @tot_cd_electric = SUM(cons_wip_cost_amount) FROM istem_costing.dbo.tr_wip_cost WHERE comp_id=1 AND f_year=@f_year AND f_month=@f_month AND dept=700 AND cost_sheet_id='C02'


    PRINT 'Proc : 6';
    UPDATE T SET
        T.cd_electric = NULLIF(@tot_cd_electric, 0) / NULLIF(@tot_eq_electric, 0) * NULLIF(T.eq_electric, 0)
    FROM @_TEMP AS T


    PRINT 'Proc : 7';
    UPDATE T SET
        T.material_cost = S1.material_cost
    FROM @_TEMP AS T
    LEFT JOIN (
        SELECT
            item_code,
            material_cost
        FROM istem_costing.dbo.tr_prod
        WHERE
            comp_id=@comp_id
            AND f_year=@f_year
            AND f_month=@f_month
            AND dept=@dept20
    ) AS S1 ON (S1.item_code=T.item_code)


    PRINT 'Proc : 8';
    SELECT @tot_material_cost=SUM(ISNULL(material_cost, 0)) FROM @_TEMP

    -- Total Fixed Cost per Dept
    PRINT 'Proc : 9';
    SELECT
        @tot_pc_labour=SUM(A.manex_amount)
    FROM istem_costing.dbo.tr_manex AS A
    LEFT JOIN ms_cost_group AS B ON (B.comp_id=A.comp_id AND B.cost_sheet_id=A.cost_sheet_id)
    WHERE
        A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept=@dept20
        AND A.tr_code='FCD'
        AND B.fix_cost_group='R10'

    -- Total Fixed Cost per Dept
    PRINT 'Proc : 10';
    SELECT
        @tot_pc_repair=SUM(A.manex_amount)
    FROM istem_costing.dbo.tr_manex AS A
    LEFT JOIN ms_cost_group AS B ON (B.comp_id=A.comp_id AND B.cost_sheet_id=A.cost_sheet_id)
    WHERE
        A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept=@dept20
        AND A.tr_code='FCD'
        AND B.fix_cost_group='R11'

    -- Total Fixed Cost per Dept
    PRINT 'Proc : 11';
    SELECT
        @tot_pc_fixed=ISNULL(SUM(A.cons_wip_cost_amount), 0) - ISNULL((ISNULL(@tot_pc_labour, 0)+ISNULL(@tot_pc_repair, 0)), 0)
    FROM istem_costing.dbo.tr_wip_cost AS A
    WHERE
        A.comp_id=@comp_id
        AND A.f_year=@f_year
        AND A.f_month=@f_month
        AND A.dept=@dept20
        AND A.cost_sheet_id = 'H99'

    PRINT 'Proc : 12';
    UPDATE T SET
        T.pc_fixed = NULLIF(@tot_pc_fixed, 0)/NULLIF(@tot_eq_production, 0) * NULLIF(T.eq_production, 0)
        ,T.pc_labour = NULLIF(@tot_pc_labour, 0)/NULLIF(@tot_eq_labor, 0) * NULLIF(T.eq_labor, 0)
        ,T.pc_repair = NULLIF(@tot_pc_repair, 0)/NULLIF(@tot_eq_repair, 0) * NULLIF(T.eq_repair, 0)
    FROM @_TEMP AS T

    PRINT 'Proc : 13';
    UPDATE T SET
        T.total_cost = NULLIF(ISNULL(T.pc_fixed, 0) + ISNULL(T.pc_labour, 0) + ISNULL(T.pc_repair, 0) + ISNULL(T.leno, 0) + ISNULL(T.material_cost, 0) + ISNULL(T.cd_electric, 0), 0)
    FROM @_TEMP AS T


    PRINT 'Proc : 14';
    UPDATE T SET
        T.unit_cost = NULLIF(NULLIF(T.total_cost, 0)/NULLIF(T.product_qty, 0), 0)
    FROM @_TEMP AS T


    SELECT * FROM @_TEMP;
END
