CREATE PROCEDURE  sp_AcctCalcEngAllocFixCost_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN

    -- sp_AcctCalcEngAllocFixCost
    PRINT 'SUB5 : sp_AcctCalcEngAllocFixCost';

    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept_10 INT
    DECLARE @dept_20 INT
    DECLARE @dept_30 INT
    DECLARE @dept_40 INT
    DECLARE @dept_61 INT
    DECLARE @dept_62 INT
    DECLARE @proc_time AS DATETIME
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_no INT

    SET @dept_10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=10))
    SET @dept_20=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=20))
    SET @dept_30=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=30))
    SET @dept_40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40))
    SET @dept_61=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=61))
    SET @dept_62=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=62))

    -- PERIOD
    DECLARE @from_date DATE, @to_date DATE
    SELECT TOP 1 @from_date = F_RefDate, @to_date = T_RefDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)

    DECLARE @_TR_MANEX TABLE (
    	[comp_id] [numeric](1, 0) NOT NULL,
    	[f_year] [numeric](4, 0) NOT NULL,
    	[f_month] [numeric](2, 0) NOT NULL,
    	[tr_code] [varchar](5) NOT NULL,
    	[dept] [numeric](3, 0) NOT NULL,
    	[ori_dept] [numeric](3, 0) NOT NULL,
    	[proc_code] [varchar](7) NOT NULL,
    	[cost_sheet_id] [varchar](4) NOT NULL,
    	[manex_amount] [numeric](14, 2) NULL
    )

    DECLARE @tr_code AS VARCHAR(MAX)
    DECLARE @proc_code AS VARCHAR(MAX)
    SET @tr_code = 'FCD'
    SET @proc_code = (SELECT TOP 1 ISNULL(proc_code, '') FROM istem_costing.dbo.ms_process WHERE (comp_id=@comp_id AND tr_code=@tr_code))

    INSERT INTO @_TR_MANEX
    	(
    		comp_id
    		,f_year
    		,f_month
    		,tr_code
    		,dept
    		,ori_dept
    		,proc_code
    		,cost_sheet_id
    		,manex_amount
    	)
    	SELECT
    		@comp_id AS comp_id
    		,@f_year AS f_year
    		,@f_month AS f_month
    		,@tr_code AS tr_code
    		,A.dept AS dept
    		,A.dept AS ori_dept
    		,@proc_code AS proc_code
    		,A.cost_sheet_id AS cost_sheet_id
    		,A.v_EN_fix_amt AS manex_amount
    	FROM (
    		SELECT
    			A.dept
                ,A.cost_sheet_id
    			,SUM(A.expense_amount) AS v_EN_fix_amt
    		FROM istem_costing.dbo.tr_sap_sme AS A
    		WHERE
    			A.comp_id=@comp_id
    			AND A.f_year=@f_year
                AND A.f_month=@f_month
    			AND A.acct_code LIKE '520.20%'
    			AND A.dept IN (@dept_61, @dept_62)
    		GROUP BY
    			A.dept
    		   ,A.cost_sheet_id
    	) AS A

    DECLARE @_DEPT_SUM TABLE(
        dept INT
    	,amt DECIMAL(16,5)
    )

    INSERT INTO @_DEPT_SUM (dept, amt)
        SELECT A.dept, SUM(A.manex_amount) AS amt FROM istem_costing.dbo.tr_manex AS A WHERE A.f_year=@f_year AND A.f_month=@f_month AND A.tr_code='AUPC' AND A.dept=@dept_10 GROUP BY A.dept

    INSERT INTO @_DEPT_SUM (dept, amt)
    	SELECT A.dept, SUM(A.manex_amount) AS amt FROM @_TR_MANEX AS A GROUP BY A.dept

    DECLARE @AUPC_506 DECIMAL(16,4)
    SET @AUPC_506 = (SELECT SUM(A.manex_amount) AS amt FROM istem_costing.dbo.tr_manex AS A WHERE A.f_year=@f_year AND A.f_month=@f_month AND A.tr_code='AUPC' AND A.dept=506)

    UPDATE @_DEPT_SUM SET
    amt = ISNULL(amt, 0) + ISNULL(@AUPC_506, 0)
    WHERE dept=506

    SET @tr_code = 'ACEN'
    SET @proc_code = (SELECT TOP 1 ISNULL(proc_code, '') FROM istem_costing.dbo.ms_process WHERE (comp_id=@comp_id AND tr_code=@tr_code))
    INSERT INTO @_TR_MANEX
    	(
    		comp_id
    		,f_year
    		,f_month
    		,tr_code
    		,dept
    		,ori_dept
    		,proc_code
    		,cost_sheet_id
    		,manex_amount
    	)
    	SELECT
    		 @comp_id AS comp_id
    		,@f_year AS f_year
    		,@f_month AS f_month
    		,@tr_code AS tr_code
    		,B.dept AS dept
    		,A.dept AS ori_dept
    		,@proc_code AS proc_code
    		,C.cost_sheet_id AS cost_sheet_id
    		,A.amt * B.convert_ratio AS manex_amount
    	FROM @_DEPT_SUM AS A
    	FULL OUTER JOIN (
    		SELECT
    			A.dept
    			,A.convert_ratio
    			,A.convert_type_code
    		FROM istem_costing.dbo.tr_convert_item AS A
    		WHERE
    				A.comp_id=@comp_id
    			AND A.f_year=@f_year
    			AND A.f_month=@f_month
    			AND A.convert_type_code='X03'
    			AND A.dept IN (700, 800, 900)
    	) AS B ON (B.dept IS NOT NULL )
    	INNER JOIN istem_costing.dbo.ms_convert_type AS C ON (C.comp_id=@comp_id AND C.convert_type_code=B.convert_type_code)

    SELECT comp_id, f_year, f_month, tr_code, dept, ori_dept, proc_code, cost_sheet_id, manex_amount FROM @_TR_MANEX;

END
