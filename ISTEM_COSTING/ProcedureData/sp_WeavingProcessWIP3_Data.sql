CREATE PROCEDURE  sp_WeavingProcessWIP3_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- DECLARE
    -- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'


    DECLARE @dept INT
    		,@rec_sts VARCHAR(MAX)
    		,@proc_time AS DATETIME
    		,@proc_no INT
    		,@tr_code VARCHAR(MAX)
    		,@max_date AS DATE

    SET @tr_code='WIP3'
    SET @dept=(SELECT dept FROM ms_dept WHERE (comp_id=@comp_id AND dept_seq=30))
    SET @proc_time = GETDATE()
    SET @rec_sts='A'
    SET @proc_no=1
    SET @max_date = (SELECT MAX(stock_take_date) FROM istem_costing.dbo.tr_dept_detail1 WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept) AND (tr_code=@tr_code))

    -- TEMPORARY TABLE
    DECLARE @_TEMP_TR_DEPT_CALC TABLE (
    	comp_id numeric(1, 0) NOT NULL,
    	f_year numeric(4, 0) NOT NULL,
    	f_month numeric(2, 0) NOT NULL,
    	dept numeric(3, 0) NOT NULL,
    	tr_code varchar(5) NOT NULL,
    	mc_loc varchar(15) NOT NULL,
    	proc_code varchar(7) NOT NULL,
    	item_code varchar(45) NOT NULL,
    	mat_code varchar(45) NOT NULL,
    	mat_qty numeric(12, 2) NULL,
    	mat_qty_unit varchar(5) NULL,
    	proc_type varchar(20) NULL,
    	proc_time datetime NULL,
    	user_id varchar(6) NULL,
    	client_ip varchar(15) NULL,
    	rec_sts char(1) NULL,
    	proc_no tinyint NULL
    )

    DECLARE @_TEMP_GREY_YARN TABLE (
    	grey_no VARCHAR(max)
    	,length_gr DECIMAL(16,5)
    	,warp_ln DECIMAL(16,5)
    	,yarn_no VARCHAR(max)
    	,yarn_code VARCHAR(max)
    	,yarn_kind VARCHAR(max)
    	,yarn_wg DECIMAL(16,5)
    )
    INSERT INTO @_TEMP_GREY_YARN
    	(
    		grey_no
    		,length_gr
    		,warp_ln
    		,yarn_no
    		,yarn_code
    		,yarn_kind
    		,yarn_wg
    	)
    	SELECT
    		grey_no
    		,length_gr
    		,warp_ln
    		,yarn_no
    		,yarn_code
    		,yarn_kind
    		,yarn_wg
    	FROM (
    		SELECT
    			 grey_no
    			,length_gr
    			,warp_ln
    			,'WARP1' AS yarn_no
    			,warp1_yarncode AS yarn_code
    			,warp1_kind AS yarn_kind
    			,warp1_wg AS yarn_wg
    		FROM istem_sms.dbo.wv_fabric_analysis_master
    		WHERE ISNULL(warp1_yarncode, '') <> ''
    		UNION ALL
    		SELECT
    			 grey_no
    			,length_gr
    			,warp_ln
    			,'WARP2' AS yarn_no
    			,warp2_yarncode AS yarn_code
    			,warp2_kind AS yarn_kind
    			,warp2_wg AS yarn_wg
    		FROM istem_sms.dbo.wv_fabric_analysis_master
    		WHERE ISNULL(warp2_yarncode, '') <> ''
    		UNION ALL
    		SELECT
    			 grey_no
    			,length_gr
    			,warp_ln
    			,'WARP3' AS yarn_no
    			,warp3_yarncode AS yarn_code
    			,warp3_kind AS yarn_kind
    			,warp3_wg AS yarn_wg
    		FROM istem_sms.dbo.wv_fabric_analysis_master
    		WHERE ISNULL(warp3_yarncode, '') <> ''
    		UNION ALL
    		SELECT
    			 grey_no
    			,length_gr
    			,warp_ln
    			,'WARP4' AS yarn_no
    			,warp4_yarncode AS yarn_code
    			,warp4_kind AS yarn_kind
    			,warp4_wg AS yarn_wg
    		FROM istem_sms.dbo.wv_fabric_analysis_master
    		WHERE ISNULL(warp4_yarncode, '') <> ''
    		UNION ALL
    		SELECT
    			 grey_no
    			,length_gr
    			,warp_ln
    			,'WEFT1' AS yarn_no
    			,weft1_yarncode AS yarn_code
    			,weft1_kind AS yarn_kind
    			,weft1_wg AS yarn_wg
    		FROM istem_sms.dbo.wv_fabric_analysis_master
    		WHERE ISNULL(weft1_yarncode, '') <> ''
    		UNION ALL
    		SELECT
    			 grey_no
    			,length_gr
    			,warp_ln
    			,'WEFT2' AS yarn_no
    			,weft2_yarncode AS yarn_code
    			,weft2_kind AS yarn_kind
    			,weft2_wg AS yarn_wg
    		FROM istem_sms.dbo.wv_fabric_analysis_master
    		WHERE ISNULL(weft2_yarncode, '') <> ''
    		UNION ALL
    		SELECT
    			 grey_no
    			,length_gr
    			,warp_ln
    			,'WEFT3' AS yarn_no
    			,weft3_yarncode AS yarn_code
    			,weft3_kind AS yarn_kind
    			,weft3_wg AS yarn_wg
    		FROM istem_sms.dbo.wv_fabric_analysis_master
    		WHERE ISNULL(weft3_yarncode, '') <> ''
    		UNION ALL
    		SELECT
    			 grey_no
    			,length_gr
    			,warp_ln
    			,'WEFT4' AS yarn_no
    			,weft4_yarncode AS yarn_code
    			,weft4_kind AS yarn_kind
    			,weft4_wg AS yarn_wg
    		FROM istem_sms.dbo.wv_fabric_analysis_master
    		WHERE ISNULL(weft4_yarncode, '') <> ''

    	) AS A

    INSERT INTO @_TEMP_TR_DEPT_CALC
    	(comp_id
    		,f_year
    		,f_month
    		,dept
    		,tr_code
    		,mc_loc
    		,proc_code
    		,item_code
    		,mat_code
    		,mat_qty
    		,mat_qty_unit
    		,proc_type
    		,proc_time
    		,user_id
    		,client_ip
    		,rec_sts
    		,proc_no)
    	SELECT
    		 A.comp_id AS comp_id
    		,A.f_year AS f_year
    		,A.f_month AS f_month
    		,A.dept AS dept
    		,A.tr_code AS tr_code
    		,A.mc_loc AS mc_loc
    		,B.proc_code AS proc_code
    		,A.item_code AS item_code
    		,D.yarn_code AS mat_code
    		,CASE
                WHEN A.input_qty_unit = 'MTR' THEN SUM((A.input_qty/D.length_gr) *  D.yarn_wg)
                ELSE SUM(A.input_qty *  D.yarn_wg) -- PCS
            END AS mat_qty
    		,B.mat_qty_unit AS mat_qty_unit
    		,C.proc_type AS proc_type
    		,@proc_time AS proc_time
    		,@user_id AS user_id
    		,@client_ip AS client_ip
    		,@rec_sts AS rec_sts
    		,@proc_no AS proc_no
    	FROM istem_costing.dbo.tr_dept_detail1 AS A
    	LEFT JOIN istem_costing.dbo.ms_mc_proc AS B ON (
    		(B.comp_id=A.comp_id)
    		AND (B.dept=A.dept)
    		AND (B.mc_proc_code=A.mc_proc_code)
    	)
    	LEFT JOIN istem_costing.dbo.ms_process AS C ON (
    		(C.comp_id=A.comp_id)
    		AND (C.dept=A.dept)
    		AND (C.tr_code=A.tr_code)
    		AND (C.proc_code=B.proc_code)
    	)
    	LEFT JOIN @_TEMP_GREY_YARN AS D ON (D.grey_no=A.item_code)
    	WHERE
    		(A.comp_id=@comp_id)
    		AND (A.f_year=@f_year)
    		AND (A.f_month=@f_month)
    		AND (A.dept=@dept)
    		AND (A.stock_take_date = @max_date)
    		AND (A.tr_code=@tr_code)
    		AND (D.yarn_code<>'')
    	GROUP BY
    		 A.comp_id
    		,A.f_year
    		,A.f_month
    		,A.dept
    		,A.tr_code
    		,A.mc_loc
    		,B.proc_code
    		,A.item_code
    		,D.yarn_code
            ,A.input_qty_unit
    		,B.mat_qty_unit
    		,C.proc_type

    -- INSERT INTO istem_costing.dbo.tr_dept_calc

    SELECT
    	 comp_id
    	,f_year
    	,f_month
    	,dept
    	,tr_code
    	,mc_loc
    	,proc_code
    	,item_code
    	,mat_code
    	,SUM(mat_qty)
    	,mat_qty_unit
    	,proc_type
    	,proc_time
    	,user_id
    	,client_ip
    	,rec_sts
    	,proc_no
    FROM @_TEMP_TR_DEPT_CALC
    GROUP BY
    	 comp_id
    	,f_year
    	,f_month
    	,dept
    	,tr_code
    	,mc_loc
    	,proc_code
    	,item_code
    	,mat_code
    	,mat_qty_unit
    	,proc_type
    	,proc_time
    	,user_id
    	,client_ip
    	,rec_sts
    	,proc_no
END
