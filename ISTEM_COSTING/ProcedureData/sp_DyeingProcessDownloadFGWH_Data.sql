CREATE PROCEDURE  sp_DyeingProcessDownloadFGWH_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- sp_DyeingProcessDownloadFGWH_Data
    -- FG Warehouse

    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept40 INT
    DECLARE @proc_time AS DATETIME
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_no INT

    -- PERIOD
    DECLARE @from_date DATE, @to_date DATE
    SELECT TOP 1 @from_date = F_RefDate, @to_date = T_RefDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)

    --Konversi period SAP ke CIM
    DECLARE @target_cim_f_year INT
    DECLARE @target_cim_f_month INT

    IF (@f_month + 3) > 12
    BEGIN
        SET @target_cim_f_month = (@f_month - 9)
        SET @target_cim_f_year = @f_year + 1
    END
    ELSE
    BEGIN
        SET @target_cim_f_month = @f_month + 3
        SET @target_cim_f_year = @f_year
    END

    SET @dept40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40))
    SET @proc_time=GETDATE()
    SET @rec_sts='A'
    SET @proc_no=(SELECT ISNULL(MAX(proc_no), 0) + 1 FROM istem_costing.dbo.tr_prod_fg_hist WHERE comp_id=@comp_id AND f_year=@f_year AND f_month=@f_month AND dept=@dept40)

    IF @proc_no >= 2
    BEGIN
        SET @rec_sts='T'
    END

    DECLARE @_TR_PROD_FG TABLE (
        comp_id numeric(1,0) NOT NULL,
    	f_year numeric(4,0) NOT NULL,
    	f_month numeric(2,0) NOT NULL,
    	dept numeric(3,0) NOT NULL,
    	grey_no varchar(6) NOT NULL,
    	ci_no varchar(20) NOT NULL,
    	item_category varchar(2) NULL,
    	wh_class char(1) NOT NULL,
    	dom_exp char(1) NOT NULL,
    	undelivery bit NOT NULL,
    	qty_unit varchar(5) NOT NULL,
    	prod_qty numeric(12,2) DEFAULT 0 NULL,
    	prod_qty_mtr numeric(12,2) DEFAULT 0 NULL,
    	dy_proc_type varchar(5) NOT NULL,
    	dyes_type_code varchar(4) NOT NULL,
    	color_shade_code varchar(2) NOT NULL,
    	material_cost numeric(19,2) NULL,
    	process_cost numeric(19,2) NULL,
    	proc_time datetime NULL,
    	user_id varchar(6) NULL,
    	client_ip varchar(15) NULL,
    	rec_sts char(1) NULL,
    	proc_no tinyint NULL
    )


    -- INSERT INTO @_TR_PROD_FG
    --     (
    --         comp_id
    --         ,f_year
    --         ,f_month
    --         ,dept
    --         ,grey_no
    --         ,ci_no
    --         ,wh_class
    --         ,dom_exp
    --         ,undelivery
    --         ,qty_unit
    --         ,prod_qty
    --         ,prod_qty_mtr
    --         ,dy_proc_type
    --         ,dyes_type_code
    --         ,color_shade_code
    --         ,proc_time
    --         ,user_id
    --         ,client_ip
    --         ,rec_sts
    --         ,proc_no
    --     )
    --     SELECT
    --          @comp_id AS comp_id
    --         ,@f_year AS f_year
    --         ,@f_month AS f_month
    --         ,A.dept AS dept
    --         ,A.grey_no AS grey_no
    --         ,A.ci_no AS ci_no
    --         ,A.wh_class AS wh_class
    --         ,A.dom_exp AS dom_exp
    --         ,A.undelivery AS undelivery
    --         ,A.u_ms AS qty_unit
    --         ,A.prod_qty AS prod_qty
    --         ,A.prod_qty_mtr AS prod_qty_mtr
    --         ,'' AS dy_proc_type
    --         ,'' AS dyes_type_code
    --         ,'' AS color_shade_code
    --         ,@proc_time AS proc_time
    --         ,@user_id AS user_id
    --         ,@client_ip AS client_ip
    --         ,@rec_sts AS rec_sts
    --         ,@proc_no AS proc_no
    --     FROM (
    --         SELECT
    --             @comp_id AS comp_id
    --             ,YEAR(whs_dt) AS f_year
    --             ,MONTH(whs_dt) AS f_month
    --             ,@dept40 AS dept
    --             ,grey_no
    --             ,ci_no
    --             ,wh_class
    --             ,d_e AS dom_exp
    --             ,undelivery
    --             ,u_ms
    --             ,SUM(f_length) AS prod_qty
    --             ,CASE
    --                 WHEN u_ms='YDS' THEN SUM(f_length/1.0936)
    --                 ELSE SUM(f_length)
    --             END AS prod_qty_mtr
    --         FROM istem_sms.dbo.fg_warehouse_detail
    --         WHERE
    --             YEAR(whs_dt) = @target_cim_f_year
    --             AND MONTH(whs_dt) = @target_cim_f_month
    --             AND wh_class IN ('1','6')
    --         GROUP BY
    --             YEAR(whs_dt)
    --             ,MONTH(whs_dt)
    --             ,grey_no
    --             ,ci_no
    --             ,wh_class
    --             ,d_e
    --             ,undelivery
    --             ,u_ms
    --         UNION ALL
    --         SELECT
    --             @comp_id AS comp_id
    --             ,YEAR(whs_dt) AS f_year
    --             ,MONTH(whs_dt) AS f_month
    --             ,@dept40 AS dept
    --             ,grey_no
    --             ,ci_no
    --             ,wh_class
    --             ,d_e AS dom_exp
    --             ,undelivery
    --             ,u_ms
    --             ,SUM(f_length) AS prod_qty
    --             ,CASE
    --                 WHEN u_ms='YDS' THEN SUM(f_length/1.0936)
    --                 ELSE SUM(f_length)
    --             END AS prod_qty_mtr
    --         FROM istem_sms.dbo.fg_cancel_wh_detail_hist
    --         WHERE
    --             YEAR(whs_dt) = @target_cim_f_year
    --             AND MONTH(whs_dt) = @target_cim_f_month
    --             AND wh_class IN ('1','6')
    --         GROUP BY
    --             YEAR(whs_dt)
    --             ,MONTH(whs_dt)
    --             ,grey_no
    --             ,ci_no
    --             ,wh_class
    --             ,d_e
    --             ,undelivery
    --             ,u_ms
    --         UNION ALL
    --         SELECT
    --             @comp_id AS comp_id
    --             ,YEAR(whs_dt) AS f_year
    --             ,MONTH(whs_dt) AS f_month
    --             ,@dept40 AS dept
    --             ,grey_no
    --             ,ci_no
    --             ,wh_class
    --             ,d_e AS dom_exp
    --             ,undelivery
    --             ,u_ms
    --             ,SUM(f_length) AS prod_qty
    --             ,CASE
    --                 WHEN u_ms='YDS' THEN SUM(f_length/1.0936)
    --                 ELSE SUM(f_length)
    --             END AS prod_qty_mtr
    --         FROM istem_sms.dbo.fg_transfer_stock_hist
    --         WHERE
    --             YEAR(whs_dt) = @target_cim_f_year
    --             AND MONTH(whs_dt) = @target_cim_f_month
    --             AND wh_class IN ('1','6')
    --         GROUP BY
    --             YEAR(whs_dt),MONTH(whs_dt)
    --             ,grey_no
    --             ,ci_no
    --             ,wh_class
    --             ,d_e
    --             ,undelivery
    --             ,u_ms
    --         UNION ALL
    --         SELECT
    --             @comp_id AS comp_id
    --             ,YEAR(cancel_date) AS f_year
    --             ,MONTH(cancel_date) AS f_month
    --             ,@dept40 AS dept
    --             ,grey_no
    --             ,ci_no
    --             ,wh_class
    --             ,d_e AS dom_exp
    --             ,undelivery
    --             ,u_ms
    --             ,SUM(f_length)*-1 AS prod_qty
    --             ,CASE
    --                 WHEN u_ms='YDS' THEN SUM(f_length/1.0936)*-1
    --                 ELSE SUM(f_length)*-1
    --             END AS prod_qty_mtr
    --         FROM istem_sms.dbo.fg_cancel_wh_detail_hist
    --         WHERE
    --             YEAR(cancel_date) = @target_cim_f_year
    --             AND MONTH(cancel_date) = @target_cim_f_month
    --             AND wh_class IN ('1','6')
    --         GROUP BY
    --             YEAR(cancel_date)
    --             ,MONTH(cancel_date)
    --             ,grey_no
    --             ,ci_no
    --             ,wh_class
    --             ,d_e
    --             ,undelivery
    --             ,u_ms
    --     ) AS A
    --     -- GROUP BY
    --     --      A.comp_id
    --     --     ,A.f_year
    --     --     ,A.f_month
    --     --     ,A.dept
    --     --     ,A.grey_no
    --     --     ,A.ci_no
    --     --     ,A.wh_class
    --     --     ,A.dom_exp
    --     --     ,A.undelivery
    --     --     ,A.u_ms
    --     ORDER BY
    --          A.wh_class
    --         ,A.dom_exp
    --         ,A.undelivery
    --         ,A.grey_no
    --         ,A.ci_no



    -- Update Query Terbaru : 2021-02-01
    INSERT INTO @_TR_PROD_FG
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,grey_no
            ,ci_no
            ,item_category
            ,wh_class
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,prod_qty
            ,prod_qty_mtr
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
             @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,A.dept AS dept
            ,A.grey_no AS grey_no
            ,A.ci_no AS ci_no
            ,A.item_category AS item_category
            ,A.wh_class AS wh_class
            ,A.dom_exp AS dom_exp
            ,A.undelivery AS undelivery
            ,A.u_ms AS qty_unit
            ,A.prod_qty AS prod_qty
            ,A.prod_qty_mtr AS prod_qty_mtr
            ,A.proc_type AS dy_proc_type
            ,A.dyes_type_code AS dyes_type_code
            ,A.color_shade_code AS color_shade_code
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
        FROM (
            SELECT
                comp_id,
                f_year,
                f_month,
                dept,
                grey_no,
                ci_no,
                item_category,
                wh_class,
                dom_exp,
                undelivery,
                u_ms,
                proc_type,
                dyes_type_code,
                color_shade_code,
                SUM(prod_qty) AS prod_qty,
                SUM(prod_qty_mtr) AS prod_qty_mtr
            FROM (
                --receive FROM prod (1 & 6),transf stock (7),return (2) di warehouse
                SELECT
                    1 AS comp_id,
                    YEAR(whs_dt) f_year,
                    MONTH(whs_dt) f_month,
                    900 AS dept,
                    A.grey_no,
                    A.ci_no,
                    B.item_category,
                    wh_class,
                    d_e AS dom_exp,
                    undelivery,
                    u_ms,
                    isnull(C.proc_type,'') AS proc_type,
                    isnull(D.dyes_type_code,'') AS dyes_type_code,
                    isnull(D.color_shade_code,'') AS color_shade_code,
                    SUM(f_length) AS prod_qty,
                    CASE WHEN u_ms='YDS' THEN SUM(f_length/1.0936) else
                    SUM(f_length) END AS prod_qty_mtr
                FROM istem_sms.dbo.fg_warehouse_detail AS A
                LEFT JOIN istem_sms.dbo.wv_fabric_analysis_master AS B ON A.grey_no=B.grey_no
                LEFT JOIN istem_sms.dbo.color_instruction_header AS C ON A.iw_no=C.iw_no
                LEFT JOIN istem_sms.dbo.color_instruction_detail_process AS D ON A.iw_no=D.iw_no AND A.ist_col=D.ist_color
                WHERE
                    YEAR(whs_dt) = @target_cim_f_year
                    AND MONTH(whs_dt) = @target_cim_f_month
                    AND wh_class IN ('1','6','7')
                GROUP BY
                    YEAR(A.whs_dt),
                    MONTH(A.whs_dt),
                    A.grey_no,
                    A.ci_no,
                    B.item_category,
                    A.wh_class,
                    A.d_e,
                    A.undelivery,
                    A.u_ms,
                    C.proc_type,
                    D.dyes_type_code,
                    D.color_shade_code
                UNION ALL
                -- receive FROM prod,transf stock,return di hist cancel
                SELECT
                    1 AS comp_id,
                    YEAR(A.whs_dt) f_year,
                    MONTH(A.whs_dt) f_month,
                    900 AS dept,
                    A.grey_no,
                    A.ci_no,
                    B.item_category,
                    A.wh_class,
                    A.d_e AS dom_exp,
                    A.undelivery,
                    A.u_ms,
                    isnull(C.proc_type,'') AS proc_type,
                    isnull(D.dyes_type_code,'') AS dyes_type_code,
                    isnull(D.color_shade_code,'') AS color_shade_code,
                    SUM(A.f_length) AS prod_qty,
                    CASE WHEN A.u_ms='YDS' THEN SUM(A.f_length/1.0936) ELSE  SUM(A.f_length) END AS prod_qty_mtr
                FROM istem_sms.dbo.fg_cancel_wh_detail_hist AS A
                LEFT JOIN istem_sms.dbo.wv_fabric_analysis_master AS B ON A.grey_no=B.grey_no
                LEFT JOIN istem_sms.dbo.color_instruction_header AS C ON A.iw_no=C.iw_no
                LEFT JOIN istem_sms.dbo.color_instruction_detail_process AS D ON A.iw_no=D.iw_no AND A.ist_col=D.ist_color
                WHERE
                    YEAR(whs_dt) = @target_cim_f_year
                    AND MONTH(whs_dt) = @target_cim_f_month
                    AND A.wh_class IN ('1','6','7','2')
                GROUP BY
                    YEAR(A.whs_dt),
                    MONTH(A.whs_dt),
                    A.grey_no,
                    A.ci_no,
                    B.item_category,
                    A.wh_class,
                    A.d_e,
                    A.undelivery,
                    A.u_ms,
                    C.proc_type,
                    D.dyes_type_code,
                    D.color_shade_code
                UNION ALL
                -- receive FROM prod,transf stock,return di transf stock hist
                SELECT
                    1 AS comp_id,
                    YEAR(whs_dt) f_year,
                    MONTH(whs_dt) f_month,
                    900 AS dept,
                    A.grey_no,
                    A.ci_no,
                    B.item_category,
                    wh_class,
                    A.d_e AS dom_exp,
                    A.undelivery,
                    A.u_ms,
                    '' AS proc_type,
                    '' AS dyes_type_code,
                    '' AS color_shade_code,
                    SUM(f_length) AS prod_qty,
                    CASE WHEN u_ms='YDS' THEN SUM(f_length/1.0936) else SUM(f_length) END AS prod_qty_mtr
                FROM istem_sms.dbo.fg_transfer_stock_hist AS A
                LEFT JOIN istem_sms.dbo.wv_fabric_analysis_master AS B ON A.grey_no=B.grey_no
                WHERE
                    YEAR(whs_dt) = @target_cim_f_year
                    AND MONTH(whs_dt) = @target_cim_f_month
                    AND wh_class IN ('1','6','2')
                GROUP BY
                    YEAR(whs_dt),
                    MONTH(whs_dt),
                    A.grey_no,
                    A.ci_no,
                    B.item_category,
                    A.wh_class,
                    A.d_e,
                    A.undelivery,
                    A.u_ms
                UNION ALL
                -- transfer stock (-) di transf stock hist
                SELECT
                    1 AS comp_id,
                    YEAR(whs_dt_t) f_year,
                    MONTH(whs_dt_t) f_month,
                    900 AS dept,
                    A.grey_no,
                    ci_no,
                    B.item_category,
                    '7' AS wh_class,
                    d_e AS dom_exp,
                    undelivery,
                    u_ms,
                    '' AS proc_type,
                    '' AS dyes_type_code,
                    '' AS color_shade_code,
                    SUM(f_length)*-1 AS prod_qty,
                    CASE WHEN u_ms='YDS' THEN SUM(f_length/1.0936)*-1 else SUM(f_length)*-1 END AS prod_qty_mtr
                FROM istem_sms.dbo.fg_transfer_stock_hist AS A
                LEFT JOIN istem_sms.dbo.wv_fabric_analysis_master AS B ON A.grey_no=B.grey_no
                WHERE
                    YEAR(whs_dt) = @target_cim_f_year
                    AND MONTH(whs_dt) = @target_cim_f_month
                    AND wh_class IN ('1','6','7','2')
                GROUP BY
                    YEAR(whs_dt_t),
                    MONTH(whs_dt_t),
                    A.grey_no,
                    ci_no,
                    B.item_category,
                    A.wh_class,
                    A.d_e,
                    A.undelivery,
                    A.u_ms
                UNION ALL
                -- cancel wh
                SELECT
                    1 AS comp_id,
                    YEAR(cancel_date) f_year,
                    MONTH(cancel_date) f_month,
                    900 AS dept,
                    A.grey_no,
                    ci_no,
                    B.item_category,
                    wh_class,
                    d_e AS dom_exp,
                    A.undelivery,
                    A.u_ms,ISNULL(C.proc_type,'') AS proc_type,
                    ISNULL(D.dyes_type_code,'') AS dyes_type_code,
                    ISNULL(D.color_shade_code,'') AS color_shade_code,
                    SUM(f_length)*-1 AS prod_qty,
                    CASE WHEN u_ms='YDS' THEN SUM(f_length/1.0936)*-1 ELSE SUM(f_length)*-1 END AS prod_qty_mtr
                FROM istem_sms.dbo.fg_cancel_wh_detail_hist AS A
                LEFT JOIN istem_sms.dbo.wv_fabric_analysis_master AS B ON A.grey_no=B.grey_no
                LEFT JOIN istem_sms.dbo.color_instruction_header AS C ON A.iw_no=C.iw_no
                LEFT JOIN istem_sms.dbo.color_instruction_detail_process AS D ON A.iw_no=D.iw_no AND A.ist_col=D.ist_color
                WHERE
                    YEAR(cancel_date) = @target_cim_f_year
                    AND MONTH(cancel_date) = @target_cim_f_month
                    AND wh_class IN ('1','6','7','2')
                GROUP BY
                    YEAR(cancel_date),
                    MONTH(cancel_date),
                    A.grey_no,
                    ci_no,
                    B.item_category,
                    A.wh_class,
                    A.d_e,
                    A.undelivery,
                    A.u_ms,
                    C.proc_type,
                    D.dyes_type_code,
                    D.color_shade_code
            ) AS A
            GROUP BY
                comp_id,
                f_year,
                f_month,
                dept,
                grey_no,
                ci_no,
                item_category,
                wh_class,
                dom_exp,
                undelivery,
                u_ms,
                proc_type,
                dyes_type_code,
                color_shade_code
        ) AS A


    INSERT INTO @_TR_PROD_FG
        (
            comp_id
            ,f_year
            ,f_month
            ,dept
            ,grey_no
            ,ci_no
            ,wh_class
            ,dom_exp
            ,undelivery
            ,qty_unit
            ,prod_qty
            ,prod_qty_mtr
            ,dy_proc_type
            ,dyes_type_code
            ,color_shade_code
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
             @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept40 AS dept
            ,A.grey_no AS grey_no
            ,'' AS ci_no
            ,'S' AS wh_class
            ,A.dom_exp AS dom_exp
            ,'' AS undelivery
            ,'MTR' AS qty_unit
            ,0 AS prod_qty
            ,A.prod_qty_mtr AS prod_qty_mtr
            ,'' AS dy_proc_type
            ,'' AS dyes_type_code
            ,'' AS color_shade_code
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
        FROM (
            SELECT
                year
                ,month
                ,grey_no
                ,r_flag
                ,iw_type AS dom_exp
                ,SUM(sample_meter) AS prod_qty_mtr
            FROM istem_sms.dbo.dy_manufacturing_report
            WHERE
                month=@target_cim_f_month
                AND year=@target_cim_f_year
                AND sample_meter <> 0
            GROUP BY year, month, grey_no, r_flag, iw_type
        ) AS A
        ORDER BY A.r_flag, A.dom_exp, A.grey_no

    SELECT
         A.comp_id AS comp_id
        ,A.f_year AS f_year
        ,A.f_month AS f_month
        ,A.dept AS dept
        ,A.grey_no AS grey_no
        ,A.ci_no AS ci_no
        ,A.item_category AS item_category
        ,A.wh_class AS wh_class
        ,A.dom_exp AS dom_exp
        ,A.undelivery AS undelivery
        ,A.qty_unit AS qty_unit
        ,SUM(A.prod_qty) AS prod_qty
        ,SUM(A.prod_qty_mtr) AS prod_qty_mtr
        ,A.dy_proc_type AS dy_proc_type
        ,A.dyes_type_code AS dyes_type_code
        ,A.color_shade_code AS color_shade_code
        ,NULLIF(SUM(A.prod_qty_mtr), 0) / NULLIF(SUM(B.tot_mat_qty), 0) * NULLIF(SUM(B.tot_mat_amount), 0) AS material_cost
        ,@proc_time AS proc_time
        ,@user_id AS user_id
        ,@client_ip AS client_ip
        ,@rec_sts AS rec_sts
        ,@proc_no AS proc_no
    FROM @_TR_PROD_FG AS A
    LEFT JOIN (
        SELECT
             A.grey_no
            ,SUM(A.sum_of_prod_qty_mtr) AS tot_mat_qty
            ,SUM(B.cons_mat_amount) AS tot_mat_amount
        FROM (
            SELECT
                grey_no
                ,SUM(prod_qty_mtr) AS sum_of_prod_qty_mtr
            FROM @_TR_PROD_FG
            GROUP BY grey_no
        ) AS A
        LEFT JOIN (
            SELECT
                mat_code AS grey_no
                ,SUM(cons_mat_amount) AS cons_mat_amount
            FROM istem_costing.dbo.tr_wip
            WHERE
                    comp_id=@comp_id
                AND f_year=@f_year
                AND f_month=@f_month
                AND dept IN (@dept40)
            GROUP BY mat_code
        ) AS B ON (B.grey_no=A.grey_no)
        GROUP BY A.grey_no
    ) B ON (B.grey_no=A.grey_no)
    GROUP BY
         A.comp_id
        ,A.f_year
        ,A.f_month
        ,A.dept
        ,A.grey_no
        ,A.ci_no
        ,A.item_category
        ,A.wh_class
        ,A.dom_exp
        ,A.undelivery
        ,A.qty_unit
        ,A.dy_proc_type
        ,A.dyes_type_code
        ,A.color_shade_code
    ORDER BY
         A.wh_class
        ,A.dom_exp
        ,A.undelivery
        ,A.grey_no
        ,A.ci_no
END
