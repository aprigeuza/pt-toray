CREATE PROCEDURE  sp_WeavingDownload_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    DECLARE @dept INT
    		,@dept_seq INT
    		,@max_date DATE

    DECLARE @LP_f_year INT, @LP_f_month INT
    IF @F_Month = 1
    BEGIN
    	SET @LP_f_month = 12
    	SET @LP_f_year = @f_year - 1
    END
    ELSE
    BEGIN
    	SET @LP_f_month = @f_month - 1
    	SET @LP_f_year = @f_year
    END

    SET @dept_seq = 30

    SELECT @dept = dept FROM istem_costing.dbo.ms_dept WHERE (dept_seq=@dept_seq)

    SELECT * FROM (
    	SELECT
    		A.item_code
    		,A.mat_code
    		,C.yarn_name
    		,C.yarn_prev_name
    		,NULLIF(A.mat_qty, 0) AS mat_qty
    		,A.proc_code
    		,B.proc_name
    		,CASE
    		WHEN A.proc_type = '5.WIP-RM' AND ((A.f_year=@LP_f_year) AND (A.f_month=@LP_f_month)) THEN '1.WIP-RM'
    		WHEN A.proc_type = '6.WIP-SP' AND ((A.f_year=@LP_f_year) AND (A.f_month=@LP_f_month)) THEN '2.WIP-SP'
    		WHEN A.proc_type = '7.WIP-P' AND ((A.f_year=@LP_f_year) AND (A.f_month=@LP_f_month)) THEN '3.WIP-P'
    		ELSE A.proc_type
    		END AS proc_type
    		,CASE WHEN C.yarn_kind='1' THEN 'Single'
    			WHEN C.yarn_kind='2' THEN 'Double'
    			WHEN C.yarn_kind='L' THEN 'Leno'
    			ELSE ''
    		END AS singdoub
    		,CASE WHEN C.yarn_kind='1' THEN 1
    			WHEN C.yarn_kind='2' THEN 2
    			WHEN C.yarn_kind='L' THEN 3
    			ELSE ''
    		END AS sort_order
    	FROM istem_costing.dbo.tr_dept_calc AS A
    	LEFT JOIN istem_costing.dbo.ms_process AS B ON (
    			B.comp_id=A.comp_id
    		AND B.dept=A.dept
    		AND B.tr_code=A.tr_code
    		AND B.proc_code=A.proc_code
    	)
    	LEFT JOIN istem_sms.dbo.sp_ms_yarn AS C ON (C.yarn_code=A.mat_code)
    	WHERE
    		(A.comp_id=@comp_id)
    	AND ((A.f_year=@f_year) AND (A.f_month=@f_month))
    	AND (A.dept=@dept)

    	UNION ALL

    	SELECT
    		A.item_code
    		,A.mat_code
    		,C.yarn_name
    		,C.yarn_prev_name
    		,NULLIF(A.mat_qty, 0) AS mat_qty
    		,A.proc_code
    		,B.proc_name
    		,CASE
    		WHEN A.proc_type = '5.WIP-RM' AND ((A.f_year=@LP_f_year) AND (A.f_month=@LP_f_month)) THEN '1.WIP-RM'
    		WHEN A.proc_type = '6.WIP-SP' AND ((A.f_year=@LP_f_year) AND (A.f_month=@LP_f_month)) THEN '2.WIP-SP'
    		WHEN A.proc_type = '7.WIP-P' AND ((A.f_year=@LP_f_year) AND (A.f_month=@LP_f_month)) THEN '3.WIP-P'
    		ELSE A.proc_type
    		END AS proc_type
    		,CASE WHEN C.yarn_kind='1' THEN 'Single'
    			WHEN C.yarn_kind='2' THEN 'Double'
    			WHEN C.yarn_kind='L' THEN 'Leno'
    			ELSE ''
    		END AS singdoub
    		,CASE WHEN C.yarn_kind='1' THEN 1
    			WHEN C.yarn_kind='2' THEN 2
    			WHEN C.yarn_kind='L' THEN 3
    			ELSE ''
    		END AS sort_order
    	FROM istem_costing.dbo.tr_dept_calc AS A
    	LEFT JOIN istem_costing.dbo.ms_process AS B ON (
    			B.comp_id=A.comp_id
    		AND B.dept=A.dept
    		AND B.tr_code=A.tr_code
    		AND B.proc_code=A.proc_code
    	)
    	LEFT JOIN istem_sms.dbo.sp_ms_yarn AS C ON (C.yarn_code=A.mat_code)
    	WHERE
    		(A.comp_id=@comp_id)
    	AND ((A.f_year=@LP_f_year) AND (A.f_month=@LP_f_month))
    	AND (A.dept=@dept)
    	AND (A.proc_type IN ('5.WIP-RM', '6.WIP-SP', '7.WIP-P'))
    ) A
    ORDER BY A.singdoub, A.proc_code DESC, A.proc_type ASC
END
