CREATE PROCEDURE  sp_AcctDownSME_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- sp_AcctDownSME_Data
    -- DECLARE
    -- @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'


    DECLARE @FromDate Date, @ToDate Date

    -- Sap Period
    SELECT
    	@FromDate = F_RefDate
    	,@ToDate = T_RefDate
    FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR
    WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)

    DECLARE @Date1 DATE
    DECLARE @Date2 DATE

    SELECT @Date1=T0.RateDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.ORTT T0 WHERE T0.RateDate = @FromDate
    SELECT @Date2=T1.RateDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.ORTT T1 WHERE T1.RateDate = @ToDate

    DECLARE @proc_time AS DATETIME
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_no INT

    SET @rec_sts='A'
    SET @proc_time=GETDATE()
    SET @proc_no=1


    -- Cek tr_sap_sme
    -- Find Data
    DECLARE @c INT
    SET @c = (SELECT COUNT(*) FROM istem_costing.dbo.tr_sap_sme WHERE (comp_id=@comp_id) AND (f_year = @f_year) AND (f_month = @f_month))

    IF @c >= 1
    BEGIN
    	-- Jika sudah ada
    	-- Jadikan rec_sts = T
    	SET @rec_sts = 'T'

    	SET @proc_no = (SELECT MAX(proc_no) + 1 FROM istem_costing.dbo.tr_sap_sme WHERE (comp_id=@comp_id) AND (f_year = @f_year) AND (f_month = @f_month))
    END

    SELECT
    	 @comp_id AS comp_id
    	,@f_year AS f_year
    	,@f_month AS f_month
    	,T11.Dept AS dept
    	,T11.Account AS acct_code
    	,T11.AcctName AS acct_name
    	,CASE WHEN (T11.U_cost_ID NOT LIKE 'ZZ%') THEN T11.expense_amount ELSE T11.Debit END AS expense_amount
    	,T11.U_cost_ID AS cost_sheet_id
    	,@proc_time AS proc_time
    	,@user_id AS user_id
    	,@client_ip AS client_ip
    	,@rec_sts AS rec_sts
    	,@proc_no AS proc_no
    FROM (
    	SELECT
    		 T10.Dept
    		,T10.Account
    		,T10.AcctName
    		,T10.Debit
    		,T10.expense_amount
    		,CASE
    		   WHEN T10.Account = '512.000.010.000' then 'Transfer FROM WIP B/F'
    		   WHEN T10.Account = '513.000.010.000' then 'Transfer to Manufacturing A/C'
    		   ELSE T2.PrcName
    		END AS DeptName
    		,T10.U_cost_ID
    	FROM (
    		SELECT
    			CASE
    				WHEN T0.Account =  '513.000.010.000' THEN '999'
    				WHEN T0.Account=   '512.000.010.000' THEN '888'
    				ELSE ISNULL(T0.ProfitCode,'')
    			END AS Dept
    			-- ,ISNULL(T0.ProfitCode,'') As Dept
    			,T0.Account
    			,T1.AcctName
    			,SUM(T0.Debit) AS Debit
    			,SUM(T0.Debit)-SUM(T0.Credit) AS expense_amount
    			,T1.U_cost_ID
    		FROM SAP_ISM.SBO_ISM_LIVE.DBO.JDT1 T0
    		LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OACT T1 ON T0.Account = T1.AcctCode
            LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OJDT T2 ON T0.transid=T2.transid
    		WHERE
    				(T0.RefDate BETWEEN @Date1 AND @Date2)
    			AND (T0.Account LIKE '520.%' OR T0.Account LIKE '513%' OR T0.Account LIKE '512%')
                AND ISNULL(T2.ref3, '')<>'JV-COSTING'
                AND ISNULL(T0.ProfitCode,'')<>''
    		GROUP BY
    			ISNULL(T0.ProfitCode,''), T0.Account, T1.AcctName, T1.U_cost_ID, T0.ProfitCode
    	) T10
    	LEFT JOIN SAP_ISM.SBO_ISM_LIVE.DBO.OPRC T2 ON T10.dept = T2.PrcCode
    ) T11
    WHERE
    	T11.Dept IS NOT NULL
    	AND T11.U_cost_ID IS NOT NULL
    ORDER BY
    	T11.dept, T11.Account
END
