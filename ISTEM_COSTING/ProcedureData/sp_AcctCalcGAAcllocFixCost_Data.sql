CREATE PROCEDURE  sp_AcctCalcGAAcllocFixCost_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN

    -- sp_AcctCalcGAAcllocFixCost_Data
    PRINT 'SUB4 : sp_AcctCalcGAAcllocFixCost_Data'

    -- SUB4	Hitung Alokasi Fixed Cost Amount GA  untuk tiap2 Dept produksi, berdasarkan ratio allocation general factory

    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24)
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'

    DECLARE @dept_10 INT
    DECLARE @dept_20 INT
    DECLARE @dept_30 INT
    DECLARE @dept_40 INT
    DECLARE @dept_61 INT
    DECLARE @dept_66 INT

    SET @dept_10=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=10))
    SET @dept_20=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=20))
    SET @dept_30=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=30))
    SET @dept_40=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=40))
    SET @dept_61=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=61))
    SET @dept_66=(SELECT dept FROM istem_costing.dbo.ms_dept WHERE (comp_id=@comp_id AND dept_seq=66))

    -- PERIOD
    DECLARE @from_date DATE, @to_date DATE
    SELECT TOP 1 @from_date = F_RefDate, @to_date = T_RefDate FROM SAP_ISM.SBO_ISM_LIVE.DBO.OFPR WHERE (CAST(SUBSTRING(Code, 1, 4) AS INT) = @f_year) AND (CAST(SUBSTRING(Code, 6, 2) AS INT) = @f_month)

    DECLARE @_TR_MANEX TABLE (
    	[comp_id] [numeric](1, 0) NOT NULL,
    	[f_year] [numeric](4, 0) NOT NULL,
    	[f_month] [numeric](2, 0) NOT NULL,
    	[tr_code] [varchar](5) NOT NULL,
    	[dept] [numeric](3, 0) NOT NULL,
    	[ori_dept] [numeric](3, 0) NOT NULL,
    	[proc_code] [varchar](7) NOT NULL,
    	[cost_sheet_id] [varchar](4) NOT NULL,
    	[manex_amount] [numeric](14, 2) NULL,
    	[proc_time] [datetime] NULL,
    	[user_id] [varchar](6) NULL,
    	[client_ip] [varchar](15) NULL,
    	[rec_sts] [char](1) NULL,
    	[proc_no] [tinyint] NULL
    )

    DECLARE @tr_code AS VARCHAR(MAX)
    DECLARE @proc_code AS VARCHAR(MAX)
    SET @tr_code = 'FCD'
    SET @proc_code = (SELECT TOP 1 ISNULL(proc_code, '') FROM istem_costing.dbo.ms_process WHERE comp_id=@comp_id AND tr_code=@tr_code);

    INSERT INTO @_TR_MANEX
    	(
    		comp_id
    		,f_year
    		,f_month
    		,tr_code
    		,dept
    		,ori_dept
    		,proc_code
    		,cost_sheet_id
    		,manex_amount
    	)
    	SELECT
    		 @comp_id AS comp_id
    		,@f_year AS f_year
    		,@f_month AS f_month
    		,@tr_code AS tr_code
    		,@dept_10 AS dept
    		,A.dept AS ori_dept
    		,@proc_code AS proc_code
    		,A.cost_sheet_id AS cost_sheet_id
    		,SUM(A.expense_amount) AS manex_amount
    	FROM istem_costing.dbo.tr_sap_sme AS A
        WHERE
            A.comp_id=@comp_id
            AND A.f_year=@f_year
            AND A.f_month=@f_month
            AND A.dept IN (@dept_10, @dept_66)
        GROUP BY
            A.dept
            ,A.cost_sheet_id

    DECLARE @v_GA_fix_amt AS DECIMAL(16,5)
    SET @v_GA_fix_amt = (SELECT SUM(manex_amount) AS v_GA_fix_amt FROM @_TR_MANEX)
    SET @tr_code = 'ACGA'
    INSERT INTO @_TR_MANEX
    	(
    		comp_id
    		,f_year
    		,f_month
    		,tr_code
    		,dept
    		,ori_dept
    		,proc_code
    		,cost_sheet_id
    		,manex_amount
    	)
    	SELECT
    		 @comp_id AS comp_id
    		,@f_year AS f_year
    		,@f_month AS f_month
    		,@tr_code AS tr_code
    		,A.dept AS dept
    		,@dept_10 AS ori_dept
    		,B.proc_code AS proc_code
    		,B.cost_sheet_id AS cost_sheet_id
    		,@v_GA_fix_amt * A.convert_ratio AS manex_amount
    	FROM istem_costing.dbo.tr_convert_item AS A
    	INNER JOIN istem_costing.dbo.ms_convert_type AS B ON (B.comp_id=@comp_id AND B.convert_type_code=A.convert_type_code)
    	WHERE
    		A.comp_id=@comp_id
    		AND A.f_year=@f_year
    		AND A.f_month=@f_month
    		AND A.convert_type_code='X02'
    		AND A.dept IN (700, 800, 900, 505)
    	-- GROUP BY
    		--  A.comp_id
    		-- ,A.f_year
    		-- ,A.f_month
    		-- ,A.dept
    		-- ,A.ori_dept
    		-- ,A.proc_code
    		-- ,A.cost_sheet_id


    SELECT comp_id, f_year, f_month, tr_code, dept, ori_dept, proc_code, cost_sheet_id, manex_amount FROM @_TR_MANEX;
END
