CREATE PROCEDURE  sp_SpinningProcessWIP_Data
	@comp_id INT, @f_year INT, @f_month INT
AS
BEGIN
    DECLARE @user_id VARCHAR(20), @client_ip VARCHAR(24)

    SET @user_id='SYS'
    SET @client_ip='0.0.0.0'

    -- DECLARE
    --     @comp_id INT, @f_year INT, @f_month INT, @user_id VARCHAR(20), @client_ip VARCHAR(24);
    --
    -- SET @comp_id=1
    -- SET @f_year=2020
    -- SET @f_month=10
    -- SET @user_id='SYS'
    -- SET @client_ip='192.168.1.1'


    DECLARE @dept INT
    DECLARE @tr_code VARCHAR(MAX)
    DECLARE @rec_sts VARCHAR(MAX)
    DECLARE @proc_time AS DATETIME
    DECLARE @proc_no INT
    DECLARE @max_date DATE

    SET @dept=(SELECT dept FROM ms_dept WHERE (comp_id=@comp_id AND dept_seq=20))
    SET @tr_code='WIP'
    SET @proc_time = GETDATE()
    SET @rec_sts='A'
    SET @proc_no=1

    -- Ambil hanya tanggal Terakhir
    SET @max_date = (SELECT MAX(stock_take_date) FROM istem_costing.dbo.tr_dept_detail1 WHERE (f_year = @f_year) AND (f_month = @f_month) AND (dept = @dept) AND (tr_code=@tr_code))

    DECLARE @_TEMP TABLE (
    	[comp_id] [numeric](1, 0) NOT NULL,
    	[f_year] [numeric](4, 0) NOT NULL,
    	[f_month] [numeric](2, 0) NOT NULL,
    	[dept] [numeric](3, 0) NOT NULL,
    	[tr_code] [varchar](5) NOT NULL,
    	[mc_loc] [varchar](15) NOT NULL,
    	[proc_code] [varchar](7) NOT NULL,
    	[item_code] [varchar](45) NOT NULL,
    	[mat_code] [varchar](45) NULL,
    	[mat_qty] [numeric](12, 2) NULL,
    	-- [mat_amount] [numeric](14, 2) NULL,
    	[mat_qty_unit] [varchar](5) NULL,
    	[proc_type] [varchar](20) NULL,
    	[proc_time] [datetime] NULL,
    	[user_id] [varchar](6) NULL,
    	[client_ip] [varchar](15) NULL,
    	[rec_sts] [char](1) NULL,
    	[proc_no] [tinyint] NULL
    )

    INSERT INTO @_TEMP
        (
             comp_id
            ,f_year
            ,f_month
            ,dept
            ,tr_code
            ,mc_loc
            ,proc_code
            ,item_code
            ,mat_code
            ,mat_qty
            -- ,mat_amount
            ,mat_qty_unit
            ,proc_type
            ,proc_time
            ,user_id
            ,client_ip
            ,rec_sts
            ,proc_no
        )
        SELECT
             @comp_id AS comp_id
            ,@f_year AS f_year
            ,@f_month AS f_month
            ,@dept AS dept
            ,@tr_code AS tr_code
            ,A.mc_loc AS mc_loc
            ,B.proc_code AS proc_code
            ,A.item_code AS item_code
            ,C.mat_code AS mat_code
            ,SUM(ISNULL((A.input_qty*(C.mat_comp_actual/100)), 0)) AS mat_qty
            -- ,mat_amount
            ,B.input_qty_unit AS mat_qty_unit
            ,B.proc_type AS proc_type
            ,@proc_time AS proc_time
            ,@user_id AS user_id
            ,@client_ip AS client_ip
            ,@rec_sts AS rec_sts
            ,@proc_no AS proc_no
        FROM istem_costing.dbo.tr_dept_detail1 AS A
        INNER JOIN (
            SELECT
                A.mc_proc_code
                ,A.proc_code
                ,A.input_qty_unit
                ,B.proc_type
            FROM istem_costing.dbo.ms_mc_proc AS A
            INNER JOIN istem_costing.dbo.ms_process AS B ON (B.proc_code=A.proc_code)
            WHERE
                    (A.comp_id=@comp_id)
                AND (A.dept=@dept)
                AND (B.comp_id=@comp_id)
                AND (B.dept=@dept)
                AND (B.tr_code=@tr_code)
            GROUP BY
                A.mc_proc_code
                ,A.proc_code
                ,A.input_qty_unit
                ,A.proc_code
                ,B.proc_type
        ) AS B ON (B.mc_proc_code=A.mc_proc_code)
        INNER JOIN (
            SELECT
                (Y.yarn_type+Y.yarn_mat_type+Y.yarn_sf_length+yarn_version) AS item_code
                ,D.mat_comp
                ,D.mat_comp_actual
                ,D.mat_code
            FROM istem_sms.dbo.sp_ms_yarn AS Y
            INNER JOIN istem_sms.dbo.sp_ms_yarn_detail AS D ON (D.yarn_code = Y.yarn_code)
            GROUP BY
                (Y.yarn_type+Y.yarn_mat_type+Y.yarn_sf_length+yarn_version)
                ,D.mat_comp
                ,D.mat_comp_actual
                ,D.mat_code
        ) AS C ON (C.item_code=A.item_code)
        WHERE
        	    comp_id=@comp_id
        	AND f_year=@f_year
        	AND f_month=@f_month
        	AND tr_code=@tr_code
            AND stock_take_date=@max_date
        GROUP BY
            A.mc_loc
            ,B.proc_code
            ,A.item_code
            ,C.mat_code
            ,C.mat_comp_actual
            ,B.input_qty_unit
            ,B.proc_type

    SELECT [comp_id]
        ,[f_year]
        ,[f_month]
        ,[dept]
        ,[tr_code]
        ,[mc_loc]
        ,[proc_code]
        ,[item_code]
        ,[mat_code]
        ,[mat_qty]
        -- ,[mat_amount]
        ,[mat_qty_unit]
        ,[proc_type]
        ,[proc_time]
        ,[user_id]
        ,[client_ip]
        ,[rec_sts]
        ,[proc_no]
    FROM @_TEMP
END
